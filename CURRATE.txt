      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4015-ACTT-EFCTV' TO DBLINK-CURR-PARAGRAPH.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4015-ACTT-EFCTV' TO RECORD-NAME.                      276 BRTN
YYY991     MOVE 'F-ACCT' TO AREA-NAME.                                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4015-ACTT-EFCTV TO DBLINK-RECORD-MADE-CURRENT.  276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4015-ACTT-EFCTV-EXIT                     276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE ATE-FK2-ACT-ACCT-TYP TO DBLINK-R4015-ACTT-EFCTV-01  276 BRTN
YYY991         MOVE ATE-START-DT TO DBLINK-R4015-ACTT-EFCTV-02          276 BRTN
YYY991         MOVE ATE-TIME-STMP TO DBLINK-R4015-ACTT-EFCTV-03         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4015-ACTT-EFCTV TO DBLINK-VIEW-NAME-CURRENT.    276 BRTN
YYY991     MOVE DBLINK-R4015-ACTT-EFCTV-KEY TO DBLINK-VIEW-200-CURRENT. 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4014-4015-O                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4014-4015-O-F             276 BRTN
YYY991     IF ATE-ACT-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE ATE-ACT-SETF TO DBLINK-S-4014-4015-O-F              276 BRTN
YYY991         MOVE ATE-FK1-ACT-ACCT-TYP TO DBLINK-S-4014-4015-O-01     276 BRTN
YYY991         MOVE ATE-SBRDT-ACCT-TYP TO DBLINK-S-4014-4015-O-02       276 BRTN
YYY991         MOVE ATE-START-DT TO DBLINK-S-4014-4015-O-03             276 BRTN
YYY991         MOVE ATE-TIME-STMP TO DBLINK-S-4014-4015-O-04            276 BRTN
YYY991         MOVE ATE-ACT-SET-TS TO DBLINK-S-4014-4015-O-05           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4014-4015-P                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ATE-FK2-ACT-ACCT-TYP TO DBLINK-S-4014-4015-P-01.        276 BRTN
YYY991     MOVE ATE-START-DT TO DBLINK-S-4014-4015-P-02.                276 BRTN
YYY991     MOVE ATE-TIME-STMP TO DBLINK-S-4014-4015-P-03.               276 BRTN
