    ***Created by Convert/DC version V8R03 on 11/01/00 at 10:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-CH67-Z = 'Y'
               MOVE WK01-DATE-CH67 TO SLCTN-DATE-CH67 OF FCHWK67
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CH67-Z = 'Y'
               MOVE WK01-STATUS-CH67 TO SLCTN-STATUS-CH67 OF FCHWK67
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COA-CODE-CH67-Z = 'Y'
               MOVE WK01-COA-CODE-CH67 TO RQST-COA-CODE-CH67 OF FCHWK67
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COST-SHARE-CODE-CH67-Z = 'Y'
               MOVE WK01-COST-SHARE-CODE-CH67 TO
                RQST-COST-SHARE-CODE-CH67 OF FCHWK67
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH671-Z = 'Y'
               MOVE WK01-CODE-CH671 TO ACTN-CODE-CH67 OF FCHWM67(0001)
           END-IF.
           MOVE WK01-CODE-CH671-F TO WK01-CODE-CH67-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH672-Z = 'Y'
               MOVE WK01-CODE-CH672 TO ACTN-CODE-CH67 OF FCHWM67(0002)
           END-IF.
           MOVE WK01-CODE-CH672-F TO WK01-CODE-CH67-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH673-Z = 'Y'
               MOVE WK01-CODE-CH673 TO ACTN-CODE-CH67 OF FCHWM67(0003)
           END-IF.
           MOVE WK01-CODE-CH673-F TO WK01-CODE-CH67-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH674-Z = 'Y'
               MOVE WK01-CODE-CH674 TO ACTN-CODE-CH67 OF FCHWM67(0004)
           END-IF.
           MOVE WK01-CODE-CH674-F TO WK01-CODE-CH67-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH675-Z = 'Y'
               MOVE WK01-CODE-CH675 TO ACTN-CODE-CH67 OF FCHWM67(0005)
           END-IF.
           MOVE WK01-CODE-CH675-F TO WK01-CODE-CH67-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH676-Z = 'Y'
               MOVE WK01-CODE-CH676 TO ACTN-CODE-CH67 OF FCHWM67(0006)
           END-IF.
           MOVE WK01-CODE-CH676-F TO WK01-CODE-CH67-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH677-Z = 'Y'
               MOVE WK01-CODE-CH677 TO ACTN-CODE-CH67 OF FCHWM67(0007)
           END-IF.
           MOVE WK01-CODE-CH677-F TO WK01-CODE-CH67-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH678-Z = 'Y'
               MOVE WK01-CODE-CH678 TO ACTN-CODE-CH67 OF FCHWM67(0008)
           END-IF.
           MOVE WK01-CODE-CH678-F TO WK01-CODE-CH67-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH679-Z = 'Y'
               MOVE WK01-CODE-CH679 TO ACTN-CODE-CH67 OF FCHWM67(0009)
           END-IF.
           MOVE WK01-CODE-CH679-F TO WK01-CODE-CH67-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH6710-Z = 'Y'
               MOVE WK01-CODE-CH6710 TO ACTN-CODE-CH67 OF FCHWM67(0010)
           END-IF.
           MOVE WK01-CODE-CH6710-F TO WK01-CODE-CH67-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH6711-Z = 'Y'
               MOVE WK01-CODE-CH6711 TO ACTN-CODE-CH67 OF FCHWM67(0011)
           END-IF.
           MOVE WK01-CODE-CH6711-F TO WK01-CODE-CH67-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH671-Z = 'Y'
               MOVE WK01-CODE-4030-CH671 TO COA-CODE-4030-CH67 OF
                FCHWM67(0001)
           END-IF.
           MOVE WK01-CODE-4030-CH671-F TO WK01-CODE-4030-CH67-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH672-Z = 'Y'
               MOVE WK01-CODE-4030-CH672 TO COA-CODE-4030-CH67 OF
                FCHWM67(0002)
           END-IF.
           MOVE WK01-CODE-4030-CH672-F TO WK01-CODE-4030-CH67-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH673-Z = 'Y'
               MOVE WK01-CODE-4030-CH673 TO COA-CODE-4030-CH67 OF
                FCHWM67(0003)
           END-IF.
           MOVE WK01-CODE-4030-CH673-F TO WK01-CODE-4030-CH67-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH674-Z = 'Y'
               MOVE WK01-CODE-4030-CH674 TO COA-CODE-4030-CH67 OF
                FCHWM67(0004)
           END-IF.
           MOVE WK01-CODE-4030-CH674-F TO WK01-CODE-4030-CH67-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH675-Z = 'Y'
               MOVE WK01-CODE-4030-CH675 TO COA-CODE-4030-CH67 OF
                FCHWM67(0005)
           END-IF.
           MOVE WK01-CODE-4030-CH675-F TO WK01-CODE-4030-CH67-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH676-Z = 'Y'
               MOVE WK01-CODE-4030-CH676 TO COA-CODE-4030-CH67 OF
                FCHWM67(0006)
           END-IF.
           MOVE WK01-CODE-4030-CH676-F TO WK01-CODE-4030-CH67-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH677-Z = 'Y'
               MOVE WK01-CODE-4030-CH677 TO COA-CODE-4030-CH67 OF
                FCHWM67(0007)
           END-IF.
           MOVE WK01-CODE-4030-CH677-F TO WK01-CODE-4030-CH67-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH678-Z = 'Y'
               MOVE WK01-CODE-4030-CH678 TO COA-CODE-4030-CH67 OF
                FCHWM67(0008)
           END-IF.
           MOVE WK01-CODE-4030-CH678-F TO WK01-CODE-4030-CH67-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH679-Z = 'Y'
               MOVE WK01-CODE-4030-CH679 TO COA-CODE-4030-CH67 OF
                FCHWM67(0009)
           END-IF.
           MOVE WK01-CODE-4030-CH679-F TO WK01-CODE-4030-CH67-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH6710-Z = 'Y'
               MOVE WK01-CODE-4030-CH6710 TO COA-CODE-4030-CH67 OF
                FCHWM67(0010)
           END-IF.
           MOVE WK01-CODE-4030-CH6710-F TO WK01-CODE-4030-CH67-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH6711-Z = 'Y'
               MOVE WK01-CODE-4030-CH6711 TO COA-CODE-4030-CH67 OF
                FCHWM67(0011)
           END-IF.
           MOVE WK01-CODE-4030-CH6711-F TO WK01-CODE-4030-CH67-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH671-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH671 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0001)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH671-F TO
                WK01-SHARE-CODE-4030-CH67-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH672-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH672 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0002)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH672-F TO
                WK01-SHARE-CODE-4030-CH67-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH673-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH673 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0003)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH673-F TO
                WK01-SHARE-CODE-4030-CH67-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH674-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH674 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0004)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH674-F TO
                WK01-SHARE-CODE-4030-CH67-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH675-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH675 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0005)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH675-F TO
                WK01-SHARE-CODE-4030-CH67-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH676-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH676 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0006)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH676-F TO
                WK01-SHARE-CODE-4030-CH67-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH677-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH677 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0007)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH677-F TO
                WK01-SHARE-CODE-4030-CH67-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH678-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH678 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0008)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH678-F TO
                WK01-SHARE-CODE-4030-CH67-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH679-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH679 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0009)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH679-F TO
                WK01-SHARE-CODE-4030-CH67-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH6710-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH6710 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0010)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH6710-F TO
                WK01-SHARE-CODE-4030-CH67-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH6711-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH6711 TO
                COST-SHARE-CODE-4030-CH67 OF FCHWM67(0011)
           END-IF.
           MOVE WK01-SHARE-CODE-4030-CH6711-F TO
                WK01-SHARE-CODE-4030-CH67-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH671-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH671 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0001)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH671-F TO
                WK01-SHARE-DESC-4064-CH67-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH672-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH672 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0002)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH672-F TO
                WK01-SHARE-DESC-4064-CH67-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH673-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH673 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0003)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH673-F TO
                WK01-SHARE-DESC-4064-CH67-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH674-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH674 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0004)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH674-F TO
                WK01-SHARE-DESC-4064-CH67-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH675-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH675 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0005)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH675-F TO
                WK01-SHARE-DESC-4064-CH67-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH676-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH676 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0006)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH676-F TO
                WK01-SHARE-DESC-4064-CH67-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH677-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH677 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0007)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH677-F TO
                WK01-SHARE-DESC-4064-CH67-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH678-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH678 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0008)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH678-F TO
                WK01-SHARE-DESC-4064-CH67-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH679-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH679 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0009)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH679-F TO
                WK01-SHARE-DESC-4064-CH67-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH6710-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH6710 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0010)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH6710-F TO
                WK01-SHARE-DESC-4064-CH67-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH6711-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH6711 TO
                COST-SHARE-DESC-4064-CH67 OF FCHWM67(0011)
           END-IF.
           MOVE WK01-SHARE-DESC-4064-CH6711-F TO
                WK01-SHARE-DESC-4064-CH67-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH671-Z = 'Y'
               MOVE WK01-4064-CH671 TO STATUS-4064-CH67 OF FCHWM67(0001)
           END-IF.
           MOVE WK01-4064-CH671-F TO WK01-4064-CH67-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH672-Z = 'Y'
               MOVE WK01-4064-CH672 TO STATUS-4064-CH67 OF FCHWM67(0002)
           END-IF.
           MOVE WK01-4064-CH672-F TO WK01-4064-CH67-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH673-Z = 'Y'
               MOVE WK01-4064-CH673 TO STATUS-4064-CH67 OF FCHWM67(0003)
           END-IF.
           MOVE WK01-4064-CH673-F TO WK01-4064-CH67-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH674-Z = 'Y'
               MOVE WK01-4064-CH674 TO STATUS-4064-CH67 OF FCHWM67(0004)
           END-IF.
           MOVE WK01-4064-CH674-F TO WK01-4064-CH67-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH675-Z = 'Y'
               MOVE WK01-4064-CH675 TO STATUS-4064-CH67 OF FCHWM67(0005)
           END-IF.
           MOVE WK01-4064-CH675-F TO WK01-4064-CH67-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH676-Z = 'Y'
               MOVE WK01-4064-CH676 TO STATUS-4064-CH67 OF FCHWM67(0006)
           END-IF.
           MOVE WK01-4064-CH676-F TO WK01-4064-CH67-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH677-Z = 'Y'
               MOVE WK01-4064-CH677 TO STATUS-4064-CH67 OF FCHWM67(0007)
           END-IF.
           MOVE WK01-4064-CH677-F TO WK01-4064-CH67-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH678-Z = 'Y'
               MOVE WK01-4064-CH678 TO STATUS-4064-CH67 OF FCHWM67(0008)
           END-IF.
           MOVE WK01-4064-CH678-F TO WK01-4064-CH67-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH679-Z = 'Y'
               MOVE WK01-4064-CH679 TO STATUS-4064-CH67 OF FCHWM67(0009)
           END-IF.
           MOVE WK01-4064-CH679-F TO WK01-4064-CH67-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH6710-Z = 'Y'
               MOVE WK01-4064-CH6710 TO STATUS-4064-CH67 OF
                FCHWM67(0010)
           END-IF.
           MOVE WK01-4064-CH6710-F TO WK01-4064-CH67-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-4064-CH6711-Z = 'Y'
               MOVE WK01-4064-CH6711 TO STATUS-4064-CH67 OF
                FCHWM67(0011)
           END-IF.
           MOVE WK01-4064-CH6711-F TO WK01-4064-CH67-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH671-Z = 'Y'
               MOVE WK01-DATE-4064-CH671 TO START-DATE-4064-CH67 OF
                FCHWM67(0001)
           END-IF.
           MOVE WK01-DATE-4064-CH671-F TO WK01-DATE-4064-CH67-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH672-Z = 'Y'
               MOVE WK01-DATE-4064-CH672 TO START-DATE-4064-CH67 OF
                FCHWM67(0002)
           END-IF.
           MOVE WK01-DATE-4064-CH672-F TO WK01-DATE-4064-CH67-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH673-Z = 'Y'
               MOVE WK01-DATE-4064-CH673 TO START-DATE-4064-CH67 OF
                FCHWM67(0003)
           END-IF.
           MOVE WK01-DATE-4064-CH673-F TO WK01-DATE-4064-CH67-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH674-Z = 'Y'
               MOVE WK01-DATE-4064-CH674 TO START-DATE-4064-CH67 OF
                FCHWM67(0004)
           END-IF.
           MOVE WK01-DATE-4064-CH674-F TO WK01-DATE-4064-CH67-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH675-Z = 'Y'
               MOVE WK01-DATE-4064-CH675 TO START-DATE-4064-CH67 OF
                FCHWM67(0005)
           END-IF.
           MOVE WK01-DATE-4064-CH675-F TO WK01-DATE-4064-CH67-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH676-Z = 'Y'
               MOVE WK01-DATE-4064-CH676 TO START-DATE-4064-CH67 OF
                FCHWM67(0006)
           END-IF.
           MOVE WK01-DATE-4064-CH676-F TO WK01-DATE-4064-CH67-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH677-Z = 'Y'
               MOVE WK01-DATE-4064-CH677 TO START-DATE-4064-CH67 OF
                FCHWM67(0007)
           END-IF.
           MOVE WK01-DATE-4064-CH677-F TO WK01-DATE-4064-CH67-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH678-Z = 'Y'
               MOVE WK01-DATE-4064-CH678 TO START-DATE-4064-CH67 OF
                FCHWM67(0008)
           END-IF.
           MOVE WK01-DATE-4064-CH678-F TO WK01-DATE-4064-CH67-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH679-Z = 'Y'
               MOVE WK01-DATE-4064-CH679 TO START-DATE-4064-CH67 OF
                FCHWM67(0009)
           END-IF.
           MOVE WK01-DATE-4064-CH679-F TO WK01-DATE-4064-CH67-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6710-Z = 'Y'
               MOVE WK01-DATE-4064-CH6710 TO START-DATE-4064-CH67 OF
                FCHWM67(0010)
           END-IF.
           MOVE WK01-DATE-4064-CH6710-F TO WK01-DATE-4064-CH67-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6711-Z = 'Y'
               MOVE WK01-DATE-4064-CH6711 TO START-DATE-4064-CH67 OF
                FCHWM67(0011)
           END-IF.
           MOVE WK01-DATE-4064-CH6711-F TO WK01-DATE-4064-CH67-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A1-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A1 TO END-DATE-4064-CH67 OF
                FCHWM67(0001)
           END-IF.
           MOVE WK01-DATE-4064-CH6A1-F TO WK01-DATE-4064-CH6A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A2-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A2 TO END-DATE-4064-CH67 OF
                FCHWM67(0002)
           END-IF.
           MOVE WK01-DATE-4064-CH6A2-F TO WK01-DATE-4064-CH6A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A3-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A3 TO END-DATE-4064-CH67 OF
                FCHWM67(0003)
           END-IF.
           MOVE WK01-DATE-4064-CH6A3-F TO WK01-DATE-4064-CH6A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A4-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A4 TO END-DATE-4064-CH67 OF
                FCHWM67(0004)
           END-IF.
           MOVE WK01-DATE-4064-CH6A4-F TO WK01-DATE-4064-CH6A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A5-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A5 TO END-DATE-4064-CH67 OF
                FCHWM67(0005)
           END-IF.
           MOVE WK01-DATE-4064-CH6A5-F TO WK01-DATE-4064-CH6A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A6-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A6 TO END-DATE-4064-CH67 OF
                FCHWM67(0006)
           END-IF.
           MOVE WK01-DATE-4064-CH6A6-F TO WK01-DATE-4064-CH6A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A7-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A7 TO END-DATE-4064-CH67 OF
                FCHWM67(0007)
           END-IF.
           MOVE WK01-DATE-4064-CH6A7-F TO WK01-DATE-4064-CH6A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A8-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A8 TO END-DATE-4064-CH67 OF
                FCHWM67(0008)
           END-IF.
           MOVE WK01-DATE-4064-CH6A8-F TO WK01-DATE-4064-CH6A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A9-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A9 TO END-DATE-4064-CH67 OF
                FCHWM67(0009)
           END-IF.
           MOVE WK01-DATE-4064-CH6A9-F TO WK01-DATE-4064-CH6A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A10-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A10 TO END-DATE-4064-CH67 OF
                FCHWM67(0010)
           END-IF.
           MOVE WK01-DATE-4064-CH6A10-F TO WK01-DATE-4064-CH6A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH6A11-Z = 'Y'
               MOVE WK01-DATE-4064-CH6A11 TO END-DATE-4064-CH67 OF
                FCHWM67(0011)
           END-IF.
           MOVE WK01-DATE-4064-CH6A11-F TO WK01-DATE-4064-CH6A-F (11).
