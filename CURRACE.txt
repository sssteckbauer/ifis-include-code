      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4034-ACCT-EFCTV' TO DBLINK-CURR-PARAGRAPH.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4034-ACCT-EFCTV' TO RECORD-NAME.                      276 BRTN
YYY991     MOVE 'F-ACCT' TO AREA-NAME.                                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4034-ACCT-EFCTV TO DBLINK-RECORD-MADE-CURRENT.  276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4034-ACCT-EFCTV-EXIT                     276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE ACE-FK1-ACC-ACCT-CD TO DBLINK-R4034-ACCT-EFCTV-01   276 BRTN
YYY991         MOVE ACE-START-DT TO DBLINK-R4034-ACCT-EFCTV-02          276 BRTN
YYY991         MOVE ACE-TIME-STMP TO DBLINK-R4034-ACCT-EFCTV-03         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4034-ACCT-EFCTV TO DBLINK-VIEW-NAME-CURRENT.    276 BRTN
YYY991     MOVE DBLINK-R4034-ACCT-EFCTV-KEY TO DBLINK-VIEW-200-CURRENT. 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4003-4034-O                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4003-4034-O-F             276 BRTN
YYY991     IF ACE-ACC-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE ACE-ACC-SETF TO DBLINK-S-4003-4034-O-F              276 BRTN
YYY991         MOVE ACE-FK3-ACC-ACCT-CD TO DBLINK-S-4003-4034-O-01      276 BRTN
YYY991         MOVE ACE-FK1-ACC-ACCT-CD TO DBLINK-S-4003-4034-O-02      276 BRTN
YYY991         MOVE ACE-START-DT TO DBLINK-S-4003-4034-O-03             276 BRTN
YYY991         MOVE ACE-TIME-STMP TO DBLINK-S-4003-4034-O-04            276 BRTN
YYY991         MOVE ACE-ACC-SET-TS TO DBLINK-S-4003-4034-O-05           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4003-4034-P                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ACE-FK1-ACC-ACCT-CD TO DBLINK-S-4003-4034-P-01.         276 BRTN
YYY991     MOVE ACE-START-DT TO DBLINK-S-4003-4034-P-02.                276 BRTN
YYY991     MOVE ACE-TIME-STMP TO DBLINK-S-4003-4034-P-03.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4014-4034                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ACE-FK2-ACT-ACCT-TYP-C TO DBLINK-S-4014-4034-01.        276 BRTN
YYY991     MOVE ACE-FK1-ACC-ACCT-CD TO DBLINK-S-4014-4034-02.           276 BRTN
YYY991     MOVE ACE-START-DT TO DBLINK-S-4014-4034-03.                  276 BRTN
YYY991     MOVE ACE-TIME-STMP TO DBLINK-S-4014-4034-04.                 276 BRTN
