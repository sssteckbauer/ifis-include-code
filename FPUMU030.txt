    ***Created by Convert/DC version V8R03 on 11/20/00 at 11:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4070-PU03-Z = 'Y'
               MOVE WK01-CODE-4070-PU03 TO RQST-CODE-4070-PU03 OF
                FPUWK03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CMPLT-IND-4070-PU03-Z = 'Y'
               MOVE WK01-CMPLT-IND-4070-PU03 TO
                RQST-CMPLT-IND-4070-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU03-Z = 'Y'
               MOVE WK01-CODE-PU03 TO ACTN-CODE-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TRADE-IN-IND-PU03-Z = 'Y'
               MOVE WK01-TRADE-IN-IND-PU03 TO
                TRADE-IN-IND-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TRADE-IN-UCID-PU03-Z = 'Y'
               MOVE WK01-TRADE-IN-UCID-PU03 TO
                TRADE-IN-UCID-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCESSORY-IND-PU03-Z = 'Y'
               MOVE WK01-ACCESSORY-IND-PU03 TO
                ACCESSORY-IND-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCESSORY-UCID-PU03-Z = 'Y'
               MOVE WK01-ACCESSORY-UCID-PU03 TO
                ACCESSORY-UCID-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FABRICATION-IND-PU03-Z = 'Y'
               MOVE WK01-FABRICATION-IND-PU03 TO
                FABRICATION-IND-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FABRICATION-NUM-PU03-Z = 'Y'
               MOVE WK01-FABRICATION-NUM-PU03 TO
                FABRICATION-NUM-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-4071-PU03-Z = 'Y'
               MOVE WK01-TO-CODE-4071-PU03 TO SHIP-TO-CODE-4071-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4070-PU03-Z = 'Y'
               MOVE WK01-NAME-4070-PU03 TO RQST-NAME-4070-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4070-PU03-Z = 'Y'
               MOVE WK01-IND-4070-PU03 TO APRVL-IND-4070-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4071-PU03-Z = 'Y'
               MOVE WK01-ERROR-IND-4071-PU03 TO DTL-ERROR-IND-4071-PU03
                OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4071-PU03-Z = 'Y'
               MOVE WK01-NMBR-4071-PU03 TO ITEM-NMBR-4071-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4074-PU03-Z = 'Y'
               MOVE WK01-CODE-4074-PU03 TO CMDTY-CODE-4074-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4074-PU03-Z = 'Y'
               MOVE WK01-DESC-4074-PU03 TO CMDTY-DESC-4074-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MEA-CODE-4071-PU03-Z = 'Y'
               MOVE WK01-MEA-CODE-4071-PU03 TO UNIT-MEA-CODE-4071-PU03
                OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ITEM-NMBR-PU03-Z = 'Y'
               MOVE WK01-ITEM-NMBR-PU03 TO RQST-ITEM-NMBR-PU03 OF
                FPUWK03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4070-PU03-Z = 'Y'
               MOVE WK01-DATE-4070-PU03 TO CNCL-DATE-4070-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-ACCT-4071-PU03-Z = 'Y'
               MOVE WK01-CNTR-ACCT-4071-PU03 TO DTL-CNTR-ACCT-4071-PU03
                OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MEA-DESC-4075-PU03-Z = 'Y'
               MOVE WK01-MEA-DESC-4075-PU03 TO UNIT-MEA-DESC-4075-PU03
                OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4071-PU03-Z = 'Y'
               MOVE WK01-CODE-4071-PU03 TO PRJCT-CODE-4071-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4083-PU03-Z = 'Y'
               MOVE WK01-NMBR-4083-PU03 TO PO-NMBR-4083-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ITEM-NMBR-4085-PU03-Z = 'Y'
               MOVE WK01-ITEM-NMBR-4085-PU03 TO PO-ITEM-NMBR-4085-PU03
                OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4071-PU0A-Z = 'Y'
               MOVE WK01-CODE-4071-PU0A TO AGRMT-CODE-4071-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4071-PU03-Z = 'Y'
               MOVE WK01-DATE-4071-PU03 TO ACTVY-DATE-4071-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COUNT-4070-PU03-Z = 'Y'
               MOVE WK01-COUNT-4070-PU03 TO ITEM-COUNT-4070-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-4071-PU03-Z = 'Y'
               MOVE WK01-4071-PU03 TO QTY-4071-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-4071-PU03-Z = 'Y'
               MOVE WK01-PRICE-4071-PU03 TO UNIT-PRICE-4071-PU03 OF
                FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-PU03-Z = 'Y'
               MOVE WK01-PRICE-PU03 TO EXTENDED-PRICE-PU03 OF FPUWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-PU03-Z = 'Y'
               MOVE WK01-FLAG-PU03 TO TEXT-FLAG-PU03 OF FPUWM03
           END-IF.
