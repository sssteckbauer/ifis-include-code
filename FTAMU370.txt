    ***Created by Convert/DC version V8R03 on 12/08/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-IND-TA37-Z = 'Y'
               MOVE WK01-TYPE-IND-TA37 TO DCMNT-TYPE-IND-TA37 OF FTAWK37
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-LITERAL-TA37-Z = 'Y'
               MOVE WK01-TYPE-LITERAL-TA37 TO DCMNT-TYPE-LITERAL-TA37
                OF FTAWK37
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-TA37-Z = 'Y'
               MOVE WK01-NMBR-TA37 TO DCMNT-NMBR-TA37 OF FTAWK37
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TRAVELER-TA37-Z = 'Y'
               MOVE WK01-TRAVELER-TA37 TO DCMNT-TRAVELER-TA37 OF FTAWM37
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-TA3A-Z = 'Y'
               MOVE WK01-NMBR-TA3A TO EVENT-NMBR-TA37 OF FTAWM37
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TRAVELER-TA3A-Z = 'Y'
               MOVE WK01-TRAVELER-TA3A TO EVENT-TRAVELER-TA37 OF FTAWM37
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRSN-NAME-KEY-TA37-Z = 'Y'
               MOVE WK01-PRSN-NAME-KEY-TA37 TO PRSN-NAME-KEY-TA37
                OF FTAWM37
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SOC-SEC-NMBR-TA37-Z = 'Y'
               MOVE WK01-SOC-SEC-NMBR-TA37 TO SOC-SEC-NMBR-TA37 OF
                FTAWM37
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-EMP-NMBR-TA37-Z = 'Y'
               MOVE WK01-EMP-NMBR-TA37 TO EMP-NMBR-TA37 OF
                FTAWM37
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA37-Z = 'Y'
               MOVE WK01-CODE-TA37 TO ACTN-CODE-TA37 OF FTAWK37
           END-IF.
