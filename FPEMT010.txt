    ***Created by Convert/DC version V8R03 on 12/11/00 at 08:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE011-Z = 'Y'
               MOVE WK01-CODE-PE011 TO ACTN-CODE-PE01 OF FPEWM01(0001)
           END-IF.
           MOVE WK01-CODE-PE011-F TO WK01-CODE-PE01-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE012-Z = 'Y'
               MOVE WK01-CODE-PE012 TO ACTN-CODE-PE01 OF FPEWM01(0002)
           END-IF.
           MOVE WK01-CODE-PE012-F TO WK01-CODE-PE01-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE013-Z = 'Y'
               MOVE WK01-CODE-PE013 TO ACTN-CODE-PE01 OF FPEWM01(0003)
           END-IF.
           MOVE WK01-CODE-PE013-F TO WK01-CODE-PE01-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE014-Z = 'Y'
               MOVE WK01-CODE-PE014 TO ACTN-CODE-PE01 OF FPEWM01(0004)
           END-IF.
           MOVE WK01-CODE-PE014-F TO WK01-CODE-PE01-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE015-Z = 'Y'
               MOVE WK01-CODE-PE015 TO ACTN-CODE-PE01 OF FPEWM01(0005)
           END-IF.
           MOVE WK01-CODE-PE015-F TO WK01-CODE-PE01-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE016-Z = 'Y'
               MOVE WK01-CODE-PE016 TO ACTN-CODE-PE01 OF FPEWM01(0006)
           END-IF.
           MOVE WK01-CODE-PE016-F TO WK01-CODE-PE01-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE017-Z = 'Y'
               MOVE WK01-CODE-PE017 TO ACTN-CODE-PE01 OF FPEWM01(0007)
           END-IF.
           MOVE WK01-CODE-PE017-F TO WK01-CODE-PE01-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE018-Z = 'Y'
               MOVE WK01-CODE-PE018 TO ACTN-CODE-PE01 OF FPEWM01(0008)
           END-IF.
           MOVE WK01-CODE-PE018-F TO WK01-CODE-PE01-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE019-Z = 'Y'
               MOVE WK01-CODE-PE019 TO ACTN-CODE-PE01 OF FPEWM01(0009)
           END-IF.
           MOVE WK01-CODE-PE019-F TO WK01-CODE-PE01-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE0110-Z = 'Y'
               MOVE WK01-CODE-PE0110 TO ACTN-CODE-PE01 OF FPEWM01(0010)
           END-IF.
           MOVE WK01-CODE-PE0110-F TO WK01-CODE-PE01-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE0111-Z = 'Y'
               MOVE WK01-CODE-PE0111 TO ACTN-CODE-PE01 OF FPEWM01(0011)
           END-IF.
           MOVE WK01-CODE-PE0111-F TO WK01-CODE-PE01-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE0112-Z = 'Y'
               MOVE WK01-CODE-PE0112 TO ACTN-CODE-PE01 OF FPEWM01(0012)
           END-IF.
           MOVE WK01-CODE-PE0112-F TO WK01-CODE-PE01-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE011-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE011 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0001)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE011-F TO
                WK01-TYPE-CODE-6137-PE01-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE012-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE012 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0002)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE012-F TO
                WK01-TYPE-CODE-6137-PE01-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE013-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE013 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0003)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE013-F TO
                WK01-TYPE-CODE-6137-PE01-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE014-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE014 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0004)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE014-F TO
                WK01-TYPE-CODE-6137-PE01-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE015-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE015 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0005)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE015-F TO
                WK01-TYPE-CODE-6137-PE01-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE016-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE016 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0006)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE016-F TO
                WK01-TYPE-CODE-6137-PE01-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE017-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE017 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0007)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE017-F TO
                WK01-TYPE-CODE-6137-PE01-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE018-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE018 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0008)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE018-F TO
                WK01-TYPE-CODE-6137-PE01-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE019-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE019 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0009)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE019-F TO
                WK01-TYPE-CODE-6137-PE01-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE0110-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE0110 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0010)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE0110-F TO
                WK01-TYPE-CODE-6137-PE01-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE0111-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE0111 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0011)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE0111-F TO
                WK01-TYPE-CODE-6137-PE01-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-PE0112-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-PE0112 TO
                ADR-TYPE-CODE-6137-PE01 OF FPEWM01(0012)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-PE0112-F TO
                WK01-TYPE-CODE-6137-PE01-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE011-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE011 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0001)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE011-F TO WK01-PRTY-SEQ-6137-PE01-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE012-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE012 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0002)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE012-F TO WK01-PRTY-SEQ-6137-PE01-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE013-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE013 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0003)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE013-F TO WK01-PRTY-SEQ-6137-PE01-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE014-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE014 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0004)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE014-F TO WK01-PRTY-SEQ-6137-PE01-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE015-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE015 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0005)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE015-F TO WK01-PRTY-SEQ-6137-PE01-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE016-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE016 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0006)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE016-F TO WK01-PRTY-SEQ-6137-PE01-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE017-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE017 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0007)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE017-F TO WK01-PRTY-SEQ-6137-PE01-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE018-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE018 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0008)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE018-F TO WK01-PRTY-SEQ-6137-PE01-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE019-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE019 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0009)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE019-F TO WK01-PRTY-SEQ-6137-PE01-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE0110-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE0110 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0010)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE0110-F TO
                WK01-PRTY-SEQ-6137-PE01-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE0111-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE0111 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0011)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE0111-F TO
                WK01-PRTY-SEQ-6137-PE01-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-PE0112-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-PE0112 TO
                DSPLY-PRTY-SEQ-6137-PE01 OF FPEWM01(0012)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-PE0112-F TO
                WK01-PRTY-SEQ-6137-PE01-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE011-Z = 'Y'
               MOVE WK01-DESC-6137-PE011 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0001)
           END-IF.
           MOVE WK01-DESC-6137-PE011-F TO WK01-DESC-6137-PE01-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE012-Z = 'Y'
               MOVE WK01-DESC-6137-PE012 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0002)
           END-IF.
           MOVE WK01-DESC-6137-PE012-F TO WK01-DESC-6137-PE01-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE013-Z = 'Y'
               MOVE WK01-DESC-6137-PE013 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0003)
           END-IF.
           MOVE WK01-DESC-6137-PE013-F TO WK01-DESC-6137-PE01-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE014-Z = 'Y'
               MOVE WK01-DESC-6137-PE014 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0004)
           END-IF.
           MOVE WK01-DESC-6137-PE014-F TO WK01-DESC-6137-PE01-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE015-Z = 'Y'
               MOVE WK01-DESC-6137-PE015 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0005)
           END-IF.
           MOVE WK01-DESC-6137-PE015-F TO WK01-DESC-6137-PE01-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE016-Z = 'Y'
               MOVE WK01-DESC-6137-PE016 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0006)
           END-IF.
           MOVE WK01-DESC-6137-PE016-F TO WK01-DESC-6137-PE01-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE017-Z = 'Y'
               MOVE WK01-DESC-6137-PE017 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0007)
           END-IF.
           MOVE WK01-DESC-6137-PE017-F TO WK01-DESC-6137-PE01-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE018-Z = 'Y'
               MOVE WK01-DESC-6137-PE018 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0008)
           END-IF.
           MOVE WK01-DESC-6137-PE018-F TO WK01-DESC-6137-PE01-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE019-Z = 'Y'
               MOVE WK01-DESC-6137-PE019 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0009)
           END-IF.
           MOVE WK01-DESC-6137-PE019-F TO WK01-DESC-6137-PE01-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0110-Z = 'Y'
               MOVE WK01-DESC-6137-PE0110 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0010)
           END-IF.
           MOVE WK01-DESC-6137-PE0110-F TO WK01-DESC-6137-PE01-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0111-Z = 'Y'
               MOVE WK01-DESC-6137-PE0111 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0011)
           END-IF.
           MOVE WK01-DESC-6137-PE0111-F TO WK01-DESC-6137-PE01-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0112-Z = 'Y'
               MOVE WK01-DESC-6137-PE0112 TO SHORT-DESC-6137-PE01 OF
                FPEWM01(0012)
           END-IF.
           MOVE WK01-DESC-6137-PE0112-F TO WK01-DESC-6137-PE01-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A1-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A1 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0001)
           END-IF.
           MOVE WK01-DESC-6137-PE0A1-F TO WK01-DESC-6137-PE0A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A2-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A2 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0002)
           END-IF.
           MOVE WK01-DESC-6137-PE0A2-F TO WK01-DESC-6137-PE0A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A3-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A3 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0003)
           END-IF.
           MOVE WK01-DESC-6137-PE0A3-F TO WK01-DESC-6137-PE0A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A4-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A4 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0004)
           END-IF.
           MOVE WK01-DESC-6137-PE0A4-F TO WK01-DESC-6137-PE0A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A5-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A5 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0005)
           END-IF.
           MOVE WK01-DESC-6137-PE0A5-F TO WK01-DESC-6137-PE0A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A6-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A6 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0006)
           END-IF.
           MOVE WK01-DESC-6137-PE0A6-F TO WK01-DESC-6137-PE0A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A7-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A7 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0007)
           END-IF.
           MOVE WK01-DESC-6137-PE0A7-F TO WK01-DESC-6137-PE0A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A8-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A8 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0008)
           END-IF.
           MOVE WK01-DESC-6137-PE0A8-F TO WK01-DESC-6137-PE0A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A9-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A9 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0009)
           END-IF.
           MOVE WK01-DESC-6137-PE0A9-F TO WK01-DESC-6137-PE0A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A10-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A10 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0010)
           END-IF.
           MOVE WK01-DESC-6137-PE0A10-F TO WK01-DESC-6137-PE0A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A11-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A11 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0011)
           END-IF.
           MOVE WK01-DESC-6137-PE0A11-F TO WK01-DESC-6137-PE0A-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0A12-Z = 'Y'
               MOVE WK01-DESC-6137-PE0A12 TO LONG-DESC-6137-PE01 OF
                FPEWM01(0012)
           END-IF.
           MOVE WK01-DESC-6137-PE0A12-F TO WK01-DESC-6137-PE0A-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-CODE-PE01-Z = 'Y'
               MOVE WK01-ADR-TYPE-CODE-PE01 TO RQST-ADR-TYPE-CODE-PE01
                OF FPEWK01
           END-IF.
