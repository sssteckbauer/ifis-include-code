       975-TO-R4071-RQST-DTL SECTION.
           MOVE RQD-USER-CD TO
               USER-CODE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-FK-RQT-RQST-CD TO
               RQST-CODE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-ITEM-NBR TO
               ITEM-NMBR-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-CMDTY-DESC TO
               CMDTY-DESC-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-TRANS-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               TRANS-DATE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-QTY TO
               QTY-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-UNIT-PRICE TO
               UNIT-PRICE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-UNIT-MEA-CD TO
               UNIT-MEA-CODE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-SHIP-TO-CD TO
               SHIP-TO-CODE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-DTL-CNTR-ACCT TO
               DTL-CNTR-ACCT-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-DTL-ERROR-IND TO
               DTL-ERROR-IND-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-DLVRY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               DLVRY-DATE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-AGRMT-CD TO
               AGRMT-CODE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-VNDR-IREF-ID TO
               VNDR-INTRL-REF-ID-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-PRJCT-CD TO
               PRJCT-CODE-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-PART-NBR TO
               PART-NMBR-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-ENTPSN-IND TO
               ENTY-PRSN-IND-4071 OF R4071-RQST-DTL
                                                                     .
           MOVE RQD-LIEN-CLOSED-CD TO
               LIEN-CLOSED-CODE-4071 OF R4071-RQST-DTL
                                                                     .
       975-TO-R4071-RQST-DTL-EXIT.
           EXIT.
