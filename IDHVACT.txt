       976-FROM-R4014-ACTT SECTION.

FCYI **        MOVE USER-CODE-4014 OF R4014-ACTT
FCYI           MOVE DBLINK-USER-CD
               TO ACT-USER-CD.

               MOVE LAST-ACTVY-DATE-4014 OF R4014-ACTT
               TO ACT-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4014 OF R4014-ACTT
               TO ACT-UNVRS-CD.

               MOVE COA-CODE-4014 OF R4014-ACTT
               TO ACT-COA-CD.

               MOVE ACCT-TYPE-CODE-4014 OF R4014-ACTT
               TO ACT-ACCT-TYP-CD.

           IF ACTVY-DATE-4014 OF R4014-ACTT  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ACT-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4014 OF R4014-ACTT
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ACT-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ACT-ACTVY-DT
               END-IF
           END-IF.
       976-FROM-R4014-ACTT-EXIT.
           EXIT.
