       975-TO-R4099-RETURNS SECTION.
           MOVE RTN-USER-CD TO
               USER-CODE-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-UNVRS-CD TO
               UNVRS-CODE-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-PO-RTRN-CD TO
               PO-RTRN-CODE-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-CARR-IREF-ID TO
               CARR-INTRL-REF-ID-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-ADR-TYP-CD TO
               ADR-TYPE-CODE-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-PYMT-CD TO
               PYMT-CODE-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-NBR-PCS TO
               NMBR-PCS-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-FRGHT-CHRG TO
               FRGHT-CHRG-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-SHIP-WT TO
               SHIP-WT-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-BILL-OF-LADING TO
               BILL-OF-LADING-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-FRGN-DMSTC-IND TO
               FRGN-DMSTC-IND-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-VNDR-IREF-ID TO
               VNDR-INTRL-REF-ID-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-CONSGNE-NAME TO
               CONSGNE-NAME-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-CARR-ENTPSN-IND TO
               CARR-ENTY-PRSN-IND-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-ENTPSN-IND TO
               ENTY-PRSN-IND-4099 OF R4099-RETURNS
                                                                     .
           MOVE RTN-DOC-REF-NBR TO
               DCMNT-REF-NMBR-4099 OF R4099-RETURNS
                                                                     .
       975-TO-R4099-RETURNS-EXIT.
           EXIT.
