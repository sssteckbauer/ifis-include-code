      *9400-SUM-MASTER-OUT-HASH-TOTS.

           IF  MST-INCOME-REC
               IF  MST-INITIAL-REC
                   ADD MST-INIT-AMT        TO HSHO-INC-INIT-CURR-YR-AMT
                   ADD MST-PREV-YR-ADJ-AMT TO HSHO-INC-PREV-YR-AMT
                   ADD +1                  TO HSHO-INC-INIT-CNT
               ELSE
               IF  MST-TRANS-REC
                   ADD MST-TRANS-AMT       TO HSHO-INC-TRANS-AMT
                   ADD +1                  TO HSHO-INC-TRANS-CNT
               ELSE
                   ADD MST-SUM-ADJ-AMT     TO HSHO-INC-ADJ-BUDGET-AMT
                   ADD MST-SUM-ADJTD-AMT   TO HSHO-INC-ADJ-TO-DTE-AMT
                   ADD MST-SUM-INCTD-AMT   TO HSHO-INC-INC-TO-DTE-AMT
                   ADD +1                  TO HSHO-INC-SUMRY-CNT
           ELSE
           IF  MST-APPROP-REC
               IF  MST-INITIAL-REC
                   ADD MST-INIT-AMT        TO HSHO-APR-INIT-CURR-YR-AMT
                   ADD MST-PREV-YR-ADJ-AMT TO HSHO-APR-PREV-YR-AMT
                   ADD MST-INIT-FTE        TO HSHO-APR-INIT-CURR-YR-FTE
                   ADD MST-PREV-YR-ADJ-FTE TO HSHO-APR-PREV-YR-FTE
                   ADD +1                  TO HSHO-APR-INIT-CNT
               ELSE
               IF  MST-TRANS-REC
                   ADD MST-TRANS-AMT       TO HSHO-APR-TRANS-AMT
                   ADD MST-TRANS-FTE       TO HSHO-APR-TRANS-FTE
                   ADD +1                  TO HSHO-APR-TRANS-CNT
               ELSE
                   ADD MST-SUM-ADJ-AMT     TO HSHO-APR-ADJ-BUDGET-AMT
                   ADD MST-SUM-ADJTD-AMT   TO HSHO-APR-ADJ-TO-DTE-AMT
                   ADD MST-SUM-INCTD-AMT   TO HSHO-APR-INC-TO-DTE-AMT
                   ADD MST-SUM-ADJ-FTE     TO HSHO-APR-ADJ-BUDGET-FTE
                   ADD MST-SUM-ADJTD-FTE   TO HSHO-APR-ADJ-TO-DTE-FTE
                   ADD MST-SUM-INCTD-FTE   TO HSHO-APR-INC-TO-DTE-FTE
                   ADD +1                  TO HSHO-APR-SUMRY-CNT.

       9410-SUM-TOTAL-COUNTS.

           COMPUTE HSHO-INC-TOTAL-CNT = HSHO-INC-INIT-CNT +
               HSHO-INC-TRANS-CNT + HSHO-INC-SUMRY-CNT.

           COMPUTE HSHO-APR-TOTAL-CNT = HSHO-APR-INIT-CNT +
               HSHO-APR-TRANS-CNT + HSHO-APR-SUMRY-CNT.
       EJECT
       9450-PRINT-MASTER-OUT-HSH-TOTS.

           PERFORM 9410-SUM-TOTAL-COUNTS.

           IF HOLD-EXTRACT EQUAL SPACES
              MOVE '    ( O U T ' TO H1-FLD1 OF HASH-HEAD1
              MOVE 'P U T MASTER )'    TO H1-FLD2 OF HASH-HEAD1
           ELSE
              MOVE '  ( O U T P ' TO H1-FLD1 OF HASH-HEAD1
              MOVE 'U T  EXTRACT )' TO H1-FLD2 OF HASH-HEAD1.

           MOVE RPT-HEAD-LINE-1 TO HOLD-HASH-HEAD.
           MOVE HOLD-PROG-NAME  TO H1-FLD1A.

           MOVE ZERO TO RPT-LINE-CNT.

           MOVE HASH-HEAD1 TO REPORT-LINE.
           PERFORM 8900-PRINT-REPORT.

           MOVE HASH-HEAD2 TO REPORT-LINE.
           PERFORM 8900-PRINT-REPORT.

           MOVE HASH-HEAD3 TO REPORT-LINE.
           PERFORM 8900-PRINT-REPORT.

           MOVE '-' TO HA-CC.
           PERFORM 9460-PRINT-HASH-AMOUNTS
               VARYING HASH-SUB FROM 1 BY 1
                   UNTIL HASH-SUB GREATER THAN 6.

           MOVE SPACES TO REPORT-LINE.
           MOVE '0' TO RPT-CC.
           MOVE 'NUMBER OF RECORDS -' TO RPT-AREA.
           PERFORM 8900-PRINT-REPORT.

           PERFORM 9470-PRINT-HASH-COUNTS
               VARYING HASH-SUB FROM 1 BY 1
                   UNTIL HASH-SUB GREATER THAN 4.



       9460-PRINT-HASH-AMOUNTS.

           MOVE HASH-AMOUNT-TITLE (HASH-SUB) TO HA-TITLE.
           MOVE HSHO-INC-AMOUNT   (HASH-SUB) TO HA-INC-AMT.
           MOVE HSHO-APR-AMOUNT   (HASH-SUB) TO HA-APR-AMT.
           MOVE HSHO-APR-FTE      (HASH-SUB) TO HA-APR-FTE.

           IF  HSHO-INC-AMOUNT (HASH-SUB) GREATER THAN ZERO
               MOVE 'CR'                     TO HA-INC-AMT-CR
           ELSE
               MOVE SPACES                   TO HA-INC-AMT-CR.

           IF  HSHO-APR-AMOUNT (HASH-SUB) GREATER THAN ZERO
               MOVE 'CR'                     TO HA-APR-AMT-CR
           ELSE
               MOVE SPACES                   TO HA-APR-AMT-CR.

           IF  HSHO-APR-FTE (HASH-SUB) GREATER THAN ZERO
               MOVE 'CR'                     TO HA-APR-FTE-CR
           ELSE
               MOVE SPACES                   TO HA-APR-FTE-CR.

           COMPUTE HASH-TOTAL-AMOUNT = HSHO-INC-AMOUNT (HASH-SUB) +
                                       HSHO-APR-AMOUNT (HASH-SUB).

           MOVE HASH-TOTAL-AMOUNT TO HA-TOT-AMT.

           IF  HASH-TOTAL-AMOUNT GREATER THAN ZERO
               MOVE 'CR' TO HA-TOT-AMT-CR
           ELSE
               MOVE SPACES TO HA-TOT-AMT-CR.

           MOVE HASH-AMOUNT-LINE TO REPORT-LINE.
           PERFORM 8900-PRINT-REPORT.
           MOVE SPACE TO HA-CC.



       9470-PRINT-HASH-COUNTS.

           MOVE HASH-COUNT-TITLE (HASH-SUB) TO HC-TITLE.
           MOVE HSHO-INC-COUNT   (HASH-SUB) TO HC-INC-CNT.
           MOVE HSHO-APR-COUNT   (HASH-SUB) TO HC-APR-CNT.

           COMPUTE HASH-TOTAL-COUNT = HSHO-INC-COUNT (HASH-SUB) +
                                      HSHO-APR-COUNT (HASH-SUB).

           MOVE HASH-TOTAL-COUNT            TO HC-TOT-CNT.
           MOVE HASH-COUNT-LINE             TO REPORT-LINE.
           PERFORM 8900-PRINT-REPORT.
