      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYII12 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYII12.

      *
      ******************************************************************
      *******
      ***  DISPLAY APPROPRIATE PAGE STATUS
      ******************************************************************
      *******
      ***  PAGE-FLAG VALUES - 'B' = BACKWARD
      ***                     'F' = FORWARD
      ***  PAGE-STATUS-CODE VALUES - 'F' = FIRST PAGE
      ***                            'N' =  NEXT PAGE
      ***                            'P' = PRIOR PAGE
      ***                            'L' =  LAST PAGE
      ******************************************************************
      *******
      ***  PAGE BACKWARD REQUESTED
      ******************************************************************
      *******
           IF PAGE-FLAG-SY00 = 'B'
               IF SAVE-DBKEY-ID-MINUS-ONE-SY00 = ZEROS
                   MOVE 'F' TO PAGE-STATUS-CODE-SY00
               ELSE
                   MOVE 'P' TO PAGE-STATUS-CODE-SY00
               END-IF
           END-IF.
      *
      ******************************************************************
      *******
      ***  PAGE FORWARD REQUESTED
      ******************************************************************
      *******
           IF PAGE-FLAG-SY00 = 'F'
               IF SAVE-DBKEY-ID-PLUS-ONE-SY00 = ZEROS
                   MOVE 'L' TO PAGE-STATUS-CODE-SY00
               ELSE
                   MOVE 'N' TO PAGE-STATUS-CODE-SY00
               END-IF
           END-IF.
      *
      ******************************************************************
      *******
      ***  FIRST TIME, REFRESH, OR NEW KEY VALUES - DISPLAY 'FIRST PAGE'
      ******************************************************************
      *******
           IF SAVE-DBKEY-ID-MINUS-ONE-SY00 = ZEROS
               MOVE 'F' TO PAGE-STATUS-CODE-SY00
           ELSE
               IF PAGE-FLAG-SY00 = SPACES
                   MOVE SPACES TO PAGE-STATUS-CODE-SY00
               END-IF
           END-IF.
      *
      *%   INQUIRE MAP
      *%   IF PAGE-FLAG-SY00 NOT = SPACES AND
      *%      ANY DATA CHANGED
           IF  PAGE-FLAG-SY00 NOT = SPACES
            AND  WK01-FLAG-ANY-FIELDS-CHANGED    = 'Y'
               MOVE SPACES TO MAP-ELMNT-NAME-SY00
               MOVE 990007 TO MSG-ID-SY00
               PERFORM 200-ZZYII03-GETMESGS
                  THRU 200-ZZYII03-GETMESGS-EXIT
               IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                  GO TO 200-FSYII12-EXIT
               END-IF
      ***  ACTIONS ENTERED ARE IGNORED BY PAGING
           END-IF.
      *
           MOVE SPACES TO PAGE-FLAG-SY00.
           PERFORM 200-DISPMAP-S01
              THRU 200-DISPMAP-S01-EXIT.
           IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
              GO TO 200-FSYII12-EXIT
           END-IF.
      *
      ******************************************************************
      *******
      *

       200-FSYII12-EXIT.
           EXIT.
