    ***Created by Convert/DC version V8R03 on 11/09/00 at 15:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH54-Z = 'Y'
               MOVE WK01-CODE-CH54 TO ACTN-CODE-CH54 OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-COPY-CH54-Z = 'Y'
               MOVE WK01-CLASS-CODE-COPY-CH54 TO
                RULE-CLASS-CODE-COPY-CH54 OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4180-CH54-Z = 'Y'
               MOVE WK01-CLASS-CODE-4180-CH54 TO
                RULE-CLASS-CODE-4180-CH54 OF FCHWK54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4181-CH54-Z = 'Y'
               MOVE WK01-CLASS-DESC-4181-CH54 TO
                RULE-CLASS-DESC-4181-CH54 OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-TYPE-4181-CH54-Z = 'Y'
               MOVE WK01-CLASS-TYPE-4181-CH54 TO
                RULE-CLASS-TYPE-4181-CH54 OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLSS-TYP-DSC-4181-CH54-Z = 'Y'
               MOVE WK01-CLSS-TYP-DSC-4181-CH54 TO
                RULE-CLASS-TYPE-DESC-4181-CH54 OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BDGT-IND-4181-CH54-Z = 'Y'
               MOVE WK01-BDGT-IND-4181-CH54 TO RSRV-BDGT-IND-4181-CH54
                OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BDGT-DESC-4181-CH54-Z = 'Y'
               MOVE WK01-BDGT-DESC-4181-CH54 TO
                RSRV-BDGT-DESC-4181-CH54 OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MTHD-IND-4181-CH54-Z = 'Y'
               MOVE WK01-MTHD-IND-4181-CH54 TO BAL-MTHD-IND-4181-CH54
                OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MTHD-DESC-4181-CH54-Z = 'Y'
               MOVE WK01-MTHD-DESC-4181-CH54 TO BAL-MTHD-DESC-4181-CH54
                OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4181-CH54-Z = 'Y'
               MOVE WK01-DATE-4181-CH54 TO START-DATE-4181-CH54 OF
                FCHWK54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHNG-DATE-CH54-Z = 'Y'
               MOVE WK01-CHNG-DATE-CH54 TO NEXT-CHNG-DATE-CH54 OF
                FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4181-CH5A-Z = 'Y'
               MOVE WK01-DATE-4181-CH5A TO END-DATE-4181-CH54 OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4180-CH54-Z = 'Y'
               MOVE WK01-DATE-4180-CH54 TO ACTVY-DATE-4180-CH54 OF
                FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STAMP-4181-CH54-Z = 'Y'
               MOVE WK01-STAMP-4181-CH54 TO TIME-STAMP-4181-CH54 OF
                FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-CH54-Z = 'Y'
               MOVE WK01-PM-FLAG-CH54 TO AM-PM-FLAG-CH54 OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4181-CH54-Z = 'Y'
               MOVE WK01-IND-4181-CH54 TO CMPLT-IND-4181-CH54 OF FCHWM54
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4181-CH54-Z = 'Y'
               MOVE WK01-DESC-4181-CH54 TO STATUS-DESC-4181-CH54 OF
                FCHWM54
           END-IF.
