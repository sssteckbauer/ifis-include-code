       975-TO-R6200-ACH-DATA SECTION.
           MOVE ACD-USER-CD TO
               USER-CODE-6200 OF R6200-ACH-DATA
                                                                     .
           MOVE ACD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-6200 OF R6200-ACH-DATA
                                                                     .
           MOVE ACD-UNVRS-CD TO
               UNVRS-CODE-6200 OF R6200-ACH-DATA
                                                                     .
           MOVE ACD-IREF-ID TO
               INTRL-REF-ID-6200 OF R6200-ACH-DATA
                                                                     .
           MOVE ACD-ENTPSN-IND TO
               ENTY-PRSN-IND-6200 OF R6200-ACH-DATA
                                                                     .
           MOVE ACD-ACH-ID-DGT-ONE TO
               ACH-ID-DIGIT-ONE-6200 OF R6200-ACH-DATA
                                                                     .
           MOVE ACD-ACH-ID-LST-NINE TO
               ACH-ID-LAST-NINE-6200 OF R6200-ACH-DATA
                                                                     .
           MOVE ACD-ACH-STOP-IND TO
               ACH-STOP-IND-6200 OF R6200-ACH-DATA
                                                                     .
       975-TO-R6200-ACH-DATA-EXIT.
           EXIT.
