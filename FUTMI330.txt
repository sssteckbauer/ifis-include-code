    ***Created by Convert/DC version V8R03 on 12/11/00 at 09:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT331-Z = 'Y'
               MOVE WK01-CODE-UT331 TO ACTN-CODE-UT33 OF FUTWM33(0001)
           END-IF.
           MOVE WK01-CODE-UT331-F TO WK01-CODE-UT33-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT332-Z = 'Y'
               MOVE WK01-CODE-UT332 TO ACTN-CODE-UT33 OF FUTWM33(0002)
           END-IF.
           MOVE WK01-CODE-UT332-F TO WK01-CODE-UT33-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT333-Z = 'Y'
               MOVE WK01-CODE-UT333 TO ACTN-CODE-UT33 OF FUTWM33(0003)
           END-IF.
           MOVE WK01-CODE-UT333-F TO WK01-CODE-UT33-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT334-Z = 'Y'
               MOVE WK01-CODE-UT334 TO ACTN-CODE-UT33 OF FUTWM33(0004)
           END-IF.
           MOVE WK01-CODE-UT334-F TO WK01-CODE-UT33-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT335-Z = 'Y'
               MOVE WK01-CODE-UT335 TO ACTN-CODE-UT33 OF FUTWM33(0005)
           END-IF.
           MOVE WK01-CODE-UT335-F TO WK01-CODE-UT33-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT336-Z = 'Y'
               MOVE WK01-CODE-UT336 TO ACTN-CODE-UT33 OF FUTWM33(0006)
           END-IF.
           MOVE WK01-CODE-UT336-F TO WK01-CODE-UT33-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT337-Z = 'Y'
               MOVE WK01-CODE-UT337 TO ACTN-CODE-UT33 OF FUTWM33(0007)
           END-IF.
           MOVE WK01-CODE-UT337-F TO WK01-CODE-UT33-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT338-Z = 'Y'
               MOVE WK01-CODE-UT338 TO ACTN-CODE-UT33 OF FUTWM33(0008)
           END-IF.
           MOVE WK01-CODE-UT338-F TO WK01-CODE-UT33-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT339-Z = 'Y'
               MOVE WK01-CODE-UT339 TO ACTN-CODE-UT33 OF FUTWM33(0009)
           END-IF.
           MOVE WK01-CODE-UT339-F TO WK01-CODE-UT33-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT3310-Z = 'Y'
               MOVE WK01-CODE-UT3310 TO ACTN-CODE-UT33 OF FUTWM33(0010)
           END-IF.
           MOVE WK01-CODE-UT3310-F TO WK01-CODE-UT33-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT3311-Z = 'Y'
               MOVE WK01-CODE-UT3311 TO ACTN-CODE-UT33 OF FUTWM33(0011)
           END-IF.
           MOVE WK01-CODE-UT3311-F TO WK01-CODE-UT33-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT3312-Z = 'Y'
               MOVE WK01-CODE-UT3312 TO ACTN-CODE-UT33 OF FUTWM33(0012)
           END-IF.
           MOVE WK01-CODE-UT3312-F TO WK01-CODE-UT33-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT3313-Z = 'Y'
               MOVE WK01-CODE-UT3313 TO ACTN-CODE-UT33 OF FUTWM33(0013)
           END-IF.
           MOVE WK01-CODE-UT3313-F TO WK01-CODE-UT33-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT331-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT331 TO ACH-ID-LAST-NINE-UT33 OF
                FUTWM33(0001)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT331-F TO WK01-ID-LAST-NINE-UT33-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT332-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT332 TO ACH-ID-LAST-NINE-UT33 OF
                FUTWM33(0002)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT332-F TO WK01-ID-LAST-NINE-UT33-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT333-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT333 TO ACH-ID-LAST-NINE-UT33 OF
                FUTWM33(0003)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT333-F TO WK01-ID-LAST-NINE-UT33-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT334-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT334 TO ACH-ID-LAST-NINE-UT33 OF
                FUTWM33(0004)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT334-F TO WK01-ID-LAST-NINE-UT33-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT335-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT335 TO ACH-ID-LAST-NINE-UT33 OF
                FUTWM33(0005)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT335-F TO WK01-ID-LAST-NINE-UT33-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT336-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT336 TO ACH-ID-LAST-NINE-UT33 OF
                FUTWM33(0006)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT336-F TO WK01-ID-LAST-NINE-UT33-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT337-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT337 TO ACH-ID-LAST-NINE-UT33 OF
                FUTWM33(0007)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT337-F TO WK01-ID-LAST-NINE-UT33-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT338-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT338 TO ACH-ID-LAST-NINE-UT33 OF
                FUTWM33(0008)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT338-F TO WK01-ID-LAST-NINE-UT33-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT339-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT339 TO ACH-ID-LAST-NINE-UT33 OF
                FUTWM33(0009)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT339-F TO WK01-ID-LAST-NINE-UT33-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT3310-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT3310 TO ACH-ID-LAST-NINE-UT33
                OF FUTWM33(0010)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT3310-F TO WK01-ID-LAST-NINE-UT33-F
                (10).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT3311-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT3311 TO ACH-ID-LAST-NINE-UT33
                OF FUTWM33(0011)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT3311-F TO WK01-ID-LAST-NINE-UT33-F
                (11).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT3312-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT3312 TO ACH-ID-LAST-NINE-UT33
                OF FUTWM33(0012)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT3312-F TO WK01-ID-LAST-NINE-UT33-F
                (12).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-UT3313-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-UT3313 TO ACH-ID-LAST-NINE-UT33
                OF FUTWM33(0013)
           END-IF.
           MOVE WK01-ID-LAST-NINE-UT3313-F TO WK01-ID-LAST-NINE-UT33-F
                (13).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT331-Z = 'Y'
               MOVE WK01-NAME-UT331 TO VNDR-NAME-UT33 OF FUTWM33(0001)
           END-IF.
           MOVE WK01-NAME-UT331-F TO WK01-NAME-UT33-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT332-Z = 'Y'
               MOVE WK01-NAME-UT332 TO VNDR-NAME-UT33 OF FUTWM33(0002)
           END-IF.
           MOVE WK01-NAME-UT332-F TO WK01-NAME-UT33-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT333-Z = 'Y'
               MOVE WK01-NAME-UT333 TO VNDR-NAME-UT33 OF FUTWM33(0003)
           END-IF.
           MOVE WK01-NAME-UT333-F TO WK01-NAME-UT33-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT334-Z = 'Y'
               MOVE WK01-NAME-UT334 TO VNDR-NAME-UT33 OF FUTWM33(0004)
           END-IF.
           MOVE WK01-NAME-UT334-F TO WK01-NAME-UT33-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT335-Z = 'Y'
               MOVE WK01-NAME-UT335 TO VNDR-NAME-UT33 OF FUTWM33(0005)
           END-IF.
           MOVE WK01-NAME-UT335-F TO WK01-NAME-UT33-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT336-Z = 'Y'
               MOVE WK01-NAME-UT336 TO VNDR-NAME-UT33 OF FUTWM33(0006)
           END-IF.
           MOVE WK01-NAME-UT336-F TO WK01-NAME-UT33-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT337-Z = 'Y'
               MOVE WK01-NAME-UT337 TO VNDR-NAME-UT33 OF FUTWM33(0007)
           END-IF.
           MOVE WK01-NAME-UT337-F TO WK01-NAME-UT33-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT338-Z = 'Y'
               MOVE WK01-NAME-UT338 TO VNDR-NAME-UT33 OF FUTWM33(0008)
           END-IF.
           MOVE WK01-NAME-UT338-F TO WK01-NAME-UT33-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT339-Z = 'Y'
               MOVE WK01-NAME-UT339 TO VNDR-NAME-UT33 OF FUTWM33(0009)
           END-IF.
           MOVE WK01-NAME-UT339-F TO WK01-NAME-UT33-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT3310-Z = 'Y'
               MOVE WK01-NAME-UT3310 TO VNDR-NAME-UT33 OF FUTWM33(0010)
           END-IF.
           MOVE WK01-NAME-UT3310-F TO WK01-NAME-UT33-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT3311-Z = 'Y'
               MOVE WK01-NAME-UT3311 TO VNDR-NAME-UT33 OF FUTWM33(0011)
           END-IF.
           MOVE WK01-NAME-UT3311-F TO WK01-NAME-UT33-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT3312-Z = 'Y'
               MOVE WK01-NAME-UT3312 TO VNDR-NAME-UT33 OF FUTWM33(0012)
           END-IF.
           MOVE WK01-NAME-UT3312-F TO WK01-NAME-UT33-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT3313-Z = 'Y'
               MOVE WK01-NAME-UT3313 TO VNDR-NAME-UT33 OF FUTWM33(0013)
           END-IF.
           MOVE WK01-NAME-UT3313-F TO WK01-NAME-UT33-F (13).
