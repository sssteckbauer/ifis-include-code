    ***Created by Convert/DC version V8R03 on 12/05/00 at 14:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4000-TA30 OF FTAWM30 TO WK01-CODE-4000-TA30.
      *%--------------------------------------------------------------%*
           MOVE FULL-DESC-4417-TA30 OF FTAWM30 TO WK01-DESC-4417-TA30.
      *%--------------------------------------------------------------%*
           MOVE SHORT-DESC-4417-TA30 OF FTAWM30 TO WK01-DESC-4417-TA3A.
      *%--------------------------------------------------------------%*
           MOVE RULE-CLASS-CODE-4180-TA30 OF FTAWM30 TO
                WK01-CLASS-CODE-4180-TA30.
      *%--------------------------------------------------------------%*
           MOVE RULE-CLASS-DESC-4181-TA30 OF FTAWM30 TO
                WK01-CLASS-DESC-4181-TA30.
      *%--------------------------------------------------------------%*
           MOVE BANK-ACCT-CODE-4033-TA30 OF FTAWM30 TO
                WK01-ACCT-CODE-4033-TA30.
      *%--------------------------------------------------------------%*
           MOVE BANK-CODE-DESC-4062-TA30 OF FTAWM30 TO
                WK01-CODE-DESC-4062-TA30.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-CODE-4417-TA30 OF FTAWM30 TO
                WK01-INDX-CODE-4417-TA30.
      *%--------------------------------------------------------------%*
           MOVE FUND-CODE-4417-TA30 OF FTAWM30 TO WK01-CODE-4417-TA30.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-4417-TA30 OF FTAWM30 TO WK01-CODE-4417-TA3A.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4417-TA30 OF FTAWM30 TO WK01-CODE-4417-TA3B.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-CODE-4417-TA30 OF FTAWM30 TO WK01-CODE-4417-TA3C.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-CODE-4417-TA30 OF FTAWM30 TO WK01-CODE-4417-TA3D.
      *%--------------------------------------------------------------%*
           MOVE LCTN-CODE-4417-TA30 OF FTAWM30 TO WK01-CODE-4417-TA3E.
      *%--------------------------------------------------------------%*
           MOVE IMMDT-CHECK-IND-4417-TA30 OF FTAWM30 TO
                WK01-CHECK-IND-4417-TA30.
      *%--------------------------------------------------------------%*
           MOVE MNL-CHECK-IND-4417-TA30 OF FTAWM30 TO
                WK01-CHECK-IND-4417-TA3A.
      *%--------------------------------------------------------------%*
           MOVE BATCH-CHECK-IND-4417-TA30 OF FTAWM30 TO
                WK01-CHECK-IND-4417-TA3B.
      *%--------------------------------------------------------------%*
           MOVE CASH-IND-4417-TA30 OF FTAWM30 TO WK01-IND-4417-TA30.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-TA30 OF FTAWM30 TO WK01-CODE-TA30.
      *%--------------------------------------------------------------%*
           MOVE ADVNC-TYPE-4417-TA30 OF FTAWK30 TO WK01-TYPE-4417-TA30.
