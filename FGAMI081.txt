    ***Created by Convert/DC version V8R03 on 11/09/00 at 15:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-GA08 OF FGAWK08 TO WK01-CODE-GA08.
      *%--------------------------------------------------------------%*
           MOVE FSCL-YR-GA08 OF FGAWK08 TO WK01-YR-GA08.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0001) TO
                WK01-DATA-CODE-2-GA081.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0002) TO
                WK01-DATA-CODE-2-GA082.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0003) TO
                WK01-DATA-CODE-2-GA083.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0004) TO
                WK01-DATA-CODE-2-GA084.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0005) TO
                WK01-DATA-CODE-2-GA085.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0006) TO
                WK01-DATA-CODE-2-GA086.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0007) TO
                WK01-DATA-CODE-2-GA087.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0008) TO
                WK01-DATA-CODE-2-GA088.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0009) TO
                WK01-DATA-CODE-2-GA089.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0010) TO
                WK01-DATA-CODE-2-GA0810.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0011) TO
                WK01-DATA-CODE-2-GA0811.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0012) TO
                WK01-DATA-CODE-2-GA0812.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0013) TO
                WK01-DATA-CODE-2-GA0813.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0014) TO
                WK01-DATA-CODE-2-GA0814.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-2-GA08 OF FGAWM08(0015) TO
                WK01-DATA-CODE-2-GA0815.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0001) TO
                WK01-DATA-CODE-3-GA081.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0002) TO
                WK01-DATA-CODE-3-GA082.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0003) TO
                WK01-DATA-CODE-3-GA083.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0004) TO
                WK01-DATA-CODE-3-GA084.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0005) TO
                WK01-DATA-CODE-3-GA085.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0006) TO
                WK01-DATA-CODE-3-GA086.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0007) TO
                WK01-DATA-CODE-3-GA087.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0008) TO
                WK01-DATA-CODE-3-GA088.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0009) TO
                WK01-DATA-CODE-3-GA089.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0010) TO
                WK01-DATA-CODE-3-GA0810.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0011) TO
                WK01-DATA-CODE-3-GA0811.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0012) TO
                WK01-DATA-CODE-3-GA0812.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0013) TO
                WK01-DATA-CODE-3-GA0813.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0014) TO
                WK01-DATA-CODE-3-GA0814.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-3-GA08 OF FGAWM08(0015) TO
                WK01-DATA-CODE-3-GA0815.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0001) TO
                WK01-DATA-CODE-4-GA081.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0002) TO
                WK01-DATA-CODE-4-GA082.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0003) TO
                WK01-DATA-CODE-4-GA083.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0004) TO
                WK01-DATA-CODE-4-GA084.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0005) TO
                WK01-DATA-CODE-4-GA085.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0006) TO
                WK01-DATA-CODE-4-GA086.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0007) TO
                WK01-DATA-CODE-4-GA087.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0008) TO
                WK01-DATA-CODE-4-GA088.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0009) TO
                WK01-DATA-CODE-4-GA089.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0010) TO
                WK01-DATA-CODE-4-GA0810.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0011) TO
                WK01-DATA-CODE-4-GA0811.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0012) TO
                WK01-DATA-CODE-4-GA0812.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0013) TO
                WK01-DATA-CODE-4-GA0813.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0014) TO
                WK01-DATA-CODE-4-GA0814.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-4-GA08 OF FGAWM08(0015) TO
                WK01-DATA-CODE-4-GA0815.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0001) TO
                WK01-ADJMT-AMT-GA081.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0002) TO
                WK01-ADJMT-AMT-GA082.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0003) TO
                WK01-ADJMT-AMT-GA083.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0004) TO
                WK01-ADJMT-AMT-GA084.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0005) TO
                WK01-ADJMT-AMT-GA085.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0006) TO
                WK01-ADJMT-AMT-GA086.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0007) TO
                WK01-ADJMT-AMT-GA087.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0008) TO
                WK01-ADJMT-AMT-GA088.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0009) TO
                WK01-ADJMT-AMT-GA089.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0010) TO
                WK01-ADJMT-AMT-GA0810.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0011) TO
                WK01-ADJMT-AMT-GA0811.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0012) TO
                WK01-ADJMT-AMT-GA0812.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0013) TO
                WK01-ADJMT-AMT-GA0813.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0014) TO
                WK01-ADJMT-AMT-GA0814.
      *%--------------------------------------------------------------%*
           MOVE BDGT-ADJMT-AMT-GA08 OF FGAWM08(0015) TO
                WK01-ADJMT-AMT-GA0815.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0001) TO
                WK01-ACTVY-AMT-GA081.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0002) TO
                WK01-ACTVY-AMT-GA082.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0003) TO
                WK01-ACTVY-AMT-GA083.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0004) TO
                WK01-ACTVY-AMT-GA084.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0005) TO
                WK01-ACTVY-AMT-GA085.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0006) TO
                WK01-ACTVY-AMT-GA086.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0007) TO
                WK01-ACTVY-AMT-GA087.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0008) TO
                WK01-ACTVY-AMT-GA088.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0009) TO
                WK01-ACTVY-AMT-GA089.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0010) TO
                WK01-ACTVY-AMT-GA0810.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0011) TO
                WK01-ACTVY-AMT-GA0811.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0012) TO
                WK01-ACTVY-AMT-GA0812.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0013) TO
                WK01-ACTVY-AMT-GA0813.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0014) TO
                WK01-ACTVY-AMT-GA0814.
      *%--------------------------------------------------------------%*
           MOVE YTD-ACTVY-AMT-GA08 OF FGAWM08(0015) TO
                WK01-ACTVY-AMT-GA0815.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0001) TO
                WK01-RSRVTN-AMT-GA081.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0002) TO
                WK01-RSRVTN-AMT-GA082.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0003) TO
                WK01-RSRVTN-AMT-GA083.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0004) TO
                WK01-RSRVTN-AMT-GA084.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0005) TO
                WK01-RSRVTN-AMT-GA085.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0006) TO
                WK01-RSRVTN-AMT-GA086.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0007) TO
                WK01-RSRVTN-AMT-GA087.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0008) TO
                WK01-RSRVTN-AMT-GA088.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0009) TO
                WK01-RSRVTN-AMT-GA089.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0010) TO
                WK01-RSRVTN-AMT-GA0810.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0011) TO
                WK01-RSRVTN-AMT-GA0811.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0012) TO
                WK01-RSRVTN-AMT-GA0812.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0013) TO
                WK01-RSRVTN-AMT-GA0813.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0014) TO
                WK01-RSRVTN-AMT-GA0814.
      *%--------------------------------------------------------------%*
           MOVE BDGT-RSRVTN-AMT-GA08 OF FGAWM08(0015) TO
                WK01-RSRVTN-AMT-GA0815.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0001) TO
                WK01-BAL-AMT-GA081.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0002) TO
                WK01-BAL-AMT-GA082.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0003) TO
                WK01-BAL-AMT-GA083.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0004) TO
                WK01-BAL-AMT-GA084.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0005) TO
                WK01-BAL-AMT-GA085.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0006) TO
                WK01-BAL-AMT-GA086.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0007) TO
                WK01-BAL-AMT-GA087.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0008) TO
                WK01-BAL-AMT-GA088.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0009) TO
                WK01-BAL-AMT-GA089.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0010) TO
                WK01-BAL-AMT-GA0810.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0011) TO
                WK01-BAL-AMT-GA0811.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0012) TO
                WK01-BAL-AMT-GA0812.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0013) TO
                WK01-BAL-AMT-GA0813.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0014) TO
                WK01-BAL-AMT-GA0814.
      *%--------------------------------------------------------------%*
           MOVE AVLBL-BAL-AMT-GA08 OF FGAWM08(0015) TO
                WK01-BAL-AMT-GA0815.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-TITLE-GA08 OF FGAWK08(0002) TO WK01-TITLE-GA082.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-TITLE-GA08 OF FGAWK08(0003) TO WK01-TITLE-GA083.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-TITLE-GA08 OF FGAWK08(0004) TO WK01-TITLE-GA084.
      *%--------------------------------------------------------------%*
           MOVE ACTG-PRD-GA08 OF FGAWK08 TO WK01-PRD-GA08.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-CODE-GA08 OF FGAWK08 TO WK01-INDX-CODE-GA08.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-TITLE-GA08 OF FGAWK08(0001) TO WK01-TITLE-GA081.
      *%--------------------------------------------------------------%*
           MOVE DSPLY-DATA-CODE-1-GA08 OF FGAWK08 TO
                WK01-DATA-CODE-1-GA08.
      *%--------------------------------------------------------------%*
           MOVE PRD-START-DATE-4024-GA08 OF FGAWK08 TO
                WK01-START-DATE-4024-GA08.
      *%--------------------------------------------------------------%*
           MOVE PRD-END-DATE-4024-GA08 OF FGAWK08 TO
                WK01-END-DATE-4024-GA08.
