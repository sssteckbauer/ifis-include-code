    ***Created by Convert/DC version V8R03 on 12/05/00 at 10:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE TARGET-ID-UT23 OF FUTWM23 TO WK01-ID-UT23.
      *%--------------------------------------------------------------%*
           MOVE TARGET-FULL-NAME-UT23 OF FUTWM23 TO WK01-FULL-NAME-UT23.
      *%--------------------------------------------------------------%*
           MOVE ADD-ID-FULL-NAME-UT23 OF FUTWM23 TO
                WK01-ID-FULL-NAME-UT23.
      *%--------------------------------------------------------------%*
           MOVE ADD-ID-UT23 OF FUTWM23 TO WK01-ID-UT2A.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-TYPE-UT23 OF FUTWM23 TO WK01-DCMNT-TYPE-UT23.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-UT23 OF FUTWM23 TO WK01-DCMNT-DESC-UT23.
      *%--------------------------------------------------------------%*
           MOVE APRVL-TMPLT-CODE-UT23 OF FUTWM23
                                           TO WK01-TMPLT-CODE-UT23.
      *%--------------------------------------------------------------%*
           MOVE APRVL-TMPLT-DESC-UT23 OF FUTWM23
                                           TO WK01-TMPLT-DESC-UT23.
      *%--------------------------------------------------------------%*
           MOVE CMPLT-IND-UT23 OF FUTWM23 TO WK01-IND-UT23.
