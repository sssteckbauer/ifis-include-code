    ***Created by Convert/DC version V8R03 on 12/14/00 at 13:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FZYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FZYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FZYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FZYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FZYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FZYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FZYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE FICE-CODE-6001-PE44 OF FPEWM44 TO WK01-CODE-6001-PE44.
      *%--------------------------------------------------------------%*
           MOVE HOLD-TYPE-CODE-2132-PE44 OF FPEWM44 TO
                WK01-TYPE-CODE-2132-PE44.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-2132-PE44 OF FPEWM44 TO WK01-DESC-2132-PE44.
      *%--------------------------------------------------------------%*
           MOVE HOLD-CODE-2134-PE44 OF FPEWM44 TO WK01-CODE-2134-PE44.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-2134-PE44 OF FPEWM44 TO WK01-DESC-2134-PE44.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FZYWG00M TO WK01-NAME-6001-SY0A.
      *%--------------------------------------------------------------%*
           MOVE ENTY-TYPE-CODE-6182-PE44 OF FPEWM44 TO
                WK01-TYPE-CODE-6182-PE44.
      *%--------------------------------------------------------------%*
           MOVE ENTY-CODE-6349-PE44 OF FPEWM44 TO WK01-CODE-6349-PE44.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE44 OF FPEWM44 TO WK01-CODE-6153-PE44.
      *%--------------------------------------------------------------%*
           MOVE STATE-CODE-6151-PE44 OF FPEWM44 TO WK01-CODE-6151-PE44.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TYPE-CODE-6140-PE44 OF FPEWM44 TO
                WK01-TYPE-CODE-6140-PE44.
      *%--------------------------------------------------------------%*
           MOVE CMNT-CODE-6345-PE44 OF FPEWM44 TO WK01-CODE-6345-PE44.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-6182-PE44 OF FPEWM44 TO WK01-DESC-6182-PE44.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-6349-PE44 OF FPEWM44 TO WK01-DESC-6349-PE44.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE44 OF FPEWM44 TO WK01-NAME-6153-PE44.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6151-PE44 OF FPEWM44 TO WK01-NAME-6151-PE44.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-6345-PE44 OF FPEWM44 TO WK01-DESC-6345-PE44.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-6140-PE44 OF FPEWM44 TO WK01-DESC-6140-PE44.
      *%--------------------------------------------------------------%*
           MOVE WORK-PRSN-ID-ONE-PE44 OF FPEWM44 TO
                WK01-PRSN-ID-ONE-PE44.
      *%--------------------------------------------------------------%*
           MOVE WORK-PRSN-ID-TWO-PE44 OF FPEWM44 TO
                WK01-PRSN-ID-TWO-PE44.
      *%--------------------------------------------------------------%*
           MOVE WORK-PRSN-ID-THREE-PE44 OF FPEWM44 TO
                WK01-PRSN-ID-THREE-PE44.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6117-PE44 OF FPEWM44 TO WK01-KEY-6117-PE44.
      *%--------------------------------------------------------------%*
           MOVE ADR-TYPE-CODE-6137-PE44 OF FPEWM44 TO
                WK01-TYPE-CODE-6137-PE44.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-6137-PE44 OF FPEWM44 TO WK01-DESC-6137-PE44.
      *%--------------------------------------------------------------%*
           MOVE MAP-RQST-DATE-PE44 OF FPEWM44 TO WK01-RQST-DATE-PE44.
      *%--------------------------------------------------------------%*
           MOVE ENTY-ID-LAST-NINE-6186-PE44 OF FPEWM44 TO
                WK01-ID-LAST-NINE-6186-PE44.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6311-PE44 OF FPEWM44 TO WK01-KEY-6311-PE44.
      *%--------------------------------------------------------------%*
           MOVE MAP-TERM-CODE-PE44 OF FPEWM44 TO WK01-TERM-CODE-PE44.
      *%--------------------------------------------------------------%*
           MOVE OFC-CODE-2155-PE44 OF FPEWM44 TO WK01-CODE-2155-PE44.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-2155-PE44 OF FPEWM44 TO WK01-NAME-2155-PE44.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FZYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FZYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FZYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE ARCHVD-TEXT-SY01 OF FZYWG01M TO WK01-TEXT-SY0C.
