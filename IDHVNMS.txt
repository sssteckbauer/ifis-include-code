       976-FROM-NAMESYN-083 SECTION.

               MOVE SYN-NAME-083 OF NAMESYN-083
               TO NMS-SYN-NAME.

               MOVE RDF-NAM-083 OF NAMESYN-083
               TO NMS-RDF-NAM.

               MOVE DEPEND-ON-083 OF NAMESYN-083
               TO NMS-DEPEND-ON.

               MOVE NAMESYN-FLAG-083 OF NAMESYN-083
               TO NMS-NAMESYN-FLAG.

               MOVE DR-LPOS-083 OF NAMESYN-083
               TO NMS-DR-LPOS.

               MOVE BUILDER-083 OF NAMESYN-083
               TO NMS-BUILDER.
       976-FROM-NAMESYN-083-EXIT.
           EXIT.
