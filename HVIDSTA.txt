       975-TO-R6151-STATE SECTION.
           MOVE STA-USER-CD TO
               USER-CODE-6151 OF R6151-STATE
                                                                     .
           MOVE STA-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-6151 OF R6151-STATE
                                                                     .
           MOVE STA-FK-UNV-UNVRS-CD TO
               UNVRS-CODE-6151 OF R6151-STATE
                                                                     .
           MOVE STA-CNTRY-CD TO
               CNTRY-CODE-6151 OF R6151-STATE
                                                                     .
           MOVE STA-STATE-CD TO
               STATE-CODE-6151 OF R6151-STATE
                                                                     .
           MOVE STA-FULL-NAME TO
               FULL-NAME-6151 OF R6151-STATE
                                                                     .
           MOVE STA-RCRD-USED-FLAG TO
               RCRD-USED-FLAG-6151 OF R6151-STATE
                                                                     .
       975-TO-R6151-STATE-EXIT.
           EXIT.
