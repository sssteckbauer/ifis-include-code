    ***Created by Convert/DC version V8R03 on 11/09/00 at 15:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
FCSAH *    MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
FCSAH      MOVE SPACES TO WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE RQST-DCMNT-NMBR-GA31 OF FGAWK31 TO WK01-DCMNT-NMBR-GA31.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-GA31 OF FGAWM31(0001) TO WK01-CODE-GA311.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-GA31 OF FGAWM31(0002) TO WK01-CODE-GA312.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-GA31 OF FGAWM31(0003) TO WK01-CODE-GA313.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-GA31 OF FGAWM31(0004) TO WK01-CODE-GA314.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-GA31 OF FGAWM31(0005) TO WK01-CODE-GA315.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-GA31 OF FGAWM31(0006) TO WK01-CODE-GA316.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-GA31 OF FGAWM31(0001) TO WK01-NMBR-GA311.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-GA31 OF FGAWM31(0002) TO WK01-NMBR-GA312.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-GA31 OF FGAWM31(0003) TO WK01-NMBR-GA313.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-GA31 OF FGAWM31(0004) TO WK01-NMBR-GA314.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-GA31 OF FGAWM31(0005) TO WK01-NMBR-GA315.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-GA31 OF FGAWM31(0006) TO WK01-NMBR-GA316.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-GA31 OF FGAWM31(0001) TO WK01-DESC-GA311.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-GA31 OF FGAWM31(0002) TO WK01-DESC-GA312.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-GA31 OF FGAWM31(0003) TO WK01-DESC-GA313.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-GA31 OF FGAWM31(0004) TO WK01-DESC-GA314.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-GA31 OF FGAWM31(0005) TO WK01-DESC-GA315.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-GA31 OF FGAWM31(0006) TO WK01-DESC-GA316.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-AMT-GA31 OF FGAWM31(0001) TO WK01-AMT-GA311.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-AMT-GA31 OF FGAWM31(0002) TO WK01-AMT-GA312.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-AMT-GA31 OF FGAWM31(0003) TO WK01-AMT-GA313.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-AMT-GA31 OF FGAWM31(0004) TO WK01-AMT-GA314.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-AMT-GA31 OF FGAWM31(0005) TO WK01-AMT-GA315.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-AMT-GA31 OF FGAWM31(0006) TO WK01-AMT-GA316.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-GA31 OF FGAWM31(0001) TO WK01-DATE-GA311.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-GA31 OF FGAWM31(0002) TO WK01-DATE-GA312.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-GA31 OF FGAWM31(0003) TO WK01-DATE-GA313.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-GA31 OF FGAWM31(0004) TO WK01-DATE-GA314.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-GA31 OF FGAWM31(0005) TO WK01-DATE-GA315.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-GA31 OF FGAWM31(0006) TO WK01-DATE-GA316.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-IND-GA31 OF FGAWM31(0001) TO
                WK01-STATUS-IND-GA311.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-IND-GA31 OF FGAWM31(0002) TO
                WK01-STATUS-IND-GA312.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-IND-GA31 OF FGAWM31(0003) TO
                WK01-STATUS-IND-GA313.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-IND-GA31 OF FGAWM31(0004) TO
                WK01-STATUS-IND-GA314.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-IND-GA31 OF FGAWM31(0005) TO
                WK01-STATUS-IND-GA315.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-IND-GA31 OF FGAWM31(0006) TO
                WK01-STATUS-IND-GA316.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-GA31 OF FGAWM31(0001) TO WK01-STATUS-GA311.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-GA31 OF FGAWM31(0002) TO WK01-STATUS-GA312.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-GA31 OF FGAWM31(0003) TO WK01-STATUS-GA313.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-GA31 OF FGAWM31(0004) TO WK01-STATUS-GA314.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-GA31 OF FGAWM31(0005) TO WK01-STATUS-GA315.
      *%--------------------------------------------------------------%*
           MOVE CURR-STATUS-GA31 OF FGAWM31(0006) TO WK01-STATUS-GA316.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-IND-GA31 OF FGAWM31(0001) TO
                WK01-STATUS-IND-GA3A1.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-IND-GA31 OF FGAWM31(0002) TO
                WK01-STATUS-IND-GA3A2.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-IND-GA31 OF FGAWM31(0003) TO
                WK01-STATUS-IND-GA3A3.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-IND-GA31 OF FGAWM31(0004) TO
                WK01-STATUS-IND-GA3A4.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-IND-GA31 OF FGAWM31(0005) TO
                WK01-STATUS-IND-GA3A5.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-IND-GA31 OF FGAWM31(0006) TO
                WK01-STATUS-IND-GA3A6.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-GA31 OF FGAWM31(0001) TO WK01-STATUS-GA3A1.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-GA31 OF FGAWM31(0002) TO WK01-STATUS-GA3A2.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-GA31 OF FGAWM31(0003) TO WK01-STATUS-GA3A3.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-GA31 OF FGAWM31(0004) TO WK01-STATUS-GA3A4.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-GA31 OF FGAWM31(0005) TO WK01-STATUS-GA3A5.
      *%--------------------------------------------------------------%*
           MOVE PERM-STATUS-GA31 OF FGAWM31(0006) TO WK01-STATUS-GA3A6.
