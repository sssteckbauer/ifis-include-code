    ***Created by Convert/DC version V8R03 on 12/12/00 at 13:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6000-CH35 OF FCHWM35(0002) TO
                WK01-NAME-6000-CH352.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH35 OF FCHWM35 TO WK01-CODE-CH35.
      *%--------------------------------------------------------------%*
           MOVE FICE-CODE-6000-CH35 OF FCHWM35 TO WK01-CODE-6000-CH35.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6000-CH35 OF FCHWM35(0001) TO
                WK01-NAME-6000-CH351.
      *%--------------------------------------------------------------%*
           MOVE CITY-NAME-6000-CH35 OF FCHWM35 TO WK01-NAME-6000-CH3A.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-CH35 OF FCHWM35 TO WK01-AREA-CODE-CH35.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6000-CH35 OF FCHWM35(0001) TO
                WK01-ADR-6000-CH351.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6000-CH35 OF FCHWM35(0002) TO
                WK01-ADR-6000-CH352.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6000-CH35 OF FCHWM35(0003) TO
                WK01-ADR-6000-CH353.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6000-CH35 OF FCHWM35(0004) TO
                WK01-ADR-6000-CH354.
      *%--------------------------------------------------------------%*
           MOVE STATE-CODE-6000-CH35 OF FCHWM35 TO WK01-CODE-6000-CH3A.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-CH35 OF FCHWM35 TO WK01-XCHNG-ID-CH35.
      *%--------------------------------------------------------------%*
           MOVE ZIP-CODE-6000-CH35 OF FCHWM35 TO WK01-CODE-6000-CH3B.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-CH35 OF FCHWM35 TO WK01-SEQ-ID-CH35.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-CH35 OF FCHWM35 TO WK01-XTNSN-ID-CH35.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6000-CH35 OF FCHWM35 TO WK01-CODE-6000-CH3C.
