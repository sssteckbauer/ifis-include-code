      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R6607' TO DBLINK-CURR-PARAGRAPH.              276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R6607' TO RECORD-NAME.                                 276 BRTN
YYY991     MOVE 'F-BATCHX' TO AREA-NAME.                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6607 TO DBLINK-RECORD-MADE-CURRENT.             276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R6607-EXIT                                276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE CBD-FK-CBH-BTCH-USR-CD TO DBLINK-R6607-01           276 BRTN
YYY991         MOVE CBD-FK-COMM-REF-NBR TO DBLINK-R6607-02              276 BRTN
YYY991         MOVE CBD-FK-TEXT-CD TO DBLINK-R6607-03                   276 BRTN
YYY991         MOVE CBD-CBH-SET-TS TO DBLINK-R6607-04                   276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6607 TO DBLINK-VIEW-NAME-CURRENT.               276 BRTN
YYY991     MOVE DBLINK-R6607-KEY TO DBLINK-VIEW-200-CURRENT.            276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6605-6607                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE CBD-FK-CBH-BTCH-USR-CD TO DBLINK-S-6605-6607-01.        276 BRTN
YYY991     MOVE CBD-FK-COMM-REF-NBR TO DBLINK-S-6605-6607-02.           276 BRTN
YYY991     MOVE CBD-FK-TEXT-CD TO DBLINK-S-6605-6607-03.                276 BRTN
YYY991     MOVE CBD-CBH-SET-TS TO DBLINK-S-6605-6607-04.                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6606-6607                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE CBD-FK-CBS-BTCH-USR-CD TO DBLINK-S-6606-6607-01.        276 BRTN
YYY991     MOVE CBD-FK-COMM-REF-NBR TO DBLINK-S-6606-6607-02.           276 BRTN
YYY991     MOVE CBD-FK-TEXT-CD TO DBLINK-S-6606-6607-03.                276 BRTN
YYY991     MOVE CBD-FK-CBS-DTL-SRTDATA TO DBLINK-S-6606-6607-04.        276 BRTN
YYY991     MOVE CBD-FK-CBS-SEL-ONE TO DBLINK-S-6606-6607-05.            276 BRTN
YYY991     MOVE CBD-FK-CBS-SEL-NINE TO DBLINK-S-6606-6607-06.           276 BRTN
YYY991     MOVE CBD-RCRD-TYP TO DBLINK-S-6606-6607-07.                  276 BRTN
YYY991     MOVE CBD-CBS-SET-TS TO DBLINK-S-6606-6607-08.                276 BRTN
