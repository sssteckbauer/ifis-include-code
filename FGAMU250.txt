    ***Created by Convert/DC version V8R03 on 10/09/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4069-GA25-Z = 'Y'
               MOVE WK01-NMBR-4069-GA25 TO DCMNT-NMBR-4069-GA25 OF
                FGAWK25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4069-GA25-Z = 'Y'
               MOVE WK01-DESC-4069-GA25 TO DCMNT-DESC-4069-GA25 OF
                FGAWK25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-DR-GA25-Z = 'Y'
               MOVE WK01-AMT-DR-GA25 TO SMRY-AMT-DR-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-CR-GA25-Z = 'Y'
               MOVE WK01-AMT-CR-GA25 TO SMRY-AMT-CR-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA25-Z = 'Y'
               MOVE WK01-CODE-GA25 TO ACTN-CODE-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4069-GA25-Z = 'Y'
               MOVE WK01-IND-4069-GA25 TO CMPLT-IND-4069-GA25 OF FGAWK25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4068-GA25-Z = 'Y'
               MOVE WK01-TYPE-4068-GA25 TO JRNL-TYPE-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4181-GA25-Z = 'Y'
               MOVE WK01-CLASS-DESC-4181-GA25 TO
                RULE-CLASS-DESC-4181-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4068-GA25-Z = 'Y'
               MOVE WK01-NMBR-4068-GA25 TO ENCMBRNC-NMBR-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4068-GA25-Z = 'Y'
               MOVE WK01-CODE-4068-GA25 TO COA-CODE-4068-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-4068-GA25-Z = 'Y'
               MOVE WK01-INDX-CODE-4068-GA25 TO
                ACCT-INDX-CODE-4068-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4068-GA2A-Z = 'Y'
               MOVE WK01-CODE-4068-GA2A TO FUND-CODE-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4068-GA2B-Z = 'Y'
               MOVE WK01-CODE-4068-GA2B TO ORGZN-CODE-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4068-GA2C-Z = 'Y'
               MOVE WK01-CODE-4068-GA2C TO ACCT-CODE-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4068-GA2D-Z = 'Y'
               MOVE WK01-CODE-4068-GA2D TO PRGRM-CODE-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4068-GA2E-Z = 'Y'
               MOVE WK01-CODE-4068-GA2E TO ACTVY-CODE-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4068-GA2F-Z = 'Y'
               MOVE WK01-CODE-4068-GA2F TO LCTN-CODE-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-4068-GA25-Z = 'Y'
               MOVE WK01-CRDT-IND-4068-GA25 TO DEBIT-CRDT-IND-4068-GA25
                OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4068-GA25-Z = 'Y'
               MOVE WK01-DESC-4068-GA25 TO TRANS-DESC-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-NET-GA25-Z = 'Y'
               MOVE WK01-AMT-NET-GA25 TO SMRY-AMT-NET-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4068-GA25-Z = 'Y'
               MOVE WK01-AMT-4068-GA25 TO TRANS-AMT-4068-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ITEM-NMBR-4068-GA25-Z = 'Y'
               MOVE WK01-ITEM-NMBR-4068-GA25 TO
                ENCMBRNC-ITEM-NMBR-4068-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-4068-GA25-Z = 'Y'
               MOVE WK01-SEQ-NMBR-4068-GA25 TO
                ENCMBRNC-SEQ-NMBR-4068-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACTN-IND-4068-GA25-Z = 'Y'
               MOVE WK01-ACTN-IND-4068-GA25 TO
                ENCMBRNC-ACTN-IND-4068-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-4068-GA25-Z = 'Y'
               MOVE WK01-REF-NMBR-4068-GA25 TO DCMNT-REF-NMBR-4068-GA25
                OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4191-GA25-Z = 'Y'
               MOVE WK01-TYPE-4191-GA25 TO DCMNT-TYPE-4191-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-USED-GA25-Z = 'Y'
               MOVE WK01-SEQ-NMBR-USED-GA25 TO LAST-SEQ-NMBR-USED-GA25
                OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-GA25-Z = 'Y'
               MOVE WK01-ACTVY-DATE-GA25 TO UPDT-ACTVY-DATE-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TIME-STAMP-GA25-Z = 'Y'
               MOVE WK01-TIME-STAMP-GA25 TO UPDT-TIME-STAMP-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4068-GA2A-Z = 'Y'
               MOVE WK01-NMBR-4068-GA2A TO SEQ-NMBR-4068-GA25 OF FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4068-GA25-Z = 'Y'
               MOVE WK01-DATE-4068-GA25 TO ACTVY-DATE-4068-GA25 OF
                FGAWM25
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TRANS-GA25-Z = 'Y'
               MOVE WK01-TRANS-GA25 TO ENPET-TRANS-GA25 OF FGAWM25
           END-IF.
