    ***Created by Convert/DC version V8R03 on 12/11/00 at 10:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FZYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FZYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FZYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FZYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FZYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FZYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FZYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-PRSN-ID-ONE-PE32 OF FPEWK32 TO
                WK01-PRSN-ID-ONE-PE32.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6117-PE32 OF FPEWM32 TO WK01-KEY-6117-PE32.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-6137-PE32 OF FPEWM32 TO WK01-DESC-6137-PE32.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE32 OF FPEWM32 TO WK01-CODE-PE32.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-PE32 OF FPEWM32(0001) TO
                WK01-ADR-6139-PE321.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-PE32 OF FPEWM32(0002) TO
                WK01-ADR-6139-PE322.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-PE32 OF FPEWM32(0003) TO
                WK01-ADR-6139-PE323.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-PE32 OF FPEWM32(0004) TO
                WK01-ADR-6139-PE324.
      *%--------------------------------------------------------------%*
           MOVE CITY-NAME-6139-PE32 OF FPEWM32 TO WK01-NAME-6139-PE32.
      *%--------------------------------------------------------------%*
           MOVE STATE-CODE-6151-PE32 OF FPEWM32 TO WK01-CODE-6151-PE32.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6151-PE32 OF FPEWM32 TO WK01-CODE-6151-PE3A.
DEVMJM*
      *%--------------------------------------------------------------%*
           MOVE EMADR-LINE-6139-PE32 OF FPEWM32
             TO WK01-EMADR-LINE-6139-PE32.
DEVMJM*
      *%--------------------------------------------------------------%*
           MOVE START-DATE-6139-PE32 OF FPEWM32 TO WK01-DATE-6139-PE32.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-6139-PE32 OF FPEWM32 TO WK01-DATE-6139-PE3A.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-6139-PE32 OF FPEWM32 TO
                WK01-AREA-CODE-6139-PE32.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-6139-PE32 OF FPEWM32 TO
                WK01-XCHNG-ID-6139-PE32.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-6139-PE32 OF FPEWM32 TO
                WK01-SEQ-ID-6139-PE32.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-6139-PE32 OF FPEWM32 TO
                WK01-XTNSN-ID-6139-PE32.
      *%--------------------------------------------------------------%*
           MOVE ADR-TYPE-CODE-6137-PE32 OF FPEWK32 TO
                WK01-TYPE-CODE-6137-PE32.
      *%--------------------------------------------------------------%*
           MOVE MAP-END-DATE-PE32 OF FPEWK32 TO WK01-END-DATE-PE32.
      *%--------------------------------------------------------------%*
           MOVE ZIP-CODE-6152-PE32 OF FPEWM32 TO WK01-CODE-6152-PE32.
      *%--------------------------------------------------------------%*
           MOVE CNTY-CODE-2346-PE32 OF FPEWM32 TO WK01-CODE-2346-PE32.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-2346-PE32 OF FPEWM32 TO WK01-NAME-2346-PE32.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FZYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FZYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FZYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6151-PE32 OF FPEWM32 TO WK01-NAME-6151-PE32.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE32 OF FPEWM32 TO WK01-NAME-6153-PE32.
      *%--------------------------------------------------------------%*
           MOVE ARCHVD-TEXT-SY01 OF FZYWG01M TO WK01-TEXT-SY0C.
