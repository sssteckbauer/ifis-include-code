       975-TO-R4214-APRVL-AUD SECTION.
           MOVE APD-USER-CD TO
               USER-CODE-4214 OF R4214-APRVL-AUD
                                                                     .
           MOVE APD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4214 OF R4214-APRVL-AUD
                                                                     .
           MOVE APD-LEVEL-SEQ-NBR TO
               LEVEL-SEQ-NMBR-4214 OF R4214-APRVL-AUD
                                                                     .
           MOVE APD-APRVL-CD TO
               APRVL-CODE-4214 OF R4214-APRVL-AUD
                                                                     .
           MOVE APD-ALT-APRVL-CD TO
               ALTRNT-APRVL-CODE-4214 OF R4214-APRVL-AUD
                                                                     .
           MOVE APD-APRVL-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               APRVL-DATE-4214 OF R4214-APRVL-AUD
                                                                     .
           MOVE APD-APRVL-TIME TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               APRVL-TIME-4214 OF R4214-APRVL-AUD
                                                                     .
           MOVE APD-SRC-IND TO
               SRC-IND-4214 OF R4214-APRVL-AUD
                                                                     .
           MOVE APD-PAN-IND TO
               PAN-IND-4214 OF R4214-APRVL-AUD
                                                                     .
       975-TO-R4214-APRVL-AUD-EXIT.
           EXIT.
