      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-ZZYII54A ROUTINE                        *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-ZZYII54A.

      ******************************************************************
      ***                          PAGE BACKWARD
      ******************************************************************
      ***  IF THERE IS DATA FOR A PRIOR SCREEN, RE-EXECUTE PREMAP,
      ***  OTHERWISE, SET PAGE STATUS TO 'FIRST PAGE' AND FALL THROUGH
      ***  INTO THE BODY OF THE ENTER RESPONSE.
      ******************************************************************
           IF AGR-NEXT-FUNCTION = 'BACKWARD'
FXXCHG         IF SAVE-DBKEY-ID-4404-MNS-ONE-T20  = ZEROS AND
FXXCHG            SAVE-DBKEY-ID-4405-MNS-ONE-T20  = ZEROS AND
FXXCHG            SAVE-DBKEY-ID-4411-MNS-ONE-T20  = ZEROS
                   MOVE 'F' TO PAGE-STATUS-CODE-SY00
               ELSE
                   MOVE 'B' TO PAGE-FLAG-SY00
                   MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
                   MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY-CONTINUE
                   MOVE AC99W-NO TO AC99W-FLAG-FIRST-TIME
                   GO TO 200-ZZYII54A-EXIT
               END-IF
           END-IF.
      *
      ******************************************************************
      ***                           PAGE FORWARD
      ******************************************************************
      ***  IF THERE IS DATA FOR A NEXT SCREEN, RE-EXECUTE PREMAP OF SAME
      ***  DIALOG.  OTHERWISE, SET PAGE STATUS TO 'LAST PAGE'. CREATE A
      ***  NEW PAGE IF CURRENT ONE IS FULL.
      ******************************************************************
           IF AGR-NEXT-FUNCTION = 'FORWARD'
FXXCHG         IF SAVE-DBKEY-ID-4404-PLUS-ON-T20   = ZEROS AND
FXXCHG            SAVE-DBKEY-ID-4405-PLUS-ON-T20   = ZEROS AND
FXXCHG            SAVE-DBKEY-ID-4411-PLUS-ON-T20   = ZEROS
                   MOVE 'L' TO PAGE-STATUS-CODE-SY00
                   PERFORM 200-NEWPAGE-S08
                      THRU 200-NEWPAGE-S08-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-ZZYII54A-EXIT
                   END-IF
               ELSE
                   MOVE 'F' TO PAGE-FLAG-SY00
                   MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
                   MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY-CONTINUE
                   MOVE AC99W-NO TO AC99W-FLAG-FIRST-TIME
               END-IF
           END-IF.

       200-ZZYII54A-EXIT.
           EXIT.
