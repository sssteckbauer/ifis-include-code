       976-FROM-R4089-BID-DTL SECTION.

FCYI **        MOVE USER-CODE-4089 OF R4089-BID-DTL
FCYI           MOVE DBLINK-USER-CD
               TO BDD-USER-CD.

               MOVE LAST-ACTVY-DATE-4089 OF R4089-BID-DTL
               TO BDD-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4089 OF R4089-BID-DTL
               TO BDD-UNVRS-CD.

               MOVE CMDTY-CODE-4089 OF R4089-BID-DTL
               TO BDD-CMDTY-CD.

               MOVE CMDTY-DESC-4089 OF R4089-BID-DTL
               TO BDD-CMDTY-DESC.

           IF ITEM-NMBR-4089 OF R4089-BID-DTL  NOT NUMERIC
               MOVE ZEROS TO BDD-ITEM-NBR
           ELSE
               MOVE ITEM-NMBR-4089 OF R4089-BID-DTL
               TO BDD-ITEM-NBR
           END-IF.

               MOVE UNIT-MEA-CODE-4089 OF R4089-BID-DTL
               TO BDD-UNIT-MEA-CD.

           IF QTY-4089 OF R4089-BID-DTL  NOT NUMERIC
               MOVE ZEROS TO BDD-QTY
           ELSE
               MOVE QTY-4089 OF R4089-BID-DTL
               TO BDD-QTY
           END-IF.

               MOVE AWARD-IND-4089 OF R4089-BID-DTL
               TO BDD-AWARD-IND.

               MOVE BID-NMBR-4089 OF R4089-BID-DTL
               TO BDD-BID-NBR.
       976-FROM-R4089-BID-DTL-EXIT.
           EXIT.
