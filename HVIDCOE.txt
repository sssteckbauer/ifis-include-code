       975-TO-R4076-COA-EFCTV SECTION.
           MOVE COE-USER-CD TO
               USER-CODE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               TIME-STAMP-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-STATUS TO
               STATUS-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-COA-TITLE TO
               COA-TITLE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-FDRL-EMPLR-ID TO
               FDRL-EMPLR-ID-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ACTG-MTHD TO
               ACTG-MTHD-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-FSCL-YR-START-PRD TO
               FSCL-YR-START-PRD-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-FSCL-YR-ENDPRD TO
               FSCL-YR-END-PRD-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ACCT-IX-BDGT-CNTL TO
               ACCT-INDX-BDGT-CNTRL-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-FND-BDGT-CNTL TO
               FUND-BDGT-CNTRL-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ORGN-BDGT-CNTL TO
               ORGZN-BDGT-CNTRL-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ACCT-BDGT-CNTL TO
               ACCT-BDGT-CNTRL-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-PRGRM-BDGT-CNTL TO
               PRGRM-BDGT-CNTRL-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-CNTL-PRD-CD TO
               CNTRL-PRD-CODE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-CNTL-SVRTY-CD TO
               CNTRL-SVRTY-CODE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ENCMB-JRNL-TYP TO
               ENCMBRNC-JRNL-TYPE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-CMTMNT-TYP TO
               CMTMNT-TYPE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ROLL-BDGT-IND TO
               ROLL-BDGT-IND-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-BDGT-DSPSN TO
               BDGT-DSPSN-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ENCMB-PCT TO
               ENCMBRNC-PCT-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-BDGT-JRNL-TYP TO
               BDGT-JRNL-TYPE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-BDGT-CLSFN TO
               BDGT-CLSFN-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-CARRY-FWD-TYP TO
               CARRY-FRWRD-TYPE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-BDGT-PCT TO
               BDGT-PCT-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-DUE-TO-ACCT-CD TO
               DUE-TO-ACCT-CODE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-DUE-FROM-ACCT-CD TO
               DUE-FROM-ACCT-CODE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-FUND-BL-ACCT-CD TO
               FUND-BAL-ACCT-CODE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-AP-ACRL-ACCT-CD TO
               AP-ACRL-ACCT-CODE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-AR-ACRL-ACCT-CD TO
               AR-ACRL-ACCT-CODE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-CLOSE-LDGR-RULE TO
               CLOSE-LDGR-RULE-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ROLL-ENCMB-IND TO
               ROLL-ENCMBRNC-IND-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ROLL-PO-IND TO
               ROLL-PO-IND-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ROLL-MEM-RSRV-IND TO
               ROLL-MEMO-RSRV-IND-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ROLL-RQSTN-IND TO
               ROLL-RQSTN-IND-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-ROLL-LABOR-ENCMB TO
               ROLL-LABOR-ENCMBRNC-4076 OF R4076-COA-EFCTV
                                                                     .
           MOVE COE-CMPLT-IND TO
               CMPLT-IND-4076 OF R4076-COA-EFCTV
                                                                     .
       975-TO-R4076-COA-EFCTV-EXIT.
           EXIT.
