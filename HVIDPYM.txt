       975-TO-R4035-PAY-MTHD SECTION.
           MOVE PYM-USER-CD TO
               USER-CODE-4035 OF R4035-PAY-MTHD
                                                                     .
           MOVE PYM-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4035 OF R4035-PAY-MTHD
                                                                     .
           MOVE PYM-UNVRS-CD TO
               UNVRS-CODE-4035 OF R4035-PAY-MTHD
                                                                     .
           MOVE PYM-PAY-MTHD-CD TO
               PAY-MTHD-CODE-4035 OF R4035-PAY-MTHD
                                                                     .
           MOVE PYM-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4035 OF R4035-PAY-MTHD
                                                                     .
       975-TO-R4035-PAY-MTHD-EXIT.
           EXIT.
