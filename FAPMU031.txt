    ***Created by Convert/DC version V8R03 on 12/04/00 at 15:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP03 OF FAPWM03 TO WK01-CODE-AP03.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-4150-AP03 OF FAPWK03 TO WK01-NMBR-4150-AP03.
      *%--------------------------------------------------------------%*
           MOVE PO-NMBR-4150-AP03 OF FAPWM03 TO WK01-NMBR-4150-AP0A.
      *%--------------------------------------------------------------%*
           MOVE VNDR-ID-LAST-NINE-4150-AP03 OF FAPWM03 TO
                WK01-ID-LAST-NINE-4150-AP03.
      *%--------------------------------------------------------------%*
           MOVE VNDR-NAME-4150-AP03 OF FAPWM03 TO WK01-NAME-4150-AP03.
      *%--------------------------------------------------------------%*
           MOVE ITEM-NMBR-4151-AP03 OF FAPWK03 TO WK01-NMBR-4151-AP03.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-CODE-4151-AP03 OF FAPWM03 TO WK01-CODE-4151-AP03.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-DESC-4151-AP03 OF FAPWM03 TO WK01-DESC-4151-AP03.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-4152-AP03 OF FAPWM03 TO WK01-NMBR-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4152-AP03 OF FAPWM03 TO WK01-CODE-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-CODE-4152-AP03 OF FAPWM03 TO
                WK01-INDX-CODE-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE FUND-CODE-4152-AP03 OF FAPWM03 TO WK01-CODE-4152-AP0A.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-4152-AP03 OF FAPWM03 TO WK01-CODE-4152-AP0B.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4152-AP03 OF FAPWM03 TO WK01-CODE-4152-AP0C.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-CODE-4152-AP03 OF FAPWM03 TO WK01-CODE-4152-AP0D.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-CODE-4152-AP03 OF FAPWM03 TO WK01-CODE-4152-AP0E.
      *%--------------------------------------------------------------%*
           MOVE LCTN-CODE-4152-AP03 OF FAPWM03 TO WK01-CODE-4152-AP0F.
      *%--------------------------------------------------------------%*
           MOVE ACCT-ERROR-IND-4152-AP03 OF FAPWM03 TO
                WK01-ERROR-IND-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE BANK-CODE-DESC-4062-AP03 OF FAPWM03 TO
                WK01-CODE-DESC-4062-AP03.
      *%--------------------------------------------------------------%*
           MOVE INCM-TYPE-CODE-4155-AP03 OF FAPWM03 TO
                WK01-TYPE-CODE-4155-AP03.
      *%--------------------------------------------------------------%*
001MLJ     MOVE VNDR-ACCT-AP03 OF FAPWK03 TO
001MLJ          WK01-VNDR-ACCT-AP03.
      *%--------------------------------------------------------------%*
001MLJ     MOVE TRACKING-AP03 OF FAPWM03 TO
001MLJ          WK01-TRACKING-AP03.
      *%--------------------------------------------------------------%*
           MOVE LIQDTN-IND-4152-AP03 OF FAPWM03 TO WK01-IND-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE PREV-PAID-4151-AP03 OF FAPWM03 TO WK01-PAID-4151-AP03.
      *%--------------------------------------------------------------%*
           MOVE PREV-PAID-4152-AP03 OF FAPWM03 TO WK01-PAID-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE TO-BE-PAID-4151-AP03 OF FAPWM03 TO
                WK01-BE-PAID-4151-AP03.
      *%--------------------------------------------------------------%*
           MOVE TO-BE-PAID-4152-AP03 OF FAPWM03 TO
                WK01-BE-PAID-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE INVD-AMT-4151-AP03 OF FAPWM03 TO WK01-AMT-4151-AP03.
      *%--------------------------------------------------------------%*
           MOVE INVD-AMT-4152-AP03 OF FAPWM03 TO WK01-AMT-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE APRVD-AMT-4151-AP03 OF FAPWM03 TO WK01-AMT-4151-AP0A.
      *%--------------------------------------------------------------%*
           MOVE APRVD-AMT-4152-AP03 OF FAPWM03 TO WK01-AMT-4152-AP0A.
      *%--------------------------------------------------------------%*
DEVMR0* FP5665 - NEW PROD DISCOUNT FIELDS
           MOVE PRD-DSCNT-AMT-4151-AP03 OF FAPWM03
                                              TO WK01-PRD-AMT-4151-AP0B.
      *%--------------------------------------------------------------%*
           MOVE PRD-DSCNT-AMT-4152-AP03 OF FAPWM03
                                              TO WK01-PRD-AMT-4152-AP0B.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-AMT-4151-AP03 OF FAPWM03 TO WK01-AMT-4151-AP0B.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-AMT-4152-AP03 OF FAPWM03 TO WK01-AMT-4152-AP0B.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-AMT-AP03 OF FAPWM03 TO WK01-TRADE-IN-AMT-AP0B.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-AMT-2-AP03 OF FAPWM03 TO                       0B.
                  WK01-TRADE-IN-AMT-2-AP0B                              0B.
      *%--------------------------------------------------------------%*
           MOVE TAX-AMT-4151-AP03 OF FAPWM03 TO WK01-AMT-4151-AP0C.
      *%--------------------------------------------------------------%*
           MOVE TAX-AMT-4152-AP03 OF FAPWM03 TO WK01-AMT-4152-AP0C.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-4152-AP03 OF FAPWM03 TO WK01-CHRG-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE INVD-PCT-4152-AP03 OF FAPWM03 TO WK01-PCT-4152-AP03.
      *%--------------------------------------------------------------%*
DEVMR0* FP5665 - NEW PROD DISCOUNT FIELDS
           MOVE PRD-DSCNT-RULE-CLASS-4152-AP03 OF FAPWM03 TO
                WK01-PRD-RULE-CLASS-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-RULE-CLASS-4152-AP03 OF FAPWM03 TO
                WK01-RULE-CLASS-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-RULE-CLASS-AP03 OF FAPWM03 TO
                WK01-TRD-IN-RULE-CLASS-AP03.
      *%--------------------------------------------------------------%*
           MOVE TAX-RULE-CLASS-4152-AP03 OF FAPWM03 TO
                WK01-RULE-CLASS-4152-AP0A.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-RULE-CLASS-4152-AP03 OF FAPWM03 TO
                WK01-CHRG-RL-CLSS-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE BANK-ACCT-CODE-4152-AP03 OF FAPWM03 TO
                WK01-ACCT-CODE-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE RULE-CLASS-CODE-4152-AP03 OF FAPWM03 TO
                WK01-CLASS-CODE-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE APRVD-PCT-4152-AP03 OF FAPWM03 TO WK01-PCT-4152-AP0A.
      *%--------------------------------------------------------------%*
DEVMR0* FP5665 - NEW PROD DISCOUNT FIELDS
           MOVE PRD-DSCNT-PCT-4152-AP03 OF FAPWM03
                                              TO WK01-PRD-PCT-4152-AP0B.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-PCT-4152-AP03 OF FAPWM03 TO WK01-PCT-4152-AP0B.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-PCT-AP03 OF FAPWM03 TO WK01-TRADE-IN-PCT-AP0B.
      *%--------------------------------------------------------------%*
           MOVE TAX-PCT-4152-AP03 OF FAPWM03 TO WK01-PCT-4152-AP0C.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-4150-AP03 OF FAPWM03 TO WK01-CHRG-4150-AP03.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-PCT-4152-AP03 OF FAPWM03 TO
                WK01-CHRG-PCT-4152-AP03.
      *%--------------------------------------------------------------%*
           MOVE PO-SEQ-NMBR-AP03 OF FAPWM03 TO WK01-SEQ-NMBR-AP03.
      *%--------------------------------------------------------------%*
           MOVE TOTAL-C-O-AP03 OF FAPWM03 TO WK01-C-O-AP03.
      *%--------------------------------------------------------------%*
           MOVE USE-TAX-AMT-AP03 OF FAPWM03 TO WK01-TAX-AMT-AP03.
      *%--------------------------------------------------------------%*
           MOVE SALES-TAX-AMT-AP03 OF FAPWM03 TO WK01-TAX-AMT-AP0A.
      *%--------------------------------------------------------------%*
           MOVE USE-RULE-CLASS-AP03 OF FAPWM03 TO WK01-RULE-CLASS-AP03.
      *%--------------------------------------------------------------%*
           MOVE FED-FUND-MSG-AP03 OF FAPWM03 TO WK01-FUND-MSG-AP03.
