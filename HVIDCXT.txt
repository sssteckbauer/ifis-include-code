       975-TO-R6260 SECTION.
           MOVE CXT-USER-CD TO
               USER-CODE-6260 OF R6260
                                                                     .
           MOVE CXT-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-6260 OF R6260
                                                                     .
           MOVE CXT-UNVRS-CD TO
               UNVRS-CODE-6260 OF R6260
                                                                     .
           MOVE CXT-TEXT-CD TO
               TEXT-CODE-6260 OF R6260
                                                                     .
           MOVE CXT-SHORT-DESC TO
               SHORT-DESC-6260 OF R6260
                                                                     .
           MOVE CXT-LONG-DESC TO
               LONG-DESC-6260 OF R6260
                                                                     .
           MOVE CXT-LINE-LGTH TO
               LINE-LGTH-6260 OF R6260
                                                                     .
           MOVE CXT-LINES-PER-PAGE TO
               LINES-PER-PAGE-6260 OF R6260
                                                                     .
           MOVE CXT-TOP-MARGIN TO
               TOP-MARGIN-6260 OF R6260
                                                                     .
           MOVE CXT-LABEL-ID TO
               LABEL-ID-6260 OF R6260
                                                                     .
           MOVE CXT-VERIFY-FLAG TO
               VERIFY-FLAG-6260 OF R6260
                                                                     .
       975-TO-R6260-EXIT.
           EXIT.
