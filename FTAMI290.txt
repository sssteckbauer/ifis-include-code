    ***Created by Convert/DC version V8R03 on 12/05/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADVNC-TYPE-TA29-Z = 'Y'
               MOVE WK01-ADVNC-TYPE-TA29 TO RQST-ADVNC-TYPE-TA29 OF
                FTAWK29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA291-Z = 'Y'
               MOVE WK01-CODE-TA291 TO ACTN-CODE-TA29 OF FTAWM29(0001)
           END-IF.
           MOVE WK01-CODE-TA291-F TO WK01-CODE-TA29-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA292-Z = 'Y'
               MOVE WK01-CODE-TA292 TO ACTN-CODE-TA29 OF FTAWM29(0002)
           END-IF.
           MOVE WK01-CODE-TA292-F TO WK01-CODE-TA29-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA293-Z = 'Y'
               MOVE WK01-CODE-TA293 TO ACTN-CODE-TA29 OF FTAWM29(0003)
           END-IF.
           MOVE WK01-CODE-TA293-F TO WK01-CODE-TA29-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA294-Z = 'Y'
               MOVE WK01-CODE-TA294 TO ACTN-CODE-TA29 OF FTAWM29(0004)
           END-IF.
           MOVE WK01-CODE-TA294-F TO WK01-CODE-TA29-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA295-Z = 'Y'
               MOVE WK01-CODE-TA295 TO ACTN-CODE-TA29 OF FTAWM29(0005)
           END-IF.
           MOVE WK01-CODE-TA295-F TO WK01-CODE-TA29-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA296-Z = 'Y'
               MOVE WK01-CODE-TA296 TO ACTN-CODE-TA29 OF FTAWM29(0006)
           END-IF.
           MOVE WK01-CODE-TA296-F TO WK01-CODE-TA29-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA297-Z = 'Y'
               MOVE WK01-CODE-TA297 TO ACTN-CODE-TA29 OF FTAWM29(0007)
           END-IF.
           MOVE WK01-CODE-TA297-F TO WK01-CODE-TA29-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA298-Z = 'Y'
               MOVE WK01-CODE-TA298 TO ACTN-CODE-TA29 OF FTAWM29(0008)
           END-IF.
           MOVE WK01-CODE-TA298-F TO WK01-CODE-TA29-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA299-Z = 'Y'
               MOVE WK01-CODE-TA299 TO ACTN-CODE-TA29 OF FTAWM29(0009)
           END-IF.
           MOVE WK01-CODE-TA299-F TO WK01-CODE-TA29-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA2910-Z = 'Y'
               MOVE WK01-CODE-TA2910 TO ACTN-CODE-TA29 OF FTAWM29(0010)
           END-IF.
           MOVE WK01-CODE-TA2910-F TO WK01-CODE-TA29-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA2911-Z = 'Y'
               MOVE WK01-CODE-TA2911 TO ACTN-CODE-TA29 OF FTAWM29(0011)
           END-IF.
           MOVE WK01-CODE-TA2911-F TO WK01-CODE-TA29-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA2912-Z = 'Y'
               MOVE WK01-CODE-TA2912 TO ACTN-CODE-TA29 OF FTAWM29(0012)
           END-IF.
           MOVE WK01-CODE-TA2912-F TO WK01-CODE-TA29-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA291-Z = 'Y'
               MOVE WK01-TYPE-4417-TA291 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0001)
           END-IF.
           MOVE WK01-TYPE-4417-TA291-F TO WK01-TYPE-4417-TA29-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA292-Z = 'Y'
               MOVE WK01-TYPE-4417-TA292 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0002)
           END-IF.
           MOVE WK01-TYPE-4417-TA292-F TO WK01-TYPE-4417-TA29-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA293-Z = 'Y'
               MOVE WK01-TYPE-4417-TA293 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0003)
           END-IF.
           MOVE WK01-TYPE-4417-TA293-F TO WK01-TYPE-4417-TA29-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA294-Z = 'Y'
               MOVE WK01-TYPE-4417-TA294 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0004)
           END-IF.
           MOVE WK01-TYPE-4417-TA294-F TO WK01-TYPE-4417-TA29-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA295-Z = 'Y'
               MOVE WK01-TYPE-4417-TA295 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0005)
           END-IF.
           MOVE WK01-TYPE-4417-TA295-F TO WK01-TYPE-4417-TA29-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA296-Z = 'Y'
               MOVE WK01-TYPE-4417-TA296 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0006)
           END-IF.
           MOVE WK01-TYPE-4417-TA296-F TO WK01-TYPE-4417-TA29-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA297-Z = 'Y'
               MOVE WK01-TYPE-4417-TA297 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0007)
           END-IF.
           MOVE WK01-TYPE-4417-TA297-F TO WK01-TYPE-4417-TA29-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA298-Z = 'Y'
               MOVE WK01-TYPE-4417-TA298 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0008)
           END-IF.
           MOVE WK01-TYPE-4417-TA298-F TO WK01-TYPE-4417-TA29-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA299-Z = 'Y'
               MOVE WK01-TYPE-4417-TA299 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0009)
           END-IF.
           MOVE WK01-TYPE-4417-TA299-F TO WK01-TYPE-4417-TA29-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA2910-Z = 'Y'
               MOVE WK01-TYPE-4417-TA2910 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0010)
           END-IF.
           MOVE WK01-TYPE-4417-TA2910-F TO WK01-TYPE-4417-TA29-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA2911-Z = 'Y'
               MOVE WK01-TYPE-4417-TA2911 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0011)
           END-IF.
           MOVE WK01-TYPE-4417-TA2911-F TO WK01-TYPE-4417-TA29-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA2912-Z = 'Y'
               MOVE WK01-TYPE-4417-TA2912 TO ADVNC-TYPE-4417-TA29 OF
                FTAWM29(0012)
           END-IF.
           MOVE WK01-TYPE-4417-TA2912-F TO WK01-TYPE-4417-TA29-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA291-Z = 'Y'
               MOVE WK01-DESC-4417-TA291 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0001)
           END-IF.
           MOVE WK01-DESC-4417-TA291-F TO WK01-DESC-4417-TA29-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA292-Z = 'Y'
               MOVE WK01-DESC-4417-TA292 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0002)
           END-IF.
           MOVE WK01-DESC-4417-TA292-F TO WK01-DESC-4417-TA29-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA293-Z = 'Y'
               MOVE WK01-DESC-4417-TA293 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0003)
           END-IF.
           MOVE WK01-DESC-4417-TA293-F TO WK01-DESC-4417-TA29-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA294-Z = 'Y'
               MOVE WK01-DESC-4417-TA294 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0004)
           END-IF.
           MOVE WK01-DESC-4417-TA294-F TO WK01-DESC-4417-TA29-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA295-Z = 'Y'
               MOVE WK01-DESC-4417-TA295 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0005)
           END-IF.
           MOVE WK01-DESC-4417-TA295-F TO WK01-DESC-4417-TA29-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA296-Z = 'Y'
               MOVE WK01-DESC-4417-TA296 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0006)
           END-IF.
           MOVE WK01-DESC-4417-TA296-F TO WK01-DESC-4417-TA29-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA297-Z = 'Y'
               MOVE WK01-DESC-4417-TA297 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0007)
           END-IF.
           MOVE WK01-DESC-4417-TA297-F TO WK01-DESC-4417-TA29-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA298-Z = 'Y'
               MOVE WK01-DESC-4417-TA298 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0008)
           END-IF.
           MOVE WK01-DESC-4417-TA298-F TO WK01-DESC-4417-TA29-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA299-Z = 'Y'
               MOVE WK01-DESC-4417-TA299 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0009)
           END-IF.
           MOVE WK01-DESC-4417-TA299-F TO WK01-DESC-4417-TA29-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA2910-Z = 'Y'
               MOVE WK01-DESC-4417-TA2910 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0010)
           END-IF.
           MOVE WK01-DESC-4417-TA2910-F TO WK01-DESC-4417-TA29-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA2911-Z = 'Y'
               MOVE WK01-DESC-4417-TA2911 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0011)
           END-IF.
           MOVE WK01-DESC-4417-TA2911-F TO WK01-DESC-4417-TA29-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA2912-Z = 'Y'
               MOVE WK01-DESC-4417-TA2912 TO SHORT-DESC-4417-TA29 OF
                FTAWM29(0012)
           END-IF.
           MOVE WK01-DESC-4417-TA2912-F TO WK01-DESC-4417-TA29-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA291-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA291 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0001)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA291-F TO
                WK01-LONG-DESC-4417-TA29-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA292-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA292 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0002)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA292-F TO
                WK01-LONG-DESC-4417-TA29-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA293-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA293 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0003)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA293-F TO
                WK01-LONG-DESC-4417-TA29-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA294-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA294 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0004)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA294-F TO
                WK01-LONG-DESC-4417-TA29-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA295-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA295 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0005)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA295-F TO
                WK01-LONG-DESC-4417-TA29-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA296-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA296 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0006)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA296-F TO
                WK01-LONG-DESC-4417-TA29-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA297-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA297 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0007)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA297-F TO
                WK01-LONG-DESC-4417-TA29-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA298-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA298 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0008)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA298-F TO
                WK01-LONG-DESC-4417-TA29-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA299-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA299 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0009)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA299-F TO
                WK01-LONG-DESC-4417-TA29-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA2910-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA2910 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0010)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA2910-F TO
                WK01-LONG-DESC-4417-TA29-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA2911-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA2911 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0011)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA2911-F TO
                WK01-LONG-DESC-4417-TA29-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4417-TA2912-Z = 'Y'
               MOVE WK01-LONG-DESC-4417-TA2912 TO
                STNDD-LONG-DESC-4417-TA29 OF FTAWM29(0012)
           END-IF.
           MOVE WK01-LONG-DESC-4417-TA2912-F TO
                WK01-LONG-DESC-4417-TA29-F (12).
