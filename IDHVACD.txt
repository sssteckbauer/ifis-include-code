       976-FROM-R6200-ACH-DATA SECTION.

FCYI **        MOVE USER-CODE-6200 OF R6200-ACH-DATA
FCYI           MOVE DBLINK-USER-CD
               TO ACD-USER-CD.

               MOVE LAST-ACTVY-DATE-6200 OF R6200-ACH-DATA
               TO ACD-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-6200 OF R6200-ACH-DATA
               TO ACD-UNVRS-CD.

           IF INTRL-REF-ID-6200 OF R6200-ACH-DATA  NOT NUMERIC
               MOVE ZEROS TO ACD-IREF-ID
           ELSE
               MOVE INTRL-REF-ID-6200 OF R6200-ACH-DATA
               TO ACD-IREF-ID
           END-IF.

               MOVE ENTY-PRSN-IND-6200 OF R6200-ACH-DATA
               TO ACD-ENTPSN-IND.

               MOVE ACH-ID-DIGIT-ONE-6200 OF R6200-ACH-DATA
               TO ACD-ACH-ID-DGT-ONE.

               MOVE ACH-ID-LAST-NINE-6200 OF R6200-ACH-DATA
               TO ACD-ACH-ID-LST-NINE.

               MOVE ACH-STOP-IND-6200 OF R6200-ACH-DATA
               TO ACD-ACH-STOP-IND.
       976-FROM-R6200-ACH-DATA-EXIT.
           EXIT.
