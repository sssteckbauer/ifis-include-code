      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYOR01 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYOR01.

      *
      ******************************************************************
      *****
      *** THIS INCLUDE MODULE MUST BE PART OF A SUBROUTINE THAT IS CALLE
      *D
      *** FROM THE MAIN DIALOG TO CHECK SECURITY.
      *** THE ERROR INDICATOR WILL BE CARRIED IN THE FIELD LIST-FLAG-SY0
      *0.
      *** THE SCRTY-ULTN-FLAG-SY01 VALUE SHOULE BE CHECK AFTER CALLING T
      *HIS ROUTINE.
      *** A VALUE OF 'N' INDICATES NO SECURITY ERROR, A VALUE OF 'Y' IND
      *ICATES
      *** THAT A ORGIZATION ERROR EXISTS.
      ******************************************************************
      ******
      *** NG 04.23.93 ACR #GA100 ERROR INDICATOR COMMENTED FOR THE CASE
      *IF
      ***                        ORGZN SECURITY HAS BEEN ESTABLISHED FOR
      ***                        ANOTHER MODULE.
      ***HAC 11.18.93 ACR #FI036 ERROR IF ORGN REC NOT FOUND
      ******************************************************************
      ******
      *
           MOVE 'N' TO SCRTY-VLTN-FLAG-SY01.
      *** ERROR FLAG
      *
      *%   OBTAIN CURRENT R4192-USER-PRFL.
           MOVE 0047 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-CURR TO DBLINK-TYPE-GET
           IF DBLINK-R4192-USER-PRFL-KEY = SPACES
               MOVE DBLINK-NO-CURRENCY TO ERROR-STATUS
               MOVE +100 TO SQLCODE
           ELSE
               MOVE ZERO TO ERROR-STATUS
               MOVE 0 TO SQLCODE
               MOVE DBLINK-R4192-USER-PRFL-01 TO UPR-USER-ID
               PERFORM 906-05-R4192-USER-PRFL
                  THRU 906-05-R4192-USER-PRFL-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           END-IF.
      *%   ON (ERROR-STATUS =  '0306')
           IF NOT ( ERROR-STATUS = '0306' )
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           ELSE
               CONTINUE
           END-IF.
      *
           IF AC99W-FLAG-FIRST-TIME = AC99W-YES   OR
              ANY-ERROR-STATUS
               MOVE UNVRS-CODE-6001-GA00 TO UNVRS-CODE-4192
               MOVE AGR-USER-ID TO USER-ID-4192
      *%       OBTAIN CALC R4192-USER-PRFL
               MOVE 0048 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-CALC TO DBLINK-TYPE-GET
               MOVE USER-ID-4192 OF R4192-USER-PRFL TO UPR-USER-ID
               PERFORM 906-05-R4192-USER-PRFL
                  THRU 906-05-R4192-USER-PRFL-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   MOVE 990576 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYOR01-EXIT
                   END-IF
      *** USER NOT IN SECURITY
                   MOVE 'Y' TO SCRTY-VLTN-FLAG-SY01
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYOR01-EXIT
                   END-IF
               END-IF
               MOVE COA-CODE-4000-GA00 TO COA-CODE-4013
      *%       OBTAIN R4013-USER-DTL WITHIN S-4192-4013 USING
      *%        COA-CODE-4013
               MOVE 0049 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
               MOVE COA-CODE-4013 TO DBLINK-S-4192-4013-KEY-U
               PERFORM 906-02-S-4192-4013
                  THRU 906-02-S-4192-4013-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   MOVE 001152 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYOR01-EXIT
                   END-IF
      *** COA CODE NOT VALID
                   MOVE 'Y' TO SCRTY-VLTN-FLAG-SY01
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYOR01-EXIT
                   END-IF
               END-IF
               MOVE AC99W-DATE TO WORK-DATE-SY55
               MOVE WORK-MONTH-DATE-SY55 TO DATA-BASE-YEAR-DATE-SY55
               MOVE WORK-DAY-DATE-SY55 TO DATA-BASE-MONTH-DATE-SY55
               MOVE WORK-YEAR-DATE-SY55 TO DATA-BASE-DAY-DATE-SY55
               IF DATA-BASE-YEAR-DATE-SY55 > YEAR-DATE-SY00
                   MOVE 19 TO DATA-BASE-CENT-DATE-SY55
               ELSE
                   MOVE 20 TO DATA-BASE-CENT-DATE-SY55
               END-IF
           END-IF.
      *
      *
           MOVE AGR-APPLICATION-NAME(1:2) TO AC99W-SST01.
           COMPUTE AC99W-SST01-LEN = 2 .
           ADD 1  AC99W-SST01-LEN
               GIVING AC99W-SUB1.
           MOVE HIGH-VALUE TO AC99W-SST01-POS(AC99W-SUB1).
           MOVE SPACE TO AC99W-STRNG01.
           STRING COA-CODE-4000-GA00 DELIMITED SIZE
                  AC99W-SST01 DELIMITED HIGH-VALUE
                  '        ' DELIMITED SIZE
                  INTO AC99W-STRNG01.
           MOVE AC99W-STRNG01(1:30) TO APLCN-DATA-KEY-SY00.
      *
      *%   OBTAIN R4020-USER-ORGN WITHIN S-4192-4020 USING
      *%    APLCN-DATA-KEY-SY00.
           MOVE 0050 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
           MOVE APLCN-DATA-KEY-SY00 TO DBLINK-S-4192-4020-KEY-U
           PERFORM 906-02-S-4192-4020
              THRU 906-02-S-4192-4020-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
           IF DB-REC-NOT-FOUND
      *%       OBTAIN NEXT R4020-USER-ORGN WITHIN S-4192-4020
               MOVE 0051 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-NEXT TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4192-4020
                  THRU 906-00N-S-4192-4020-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           END-IF.
      *
           IF DB-END-OF-SET
      *%       OBTAIN FIRST WITHIN S-4192-4020
               MOVE 0052 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-FIRST TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4192-4020
                  THRU 906-00N-S-4192-4020-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-END-OF-SET
                   GO TO 200-FSYOR01-EXIT
               END-IF
      ***     MOVE 'Y' TO SCRTY-VLTN-FLAG-SY01.
               GO TO 200-FSYOR01-EXIT
           END-IF.
      *
           IF ORGZN-TYPE-CODE-4020 NOT = AGR-APPLICATION-NAME
      ***     MOVE 'Y' TO SCRTY-VLTN-FLAG-SY01.
               GO TO 200-FSYOR01-EXIT
           END-IF.
      *
           IF ORGZN-CODE-4020 = ORGZN-CODE-4002-GA00
               GO TO 200-FSYOR01-EXIT
           END-IF.
      *
           PERFORM WITH TEST BEFORE
                   UNTIL NOT ( DB-STATUS-OK   AND
              ORGZN-TYPE-CODE-4020 = AGR-APPLICATION-NAME )
               IF ORGZN-CODE-4020 = ORGZN-CODE-4002-GA00
                   GO TO 200-FSYOR01-EXIT
               END-IF
               MOVE ORGZN-CODE-4002-GA00 TO ORGZN-CODE-4002
               MOVE UNVRS-CODE-6001-GA00 TO UNVRS-CODE-4002
               MOVE COA-CODE-4000-GA00 TO COA-CODE-4002
      *%       OBTAIN CALC R4002-ORGN
               MOVE 0053 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-CALC TO DBLINK-TYPE-GET
               MOVE ORGZN-CODE-4002 OF R4002-ORGN TO ORG-ORGN-CD
               PERFORM 906-05-R4002-ORGN
                  THRU 906-05-R4002-ORGN-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   GO TO 200-FSYOR01-EXIT
               END-IF
               MOVE NMRC-WORK-DATE-SY55 TO START-DATE-4010
               MOVE '235959' TO TIME-STAMP-4010
      *%       OBTAIN R4010-ORGN-EFCTV WITHIN S-4002-4010-P USING
      *%        EFCTV-RCRD-KEY-4010
               MOVE 0054 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
               MOVE EFCTV-RCRD-KEY-4010 TO DBLINK-WS-S-4002-4010-P-KEY
      *%**DATE/TIME CNV** IDD KEY (WS)FLD -> DBLINKX-INPUT             *
               MOVE DBLINK-WS-S-4002-4010-P-02 TO DBLINKX-INPUT-PARM-08
      *%**DATE/TIME CNV**  CONVERT DATE                                *
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
FXXCHG         CALL 'IDDTECNV' USING
FXXCHG                 DBLINKX-DATE-TIME-PARMS
               IF NOT DBLINKX-OK
      *%**DATE/TIME CNV**  CONVERT DATE FAILED                         *
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO DBLINKX-OUTPUT-PARM
               END-IF
      *%**DATE/TIME CNV**  DBLINKXD -> KEY (HV)FLD                     *
               MOVE DBLINKX-OUTPUT-PARM TO DBLINK-S-4002-4010-P-02
      *%**DATE/TIME CNV** IDD KEY (WS)FLD -> DBLINKX-INPUT             *
               MOVE DBLINK-WS-S-4002-4010-P-03 TO DBLINKX-INPUT-PARM
      *%**DATE/TIME CNV**  CONVERT TIME                                *
               MOVE '3' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
               MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
FXXCHG         CALL 'IDDTECNV' USING
FXXCHG                 DBLINKX-DATE-TIME-PARMS
               IF NOT DBLINKX-OK
      *%**DATE/TIME CNV**  CONVERT TIME FAILED                         *
               MOVE DBLINKX-NULL-DB2-TIME-ISO TO DBLINKX-OUTPUT-PARM
               END-IF
      *%**DATE/TIME CNV**  DBLINKXD -> KEY (HV)FLD                     *
               MOVE DBLINKX-OUTPUT-PARM TO DBLINK-S-4002-4010-P-03
               PERFORM 906-02-S-4002-4010-P
                  THRU 906-02-S-4002-4010-P-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
      *%           OBTAIN PRIOR R4010-ORGN-EFCTV WITHIN S-4002-4010-P
                   MOVE 0055 TO DBLINK-CURR-IO-NUM
                   MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
                   MOVE DBLINK-PRIOR TO DBLINK-TYPE-GET
                   PERFORM 906-00P-S-4002-4010-P
                      THRU 906-00P-S-4002-4010-P-EXIT
                   PERFORM 999-SQL-STATUS
                      THRU 999-SQL-STATUS-EXIT
               END-IF
               PERFORM 200-FSYOR01-020
                  THRU 200-FSYOR01-020-EXIT
               IF ORGZN-CODE-4020 = ORGZN-CODE-4002
                   GO TO 200-FSYOR01-EXIT
               END-IF
      *%       OBTAIN NEXT R4020-USER-ORGN WITHIN S-4192-4020
               MOVE 0056 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-NEXT TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4192-4020
                  THRU 906-00N-S-4192-4020-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
FCAMH      END-PERFORM.
      *
           MOVE 'Y' TO SCRTY-VLTN-FLAG-SY01.
           GO TO 200-FSYOR01-EXIT.

       200-FSYOR01-EXIT.
           EXIT.
      *%---------------------------------------------------------------*
      *%                                                               *
      *%               200-FSYOR01-020 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYOR01-020.

           PERFORM WITH TEST BEFORE
                   UNTIL NOT ( DB-STATUS-OK   )
               IF PREDCSR-CODE-4010 = SPACES
                   GO TO 200-FSYOR01-020-EXIT
               END-IF
               MOVE PREDCSR-CODE-4010 TO ORGZN-CODE-4002
               MOVE UNVRS-CODE-6001-GA00 TO UNVRS-CODE-4002
               MOVE COA-CODE-4000-GA00 TO COA-CODE-4002
      *%       OBTAIN CALC R4002-ORGN
               MOVE 0057 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-CALC TO DBLINK-TYPE-GET
               MOVE ORGZN-CODE-4002 OF R4002-ORGN TO ORG-ORGN-CD
               PERFORM 906-05-R4002-ORGN
                  THRU 906-05-R4002-ORGN-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF ORGZN-CODE-4020 = ORGZN-CODE-4002
                   GO TO 200-FSYOR01-020-EXIT
               END-IF
      *%       OBTAIN R4010-ORGN-EFCTV WITHIN S-4002-4010-P USING
      *%        EFCTV-RCRD-KEY-4010
               MOVE 0058 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
               MOVE EFCTV-RCRD-KEY-4010 TO DBLINK-WS-S-4002-4010-P-KEY
      *%**DATE/TIME CNV** IDD KEY (WS)FLD -> DBLINKX-INPUT             *
               MOVE DBLINK-WS-S-4002-4010-P-02 TO DBLINKX-INPUT-PARM-08
      *%**DATE/TIME CNV**  CONVERT DATE                                *
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
FXXCHG         CALL 'IDDTECNV' USING
FXXCHG                 DBLINKX-DATE-TIME-PARMS
               IF NOT DBLINKX-OK
      *%**DATE/TIME CNV**  CONVERT DATE FAILED                         *
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO DBLINKX-OUTPUT-PARM
               END-IF
      *%**DATE/TIME CNV**  DBLINKXD -> KEY (HV)FLD                     *
               MOVE DBLINKX-OUTPUT-PARM TO DBLINK-S-4002-4010-P-02
      *%**DATE/TIME CNV** IDD KEY (WS)FLD -> DBLINKX-INPUT             *
               MOVE DBLINK-WS-S-4002-4010-P-03 TO DBLINKX-INPUT-PARM
      *%**DATE/TIME CNV**  CONVERT TIME                                *
               MOVE '3' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
               MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
FXXCHG         CALL 'IDDTECNV' USING
FXXCHG                 DBLINKX-DATE-TIME-PARMS
               IF NOT DBLINKX-OK
      *%**DATE/TIME CNV**  CONVERT TIME FAILED                         *
               MOVE DBLINKX-NULL-DB2-TIME-ISO TO DBLINKX-OUTPUT-PARM
               END-IF
      *%**DATE/TIME CNV**  DBLINKXD -> KEY (HV)FLD                     *
               MOVE DBLINKX-OUTPUT-PARM TO DBLINK-S-4002-4010-P-03
               PERFORM 906-02-S-4002-4010-P
                  THRU 906-02-S-4002-4010-P-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
      *%           OBTAIN PRIOR R4010-ORGN-EFCTV WITHIN S-4002-4010-P
                   MOVE 0059 TO DBLINK-CURR-IO-NUM
                   MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
                   MOVE DBLINK-PRIOR TO DBLINK-TYPE-GET
                   PERFORM 906-00P-S-4002-4010-P
                      THRU 906-00P-S-4002-4010-P-EXIT
                   PERFORM 999-SQL-STATUS
                      THRU 999-SQL-STATUS-EXIT
               END-IF
           END-PERFORM.

       200-FSYOR01-020-EXIT.
           EXIT.
