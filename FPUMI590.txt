    ***Created by Convert/DC version V8R03 on 11/20/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4070-PU59-Z = 'Y'
               MOVE WK01-CODE-4070-PU59 TO RQST-CODE-4070-PU59 OF
                FPUWK59
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LIT-PU59-Z = 'Y'
               MOVE WK01-LIT-PU59 TO DELETE-LIT-PU59 OF FPUWM59
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4071-PU591-Z = 'Y'
               MOVE WK01-NMBR-4071-PU591 TO ITEM-NMBR-4071-PU59 OF
                FPUWM59(0001)
           END-IF.
           MOVE WK01-NMBR-4071-PU591-F TO WK01-NMBR-4071-PU59-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4071-PU592-Z = 'Y'
               MOVE WK01-NMBR-4071-PU592 TO ITEM-NMBR-4071-PU59 OF
                FPUWM59(0002)
           END-IF.
           MOVE WK01-NMBR-4071-PU592-F TO WK01-NMBR-4071-PU59-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4071-PU593-Z = 'Y'
               MOVE WK01-NMBR-4071-PU593 TO ITEM-NMBR-4071-PU59 OF
                FPUWM59(0003)
           END-IF.
           MOVE WK01-NMBR-4071-PU593-F TO WK01-NMBR-4071-PU59-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4071-PU594-Z = 'Y'
               MOVE WK01-NMBR-4071-PU594 TO ITEM-NMBR-4071-PU59 OF
                FPUWM59(0004)
           END-IF.
           MOVE WK01-NMBR-4071-PU594-F TO WK01-NMBR-4071-PU59-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4071-PU595-Z = 'Y'
               MOVE WK01-NMBR-4071-PU595 TO ITEM-NMBR-4071-PU59 OF
                FPUWM59(0005)
           END-IF.
           MOVE WK01-NMBR-4071-PU595-F TO WK01-NMBR-4071-PU59-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-ITEM-NMBR-PU59-Z = 'Y'
               MOVE WK01-ITEM-NMBR-PU59 TO RQST-ITEM-NMBR-PU59 OF
                FPUWK59
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4070-PU59-Z = 'Y'
               MOVE WK01-NAME-4070-PU59 TO RQST-NAME-4070-PU59 OF
                FPUWM59
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4074-PU591-Z = 'Y'
               MOVE WK01-CODE-4074-PU591 TO CMDTY-CODE-4074-PU59 OF
                FPUWM59(0001)
           END-IF.
           MOVE WK01-CODE-4074-PU591-F TO WK01-CODE-4074-PU59-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4074-PU591-Z = 'Y'
               MOVE WK01-DESC-4074-PU591 TO CMDTY-DESC-4074-PU59 OF
                FPUWM59(0001)
           END-IF.
           MOVE WK01-DESC-4074-PU591-F TO WK01-DESC-4074-PU59-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4074-PU592-Z = 'Y'
               MOVE WK01-CODE-4074-PU592 TO CMDTY-CODE-4074-PU59 OF
                FPUWM59(0002)
           END-IF.
           MOVE WK01-CODE-4074-PU592-F TO WK01-CODE-4074-PU59-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4074-PU593-Z = 'Y'
               MOVE WK01-CODE-4074-PU593 TO CMDTY-CODE-4074-PU59 OF
                FPUWM59(0003)
           END-IF.
           MOVE WK01-CODE-4074-PU593-F TO WK01-CODE-4074-PU59-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4074-PU594-Z = 'Y'
               MOVE WK01-CODE-4074-PU594 TO CMDTY-CODE-4074-PU59 OF
                FPUWM59(0004)
           END-IF.
           MOVE WK01-CODE-4074-PU594-F TO WK01-CODE-4074-PU59-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4074-PU595-Z = 'Y'
               MOVE WK01-CODE-4074-PU595 TO CMDTY-CODE-4074-PU59 OF
                FPUWM59(0005)
           END-IF.
           MOVE WK01-CODE-4074-PU595-F TO WK01-CODE-4074-PU59-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-PU59-Z = 'Y'
               MOVE WK01-FLAG-PU59 TO DELETE-FLAG-PU59 OF FPUWM59
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4074-PU592-Z = 'Y'
               MOVE WK01-DESC-4074-PU592 TO CMDTY-DESC-4074-PU59 OF
                FPUWM59(0002)
           END-IF.
           MOVE WK01-DESC-4074-PU592-F TO WK01-DESC-4074-PU59-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4074-PU593-Z = 'Y'
               MOVE WK01-DESC-4074-PU593 TO CMDTY-DESC-4074-PU59 OF
                FPUWM59(0003)
           END-IF.
           MOVE WK01-DESC-4074-PU593-F TO WK01-DESC-4074-PU59-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4074-PU594-Z = 'Y'
               MOVE WK01-DESC-4074-PU594 TO CMDTY-DESC-4074-PU59 OF
                FPUWM59(0004)
           END-IF.
           MOVE WK01-DESC-4074-PU594-F TO WK01-DESC-4074-PU59-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4074-PU595-Z = 'Y'
               MOVE WK01-DESC-4074-PU595 TO CMDTY-DESC-4074-PU59 OF
                FPUWM59(0005)
           END-IF.
           MOVE WK01-DESC-4074-PU595-F TO WK01-DESC-4074-PU59-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-MEA-CODE-4071-PU591-Z = 'Y'
               MOVE WK01-MEA-CODE-4071-PU591 TO UNIT-MEA-CODE-4071-PU59
                OF FPUWM59(0001)
           END-IF.
           MOVE WK01-MEA-CODE-4071-PU591-F TO WK01-MEA-CODE-4071-PU59-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-MEA-CODE-4071-PU592-Z = 'Y'
               MOVE WK01-MEA-CODE-4071-PU592 TO UNIT-MEA-CODE-4071-PU59
                OF FPUWM59(0002)
           END-IF.
           MOVE WK01-MEA-CODE-4071-PU592-F TO WK01-MEA-CODE-4071-PU59-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-MEA-CODE-4071-PU593-Z = 'Y'
               MOVE WK01-MEA-CODE-4071-PU593 TO UNIT-MEA-CODE-4071-PU59
                OF FPUWM59(0003)
           END-IF.
           MOVE WK01-MEA-CODE-4071-PU593-F TO WK01-MEA-CODE-4071-PU59-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-MEA-CODE-4071-PU594-Z = 'Y'
               MOVE WK01-MEA-CODE-4071-PU594 TO UNIT-MEA-CODE-4071-PU59
                OF FPUWM59(0004)
           END-IF.
           MOVE WK01-MEA-CODE-4071-PU594-F TO WK01-MEA-CODE-4071-PU59-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-MEA-CODE-4071-PU595-Z = 'Y'
               MOVE WK01-MEA-CODE-4071-PU595 TO UNIT-MEA-CODE-4071-PU59
                OF FPUWM59(0005)
           END-IF.
           MOVE WK01-MEA-CODE-4071-PU595-F TO WK01-MEA-CODE-4071-PU59-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-4071-PU591-Z = 'Y'
               MOVE WK01-4071-PU591 TO QTY-4071-PU59 OF FPUWM59(0001)
           END-IF.
           MOVE WK01-4071-PU591-F TO WK01-4071-PU59-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-4071-PU592-Z = 'Y'
               MOVE WK01-4071-PU592 TO QTY-4071-PU59 OF FPUWM59(0002)
           END-IF.
           MOVE WK01-4071-PU592-F TO WK01-4071-PU59-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-4071-PU593-Z = 'Y'
               MOVE WK01-4071-PU593 TO QTY-4071-PU59 OF FPUWM59(0003)
           END-IF.
           MOVE WK01-4071-PU593-F TO WK01-4071-PU59-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-4071-PU594-Z = 'Y'
               MOVE WK01-4071-PU594 TO QTY-4071-PU59 OF FPUWM59(0004)
           END-IF.
           MOVE WK01-4071-PU594-F TO WK01-4071-PU59-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-4071-PU595-Z = 'Y'
               MOVE WK01-4071-PU595 TO QTY-4071-PU59 OF FPUWM59(0005)
           END-IF.
           MOVE WK01-4071-PU595-F TO WK01-4071-PU59-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-4071-PU591-Z = 'Y'
               MOVE WK01-PRICE-4071-PU591 TO EXTENDED-PRICE-4071-PU59
                OF FPUWM59(0001)
           END-IF.
           MOVE WK01-PRICE-4071-PU591-F TO WK01-PRICE-4071-PU59-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-4071-PU592-Z = 'Y'
               MOVE WK01-PRICE-4071-PU592 TO EXTENDED-PRICE-4071-PU59
                OF FPUWM59(0002)
           END-IF.
           MOVE WK01-PRICE-4071-PU592-F TO WK01-PRICE-4071-PU59-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-4071-PU593-Z = 'Y'
               MOVE WK01-PRICE-4071-PU593 TO EXTENDED-PRICE-4071-PU59
                OF FPUWM59(0003)
           END-IF.
           MOVE WK01-PRICE-4071-PU593-F TO WK01-PRICE-4071-PU59-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-4071-PU594-Z = 'Y'
               MOVE WK01-PRICE-4071-PU594 TO EXTENDED-PRICE-4071-PU59
                OF FPUWM59(0004)
           END-IF.
           MOVE WK01-PRICE-4071-PU594-F TO WK01-PRICE-4071-PU59-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-4071-PU595-Z = 'Y'
               MOVE WK01-PRICE-4071-PU595 TO EXTENDED-PRICE-4071-PU59
                OF FPUWM59(0005)
           END-IF.
           MOVE WK01-PRICE-4071-PU595-F TO WK01-PRICE-4071-PU59-F (5).
