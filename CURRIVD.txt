      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4151-INV-DTL' TO DBLINK-CURR-PARAGRAPH.      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4151-INV-DTL' TO RECORD-NAME.                         276 BRTN
YYY991     MOVE 'F-INVOICE' TO AREA-NAME.                               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4151-INV-DTL TO DBLINK-RECORD-MADE-CURRENT.     276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4151-INV-DTL-EXIT                        276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE IVD-FK-IVH-DOC-NBR TO DBLINK-R4151-INV-DTL-01       276 BRTN
YYY991         MOVE IVD-ITEM-NBR TO DBLINK-R4151-INV-DTL-02             276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4151-INV-DTL TO DBLINK-VIEW-NAME-CURRENT.       276 BRTN
YYY991     MOVE DBLINK-R4151-INV-DTL-KEY TO DBLINK-VIEW-200-CURRENT.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4151-4152                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4151-INV-DTL-01 TO DBLINK-S-4151-4152-01.       276 BRTN
YYY991     MOVE DBLINK-R4151-INV-DTL-02 TO DBLINK-S-4151-4152-02.       276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4151-4152-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4151-4306                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4151-INV-DTL-01 TO DBLINK-S-4151-4306-01.       276 BRTN
YYY991     MOVE DBLINK-R4151-INV-DTL-02 TO DBLINK-S-4151-4306-02.       276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4151-4306-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4085-4151                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4085-4151-F               276 BRTN
YYY991     IF IVD-POD-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE IVD-POD-SETF TO DBLINK-S-4085-4151-F                276 BRTN
YYY991         MOVE IVD-FK-POD-PO-NBR TO DBLINK-S-4085-4151-01          276 BRTN
YYY991         MOVE IVD-FK-POD-CHG-SEQ-NBR TO DBLINK-S-4085-4151-02     276 BRTN
YYY991         MOVE IVD-FK-POD-ITEM-NBR TO DBLINK-S-4085-4151-03        276 BRTN
YYY991         MOVE IVD-POD-SET-TS TO DBLINK-S-4085-4151-04             276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4150-4151                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE IVD-FK-IVH-DOC-NBR TO DBLINK-S-4150-4151-01.            276 BRTN
YYY991     MOVE IVD-ITEM-NBR TO DBLINK-S-4150-4151-02.                  276 BRTN
