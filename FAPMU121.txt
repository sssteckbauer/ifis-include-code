    ***Created by Convert/DC version V8R03 on 12/04/00 at 15:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP12 OF FAPWM12 TO WK01-CODE-AP12.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-CODE-4161-AP12 OF FAPWK12 TO WK01-CODE-4161-AP12.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-DESC-4172-AP12 OF FAPWM12 TO WK01-DESC-4172-AP12.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4172-AP12 OF FAPWK12 TO WK01-DATE-4172-AP12.
      *%--------------------------------------------------------------%*
           MOVE NEXT-CHNG-DATE-AP12 OF FAPWM12 TO WK01-CHNG-DATE-AP12.
      *%--------------------------------------------------------------%*
           MOVE STATUS-DESC-AP12 OF FAPWM12 TO WK01-DESC-AP12.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4172-AP12 OF FAPWM12 TO WK01-DATE-4172-AP1A.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4172-AP12 OF FAPWM12 TO WK01-DATE-4172-AP1B.
      *%--------------------------------------------------------------%*
           MOVE TIME-STAMP-4172-AP12 OF FAPWM12 TO WK01-STAMP-4172-AP12.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-AP12 OF FAPWM12 TO WK01-PM-FLAG-AP12.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-PCT-4172-AP12 OF FAPWM12 TO WK01-PCT-4172-AP12.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-DAYS-4172-AP12 OF FAPWM12 TO WK01-DAYS-4172-AP12.
      *%--------------------------------------------------------------%*
           MOVE NET-DAYS-4172-AP12 OF FAPWM12 TO WK01-DAYS-4172-AP1A.
      *%--------------------------------------------------------------%*
           MOVE END-OF-MONTH-IND-4172-AP12 OF FAPWM12 TO
                WK01-OF-MONTH-IND-4172-AP12.
