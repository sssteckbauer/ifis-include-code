    ***Created by Convert/DC version V8R03 on 07/18/00 at 15:00***

      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE RESPONSE-DESC-UT00 OF FUTWM00 TO WK01-DESC-UT00.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RESPONSE-UT00 OF FUTWM00 TO WK01-RESPONSE-UT00.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-UT00 OF FUTWM00(0001) TO WK01-CODE-UT001.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-UT00 OF FUTWM00(0002) TO WK01-CODE-UT002.
      *%--------------------------------------------------------------%*
           MOVE RSPNS-ID-UT00 OF FUTWM00(0001) TO WK01-ID-UT001.
      *%--------------------------------------------------------------%*
           MOVE RSPNS-ID-UT00 OF FUTWM00(0002) TO WK01-ID-UT002.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-UT00 OF FUTWM00(0001) TO WK01-DESC-UT0A11.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-UT00 OF FUTWM00(0002) TO WK01-DESC-UT0A12.
      *%--------------------------------------------------------------%*
           MOVE TEXT-IND-UT00 OF FUTWM00(0001) TO WK01-IND-UT001.
      *%--------------------------------------------------------------%*
           MOVE TEXT-IND-UT00 OF FUTWM00(0002) TO WK01-IND-UT002.
