      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4041-CLOSE-DTL' TO DBLINK-CURR-PARAGRAPH.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4041-CLOSE-DTL' TO RECORD-NAME.                       276 BRTN
YYY991     MOVE 'F-TABLE' TO AREA-NAME.                                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4041-CLOSE-DTL TO DBLINK-RECORD-MADE-CURRENT.   276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4041-CLOSE-DTL-EXIT                      276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE CLD-FK-CLH-FUND-TYP-CD TO DBLINK-R4041-CLOSE-DTL-01 276 BRTN
YYY991         MOVE CLD-FK-CLH-FUND-CD TO DBLINK-R4041-CLOSE-DTL-02     276 BRTN
YYY991         MOVE CLD-ENCMB-TYP TO DBLINK-R4041-CLOSE-DTL-03          276 BRTN
YYY991         MOVE CLD-PO-CLS-CD TO DBLINK-R4041-CLOSE-DTL-04          276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4041-CLOSE-DTL TO DBLINK-VIEW-NAME-CURRENT.     276 BRTN
YYY991     MOVE DBLINK-R4041-CLOSE-DTL-KEY TO DBLINK-VIEW-200-CURRENT.  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4040-4041                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE CLD-FK-CLH-FUND-TYP-CD TO DBLINK-S-4040-4041-01.        276 BRTN
YYY991     MOVE CLD-FK-CLH-FUND-CD TO DBLINK-S-4040-4041-02.            276 BRTN
YYY991     MOVE CLD-ENCMB-TYP TO DBLINK-S-4040-4041-03.                 276 BRTN
YYY991     MOVE CLD-PO-CLS-CD TO DBLINK-S-4040-4041-04.                 276 BRTN
