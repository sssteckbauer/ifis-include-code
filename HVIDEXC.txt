       975-TO-R4227-EXPR-CMDTY SECTION.
           MOVE EXC-USER-CD TO
               USER-CODE-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-CMDTY-CD TO
               CMDTY-CODE-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-ACCT-CD TO
               ACCT-CODE-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-ADR-TYP-CD TO
               ADR-TYPE-CODE-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-ADDL-CHRG-RULCLSS TO
               ADDL-CHRG-RULE-CLASS-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-RULE-CLS-CD TO
               RULE-CLASS-CODE-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-DSCNT-RULE-CLS TO
               DSCNT-RULE-CLASS-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-TAX-RULE-CLS TO
               TAX-RULE-CLASS-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-EQP-IND TO
               EQP-IND-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-ADDL-CHRG-RULE-CR TO
               ADDL-CHRG-RULE-CLASS-CR-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-RULE-CD-CR TO
               RULE-CLASS-CODE-CR-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-DSCNT-RULE-CR TO
               DSCNT-RULE-CLASS-CR-4227 OF R4227-EXPR-CMDTY
                                                                     .
           MOVE EXC-TAX-RULE-CR TO
               TAX-RULE-CLASS-CR-4227 OF R4227-EXPR-CMDTY
                                                                     .
       975-TO-R4227-EXPR-CMDTY-EXIT.
           EXIT.
