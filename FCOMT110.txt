    ***Created by Convert/DC version V8R03 on 12/13/00 at 15:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NIN-6157-C11-Z = 'Y'
               MOVE WK01-ID-LAST-NIN-6157-C11 TO
                PRSN-ID-LAST-NINE-6157-CO11 OF FCOWK11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6157-CO11-Z = 'Y'
               MOVE WK01-KEY-6157-CO11 TO NAME-KEY-6157-CO11 OF FCOWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COMM-TYPE-CO11-Z = 'Y'
               MOVE WK01-COMM-TYPE-CO11 TO KEY-COMM-TYPE-CO11 OF FCOWK11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COMM-PLAN-CO11-Z = 'Y'
               MOVE WK01-COMM-PLAN-CO11 TO KEY-COMM-PLAN-CO11 OF FCOWK11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO11-Z = 'Y'
               MOVE WK01-DESC-6258-CO11 TO LONG-DESC-6258-CO11 OF
                FCOWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO11-Z = 'Y'
               MOVE WK01-DESC-6259-CO11 TO LONG-DESC-6259-CO11 OF
                FCOWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-2155-CO11-Z = 'Y'
               MOVE WK01-NAME-2155-CO11 TO FULL-NAME-2155-CO11 OF
                FCOWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO111-Z = 'Y'
               MOVE WK01-CODE-CO111 TO ACTN-CODE-CO11 OF FCOWM11(0001)
           END-IF.
           MOVE WK01-CODE-CO111-F TO WK01-CODE-CO11-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO112-Z = 'Y'
               MOVE WK01-CODE-CO112 TO ACTN-CODE-CO11 OF FCOWM11(0002)
           END-IF.
           MOVE WK01-CODE-CO112-F TO WK01-CODE-CO11-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO113-Z = 'Y'
               MOVE WK01-CODE-CO113 TO ACTN-CODE-CO11 OF FCOWM11(0003)
           END-IF.
           MOVE WK01-CODE-CO113-F TO WK01-CODE-CO11-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO114-Z = 'Y'
               MOVE WK01-CODE-CO114 TO ACTN-CODE-CO11 OF FCOWM11(0004)
           END-IF.
           MOVE WK01-CODE-CO114-F TO WK01-CODE-CO11-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO115-Z = 'Y'
               MOVE WK01-CODE-CO115 TO ACTN-CODE-CO11 OF FCOWM11(0005)
           END-IF.
           MOVE WK01-CODE-CO115-F TO WK01-CODE-CO11-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO116-Z = 'Y'
               MOVE WK01-CODE-CO116 TO ACTN-CODE-CO11 OF FCOWM11(0006)
           END-IF.
           MOVE WK01-CODE-CO116-F TO WK01-CODE-CO11-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO117-Z = 'Y'
               MOVE WK01-CODE-CO117 TO ACTN-CODE-CO11 OF FCOWM11(0007)
           END-IF.
           MOVE WK01-CODE-CO117-F TO WK01-CODE-CO11-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO118-Z = 'Y'
               MOVE WK01-CODE-CO118 TO ACTN-CODE-CO11 OF FCOWM11(0008)
           END-IF.
           MOVE WK01-CODE-CO118-F TO WK01-CODE-CO11-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO119-Z = 'Y'
               MOVE WK01-CODE-CO119 TO ACTN-CODE-CO11 OF FCOWM11(0009)
           END-IF.
           MOVE WK01-CODE-CO119-F TO WK01-CODE-CO11-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO111-Z = 'Y'
               MOVE WK01-TYPE-6258-CO111 TO COMM-TYPE-6258-CO11 OF
                FCOWM11(0001)
           END-IF.
           MOVE WK01-TYPE-6258-CO111-F TO WK01-TYPE-6258-CO11-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO112-Z = 'Y'
               MOVE WK01-TYPE-6258-CO112 TO COMM-TYPE-6258-CO11 OF
                FCOWM11(0002)
           END-IF.
           MOVE WK01-TYPE-6258-CO112-F TO WK01-TYPE-6258-CO11-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO113-Z = 'Y'
               MOVE WK01-TYPE-6258-CO113 TO COMM-TYPE-6258-CO11 OF
                FCOWM11(0003)
           END-IF.
           MOVE WK01-TYPE-6258-CO113-F TO WK01-TYPE-6258-CO11-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO114-Z = 'Y'
               MOVE WK01-TYPE-6258-CO114 TO COMM-TYPE-6258-CO11 OF
                FCOWM11(0004)
           END-IF.
           MOVE WK01-TYPE-6258-CO114-F TO WK01-TYPE-6258-CO11-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO115-Z = 'Y'
               MOVE WK01-TYPE-6258-CO115 TO COMM-TYPE-6258-CO11 OF
                FCOWM11(0005)
           END-IF.
           MOVE WK01-TYPE-6258-CO115-F TO WK01-TYPE-6258-CO11-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO116-Z = 'Y'
               MOVE WK01-TYPE-6258-CO116 TO COMM-TYPE-6258-CO11 OF
                FCOWM11(0006)
           END-IF.
           MOVE WK01-TYPE-6258-CO116-F TO WK01-TYPE-6258-CO11-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO117-Z = 'Y'
               MOVE WK01-TYPE-6258-CO117 TO COMM-TYPE-6258-CO11 OF
                FCOWM11(0007)
           END-IF.
           MOVE WK01-TYPE-6258-CO117-F TO WK01-TYPE-6258-CO11-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO118-Z = 'Y'
               MOVE WK01-TYPE-6258-CO118 TO COMM-TYPE-6258-CO11 OF
                FCOWM11(0008)
           END-IF.
           MOVE WK01-TYPE-6258-CO118-F TO WK01-TYPE-6258-CO11-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO119-Z = 'Y'
               MOVE WK01-TYPE-6258-CO119 TO COMM-TYPE-6258-CO11 OF
                FCOWM11(0009)
           END-IF.
           MOVE WK01-TYPE-6258-CO119-F TO WK01-TYPE-6258-CO11-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO1A11-Z = 'Y'
               MOVE WK01-DESC-6258-CO1A11 TO SHORT-DESC-6258-CO11 OF
                FCOWM11(0001)
           END-IF.
           MOVE WK01-DESC-6258-CO1A11-F TO WK01-DESC-6258-CO1A1-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO1A12-Z = 'Y'
               MOVE WK01-DESC-6258-CO1A12 TO SHORT-DESC-6258-CO11 OF
                FCOWM11(0002)
           END-IF.
           MOVE WK01-DESC-6258-CO1A12-F TO WK01-DESC-6258-CO1A1-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO1A13-Z = 'Y'
               MOVE WK01-DESC-6258-CO1A13 TO SHORT-DESC-6258-CO11 OF
                FCOWM11(0003)
           END-IF.
           MOVE WK01-DESC-6258-CO1A13-F TO WK01-DESC-6258-CO1A1-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO1A14-Z = 'Y'
               MOVE WK01-DESC-6258-CO1A14 TO SHORT-DESC-6258-CO11 OF
                FCOWM11(0004)
           END-IF.
           MOVE WK01-DESC-6258-CO1A14-F TO WK01-DESC-6258-CO1A1-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO1A15-Z = 'Y'
               MOVE WK01-DESC-6258-CO1A15 TO SHORT-DESC-6258-CO11 OF
                FCOWM11(0005)
           END-IF.
           MOVE WK01-DESC-6258-CO1A15-F TO WK01-DESC-6258-CO1A1-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO1A16-Z = 'Y'
               MOVE WK01-DESC-6258-CO1A16 TO SHORT-DESC-6258-CO11 OF
                FCOWM11(0006)
           END-IF.
           MOVE WK01-DESC-6258-CO1A16-F TO WK01-DESC-6258-CO1A1-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO1A17-Z = 'Y'
               MOVE WK01-DESC-6258-CO1A17 TO SHORT-DESC-6258-CO11 OF
                FCOWM11(0007)
           END-IF.
           MOVE WK01-DESC-6258-CO1A17-F TO WK01-DESC-6258-CO1A1-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO1A18-Z = 'Y'
               MOVE WK01-DESC-6258-CO1A18 TO SHORT-DESC-6258-CO11 OF
                FCOWM11(0008)
           END-IF.
           MOVE WK01-DESC-6258-CO1A18-F TO WK01-DESC-6258-CO1A1-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO1A19-Z = 'Y'
               MOVE WK01-DESC-6258-CO1A19 TO SHORT-DESC-6258-CO11 OF
                FCOWM11(0009)
           END-IF.
           MOVE WK01-DESC-6258-CO1A19-F TO WK01-DESC-6258-CO1A1-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO111-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO111 TO
                COMM-PLAN-CODE-6259-CO11 OF FCOWM11(0001)
           END-IF.
           MOVE WK01-PLAN-CODE-6259-CO111-F TO
                WK01-PLAN-CODE-6259-CO11-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO112-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO112 TO
                COMM-PLAN-CODE-6259-CO11 OF FCOWM11(0002)
           END-IF.
           MOVE WK01-PLAN-CODE-6259-CO112-F TO
                WK01-PLAN-CODE-6259-CO11-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO113-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO113 TO
                COMM-PLAN-CODE-6259-CO11 OF FCOWM11(0003)
           END-IF.
           MOVE WK01-PLAN-CODE-6259-CO113-F TO
                WK01-PLAN-CODE-6259-CO11-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO114-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO114 TO
                COMM-PLAN-CODE-6259-CO11 OF FCOWM11(0004)
           END-IF.
           MOVE WK01-PLAN-CODE-6259-CO114-F TO
                WK01-PLAN-CODE-6259-CO11-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO115-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO115 TO
                COMM-PLAN-CODE-6259-CO11 OF FCOWM11(0005)
           END-IF.
           MOVE WK01-PLAN-CODE-6259-CO115-F TO
                WK01-PLAN-CODE-6259-CO11-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO116-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO116 TO
                COMM-PLAN-CODE-6259-CO11 OF FCOWM11(0006)
           END-IF.
           MOVE WK01-PLAN-CODE-6259-CO116-F TO
                WK01-PLAN-CODE-6259-CO11-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO117-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO117 TO
                COMM-PLAN-CODE-6259-CO11 OF FCOWM11(0007)
           END-IF.
           MOVE WK01-PLAN-CODE-6259-CO117-F TO
                WK01-PLAN-CODE-6259-CO11-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO118-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO118 TO
                COMM-PLAN-CODE-6259-CO11 OF FCOWM11(0008)
           END-IF.
           MOVE WK01-PLAN-CODE-6259-CO118-F TO
                WK01-PLAN-CODE-6259-CO11-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO119-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO119 TO
                COMM-PLAN-CODE-6259-CO11 OF FCOWM11(0009)
           END-IF.
           MOVE WK01-PLAN-CODE-6259-CO119-F TO
                WK01-PLAN-CODE-6259-CO11-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO1A11-Z = 'Y'
               MOVE WK01-DESC-6259-CO1A11 TO SHORT-DESC-6259-CO11 OF
                FCOWM11(0001)
           END-IF.
           MOVE WK01-DESC-6259-CO1A11-F TO WK01-DESC-6259-CO1A1-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO1A12-Z = 'Y'
               MOVE WK01-DESC-6259-CO1A12 TO SHORT-DESC-6259-CO11 OF
                FCOWM11(0002)
           END-IF.
           MOVE WK01-DESC-6259-CO1A12-F TO WK01-DESC-6259-CO1A1-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO1A13-Z = 'Y'
               MOVE WK01-DESC-6259-CO1A13 TO SHORT-DESC-6259-CO11 OF
                FCOWM11(0003)
           END-IF.
           MOVE WK01-DESC-6259-CO1A13-F TO WK01-DESC-6259-CO1A1-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO1A14-Z = 'Y'
               MOVE WK01-DESC-6259-CO1A14 TO SHORT-DESC-6259-CO11 OF
                FCOWM11(0004)
           END-IF.
           MOVE WK01-DESC-6259-CO1A14-F TO WK01-DESC-6259-CO1A1-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO1A15-Z = 'Y'
               MOVE WK01-DESC-6259-CO1A15 TO SHORT-DESC-6259-CO11 OF
                FCOWM11(0005)
           END-IF.
           MOVE WK01-DESC-6259-CO1A15-F TO WK01-DESC-6259-CO1A1-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO1A16-Z = 'Y'
               MOVE WK01-DESC-6259-CO1A16 TO SHORT-DESC-6259-CO11 OF
                FCOWM11(0006)
           END-IF.
           MOVE WK01-DESC-6259-CO1A16-F TO WK01-DESC-6259-CO1A1-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO1A17-Z = 'Y'
               MOVE WK01-DESC-6259-CO1A17 TO SHORT-DESC-6259-CO11 OF
                FCOWM11(0007)
           END-IF.
           MOVE WK01-DESC-6259-CO1A17-F TO WK01-DESC-6259-CO1A1-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO1A18-Z = 'Y'
               MOVE WK01-DESC-6259-CO1A18 TO SHORT-DESC-6259-CO11 OF
                FCOWM11(0008)
           END-IF.
           MOVE WK01-DESC-6259-CO1A18-F TO WK01-DESC-6259-CO1A1-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO1A19-Z = 'Y'
               MOVE WK01-DESC-6259-CO1A19 TO SHORT-DESC-6259-CO11 OF
                FCOWM11(0009)
           END-IF.
           MOVE WK01-DESC-6259-CO1A19-F TO WK01-DESC-6259-CO1A1-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO111-Z = 'Y'
               MOVE WK01-CODE-6260-CO111 TO TEXT-CODE-6260-CO11 OF
                FCOWM11(0001)
           END-IF.
           MOVE WK01-CODE-6260-CO111-F TO WK01-CODE-6260-CO11-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO112-Z = 'Y'
               MOVE WK01-CODE-6260-CO112 TO TEXT-CODE-6260-CO11 OF
                FCOWM11(0002)
           END-IF.
           MOVE WK01-CODE-6260-CO112-F TO WK01-CODE-6260-CO11-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO113-Z = 'Y'
               MOVE WK01-CODE-6260-CO113 TO TEXT-CODE-6260-CO11 OF
                FCOWM11(0003)
           END-IF.
           MOVE WK01-CODE-6260-CO113-F TO WK01-CODE-6260-CO11-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO114-Z = 'Y'
               MOVE WK01-CODE-6260-CO114 TO TEXT-CODE-6260-CO11 OF
                FCOWM11(0004)
           END-IF.
           MOVE WK01-CODE-6260-CO114-F TO WK01-CODE-6260-CO11-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO115-Z = 'Y'
               MOVE WK01-CODE-6260-CO115 TO TEXT-CODE-6260-CO11 OF
                FCOWM11(0005)
           END-IF.
           MOVE WK01-CODE-6260-CO115-F TO WK01-CODE-6260-CO11-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO116-Z = 'Y'
               MOVE WK01-CODE-6260-CO116 TO TEXT-CODE-6260-CO11 OF
                FCOWM11(0006)
           END-IF.
           MOVE WK01-CODE-6260-CO116-F TO WK01-CODE-6260-CO11-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO117-Z = 'Y'
               MOVE WK01-CODE-6260-CO117 TO TEXT-CODE-6260-CO11 OF
                FCOWM11(0007)
           END-IF.
           MOVE WK01-CODE-6260-CO117-F TO WK01-CODE-6260-CO11-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO118-Z = 'Y'
               MOVE WK01-CODE-6260-CO118 TO TEXT-CODE-6260-CO11 OF
                FCOWM11(0008)
           END-IF.
           MOVE WK01-CODE-6260-CO118-F TO WK01-CODE-6260-CO11-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO119-Z = 'Y'
               MOVE WK01-CODE-6260-CO119 TO TEXT-CODE-6260-CO11 OF
                FCOWM11(0009)
           END-IF.
           MOVE WK01-CODE-6260-CO119-F TO WK01-CODE-6260-CO11-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO111-Z = 'Y'
               MOVE WK01-DESC-6260-CO111 TO SHORT-DESC-6260-CO11 OF
                FCOWM11(0001)
           END-IF.
           MOVE WK01-DESC-6260-CO111-F TO WK01-DESC-6260-CO11-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO112-Z = 'Y'
               MOVE WK01-DESC-6260-CO112 TO SHORT-DESC-6260-CO11 OF
                FCOWM11(0002)
           END-IF.
           MOVE WK01-DESC-6260-CO112-F TO WK01-DESC-6260-CO11-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO113-Z = 'Y'
               MOVE WK01-DESC-6260-CO113 TO SHORT-DESC-6260-CO11 OF
                FCOWM11(0003)
           END-IF.
           MOVE WK01-DESC-6260-CO113-F TO WK01-DESC-6260-CO11-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO114-Z = 'Y'
               MOVE WK01-DESC-6260-CO114 TO SHORT-DESC-6260-CO11 OF
                FCOWM11(0004)
           END-IF.
           MOVE WK01-DESC-6260-CO114-F TO WK01-DESC-6260-CO11-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO115-Z = 'Y'
               MOVE WK01-DESC-6260-CO115 TO SHORT-DESC-6260-CO11 OF
                FCOWM11(0005)
           END-IF.
           MOVE WK01-DESC-6260-CO115-F TO WK01-DESC-6260-CO11-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO116-Z = 'Y'
               MOVE WK01-DESC-6260-CO116 TO SHORT-DESC-6260-CO11 OF
                FCOWM11(0006)
           END-IF.
           MOVE WK01-DESC-6260-CO116-F TO WK01-DESC-6260-CO11-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO117-Z = 'Y'
               MOVE WK01-DESC-6260-CO117 TO SHORT-DESC-6260-CO11 OF
                FCOWM11(0007)
           END-IF.
           MOVE WK01-DESC-6260-CO117-F TO WK01-DESC-6260-CO11-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO118-Z = 'Y'
               MOVE WK01-DESC-6260-CO118 TO SHORT-DESC-6260-CO11 OF
                FCOWM11(0008)
           END-IF.
           MOVE WK01-DESC-6260-CO118-F TO WK01-DESC-6260-CO11-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO119-Z = 'Y'
               MOVE WK01-DESC-6260-CO119 TO SHORT-DESC-6260-CO11 OF
                FCOWM11(0009)
           END-IF.
           MOVE WK01-DESC-6260-CO119-F TO WK01-DESC-6260-CO11-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-6274-CO111-Z = 'Y'
               MOVE WK01-ADR-TYPE-6274-CO111 TO COMM-ADR-TYPE-6274-CO11
                OF FCOWM11(0001)
           END-IF.
           MOVE WK01-ADR-TYPE-6274-CO111-F TO WK01-ADR-TYPE-6274-CO11-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-6274-CO112-Z = 'Y'
               MOVE WK01-ADR-TYPE-6274-CO112 TO COMM-ADR-TYPE-6274-CO11
                OF FCOWM11(0002)
           END-IF.
           MOVE WK01-ADR-TYPE-6274-CO112-F TO WK01-ADR-TYPE-6274-CO11-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-6274-CO113-Z = 'Y'
               MOVE WK01-ADR-TYPE-6274-CO113 TO COMM-ADR-TYPE-6274-CO11
                OF FCOWM11(0003)
           END-IF.
           MOVE WK01-ADR-TYPE-6274-CO113-F TO WK01-ADR-TYPE-6274-CO11-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-6274-CO114-Z = 'Y'
               MOVE WK01-ADR-TYPE-6274-CO114 TO COMM-ADR-TYPE-6274-CO11
                OF FCOWM11(0004)
           END-IF.
           MOVE WK01-ADR-TYPE-6274-CO114-F TO WK01-ADR-TYPE-6274-CO11-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-6274-CO115-Z = 'Y'
               MOVE WK01-ADR-TYPE-6274-CO115 TO COMM-ADR-TYPE-6274-CO11
                OF FCOWM11(0005)
           END-IF.
           MOVE WK01-ADR-TYPE-6274-CO115-F TO WK01-ADR-TYPE-6274-CO11-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-6274-CO116-Z = 'Y'
               MOVE WK01-ADR-TYPE-6274-CO116 TO COMM-ADR-TYPE-6274-CO11
                OF FCOWM11(0006)
           END-IF.
           MOVE WK01-ADR-TYPE-6274-CO116-F TO WK01-ADR-TYPE-6274-CO11-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-6274-CO117-Z = 'Y'
               MOVE WK01-ADR-TYPE-6274-CO117 TO COMM-ADR-TYPE-6274-CO11
                OF FCOWM11(0007)
           END-IF.
           MOVE WK01-ADR-TYPE-6274-CO117-F TO WK01-ADR-TYPE-6274-CO11-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-6274-CO118-Z = 'Y'
               MOVE WK01-ADR-TYPE-6274-CO118 TO COMM-ADR-TYPE-6274-CO11
                OF FCOWM11(0008)
           END-IF.
           MOVE WK01-ADR-TYPE-6274-CO118-F TO WK01-ADR-TYPE-6274-CO11-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-6274-CO119-Z = 'Y'
               MOVE WK01-ADR-TYPE-6274-CO119 TO COMM-ADR-TYPE-6274-CO11
                OF FCOWM11(0009)
           END-IF.
           MOVE WK01-ADR-TYPE-6274-CO119-F TO WK01-ADR-TYPE-6274-CO11-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO111-Z = 'Y'
               MOVE WK01-CODE-2155-CO111 TO OFC-CODE-2155-CO11 OF
                FCOWM11(0001)
           END-IF.
           MOVE WK01-CODE-2155-CO111-F TO WK01-CODE-2155-CO11-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO112-Z = 'Y'
               MOVE WK01-CODE-2155-CO112 TO OFC-CODE-2155-CO11 OF
                FCOWM11(0002)
           END-IF.
           MOVE WK01-CODE-2155-CO112-F TO WK01-CODE-2155-CO11-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO113-Z = 'Y'
               MOVE WK01-CODE-2155-CO113 TO OFC-CODE-2155-CO11 OF
                FCOWM11(0003)
           END-IF.
           MOVE WK01-CODE-2155-CO113-F TO WK01-CODE-2155-CO11-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO114-Z = 'Y'
               MOVE WK01-CODE-2155-CO114 TO OFC-CODE-2155-CO11 OF
                FCOWM11(0004)
           END-IF.
           MOVE WK01-CODE-2155-CO114-F TO WK01-CODE-2155-CO11-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO115-Z = 'Y'
               MOVE WK01-CODE-2155-CO115 TO OFC-CODE-2155-CO11 OF
                FCOWM11(0005)
           END-IF.
           MOVE WK01-CODE-2155-CO115-F TO WK01-CODE-2155-CO11-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO116-Z = 'Y'
               MOVE WK01-CODE-2155-CO116 TO OFC-CODE-2155-CO11 OF
                FCOWM11(0006)
           END-IF.
           MOVE WK01-CODE-2155-CO116-F TO WK01-CODE-2155-CO11-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO117-Z = 'Y'
               MOVE WK01-CODE-2155-CO117 TO OFC-CODE-2155-CO11 OF
                FCOWM11(0007)
           END-IF.
           MOVE WK01-CODE-2155-CO117-F TO WK01-CODE-2155-CO11-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO118-Z = 'Y'
               MOVE WK01-CODE-2155-CO118 TO OFC-CODE-2155-CO11 OF
                FCOWM11(0008)
           END-IF.
           MOVE WK01-CODE-2155-CO118-F TO WK01-CODE-2155-CO11-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO119-Z = 'Y'
               MOVE WK01-CODE-2155-CO119 TO OFC-CODE-2155-CO11 OF
                FCOWM11(0009)
           END-IF.
           MOVE WK01-CODE-2155-CO119-F TO WK01-CODE-2155-CO11-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6273-CO111-Z = 'Y'
               MOVE WK01-REF-NMBR-6273-CO111 TO COMM-REF-NMBR-6273-CO11
                OF FCOWM11(0001)
           END-IF.
           MOVE WK01-REF-NMBR-6273-CO111-F TO WK01-REF-NMBR-6273-CO11-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6273-CO112-Z = 'Y'
               MOVE WK01-REF-NMBR-6273-CO112 TO COMM-REF-NMBR-6273-CO11
                OF FCOWM11(0002)
           END-IF.
           MOVE WK01-REF-NMBR-6273-CO112-F TO WK01-REF-NMBR-6273-CO11-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6273-CO113-Z = 'Y'
               MOVE WK01-REF-NMBR-6273-CO113 TO COMM-REF-NMBR-6273-CO11
                OF FCOWM11(0003)
           END-IF.
           MOVE WK01-REF-NMBR-6273-CO113-F TO WK01-REF-NMBR-6273-CO11-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6273-CO114-Z = 'Y'
               MOVE WK01-REF-NMBR-6273-CO114 TO COMM-REF-NMBR-6273-CO11
                OF FCOWM11(0004)
           END-IF.
           MOVE WK01-REF-NMBR-6273-CO114-F TO WK01-REF-NMBR-6273-CO11-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6273-CO115-Z = 'Y'
               MOVE WK01-REF-NMBR-6273-CO115 TO COMM-REF-NMBR-6273-CO11
                OF FCOWM11(0005)
           END-IF.
           MOVE WK01-REF-NMBR-6273-CO115-F TO WK01-REF-NMBR-6273-CO11-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6273-CO116-Z = 'Y'
               MOVE WK01-REF-NMBR-6273-CO116 TO COMM-REF-NMBR-6273-CO11
                OF FCOWM11(0006)
           END-IF.
           MOVE WK01-REF-NMBR-6273-CO116-F TO WK01-REF-NMBR-6273-CO11-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6273-CO117-Z = 'Y'
               MOVE WK01-REF-NMBR-6273-CO117 TO COMM-REF-NMBR-6273-CO11
                OF FCOWM11(0007)
           END-IF.
           MOVE WK01-REF-NMBR-6273-CO117-F TO WK01-REF-NMBR-6273-CO11-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6273-CO118-Z = 'Y'
               MOVE WK01-REF-NMBR-6273-CO118 TO COMM-REF-NMBR-6273-CO11
                OF FCOWM11(0008)
           END-IF.
           MOVE WK01-REF-NMBR-6273-CO118-F TO WK01-REF-NMBR-6273-CO11-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6273-CO119-Z = 'Y'
               MOVE WK01-REF-NMBR-6273-CO119 TO COMM-REF-NMBR-6273-CO11
                OF FCOWM11(0009)
           END-IF.
           MOVE WK01-REF-NMBR-6273-CO119-F TO WK01-REF-NMBR-6273-CO11-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-OFC-CODE-CO11-Z = 'Y'
               MOVE WK01-OFC-CODE-CO11 TO KEY-OFC-CODE-CO11 OF FCOWK11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-CO11-Z = 'Y'
               MOVE WK01-DATE-CO11 TO RQST-DATE-CO11 OF FCOWK11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6274-CO111-Z = 'Y'
               MOVE WK01-DATE-6274-CO111 TO COMM-DATE-6274-CO11 OF
                FCOWM11(0001)
           END-IF.
           MOVE WK01-DATE-6274-CO111-F TO WK01-DATE-6274-CO11-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6274-CO112-Z = 'Y'
               MOVE WK01-DATE-6274-CO112 TO COMM-DATE-6274-CO11 OF
                FCOWM11(0002)
           END-IF.
           MOVE WK01-DATE-6274-CO112-F TO WK01-DATE-6274-CO11-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6274-CO113-Z = 'Y'
               MOVE WK01-DATE-6274-CO113 TO COMM-DATE-6274-CO11 OF
                FCOWM11(0003)
           END-IF.
           MOVE WK01-DATE-6274-CO113-F TO WK01-DATE-6274-CO11-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6274-CO114-Z = 'Y'
               MOVE WK01-DATE-6274-CO114 TO COMM-DATE-6274-CO11 OF
                FCOWM11(0004)
           END-IF.
           MOVE WK01-DATE-6274-CO114-F TO WK01-DATE-6274-CO11-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6274-CO115-Z = 'Y'
               MOVE WK01-DATE-6274-CO115 TO COMM-DATE-6274-CO11 OF
                FCOWM11(0005)
           END-IF.
           MOVE WK01-DATE-6274-CO115-F TO WK01-DATE-6274-CO11-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6274-CO116-Z = 'Y'
               MOVE WK01-DATE-6274-CO116 TO COMM-DATE-6274-CO11 OF
                FCOWM11(0006)
           END-IF.
           MOVE WK01-DATE-6274-CO116-F TO WK01-DATE-6274-CO11-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6274-CO117-Z = 'Y'
               MOVE WK01-DATE-6274-CO117 TO COMM-DATE-6274-CO11 OF
                FCOWM11(0007)
           END-IF.
           MOVE WK01-DATE-6274-CO117-F TO WK01-DATE-6274-CO11-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6274-CO118-Z = 'Y'
               MOVE WK01-DATE-6274-CO118 TO COMM-DATE-6274-CO11 OF
                FCOWM11(0008)
           END-IF.
           MOVE WK01-DATE-6274-CO118-F TO WK01-DATE-6274-CO11-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6274-CO119-Z = 'Y'
               MOVE WK01-DATE-6274-CO119 TO COMM-DATE-6274-CO11 OF
                FCOWM11(0009)
           END-IF.
           MOVE WK01-DATE-6274-CO119-F TO WK01-DATE-6274-CO11-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0C-Z = 'Y'
               MOVE WK01-TEXT-SY0C TO ARCHVD-TEXT-SY01 OF FZYWG01M
           END-IF.
