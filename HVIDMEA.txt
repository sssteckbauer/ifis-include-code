       975-TO-R4075-UNIT-MEA SECTION.
           MOVE MEA-USER-CD TO
               USER-CODE-4075 OF R4075-UNIT-MEA
                                                                     .
           MOVE MEA-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4075 OF R4075-UNIT-MEA
                                                                     .
           MOVE MEA-UNVRS-CD TO
               UNVRS-CODE-4075 OF R4075-UNIT-MEA
                                                                     .
           MOVE MEA-UNIT-MEA-CD TO
               UNIT-MEA-CODE-4075 OF R4075-UNIT-MEA
                                                                     .
           MOVE MEA-UNIT-MEA-DESC TO
               UNIT-MEA-DESC-4075 OF R4075-UNIT-MEA
                                                                     .
           MOVE MEA-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4075 OF R4075-UNIT-MEA
                                                                     .
           MOVE MEA-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4075 OF R4075-UNIT-MEA
                                                                     .
           MOVE MEA-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4075 OF R4075-UNIT-MEA
                                                                     .
       975-TO-R4075-UNIT-MEA-EXIT.
           EXIT.
