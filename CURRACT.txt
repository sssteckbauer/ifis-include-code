      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4014-ACTT' TO DBLINK-CURR-PARAGRAPH.         276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4014-ACTT' TO RECORD-NAME.                            276 BRTN
YYY991     MOVE 'F-ACCT' TO AREA-NAME.                                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4014-ACTT TO DBLINK-RECORD-MADE-CURRENT.        276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4014-ACTT-EXIT                           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE ACT-ACCT-TYP-CD TO DBLINK-R4014-ACTT-01             276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4014-ACTT TO DBLINK-VIEW-NAME-CURRENT.          276 BRTN
YYY991     MOVE DBLINK-R4014-ACTT-KEY TO DBLINK-VIEW-200-CURRENT.       276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4014-4015-O                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4014-ACTT-01 TO DBLINK-S-4014-4015-O-01.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4014-4015-O-KEY-M.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4014-4015-P                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4014-ACTT-01 TO DBLINK-S-4014-4015-P-01.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4014-4015-P-KEY-M.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4014-4034                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4014-ACTT-01 TO DBLINK-S-4014-4034-01.          276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4014-4034-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-ACTT                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ACT-ACCT-TYP-CD TO DBLINK-S-INDX-ACTT-01.               276 BRTN
