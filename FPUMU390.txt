    ***Created by Convert/DC version V8R03 on 12/04/00 at 11:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PO-CLASS-CODE-PU39-Z = 'Y'
               MOVE WK01-PO-CLASS-CODE-PU39 TO RQST-PO-CLASS-CODE-PU39
                OF FPUWK39
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU391-Z = 'Y'
               MOVE WK01-CODE-PU391 TO ACTN-CODE-PU39 OF FPUWM39(0001)
           END-IF.
           MOVE WK01-CODE-PU391-F TO WK01-CODE-PU39-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU392-Z = 'Y'
               MOVE WK01-CODE-PU392 TO ACTN-CODE-PU39 OF FPUWM39(0002)
           END-IF.
           MOVE WK01-CODE-PU392-F TO WK01-CODE-PU39-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU393-Z = 'Y'
               MOVE WK01-CODE-PU393 TO ACTN-CODE-PU39 OF FPUWM39(0003)
           END-IF.
           MOVE WK01-CODE-PU393-F TO WK01-CODE-PU39-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU394-Z = 'Y'
               MOVE WK01-CODE-PU394 TO ACTN-CODE-PU39 OF FPUWM39(0004)
           END-IF.
           MOVE WK01-CODE-PU394-F TO WK01-CODE-PU39-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU395-Z = 'Y'
               MOVE WK01-CODE-PU395 TO ACTN-CODE-PU39 OF FPUWM39(0005)
           END-IF.
           MOVE WK01-CODE-PU395-F TO WK01-CODE-PU39-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU396-Z = 'Y'
               MOVE WK01-CODE-PU396 TO ACTN-CODE-PU39 OF FPUWM39(0006)
           END-IF.
           MOVE WK01-CODE-PU396-F TO WK01-CODE-PU39-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4084-PU391-Z = 'Y'
               MOVE WK01-CLASS-CODE-4084-PU391 TO
                PO-CLASS-CODE-4084-PU39 OF FPUWM39(0001)
           END-IF.
           MOVE WK01-CLASS-CODE-4084-PU391-F TO
                WK01-CLASS-CODE-4084-PU39-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4084-PU392-Z = 'Y'
               MOVE WK01-CLASS-CODE-4084-PU392 TO
                PO-CLASS-CODE-4084-PU39 OF FPUWM39(0002)
           END-IF.
           MOVE WK01-CLASS-CODE-4084-PU392-F TO
                WK01-CLASS-CODE-4084-PU39-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4084-PU393-Z = 'Y'
               MOVE WK01-CLASS-CODE-4084-PU393 TO
                PO-CLASS-CODE-4084-PU39 OF FPUWM39(0003)
           END-IF.
           MOVE WK01-CLASS-CODE-4084-PU393-F TO
                WK01-CLASS-CODE-4084-PU39-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4084-PU394-Z = 'Y'
               MOVE WK01-CLASS-CODE-4084-PU394 TO
                PO-CLASS-CODE-4084-PU39 OF FPUWM39(0004)
           END-IF.
           MOVE WK01-CLASS-CODE-4084-PU394-F TO
                WK01-CLASS-CODE-4084-PU39-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4084-PU395-Z = 'Y'
               MOVE WK01-CLASS-CODE-4084-PU395 TO
                PO-CLASS-CODE-4084-PU39 OF FPUWM39(0005)
           END-IF.
           MOVE WK01-CLASS-CODE-4084-PU395-F TO
                WK01-CLASS-CODE-4084-PU39-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4084-PU396-Z = 'Y'
               MOVE WK01-CLASS-CODE-4084-PU396 TO
                PO-CLASS-CODE-4084-PU39 OF FPUWM39(0006)
           END-IF.
           MOVE WK01-CLASS-CODE-4084-PU396-F TO
                WK01-CLASS-CODE-4084-PU39-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4084-PU391-Z = 'Y'
               MOVE WK01-CLASS-DESC-4084-PU391 TO
                PO-CLASS-DESC-4084-PU39 OF FPUWM39(0001)
           END-IF.
           MOVE WK01-CLASS-DESC-4084-PU391-F TO
                WK01-CLASS-DESC-4084-PU39-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4084-PU392-Z = 'Y'
               MOVE WK01-CLASS-DESC-4084-PU392 TO
                PO-CLASS-DESC-4084-PU39 OF FPUWM39(0002)
           END-IF.
           MOVE WK01-CLASS-DESC-4084-PU392-F TO
                WK01-CLASS-DESC-4084-PU39-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4084-PU393-Z = 'Y'
               MOVE WK01-CLASS-DESC-4084-PU393 TO
                PO-CLASS-DESC-4084-PU39 OF FPUWM39(0003)
           END-IF.
           MOVE WK01-CLASS-DESC-4084-PU393-F TO
                WK01-CLASS-DESC-4084-PU39-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4084-PU394-Z = 'Y'
               MOVE WK01-CLASS-DESC-4084-PU394 TO
                PO-CLASS-DESC-4084-PU39 OF FPUWM39(0004)
           END-IF.
           MOVE WK01-CLASS-DESC-4084-PU394-F TO
                WK01-CLASS-DESC-4084-PU39-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4084-PU395-Z = 'Y'
               MOVE WK01-CLASS-DESC-4084-PU395 TO
                PO-CLASS-DESC-4084-PU39 OF FPUWM39(0005)
           END-IF.
           MOVE WK01-CLASS-DESC-4084-PU395-F TO
                WK01-CLASS-DESC-4084-PU39-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4084-PU396-Z = 'Y'
               MOVE WK01-CLASS-DESC-4084-PU396 TO
                PO-CLASS-DESC-4084-PU39 OF FPUWM39(0006)
           END-IF.
           MOVE WK01-CLASS-DESC-4084-PU396-F TO
                WK01-CLASS-DESC-4084-PU39-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU391-Z = 'Y'
               MOVE WK01-DATE-4084-PU391 TO START-DATE-4084-PU39 OF
                FPUWM39(0001)
           END-IF.
           MOVE WK01-DATE-4084-PU391-F TO WK01-DATE-4084-PU39-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU392-Z = 'Y'
               MOVE WK01-DATE-4084-PU392 TO START-DATE-4084-PU39 OF
                FPUWM39(0002)
           END-IF.
           MOVE WK01-DATE-4084-PU392-F TO WK01-DATE-4084-PU39-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU393-Z = 'Y'
               MOVE WK01-DATE-4084-PU393 TO START-DATE-4084-PU39 OF
                FPUWM39(0003)
           END-IF.
           MOVE WK01-DATE-4084-PU393-F TO WK01-DATE-4084-PU39-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU394-Z = 'Y'
               MOVE WK01-DATE-4084-PU394 TO START-DATE-4084-PU39 OF
                FPUWM39(0004)
           END-IF.
           MOVE WK01-DATE-4084-PU394-F TO WK01-DATE-4084-PU39-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU395-Z = 'Y'
               MOVE WK01-DATE-4084-PU395 TO START-DATE-4084-PU39 OF
                FPUWM39(0005)
           END-IF.
           MOVE WK01-DATE-4084-PU395-F TO WK01-DATE-4084-PU39-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU396-Z = 'Y'
               MOVE WK01-DATE-4084-PU396 TO START-DATE-4084-PU39 OF
                FPUWM39(0006)
           END-IF.
           MOVE WK01-DATE-4084-PU396-F TO WK01-DATE-4084-PU39-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3A1-Z = 'Y'
               MOVE WK01-DATE-4084-PU3A1 TO END-DATE-4084-PU39 OF
                FPUWM39(0001)
           END-IF.
           MOVE WK01-DATE-4084-PU3A1-F TO WK01-DATE-4084-PU3A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3A2-Z = 'Y'
               MOVE WK01-DATE-4084-PU3A2 TO END-DATE-4084-PU39 OF
                FPUWM39(0002)
           END-IF.
           MOVE WK01-DATE-4084-PU3A2-F TO WK01-DATE-4084-PU3A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3A3-Z = 'Y'
               MOVE WK01-DATE-4084-PU3A3 TO END-DATE-4084-PU39 OF
                FPUWM39(0003)
           END-IF.
           MOVE WK01-DATE-4084-PU3A3-F TO WK01-DATE-4084-PU3A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3A4-Z = 'Y'
               MOVE WK01-DATE-4084-PU3A4 TO END-DATE-4084-PU39 OF
                FPUWM39(0004)
           END-IF.
           MOVE WK01-DATE-4084-PU3A4-F TO WK01-DATE-4084-PU3A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3A5-Z = 'Y'
               MOVE WK01-DATE-4084-PU3A5 TO END-DATE-4084-PU39 OF
                FPUWM39(0005)
           END-IF.
           MOVE WK01-DATE-4084-PU3A5-F TO WK01-DATE-4084-PU3A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3A6-Z = 'Y'
               MOVE WK01-DATE-4084-PU3A6 TO END-DATE-4084-PU39 OF
                FPUWM39(0006)
           END-IF.
           MOVE WK01-DATE-4084-PU3A6-F TO WK01-DATE-4084-PU3A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3B1-Z = 'Y'
               MOVE WK01-DATE-4084-PU3B1 TO ACTVY-DATE-4084-PU39 OF
                FPUWM39(0001)
           END-IF.
           MOVE WK01-DATE-4084-PU3B1-F TO WK01-DATE-4084-PU3B-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3B2-Z = 'Y'
               MOVE WK01-DATE-4084-PU3B2 TO ACTVY-DATE-4084-PU39 OF
                FPUWM39(0002)
           END-IF.
           MOVE WK01-DATE-4084-PU3B2-F TO WK01-DATE-4084-PU3B-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3B4-Z = 'Y'
               MOVE WK01-DATE-4084-PU3B4 TO ACTVY-DATE-4084-PU39 OF
                FPUWM39(0004)
           END-IF.
           MOVE WK01-DATE-4084-PU3B4-F TO WK01-DATE-4084-PU3B-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3B5-Z = 'Y'
               MOVE WK01-DATE-4084-PU3B5 TO ACTVY-DATE-4084-PU39 OF
                FPUWM39(0005)
           END-IF.
           MOVE WK01-DATE-4084-PU3B5-F TO WK01-DATE-4084-PU3B-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3B6-Z = 'Y'
               MOVE WK01-DATE-4084-PU3B6 TO ACTVY-DATE-4084-PU39 OF
                FPUWM39(0006)
           END-IF.
           MOVE WK01-DATE-4084-PU3B6-F TO WK01-DATE-4084-PU3B-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4084-PU3B3-Z = 'Y'
               MOVE WK01-DATE-4084-PU3B3 TO ACTVY-DATE-4084-PU39 OF
                FPUWM39(0003)
           END-IF.
           MOVE WK01-DATE-4084-PU3B3-F TO WK01-DATE-4084-PU3B-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4084-PU391-Z = 'Y'
               MOVE WK01-AMT-4084-PU391 TO MAX-AMT-4084-PU39 OF
                FPUWM39(0001)
           END-IF.
           MOVE WK01-AMT-4084-PU391-F TO WK01-AMT-4084-PU39-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4084-PU392-Z = 'Y'
               MOVE WK01-AMT-4084-PU392 TO MAX-AMT-4084-PU39 OF
                FPUWM39(0002)
           END-IF.
           MOVE WK01-AMT-4084-PU392-F TO WK01-AMT-4084-PU39-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4084-PU393-Z = 'Y'
               MOVE WK01-AMT-4084-PU393 TO MAX-AMT-4084-PU39 OF
                FPUWM39(0003)
           END-IF.
           MOVE WK01-AMT-4084-PU393-F TO WK01-AMT-4084-PU39-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4084-PU394-Z = 'Y'
               MOVE WK01-AMT-4084-PU394 TO MAX-AMT-4084-PU39 OF
                FPUWM39(0004)
           END-IF.
           MOVE WK01-AMT-4084-PU394-F TO WK01-AMT-4084-PU39-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4084-PU395-Z = 'Y'
               MOVE WK01-AMT-4084-PU395 TO MAX-AMT-4084-PU39 OF
                FPUWM39(0005)
           END-IF.
           MOVE WK01-AMT-4084-PU395-F TO WK01-AMT-4084-PU39-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4084-PU396-Z = 'Y'
               MOVE WK01-AMT-4084-PU396 TO MAX-AMT-4084-PU39 OF
                FPUWM39(0006)
           END-IF.
           MOVE WK01-AMT-4084-PU396-F TO WK01-AMT-4084-PU39-F (6).
