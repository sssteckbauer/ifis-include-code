      *%---------------------------------------------------------------*
      *%                   200-FSEII01R ROUTINE                        *
      *%---------------------------------------------------------------*
      ****----------------------------------------------------------****
      **  AUTHOR    DATE    DESCRIPTION                                *
      **  ------- --------  ----------------------------------------   *
      **  DEVRRE  07/06/01  CHANGE LINK TO CALL                        *
      **                    REMOVE LINK TO FSYDU10                     *
      ****----------------------------------------------------------****
       200-FSEII01R.

      ******************************************************************
      ***  LINK TO SECURITY DIALOG TO EVALUATE AUTHORIZATION AND
      ***     PERMISSIONS
      ***  IF ACCESS IS DENIED, TRANSFER TO SECURITY VIOLATION SCREEN.
      ******************************************************************

           MOVE 'FSYDI07R' TO AC99W-NEXT-PROGRAM.

           CALL AC99W-NEXT-PROGRAM  USING
                                  DFHEIBLK
                                  DFHCOMMAREA
                                  ADSO-APPLICATION-GLOBAL-RECORD
                                  FSYWG00D
                                  FSEWG00D
                                  FSYWG01D.

           IF ATHRZN-FLAG-SY01 = 'N'
               MOVE 'I'        TO SCRTY-VLTN-FLAG-SY01
               MOVE AGR-CURRENT-RESPONSE TO AGR-PASSED-THREE
               MOVE 'SECURITY' TO AGR-PASSED-ONE
               MOVE 'TRANSFER' TO AGR-CURRENT-RESPONSE
               PERFORM 390-EXEC-NEXT-FUNCTION
                  THRU 390-EXEC-NEXT-FUNCTION-EXIT
           END-IF.

       200-FSEII01R-EXIT.  EXIT.
