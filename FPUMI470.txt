    ***Created by Convert/DC version V8R03 on 07/05/00 at 16:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-PU47-Z = 'Y'
               MOVE WK01-FLAG-PU47 TO SELECT-FLAG-PU47 OF FPUWK47
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU471-Z = 'Y'
               MOVE WK01-CODE-PU471 TO ACTN-CODE-PU47 OF FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4083-PU471-Z = 'Y'
               MOVE WK01-NMBR-4083-PU471 TO PO-NMBR-4083-PU47 OF
                FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-4096-PU471-Z = 'Y'
               MOVE WK01-SEQ-NMBR-4096-PU471 TO CHNG-SEQ-NMBR-4096-PU47
                OF FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4072-PU471-Z = 'Y'
               MOVE WK01-NAME-4072-PU471 TO BUYER-NAME-4072-PU47 OF
                FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4096-PU471-Z = 'Y'
               MOVE WK01-DATE-4096-PU471 TO ORDER-DATE-4096-PU47 OF
                FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BY-DATE-4096-PU471-Z = 'Y'
               MOVE WK01-BY-DATE-4096-PU471 TO DLVRY-BY-DATE-4096-PU47
                OF FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CMPLT-IND-4096-PU471-Z = 'Y'
               MOVE WK01-CMPLT-IND-4096-PU471 TO PO-CMPLT-IND-4096-PU47
                OF FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-PU471-Z = 'Y'
               MOVE WK01-KEY-6311-PU471 TO NAME-KEY-6311-PU47 OF
                FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4096-PU471-Z = 'Y'
               MOVE WK01-ERROR-IND-4096-PU471 TO
                HDR-ERROR-IND-4096-PU47 OF FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-DTL-4096-PU471-Z = 'Y'
               MOVE WK01-CNTR-DTL-4096-PU471 TO HDR-CNTR-DTL-4096-PU47
                OF FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-ACCT-4096-PU471-Z = 'Y'
               MOVE WK01-CNTR-ACCT-4096-PU471 TO
                HDR-CNTR-ACCT-4096-PU47 OF FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4096-PU471-Z = 'Y'
               MOVE WK01-AMT-4096-PU471 TO TOTAL-AMT-4096-PU47 OF
                FPUWM47(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU472-Z = 'Y'
               MOVE WK01-CODE-PU472 TO ACTN-CODE-PU47 OF FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4083-PU472-Z = 'Y'
               MOVE WK01-NMBR-4083-PU472 TO PO-NMBR-4083-PU47 OF
                FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-4096-PU472-Z = 'Y'
               MOVE WK01-SEQ-NMBR-4096-PU472 TO CHNG-SEQ-NMBR-4096-PU47
                OF FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4072-PU472-Z = 'Y'
               MOVE WK01-NAME-4072-PU472 TO BUYER-NAME-4072-PU47 OF
                FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4096-PU472-Z = 'Y'
               MOVE WK01-DATE-4096-PU472 TO ORDER-DATE-4096-PU47 OF
                FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BY-DATE-4096-PU472-Z = 'Y'
               MOVE WK01-BY-DATE-4096-PU472 TO DLVRY-BY-DATE-4096-PU47
                OF FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CMPLT-IND-4096-PU472-Z = 'Y'
               MOVE WK01-CMPLT-IND-4096-PU472 TO PO-CMPLT-IND-4096-PU47
                OF FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-PU472-Z = 'Y'
               MOVE WK01-KEY-6311-PU472 TO NAME-KEY-6311-PU47 OF
                FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4096-PU472-Z = 'Y'
               MOVE WK01-ERROR-IND-4096-PU472 TO
                HDR-ERROR-IND-4096-PU47 OF FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-DTL-4096-PU472-Z = 'Y'
               MOVE WK01-CNTR-DTL-4096-PU472 TO HDR-CNTR-DTL-4096-PU47
                OF FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-ACCT-4096-PU472-Z = 'Y'
               MOVE WK01-CNTR-ACCT-4096-PU472 TO
                HDR-CNTR-ACCT-4096-PU47 OF FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4096-PU472-Z = 'Y'
               MOVE WK01-AMT-4096-PU472 TO TOTAL-AMT-4096-PU47 OF
                FPUWM47(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU473-Z = 'Y'
               MOVE WK01-CODE-PU473 TO ACTN-CODE-PU47 OF FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4083-PU473-Z = 'Y'
               MOVE WK01-NMBR-4083-PU473 TO PO-NMBR-4083-PU47 OF
                FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-4096-PU473-Z = 'Y'
               MOVE WK01-SEQ-NMBR-4096-PU473 TO CHNG-SEQ-NMBR-4096-PU47
                OF FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4072-PU473-Z = 'Y'
               MOVE WK01-NAME-4072-PU473 TO BUYER-NAME-4072-PU47 OF
                FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4096-PU473-Z = 'Y'
               MOVE WK01-DATE-4096-PU473 TO ORDER-DATE-4096-PU47 OF
                FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BY-DATE-4096-PU473-Z = 'Y'
               MOVE WK01-BY-DATE-4096-PU473 TO DLVRY-BY-DATE-4096-PU47
                OF FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CMPLT-IND-4096-PU473-Z = 'Y'
               MOVE WK01-CMPLT-IND-4096-PU473 TO PO-CMPLT-IND-4096-PU47
                OF FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-PU473-Z = 'Y'
               MOVE WK01-KEY-6311-PU473 TO NAME-KEY-6311-PU47 OF
                FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4096-PU473-Z = 'Y'
               MOVE WK01-ERROR-IND-4096-PU473 TO
                HDR-ERROR-IND-4096-PU47 OF FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-DTL-4096-PU473-Z = 'Y'
               MOVE WK01-CNTR-DTL-4096-PU473 TO HDR-CNTR-DTL-4096-PU47
                OF FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-ACCT-4096-PU473-Z = 'Y'
               MOVE WK01-CNTR-ACCT-4096-PU473 TO
                HDR-CNTR-ACCT-4096-PU47 OF FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4096-PU473-Z = 'Y'
               MOVE WK01-AMT-4096-PU473 TO TOTAL-AMT-4096-PU47 OF
                FPUWM47(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PO-NMBR-PU47-Z = 'Y'
               MOVE WK01-PO-NMBR-PU47 TO RQST-PO-NMBR-PU47 OF FPUWK47
           END-IF.
