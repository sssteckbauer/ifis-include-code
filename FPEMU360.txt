    ***Created by Convert/DC version V8R03 on 12/11/00 at 09:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRSN-ID-6157-P36-Z = 'Y'
               MOVE WK01-PRSN-ID-6157-P36 TO
                WORK-PRSN-ID-6157-PE36 OF FPEWK36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6157-PE36-Z = 'Y'
               MOVE WK01-KEY-6157-PE36 TO NAME-KEY-6157-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE36-Z = 'Y'
               MOVE WK01-CODE-PE36 TO ACTN-CODE-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRSN-ID-6117-P36-Z = 'Y'
               MOVE WK01-PRSN-ID-6117-P36 TO
                WORK-PRSN-ID-6117-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SLTN-DESC-6117-PE36-Z = 'Y'
               MOVE WK01-SLTN-DESC-6117-PE36 TO
                NAME-SLTN-DESC-6117-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6117-PE36-Z = 'Y'
               MOVE WK01-KEY-6117-PE36 TO NAME-KEY-6117-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FULL-NAME-6117-PE36-Z = 'Y'
               MOVE WK01-FULL-NAME-6117-PE36 TO
                PRSN-FULL-NAME-6117-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6117-PE36-Z = 'Y'
               MOVE WK01-FLAG-6117-PE36 TO GNDR-FLAG-6117-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2158-PE36-Z = 'Y'
               MOVE WK01-CODE-2158-PE36 TO ETHNC-CODE-2158-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2351-PE36-Z = 'Y'
               MOVE WK01-CODE-2351-PE36 TO RLGN-CODE-2351-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6117-PE3A-Z = 'Y'
               MOVE WK01-FLAG-6117-PE3A TO DCSD-FLAG-6117-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NEED-CODE-2159-PE361-Z = 'Y'
               MOVE WK01-NEED-CODE-2159-PE361 TO
                SPCL-NEED-CODE-2159-PE36 OF FPEWM36(0001)
           END-IF.
           MOVE WK01-NEED-CODE-2159-PE361-F TO
                WK01-NEED-CODE-2159-PE36-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NEED-CODE-2159-PE362-Z = 'Y'
               MOVE WK01-NEED-CODE-2159-PE362 TO
                SPCL-NEED-CODE-2159-PE36 OF FPEWM36(0002)
           END-IF.
           MOVE WK01-NEED-CODE-2159-PE362-F TO
                WK01-NEED-CODE-2159-PE36-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NEED-CODE-2159-PE363-Z = 'Y'
               MOVE WK01-NEED-CODE-2159-PE363 TO
                SPCL-NEED-CODE-2159-PE36 OF FPEWM36(0003)
           END-IF.
           MOVE WK01-NEED-CODE-2159-PE363-F TO
                WK01-NEED-CODE-2159-PE36-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NEED-CODE-2159-PE364-Z = 'Y'
               MOVE WK01-NEED-CODE-2159-PE364 TO
                SPCL-NEED-CODE-2159-PE36 OF FPEWM36(0004)
           END-IF.
           MOVE WK01-NEED-CODE-2159-PE364-F TO
                WK01-NEED-CODE-2159-PE36-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-2159-PE361-Z = 'Y'
               MOVE WK01-DESC-2159-PE361 TO LONG-DESC-2159-PE36 OF
                FPEWM36(0001)
           END-IF.
           MOVE WK01-DESC-2159-PE361-F TO WK01-DESC-2159-PE36-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-2159-PE362-Z = 'Y'
               MOVE WK01-DESC-2159-PE362 TO LONG-DESC-2159-PE36 OF
                FPEWM36(0002)
           END-IF.
           MOVE WK01-DESC-2159-PE362-F TO WK01-DESC-2159-PE36-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-2159-PE363-Z = 'Y'
               MOVE WK01-DESC-2159-PE363 TO LONG-DESC-2159-PE36 OF
                FPEWM36(0003)
           END-IF.
           MOVE WK01-DESC-2159-PE363-F TO WK01-DESC-2159-PE36-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-2159-PE364-Z = 'Y'
               MOVE WK01-DESC-2159-PE364 TO LONG-DESC-2159-PE36 OF
                FPEWM36(0004)
           END-IF.
           MOVE WK01-DESC-2159-PE364-F TO WK01-DESC-2159-PE36-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6117-PE36-Z = 'Y'
               MOVE WK01-DATE-6117-PE36 TO BIRTH-DATE-6117-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-2158-PE36-Z = 'Y'
               MOVE WK01-DESC-2158-PE36 TO LONG-DESC-2158-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6117-PE36-Z = 'Y'
               MOVE WK01-DESC-6117-PE36 TO SHORT-DESC-6117-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-2351-PE36-Z = 'Y'
               MOVE WK01-DESC-2351-PE36 TO LONG-DESC-2351-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEC-NO-6117-PE36-Z = 'Y'
               MOVE WK01-SEC-NO-6117-PE36 TO
                SOC-SEC-NO-6117-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6117-PE3A-Z = 'Y'
               MOVE WK01-DATE-6117-PE3A TO DCSD-DATE-6117-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SFX-DESC-6117-PE36-Z = 'Y'
               MOVE WK01-SFX-DESC-6117-PE36 TO NAME-SFX-DESC-6117-PE36
                OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2368-PE36-Z = 'Y'
               MOVE WK01-CODE-2368-PE36 TO MRTL-CODE-2368-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-2368-PE36-Z = 'Y'
               MOVE WK01-DESC-2368-PE36 TO LONG-DESC-2368-PE36 OF
                FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-6117-PE36-Z = 'Y'
               MOVE WK01-NMBR-6117-PE36 TO PAN-NMBR-6117-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-PE36-Z = 'Y'
               MOVE WK01-DESC-PE36 TO PAN-DESC-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-PE36-Z = 'Y'
               MOVE WK01-DATE-PE36 TO PAN-DATE-PE36 OF FPEWM36
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0C-Z = 'Y'
               MOVE WK01-TEXT-SY0C TO ARCHVD-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6117-PE3B-Z = 'Y'
               MOVE WK01-DATE-6117-PE3B TO ARCHVD-DATE-6117-PE36 OF
                FPEWM36
           END-IF.
