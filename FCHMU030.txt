    ***Created by Convert/DC version V8R03 on 11/17/00 at 18:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH03-Z = 'Y'
               MOVE WK01-CODE-CH03 TO ACTN-CODE-CH03 OF FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4026-CH03-Z = 'Y'
               MOVE WK01-CODE-4026-CH03 TO COA-CODE-4026-CH03 OF FCHWK03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COST-CODE-4026-CH03-Z = 'Y'
               MOVE WK01-COST-CODE-4026-CH03 TO
                INDRT-COST-CODE-4026-CH03 OF FCHWK03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COST-DESC-4065-CH03-Z = 'Y'
               MOVE WK01-COST-DESC-4065-CH03 TO
                INDRT-COST-DESC-4065-CH03 OF FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4065-CH03-Z = 'Y'
               MOVE WK01-DATE-4065-CH03 TO START-DATE-4065-CH03 OF
                FCHWK03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHNG-DATE-CH03-Z = 'Y'
               MOVE WK01-CHNG-DATE-CH03 TO NEXT-CHNG-DATE-CH03 OF
                FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4065-CH03-Z = 'Y'
               MOVE WK01-DESC-4065-CH03 TO STATUS-DESC-4065-CH03 OF
                FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4065-CH0A-Z = 'Y'
               MOVE WK01-DATE-4065-CH0A TO END-DATE-4065-CH03 OF FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4026-CH03-Z = 'Y'
               MOVE WK01-DATE-4026-CH03 TO ACTVY-DATE-4026-CH03 OF
                FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STAMP-4065-CH03-Z = 'Y'
               MOVE WK01-STAMP-4065-CH03 TO TIME-STAMP-4065-CH03 OF
                FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-CH03-Z = 'Y'
               MOVE WK01-PM-FLAG-CH03 TO AM-PM-FLAG-CH03 OF FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COST-BASIS-4065-CH03-Z = 'Y'
               MOVE WK01-COST-BASIS-4065-CH03 TO
                INDRT-COST-BASIS-4065-CH03 OF FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TO-BASIS-IND-4065-CH03-Z = 'Y'
               MOVE WK01-TO-BASIS-IND-4065-CH03 TO
                APPLY-TO-BASIS-IND-4065-CH03 OF FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CST-STNDD-PCT-4065-CH0-Z = 'Y'
               MOVE WK01-CST-STNDD-PCT-4065-CH0 TO
                INDRT-COST-STNDD-PCT-4065-CH03 OF FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4065-CH03-Z = 'Y'
               MOVE WK01-IND-4065-CH03 TO CMPLT-IND-4065-CH03 OF FCHWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PO-SUBCNT-EXCL-IND-CH03-Z = 'Y'
               MOVE WK01-PO-SUBCNT-EXCL-IND-CH03   TO
                       PO-SUBCNT-EXCL-IND-CH03
           END-IF.
