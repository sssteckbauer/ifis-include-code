       975-TO-R4062-BANK-EFCTV SECTION.
           MOVE BKE-USER-CD TO
               USER-CODE-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               TIME-STAMP-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-STATUS TO
               STATUS-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-COA-CD TO
               COA-CODE-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-BANK-ACCT-NBR TO
               BANK-ACCT-NMBR-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-BANK-CD-DESC TO
               BANK-CODE-DESC-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-BANK-FUND-CD TO
               BANK-FUND-CODE-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-CASH-ACCT-CD TO
               CASH-ACCT-CODE-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-INTRFND-ACCT-CD TO
               INTRFND-ACCT-CODE-4062 OF R4062-BANK-EFCTV
                                                                     .
           MOVE BKE-BANK-IREF-ID TO
               BANK-INTRL-REF-ID-4062 OF R4062-BANK-EFCTV
                                                                     .
       975-TO-R4062-BANK-EFCTV-EXIT.
           EXIT.
