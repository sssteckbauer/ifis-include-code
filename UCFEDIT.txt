      ******************************************************************
      *    PROCEDURAL INCLUDE UCFEDIT (FOR COMMON UCF_PERM_FUND_V EDITS)
      *    REQUIRES RECOMPILATION OF THE FOLLOWING IFIS PROGRAMS:
      *        FCHDU04 / CICSDB2  (FUNCTION = FUNDL5  )
      *        FGADU77 / CICSDB2  (FUNCTION = FUNDPERM)
      *        UGAP303 / COBDB2        (JOB = GAM560  )
      ******************************************************************
      * -------------------- MAINTENANCE HISTORY -----------------------
FP2392* 06/22/10 DEVHV  ALLOW SPONSOR CATEGORY '08' FOR FUND GROUP CODE
      *                 '404210'. MOVE THESE TYPES OF EDITS FROM SPONSOR
      *                 CODE TO THE SPONSOR CATAGORY PARAGRAPH.
      * 07/14/10 DEVHV  REMOVED EDIT REQUIRING FUND GROUP 406310 TO BE
      *                 FOR STATE-GOVERNMENT (SPONSOR CATAGORY '02').
FP4255* 05/11/12 DEVMLJ ALLOW '61' METHOD OF PAYMENT, CHANGE ERROR
      *                 MESSAGE FROM 570057 TO 000106
FP0018* 05/22/12 DEVMLJ REMOVE EDIT FOR NUMERIC FUND NUMBER
      ******************************************************************

       200-EDIT-FUND-PERM.

           IF GPF-BUDT-FUND-CD = UCF-BUDT-FUND-CD
               NEXT SENTENCE
           ELSE
              IF GPF-BUDT-FUND-CD = '*'
                 IF UCF-BUDT-FUND-CD = 'B' OR 'N'
                   NEXT SENTENCE
                 ELSE
                   ADD 1 TO SUB1
                   MOVE 'BUDGETED FUND CD'    TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-BUDT-FUND-CD      TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570052                TO UCF-ERROR-MSG (SUB1)
                 END-IF
              ELSE
                   ADD 1 TO SUB1
                   MOVE 'BUDGETED FUND CD'    TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-BUDT-FUND-CD      TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570052                TO UCF-ERROR-MSG (SUB1)
              END-IF
           END-IF.

           IF GPF-FUND-RESTR-CD  = UCF-FUND-RESTR-CD
                   NEXT SENTENCE
           ELSE
              IF GPF-FUND-RESTR-CD = '*'
                IF UCF-FUND-RESTR-CD = 'U' OR 'R' OR 'G'
                   NEXT SENTENCE
                ELSE
                   ADD 1 TO SUB1
                   MOVE 'RSTRCTN CODE'        TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-FUND-RESTR-CD    TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570054                TO UCF-ERROR-MSG (SUB1)
                END-IF
              ELSE
                   ADD 1 TO SUB1
                   MOVE 'RSTRCTN CODE'        TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-FUND-RESTR-CD    TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570054                TO UCF-ERROR-MSG (SUB1)
              END-IF
           END-IF.

           PERFORM 200-EDIT-EMDWNT-CD
              THRU 200-EDIT-EMDWNT-CD-EXIT.

           PERFORM 200-EDIT-PAYMETHD
              THRU 200-EDIT-PAYMETHD-EXIT.

           MOVE UCF-SPNSR-CAT-CD
                       TO WS-TEST-SPNSR-CATG-CD.

           PERFORM 200-EDIT-SPNSR-CD
              THRU 200-EDIT-SPNSR-CD-EXIT.

           PERFORM 200-EDIT-SPNSR-CTG-CD
              THRU 200-EDIT-SPNSR-CTG-CD-EXIT.

           PERFORM 200-EDIT-AWD-CD
              THRU 200-EDIT-AWD-CD-EXIT.

           PERFORM 200-EDIT-ON-OFF-CMPS
              THRU 200-EDIT-ON-OFF-CMPS-EXIT.

           PERFORM 200-EDIT-FED-FLW
              THRU 200-EDIT-FED-FLW-EXIT.

           PERFORM 200-EDIT-IDC-BS-CD
              THRU 200-EDIT-IDC-BS-CD-EXIT.

           PERFORM 200-EDIT-CFDA
              THRU 200-EDIT-CFDA-EXIT.

       200-EDIT-FUND-PERM-EXIT.
           EXIT.
      /
       200-EDIT-SPNSR-CD.
           IF GPF-SPNSR-CD = ' '
                IF UCF-SPNSR-CD > SPACES
                   PERFORM  200-VALID-SPNSCODE
                      THRU  200-VALID-SPNSCODE-EXIT
                END-IF
                GO TO 200-EDIT-SPNSR-CD-EXIT
           END-IF.

           IF UCF-FUND-NUMBER = '18999'
                  OR '20999' OR '24999' OR '28999' OR '59999'
              IF UCF-SPNSR-CD NOT = SPACES
                   ADD 1 TO SUB1
                   MOVE 'SPNSR CODE'      TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-SPNSR-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570102            TO UCF-ERROR-MSG (SUB1)
              END-IF
              GO TO 200-EDIT-SPNSR-CD-EXIT
           END-IF.

           IF GPF-SPNSR-CD = 'Z'
               IF UCF-SPNSR-CD NOT = SPACES
                   ADD 1    TO SUB1
                   MOVE 'SPNSR CODE'      TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-SPNSR-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570102            TO UCF-ERROR-MSG (SUB1)
               END-IF
               GO TO 200-EDIT-SPNSR-CD-EXIT
           END-IF.

           IF GPF-SPNSR-CD = 'X'
              IF UCF-SPNSR-CD = SPACES
                   ADD 1 TO SUB1
                   MOVE 'SPNSR CODE'      TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-SPNSR-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570101            TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-EDIT-SPNSR-CD-EXIT
              END-IF
           END-IF.

           IF UCF-FUND-GRP-CD = '408420'
              IF UCF-SPNSR-CD = '9850'
                   ADD 1 TO SUB1
                   MOVE 'SPNSR CODE'      TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-SPNSR-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570034            TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-EDIT-SPNSR-CD-EXIT
              END-IF
           END-IF.

       200-EDIT-SPNSR-CD-EXIT.
           EXIT.
      /
       200-EDIT-SPNSR-CTG-CD.
           IF  UCF-SPNSR-CAT-CD = SPACES OR '01' OR '02' OR '03' OR '04'
                                         OR '05' OR '06' OR '07' OR '08'
                                         OR '09' OR '10' OR '11' OR '12'
                                         OR '13' OR '99'
                   NEXT SENTENCE
           ELSE
                   ADD 1 TO SUB1
                   MOVE 'SPNSR CATAGORY'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-SPNSR-CAT-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570200               TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-EDIT-SPNSR-CTG-CD-EXIT
           END-IF.

           IF GPF-SPNSR-CAT-CD = ' '
               GO TO 200-EDIT-SPNSR-CTG-CD-EXIT
           END-IF.

           IF UCF-FUND-NUMBER = '18999'
                  OR '20999' OR '24999' OR '28999' OR '59999'
               IF  UCF-SPNSR-CAT-CD NOT = SPACES
                   ADD 1 TO SUB1
                   MOVE 'SPNSR CATAGORY'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-SPNSR-CAT-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570202               TO UCF-ERROR-MSG (SUB1)
               END-IF
               GO TO 200-EDIT-SPNSR-CTG-CD-EXIT
           END-IF.

           IF GPF-SPNSR-CAT-CD = 'Z'
               IF UCF-SPNSR-CAT-CD NOT = SPACES
                   ADD 1 TO SUB1
FP2392             MOVE 'SPNSR CATAGORY'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-SPNSR-CAT-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570202               TO UCF-ERROR-MSG (SUB1)
               END-IF
               GO TO 200-EDIT-SPNSR-CTG-CD-EXIT
           END-IF.

           IF GPF-SPNSR-CAT-CD = 'X'
               IF  UCF-SPNSR-CAT-CD = SPACES
                   ADD 1 TO SUB1
                   MOVE 'SPNSR CATAGORY'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-SPNSR-CAT-CD     TO UCF-FIELD-VALUE (SUB1)
FP2392             MOVE 570200               TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-EDIT-SPNSR-CTG-CD-EXIT
               END-IF
           END-IF.

           IF UCF-FUND-GRP-CD = '100200' OR '406210' OR '406310'
               IF FEDERAL-GOVERNMENT
                   NEXT SENTENCE
               ELSE
                   ADD 1 TO SUB1
FP2392             MOVE 'SPNSR CATAGORY'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-SPNSR-CAT-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570201               TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-EDIT-SPNSR-CTG-CD-EXIT
               END-IF
           END-IF.

           IF UCF-FUND-GRP-CD = '404210'
               IF STATE-GOVERNMENT
FP2392         OR HGHR-EDU-INST-ASSN
                   NEXT SENTENCE
               ELSE
                   ADD 1 TO SUB1
FP2392             MOVE 'SPNSR CATAGORY'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-SPNSR-CAT-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570100               TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-EDIT-SPNSR-CTG-CD-EXIT
               END-IF
           END-IF.

           IF GPF-SPNSR-CD = 'X'
               PERFORM 200-VALID-SPNSCDCGX
                  THRU 200-VALID-SPNSCDCGX-EXIT
           END-IF.

       200-EDIT-SPNSR-CTG-CD-EXIT.
           EXIT.
      /
       200-VALID-SPNSCDCGX.
           MOVE UCF-SPNSR-CAT-CD  TO SPN-SPNSR-CATGRY-CD.
           MOVE UCF-SPNSR-CD      TO SPN-SPNSR-CD.
           MOVE 0600              TO DBLINK-CURR-IO-NUM.
           MOVE ZERO              TO ERROR-STATUS.

           EXEC SQL
              SELECT    SPNSR_CD
                 INTO : SPN-SPNSR-CD
              FROM   SPN_SPONSOR_CODE_V
              WHERE SPNSR_CATGRY_CD = :SPN-SPNSR-CATGRY-CD
               AND  SPNSR_CD        = :SPN-SPNSR-CD
           END-EXEC.

           IF SQLCODE = +100
               ADD 1 TO SUB1
FP2392         MOVE 'SPNSR CODE'         TO UCF-FIELD-NAME (SUB1)
               MOVE  UCF-SPNSR-CD        TO UCF-FIELD-VALUE (SUB1)
               MOVE 570103               TO UCF-ERROR-MSG (SUB1)
           END-IF.

       200-VALID-SPNSCDCGX-EXIT.
           EXIT.
      /
       200-VALID-SPNSCODE.
           MOVE UCF-SPNSR-CD      TO SPN-SPNSR-CD.
           MOVE 0601              TO DBLINK-CURR-IO-NUM.
           MOVE ZERO              TO ERROR-STATUS.

           EXEC SQL
              SELECT    SPNSR_CD
                 INTO : SPN-SPNSR-CD
              FROM   SPN_SPONSOR_CODE_V
              WHERE SPNSR_CD        = :SPN-SPNSR-CD
              FETCH FIRST ROW ONLY
           END-EXEC.

           IF SQLCODE = +100
               ADD 1 TO SUB1
               MOVE 'SPNSR CODE'         TO UCF-FIELD-NAME (SUB1)
               MOVE  UCF-SPNSR-CD        TO UCF-FIELD-VALUE (SUB1)
               MOVE 570034               TO UCF-ERROR-MSG (SUB1)
           END-IF.

       200-VALID-SPNSCODE-EXIT.
           EXIT.
      /
       200-EDIT-EMDWNT-CD.
           IF GPF-EMDWNT-RSTR-CD = ' '
              IF UCF-EMDWNT-RSTR-CD > SPACES
                   ADD 1 TO SUB1
                   MOVE 'ENDWMNT RSTRCTN CD'  TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-EMDWNT-RSTR-CD   TO UCF-FIELD-VALUE (SUB1)
FP2392             MOVE 570053                TO UCF-ERROR-MSG (SUB1)
              END-IF
              GO TO 200-EDIT-EMDWNT-CD-EXIT
           END-IF.


           IF GPF-EMDWNT-RSTR-CD = 'X'
              IF UCF-EMDWNT-RSTR-CD = SPACES
                   ADD 1 TO SUB1
                   MOVE 'ENDWMNT RSTRCTN CD'  TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-EMDWNT-RSTR-CD   TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570055                TO UCF-ERROR-MSG (SUB1)
              ELSE
                   PERFORM 200-VALID-EMDWNT-VALUE
                      THRU 200-VALID-EMDWNT-VALUE-EXIT
              END-IF
              GO TO 200-EDIT-EMDWNT-CD-EXIT
           END-IF.

           IF UCF-EMDWNT-RSTR-CD = LOW-VALUES
               MOVE SPACES TO UCF-EMDWNT-RSTR-CD
           END-IF.

           IF GPF-EMDWNT-RSTR-CD = 'Z'
                IF UCF-EMDWNT-RSTR-CD NOT = SPACES
                   ADD 1 TO SUB1
                   MOVE 'ENDWMNT RSTRCTN CD'  TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-EMDWNT-RSTR-CD   TO UCF-FIELD-VALUE (SUB1)
FP2392             MOVE 570053                TO UCF-ERROR-MSG (SUB1)
                END-IF
           END-IF.

       200-EDIT-EMDWNT-CD-EXIT.
           EXIT.
      /
       200-VALID-EMDWNT-VALUE.
           MOVE UCF-EMDWNT-RSTR-CD TO WS-ENDOWNMENT-CODE-TEST.

           IF  UCF-FUND-RESTR-CD = 'U'
                 IF VALID-END-1ST-FOR-U
                    NEXT SENTENCE
                 ELSE
                   ADD 1 TO SUB1
                   MOVE 'ENDWMNT RSTRCTN CD'  TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-EMDWNT-RSTR-CD   TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570053                TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-VALID-EMDWNT-VALUE-EXIT
                 END-IF
           END-IF.

           IF UCF-FUND-RESTR-CD = 'R'
                 IF VALID-END-1ST-FOR-R
                    NEXT SENTENCE
                 ELSE
                   ADD 1 TO SUB1
                   MOVE 'ENDWMNT RSTRCTN CD'  TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-EMDWNT-RSTR-CD   TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570053                TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-VALID-EMDWNT-VALUE-EXIT
                 END-IF
           END-IF.

DEVMJM     IF VALID-END-3-4-FOR-ALL
                NEXT SENTENCE
           ELSE
                ADD 1 TO SUB1
                MOVE 'ENDWMNT RSTRCTN CD'     TO UCF-FIELD-NAME (SUB1)
                MOVE  UCF-EMDWNT-RSTR-CD      TO UCF-FIELD-VALUE (SUB1)
                MOVE 570053                   TO UCF-ERROR-MSG (SUB1)
                GO TO 200-VALID-EMDWNT-VALUE-EXIT
           END-IF.

           IF ENDW-LAST-1-CAMPS = SPACES OR LOW-VALUES
                ADD 1 TO SUB1
                MOVE 'ENDWMNT RSTRCTN CD'     TO UCF-FIELD-NAME (SUB1)
                MOVE  UCF-EMDWNT-RSTR-CD      TO UCF-FIELD-VALUE (SUB1)
                MOVE 570053                   TO UCF-ERROR-MSG (SUB1)
                GO TO 200-VALID-EMDWNT-VALUE-EXIT
           END-IF.

       200-VALID-EMDWNT-VALUE-EXIT.
           EXIT.
      /
       200-EDIT-PAYMETHD.

           IF GPF-MOP-IND = 'Z'
               IF  UCF-METHOD-PAYMENT > SPACES
                   ADD 1 TO SUB1
                   MOVE 'METHOD PAYMENT'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-METHOD-PAYMENT   TO UCF-FIELD-VALUE (SUB1)
      *            MOVE 570057               TO UCF-ERROR-MSG (SUB1)
FP4155             MOVE 000106               TO UCF-ERROR-MSG (SUB1)
               END-IF
               GO TO 200-EDIT-PAYMETHD-EXIT
           END-IF.

           IF GPF-MOP-IND = 'X'
               IF  UCF-METHOD-PAYMENT = ' '
                   ADD 1 TO SUB1
                   MOVE 'METHOD PAYMENT'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-METHOD-PAYMENT   TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570056               TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-EDIT-PAYMETHD-EXIT
               END-IF
               IF  UCF-METHOD-PAYMENT = '10' OR '20' OR '50' OR '60'
FP4255                       OR '61' OR '80' OR '90' OR '95' OR '99'
                   CONTINUE
               ELSE
                   ADD 1 TO SUB1
                   MOVE 'METHOD PAYMENT'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-METHOD-PAYMENT   TO UCF-FIELD-VALUE (SUB1)
FP4255             MOVE 000106               TO UCF-ERROR-MSG (SUB1)
               END-IF
               GO TO 200-EDIT-PAYMETHD-EXIT
           END-IF.

           IF  UCF-METHOD-PAYMENT > SPACES
               IF  UCF-METHOD-PAYMENT = '10' OR '20' OR '50' OR '60'
FP4255                       OR '61' OR '80' OR '90' OR '95' OR '99'
                   CONTINUE
               ELSE
                   ADD 1 TO SUB1
                   MOVE 'METHOD PAYMENT'     TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-METHOD-PAYMENT   TO UCF-FIELD-VALUE (SUB1)
      *            MOVE 570057               TO UCF-ERROR-MSG (SUB1)
FP4255             MOVE 000106               TO UCF-ERROR-MSG (SUB1)
               END-IF
           END-IF.

       200-EDIT-PAYMETHD-EXIT.
           EXIT.
      /
       200-EDIT-AWD-CD.
           IF GPF-TYP-AWD-CD = ' '
               IF UCF-TYP-AWD > SPACES
                   IF UCF-TYP-AWD = '1' OR '2' OR '3' OR '4'
                     CONTINUE
                   ELSE
                     ADD 1 TO SUB1
                     MOVE 'AWARD TYPE'       TO UCF-FIELD-NAME (SUB1)
                     MOVE UCF-TYP-AWD        TO UCF-FIELD-VALUE (SUB1)
                     MOVE 570300             TO UCF-ERROR-MSG (SUB1)
                     GO TO 200-EDIT-AWD-CD-EXIT
                   END-IF
               END-IF
           END-IF.

           IF GPF-TYP-AWD-CD = 'Z'
                IF UCF-TYP-AWD NOT = SPACE
                    ADD 1 TO SUB1
                    MOVE 'AWARD TYPE'        TO UCF-FIELD-NAME (SUB1)
                    MOVE  UCF-TYP-AWD        TO UCF-FIELD-VALUE (SUB1)
                    MOVE 570302              TO UCF-ERROR-MSG (SUB1)
                END-IF
                GO TO 200-EDIT-AWD-CD-EXIT
           END-IF.

           IF GPF-TYP-AWD-CD = 'X'
               IF  UCF-TYP-AWD = SPACES
                   ADD 1 TO SUB1
                   MOVE 'AWARD TYPE'         TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-TYP-AWD         TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570301               TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-EDIT-AWD-CD-EXIT
               ELSE
                 IF UCF-TYP-AWD = '1' OR '2' OR '3' OR '4'
                   CONTINUE
                 ELSE
                   ADD 1 TO SUB1
                   MOVE 'AWARD TYPE'         TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-TYP-AWD         TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570300               TO UCF-ERROR-MSG (SUB1)
                 END-IF
               END-IF
           END-IF.

           IF UCF-FUND-GRP-CD = '100600' OR '408100' OR '408200'
                IF  UCF-TYP-AWD NOT = '4'
                    ADD 1 TO SUB1
                    MOVE 'AWARD TYPE'        TO UCF-FIELD-NAME (SUB1)
                    MOVE  UCF-TYP-AWD        TO UCF-FIELD-VALUE (SUB1)
                    MOVE 570300              TO UCF-ERROR-MSG (SUB1)
                END-IF
                GO TO 200-EDIT-AWD-CD-EXIT
           END-IF.

           IF UCF-FUND-GRP-CD = '100200' OR '100700' OR '100800'
                             OR '404210' OR '405210'
                             OR '406210' OR '408300' OR '408410'
                IF  UCF-TYP-AWD = '1' OR '2' OR '3'
                    CONTINUE
                ELSE
                   ADD 1 TO SUB1
                   MOVE 'AWARD TYPE'         TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-TYP-AWD         TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570300               TO UCF-ERROR-MSG (SUB1)
                END-IF
                GO TO 200-EDIT-AWD-CD-EXIT
           END-IF.

           IF UCF-FUND-GRP-CD = '404110' OR '404120'
                             OR '404220' OR '406220'
                IF  UCF-TYP-AWD = '1' OR '2' OR '3' OR ' '
                    CONTINUE
                ELSE
                   ADD 1 TO SUB1
                   MOVE 'AWARD TYPE'         TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-TYP-AWD         TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570300               TO UCF-ERROR-MSG (SUB1)
                END-IF
                GO TO 200-EDIT-AWD-CD-EXIT
           END-IF.

           IF UCF-FUND-GRP-CD = '100600' OR '408100' OR '408200'
                IF  UCF-TYP-AWD = '4'
                    CONTINUE
                ELSE
                   ADD 1 TO SUB1
                   MOVE 'AWARD TYPE'         TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-TYP-AWD         TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570300               TO UCF-ERROR-MSG (SUB1)
                END-IF
                GO TO 200-EDIT-AWD-CD-EXIT
           END-IF.

           IF UCF-FUND-GRP-CD = '406320' OR '406330'
                IF  UCF-TYP-AWD = '2'
                    CONTINUE
                ELSE
                   ADD 1 TO SUB1
                   MOVE 'AWARD TYPE'         TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-TYP-AWD         TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570300               TO UCF-ERROR-MSG (SUB1)
                END-IF
                GO TO 200-EDIT-AWD-CD-EXIT
           END-IF.

       200-EDIT-AWD-CD-EXIT.
           EXIT.
      /
       200-EDIT-ON-OFF-CMPS.

           IF GPF-ON-OFF-CAM-CD = 'Z'
               IF UCF-ON-OFF-CAM-CD NOT = SPACES
                  ADD 1 TO SUB1
                  MOVE 'ON-OFF-CMPS'         TO UCF-FIELD-NAME (SUB1)
                  MOVE  UCF-ON-OFF-CAM-CD    TO UCF-FIELD-VALUE (SUB1)
                  MOVE 570313                TO UCF-ERROR-MSG (SUB1)
               END-IF
               GO TO 200-EDIT-ON-OFF-CMPS-EXIT
           END-IF.

           IF GPF-ON-OFF-CAM-CD = 'X'
              IF UCF-ON-OFF-CAM-CD = SPACES
                 ADD 1 TO SUB1
                 MOVE 'ON-OFF-CMPS'         TO UCF-FIELD-NAME (SUB1)
                 MOVE  UCF-ON-OFF-CAM-CD    TO UCF-FIELD-VALUE (SUB1)
                 MOVE 570313                TO UCF-ERROR-MSG (SUB1)
                 GO TO 200-EDIT-ON-OFF-CMPS-EXIT
              END-IF
           END-IF.

           IF UCF-ON-OFF-CAM-CD = '1' OR '2' OR ' '
                   CONTINUE
           ELSE
                   ADD 1 TO SUB1
                   MOVE 'ON-OFF-CMPS'       TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-ON-OFF-CAM-CD  TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570313              TO UCF-ERROR-MSG (SUB1)
           END-IF.

       200-EDIT-ON-OFF-CMPS-EXIT.
           EXIT.
      /
       200-EDIT-FED-FLW.
           IF UCF-FED-FL-THRU-CD = ' ' OR '1' OR '2' OR '3' OR '4'
ARRA                                   OR '5' OR '6' OR '7'
                 CONTINUE
           ELSE
                 ADD 1 TO SUB1
                 MOVE 'FED FLOW THRU'          TO UCF-FIELD-NAME (SUB1)
                 MOVE  UCF-FED-FL-THRU-CD      TO UCF-FIELD-VALUE (SUB1)
                 MOVE 570315                   TO UCF-ERROR-MSG (SUB1)
                 GO TO 200-EDIT-FED-FLW-EXIT
           END-IF.

           IF GPF-FED-FL-THRU-CD = ' '
                 GO TO 200-EDIT-FED-FLW-EXIT
           END-IF.

           IF GPF-FED-FL-THRU-CD = 'Z'
                IF UCF-FED-FL-THRU-CD > SPACES
                    ADD 1 TO SUB1
                    MOVE 'FED FLOW THRU'       TO UCF-FIELD-NAME (SUB1)
                    MOVE  UCF-FED-FL-THRU-CD   TO UCF-FIELD-VALUE (SUB1)
                    MOVE 570315                TO UCF-ERROR-MSG (SUB1)
                END-IF
                GO TO 200-EDIT-FED-FLW-EXIT
           END-IF.

           IF UCF-FUND-GRP-CD = '100200' OR '406210' OR '406310'
ARRA                         OR '100210' OR '406110' OR '406230'
                             OR '406340'
              IF UCF-FED-FL-THRU-CD = ' ' OR '2' OR '3' OR '6' OR '7'
                   CONTINUE
              ELSE
                   ADD 1 TO SUB1
                   MOVE 'FED FLOW THRU'        TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-FED-FL-THRU-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570315                 TO UCF-ERROR-MSG (SUB1)
              END-IF
              GO TO 200-EDIT-FED-FLW-EXIT
           END-IF.

           IF UCF-FUND-GRP-CD = '404210' OR '405210' OR '408300'
                            OR  '408410' OR '408420'
              IF UCF-FED-FL-THRU-CD = '1' OR '2' OR '3' OR '6' OR '7'
                   CONTINUE
              ELSE
                   ADD 1 TO SUB1
                   MOVE 'FED FLOW THRU'        TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-FED-FL-THRU-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570315                 TO UCF-ERROR-MSG (SUB1)
              END-IF
              GO TO 200-EDIT-FED-FLW-EXIT
           END-IF.

           IF UCF-FUND-GRP-CD = '408100' OR '408200'
              IF UCF-FED-FL-THRU-CD = '1' OR '4' OR '5'
                   CONTINUE
              ELSE
                   ADD 1 TO SUB1
                   MOVE 'FED FLOW THRU'        TO UCF-FIELD-NAME (SUB1)
                   MOVE UCF-FED-FL-THRU-CD     TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570315                 TO UCF-ERROR-MSG (SUB1)
              END-IF
              GO TO 200-EDIT-FED-FLW-EXIT
           END-IF.

           IF UCF-FUND-GRP-CD = '100100'
                IF UCF-TYP-AWD = '4'
                    IF UCF-FED-FL-THRU-CD = ' ' OR '1' OR '4' OR '5'
                       CONTINUE
                    ELSE
                       ADD 1 TO SUB1
                       MOVE 'FED FLOW THRU'    TO UCF-FIELD-NAME (SUB1)
                       MOVE UCF-FED-FL-THRU-CD TO UCF-FIELD-VALUE (SUB1)
                       MOVE 570315             TO UCF-ERROR-MSG (SUB1)
                    END-IF
                END-IF
           END-IF.

           IF UCF-FUND-GRP-CD = '100600'
                IF UCF-TYP-AWD = '4'
                    IF UCF-FED-FL-THRU-CD = '1' OR '4' OR '5'
                       CONTINUE
                    ELSE
                       ADD 1 TO SUB1
                       MOVE 'FED FLOW THRU'    TO UCF-FIELD-NAME (SUB1)
                       MOVE UCF-FED-FL-THRU-CD TO UCF-FIELD-VALUE (SUB1)
                       MOVE 570315             TO UCF-ERROR-MSG (SUB1)
                    END-IF
                END-IF
           END-IF.
       200-EDIT-FED-FLW-EXIT.
           EXIT.
      /
       200-EDIT-IDC-BS-CD.
           IF UCF-IDC-RATE = 0
                IF UCF-IDC-CBASE-CD NOT = 'G'
                   ADD 1 TO SUB1
                   MOVE 'IDC RATE BASE'     TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-IDC-CBASE-CD   TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570350              TO UCF-ERROR-MSG (SUB1)
                END-IF
                GO TO 200-EDIT-IDC-BS-CD-EXIT
           END-IF.

           IF UCF-FUND-GRP-CD = '100200' OR '100700' OR '100800'
                             OR '404210' OR '405210'
                             OR '406210' OR '406310' OR '406330'
                             OR '408300' OR '408410' OR '408420'
                 IF UCF-IDC-CBASE-CD = ' '
                    ADD 1 TO SUB1
                    MOVE 'IDC RATE BASE'    TO UCF-FIELD-NAME (SUB1)
                    MOVE  UCF-IDC-CBASE-CD  TO UCF-FIELD-VALUE (SUB1)
                    MOVE 570060             TO UCF-ERROR-MSG (SUB1)
                    GO TO 200-EDIT-IDC-BS-CD-EXIT
                 END-IF
           END-IF.

           IF UCF-IDC-RATE > ZEROS
                 IF UCF-IDC-CBASE-CD = 'A' OR 'B' OR 'C' OR 'D'
                                  OR = 'E' OR 'F' OR 'O'
                    NEXT SENTENCE
                 ELSE
                   ADD 1 TO SUB1
                   MOVE 'IDC RATE BASE'     TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-IDC-CBASE-CD   TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570060              TO UCF-ERROR-MSG (SUB1)
                 END-IF
           END-IF.

       200-EDIT-IDC-BS-CD-EXIT.
           EXIT.
      /
       200-EDIT-CFDA.
           IF UCF-FUND-GRP-CD = '100200'
                             OR '406210' OR '406310'
               IF UCF-CFDA = SPACES
                   ADD 1 TO SUB1
                   MOVE 'CFDA'               TO UCF-FIELD-NAME (SUB1)
                   MOVE  UCF-CFDA            TO UCF-FIELD-VALUE (SUB1)
                   MOVE 570061               TO UCF-ERROR-MSG (SUB1)
                   GO TO 200-EDIT-CFDA-EXIT
               END-IF
           END-IF.

           IF UCF-FUND-GRP-CD = '404210' OR '405210'
                             OR '408300' OR '408410'
              IF UCF-FED-FL-THRU-CD = '2' OR '3'
                 IF UCF-CFDA = SPACES
                    ADD 1 TO SUB1
                    MOVE 'CFDA'              TO UCF-FIELD-NAME (SUB1)
                    MOVE  UCF-CFDA           TO UCF-FIELD-VALUE (SUB1)
                    MOVE 570061              TO UCF-ERROR-MSG (SUB1)
                    GO TO 200-EDIT-CFDA-EXIT
                 END-IF
              END-IF
           END-IF.

           IF (UCF-CFDA    > SPACES)
           AND UCF-CFDA  NOT NUMERIC
              ADD 1 TO SUB1
              MOVE 'CFDA'                    TO UCF-FIELD-NAME (SUB1)
              MOVE  UCF-CFDA                 TO UCF-FIELD-VALUE (SUB1)
              MOVE 570011                    TO UCF-ERROR-MSG (SUB1)
              GO TO 200-EDIT-CFDA-EXIT
           END-IF.

       200-EDIT-CFDA-EXIT.
           EXIT.
