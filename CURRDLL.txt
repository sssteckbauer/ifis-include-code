      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4225-DLVRY-LOG' TO DBLINK-CURR-PARAGRAPH.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4225-DLVRY-LOG' TO RECORD-NAME.                       276 BRTN
YYY991     MOVE 'F-PRCHS' TO AREA-NAME.                                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4225-DLVRY-LOG TO DBLINK-RECORD-MADE-CURRENT.   276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4225-DLVRY-LOG-EXIT                      276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE DLL-FK-DLH-DOC-REF TO DBLINK-R4225-DLVRY-LOG-01     276 BRTN
YYY991         MOVE DLL-FK-DLH-PO-ITEM-NBR TO DBLINK-R4225-DLVRY-LOG-02 276 BRTN
YYY991         MOVE DLL-FK-DLH-RQST-CD TO DBLINK-R4225-DLVRY-LOG-03     276 BRTN
YYY991         MOVE DLL-FK-DLH-RQSTITEMNBR TO DBLINK-R4225-DLVRY-LOG-04 276 BRTN
YYY991         MOVE DLL-DLVRY-DT TO DBLINK-R4225-DLVRY-LOG-05           276 BRTN
YYY991         MOVE DLL-DLH-SET-TS TO DBLINK-R4225-DLVRY-LOG-06         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4225-DLVRY-LOG TO DBLINK-VIEW-NAME-CURRENT.     276 BRTN
YYY991     MOVE DBLINK-R4225-DLVRY-LOG-KEY TO DBLINK-VIEW-200-CURRENT.  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4224-4225                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DLL-FK-DLH-DOC-REF TO DBLINK-S-4224-4225-01.            276 BRTN
YYY991     MOVE DLL-FK-DLH-PO-ITEM-NBR TO DBLINK-S-4224-4225-02.        276 BRTN
YYY991     MOVE DLL-FK-DLH-RQST-CD TO DBLINK-S-4224-4225-03.            276 BRTN
YYY991     MOVE DLL-FK-DLH-RQSTITEMNBR TO DBLINK-S-4224-4225-04.        276 BRTN
YYY991     MOVE DLL-DLVRY-DT TO DBLINK-S-4224-4225-05.                  276 BRTN
YYY991     MOVE DLL-DLH-SET-TS TO DBLINK-S-4224-4225-06.                276 BRTN
