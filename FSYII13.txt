      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYII13 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYII13.

      ******************************************************************
      ******
      ***            CENTER A UNIVERSITY(INSTITUTION) NAME
      *** THE UNIVERSITY RECORD MUST HAVE BEEN READ, AND BE CURRENT.
      ******************************************************************
      ******
           MOVE FULL-NAME-6001 TO ACWEXTR-PARM-IN.
FXXCHG     CALL 'ACXEXTR' USING ACWEXTR-PARMS.
           MOVE ACWEXTR-PARM-OUT TO FULL-NAME-6001-SY00.
           MOVE FULL-NAME-6001-SY00 TO ACWEXTR-PARM-IN.
FXXCHG     CALL 'ACXEXTR' USING ACWEXTR-PARMS.
           MOVE ACWEXTR-PARM-OUT TO AC99W-EXTR01
           COMPUTE AC99W-SUB1 = (ACWEXTR-PARM-OUT-LENGTH + 1).
           MOVE HIGH-VALUE TO AC99W-EXTR01-POS(AC99W-SUB1).
           MOVE SPACES TO ACWSTLEN-PARM-IN.
           MOVE AC99W-EXTR01 TO ACWSTLEN-PARM-IN.
FXXCHG     CALL 'ACXSTLEN' USING ACWSTLEN-PARMS.
           MOVE ACWSTLEN-PARM-OUT-LENGTH TO SUB-2-SY00.
           IF SUB-2-SY00 < 34
      ** 1 *
               COMPUTE SUB-1-SY00 ROUNDED =
                   ( 35 - SUB-2-SY00 ) / 2
               PERFORM WITH TEST BEFORE
                       UNTIL NOT ( SUB-1-SY00 > 1 )
      ** 2 * CENTER NAME IN FIELD FOR DISPLAY ON SCREENS
                   STRING FULL-NAME-6001-SY00 HIGH-VALUE
                           DELIMITED BY SIZE INTO ACWINSRT-PARM-INTO
                   STRING ' ' HIGH-VALUE
                           DELIMITED BY SIZE INTO ACWINSRT-PARM-INSERT
                   MOVE 1 TO ACWINSRT-PARM-POS
FXXCHG             CALL 'ACXINSRT' USING ACWINSRT-PARMS
                   MOVE ACWINSRT-PARM-OUT TO FULL-NAME-6001-SY00
                   SUBTRACT 1 FROM SUB-1-SY00
               END-PERFORM
      ** 2 *
           END-IF.
      ** 1 *
      ******************************************************************
      ******

       200-FSYII13-EXIT.
           EXIT.
