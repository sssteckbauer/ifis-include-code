       975-TO-R4201-RTRN-REAS SECTION.
           MOVE REA-USER-CD TO
               USER-CODE-4201 OF R4201-RTRN-REAS
                                                                     .
           MOVE REA-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4201 OF R4201-RTRN-REAS
                                                                     .
           MOVE REA-UNVRS-CD TO
               UNVRS-CODE-4201 OF R4201-RTRN-REAS
                                                                     .
           MOVE REA-RTRN-RSN-CD TO
               RTRN-RSN-CODE-4201 OF R4201-RTRN-REAS
                                                                     .
           MOVE REA-RTRN-RSN-DESC TO
               RTRN-RSN-DESC-4201 OF R4201-RTRN-REAS
                                                                     .
           MOVE REA-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4201 OF R4201-RTRN-REAS
                                                                     .
           MOVE REA-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4201 OF R4201-RTRN-REAS
                                                                     .
           MOVE REA-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4201 OF R4201-RTRN-REAS
                                                                     .
       975-TO-R4201-RTRN-REAS-EXIT.
           EXIT.
