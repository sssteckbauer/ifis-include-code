       976-FROM-R4404-ADVNC SECTION.

FCYI **        MOVE USER-CODE-4404 OF R4404-ADVNC
FCYI           MOVE DBLINK-USER-CD
               TO ADV-USER-CD.

               MOVE LAST-ACTVY-DATE-4404 OF R4404-ADVNC
               TO ADV-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4404 OF R4404-ADVNC
               TO ADV-UNVRS-CD.

               MOVE ADVNC-NMBR-4404 OF R4404-ADVNC
               TO ADV-ADVNC-NBR.

           IF ADVNC-DATE-4404 OF R4404-ADVNC  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ADV-ADVNC-DT
           ELSE
               MOVE ADVNC-DATE-4404 OF R4404-ADVNC
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ADV-ADVNC-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ADV-ADVNC-DT
               END-IF
           END-IF.

           IF ADVNC-AMT-4404 OF R4404-ADVNC  NOT NUMERIC
               MOVE ZEROS TO ADV-ADVNC-AMT
           ELSE
               MOVE ADVNC-AMT-4404 OF R4404-ADVNC
               TO ADV-ADVNC-AMT
           END-IF.

               MOVE ADVNC-STATUS-4404 OF R4404-ADVNC
               TO ADV-ADVNC-STATUS.

               MOVE ADR-TYPE-CODE-4404 OF R4404-ADVNC
               TO ADV-ADR-TYP-CD.

               MOVE CHECK-NMBR-4404 OF R4404-ADVNC
               TO ADV-CHECK-NBR.

           IF CHECK-DATE-4404 OF R4404-ADVNC  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ADV-CHECK-DT
           ELSE
               MOVE CHECK-DATE-4404 OF R4404-ADVNC
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ADV-CHECK-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ADV-CHECK-DT
               END-IF
           END-IF.

               MOVE CMPLT-IND-4404 OF R4404-ADVNC
               TO ADV-CMPLT-IND.

               MOVE APRVL-IND-4404 OF R4404-ADVNC
               TO ADV-APRVL-IND.

               MOVE CNCL-IND-4404 OF R4404-ADVNC
               TO ADV-CNCL-IND.

           IF CNCL-DATE-4404 OF R4404-ADVNC  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ADV-CNCL-DT
           ELSE
               MOVE CNCL-DATE-4404 OF R4404-ADVNC
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ADV-CNCL-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ADV-CNCL-DT
               END-IF
           END-IF.

               MOVE HDR-ERROR-IND-4404 OF R4404-ADVNC
               TO ADV-HDR-ERROR-IND.

           IF HDR-CNTR-DTL-4404 OF R4404-ADVNC  NOT NUMERIC
               MOVE ZEROS TO ADV-HDR-CNTR-DTL
           ELSE
               MOVE HDR-CNTR-DTL-4404 OF R4404-ADVNC
               TO ADV-HDR-CNTR-DTL
           END-IF.

               MOVE ADVNC-TYPE-4404 OF R4404-ADVNC
               TO ADV-ADVNC-TYP.

               MOVE RULE-CLASS-CODE-4404 OF R4404-ADVNC
               TO ADV-RULE-CLS-CD.

               MOVE COA-CODE-4404 OF R4404-ADVNC
               TO ADV-COA-CD.

               MOVE ACCT-INDX-CODE-4404 OF R4404-ADVNC
               TO ADV-ACCT-INDX-CD.

               MOVE FUND-CODE-4404 OF R4404-ADVNC
               TO ADV-FUND-CD.

               MOVE ORGZN-CODE-4404 OF R4404-ADVNC
               TO ADV-ORGN-CD.

               MOVE ACCT-CODE-4404 OF R4404-ADVNC
               TO ADV-ACCT-CD.

               MOVE PRGRM-CODE-4404 OF R4404-ADVNC
               TO ADV-PRGRM-CD.

               MOVE ACTVY-CODE-4404 OF R4404-ADVNC
               TO ADV-ACTVY-CD.

               MOVE LCTN-CODE-4404 OF R4404-ADVNC
               TO ADV-LCTN-CD.

               MOVE BANK-ACCT-CODE-4404 OF R4404-ADVNC
               TO ADV-BANK-ACCT-CD.

               MOVE NSF-OVRDE-4404 OF R4404-ADVNC
               TO ADV-NSF-OVRDE.

               MOVE CASH-IND-4404 OF R4404-ADVNC
               TO ADV-CASH-IND.

               MOVE BATCH-CHECK-IND-4404 OF R4404-ADVNC
               TO ADV-BATCH-CHECK-IND.

               MOVE MNL-CHECK-IND-4404 OF R4404-ADVNC
               TO ADV-MNL-CHECK-IND.

               MOVE FSCL-YR-4404 OF R4404-ADVNC
               TO ADV-FSCL-YR.

               MOVE PSTNG-PRD-4404 OF R4404-ADVNC
               TO ADV-PSTNG-PRD.

               MOVE IMMDT-CHECK-IND-4404 OF R4404-ADVNC
               TO ADV-IMMDT-CHECK-IND.
       976-FROM-R4404-ADVNC-EXIT.
           EXIT.
