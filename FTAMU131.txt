    ***Created by Convert/DC version V8R03 on 12/05/00 at 14:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           IF FICE-CODE-6001-TA13 OF FTAWM13 NOT NUMERIC
              MOVE FICE-CODE-6001-TA13 OF FTAWM13 TO
                WK01-CODE-6001-TA13-X
           ELSE
              MOVE FICE-CODE-6001-TA13 OF FTAWM13 TO WK01-CODE-6001-TA13
           END-IF.
      *%--------------------------------------------------------------%*
           MOVE TRVL-ACCT-LAST-NINE-4400-TA13 OF FTAWM13 TO
                WK01-ACCT-LAST-NIN-4400-T13.
      *%--------------------------------------------------------------%*
           MOVE VNDR-ID-LAST-NINE-4073-TA13 OF FTAWM13 TO
                WK01-ID-LAST-NINE-4073-TA13.
      *%--------------------------------------------------------------%*
           MOVE EVENT-NMBR-4401-TA13 OF FTAWM13 TO WK01-NMBR-4401-TA13.
      *%--------------------------------------------------------------%*
           MOVE TRIP-NMBR-4402-TA13 OF FTAWM13 TO WK01-NMBR-4402-TA13.
      *%--------------------------------------------------------------%*
           MOVE ADVNC-NMBR-4404-TA13 OF FTAWM13 TO WK01-NMBR-4404-TA13.
      *%--------------------------------------------------------------%*
           MOVE EXPNS-NMBR-4405-TA13 OF FTAWM13 TO WK01-NMBR-4405-TA13.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-4410-TA13 OF FTAWM13 TO WK01-NMBR-4410-TA13.
      *%--------------------------------------------------------------%*
           MOVE RCNCLTN-TYPE-4415-TA13 OF FTAWM13 TO
                WK01-TYPE-4415-TA13.
      *%--------------------------------------------------------------%*
           MOVE ADVNC-TYPE-4417-TA13 OF FTAWM13 TO WK01-TYPE-4417-TA13.
      *%--------------------------------------------------------------%*
           MOVE ADR-TYPE-CODE-6137-TA13 OF FTAWM13 TO
                WK01-TYPE-CODE-6137-TA13.
      *%--------------------------------------------------------------%*
           MOVE WORK-DATE-TA13 OF FTAWM13 TO WK01-DATE-TA13.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6117-TA13 OF FTAWM13 TO WK01-KEY-6117-TA13.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6311-TA13 OF FTAWM13 TO WK01-KEY-6311-TA13.
      *%--------------------------------------------------------------%*
           MOVE EVENT-DESC-4401-TA13 OF FTAWM13 TO WK01-DESC-4401-TA13.
      *%--------------------------------------------------------------%*
           MOVE TRIP-DESC-4402-TA13 OF FTAWM13 TO WK01-DESC-4402-TA13.
      *%--------------------------------------------------------------%*
           MOVE STNDD-LONG-DESC-4415-TA13 OF FTAWM13 TO
                WK01-LONG-DESC-4415-TA13.
      *%--------------------------------------------------------------%*
           MOVE STNDD-LONG-DESC-4417-TA13 OF FTAWM13 TO
                WK01-LONG-DESC-4417-TA13.
      *%--------------------------------------------------------------%*
           MOVE STNDD-LONG-DESC-6137-TA13 OF FTAWM13 TO
                WK01-LONG-DESC-6137-TA13.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-TA13 OF FTAWM13 TO WK01-NAME-6001-TA13.
      *%--------------------------------------------------------------%*
           MOVE PRNTR-DEST-CODE-4167-TA13 OF FTAWM13 TO
                WK01-DEST-CODE-4167-TA13.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4000-TA13 OF FTAWM13 TO WK01-CODE-4000-TA13.
