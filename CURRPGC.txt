      *      CREATED BY CONVERT/DB V8R01 ON 05/03/00 AT 10:54         *
YYY991     MOVE '985-CURR-MAPFLD-124' TO DBLINK-CURR-PARAGRAPH.         276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'MAPFLD-124' TO RECORD-NAME.                            276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-MAPFLD-124 TO DBLINK-RECORD-MADE-CURRENT.        276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-MAPFLD-124-EXIT                           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE MPF-FK-MAP-MAP-NM TO DBLINK-MAPFLD-124-01           276 BRTN
YYY991         MOVE MPF-FK-MAP-PKEY-TS TO DBLINK-MAPFLD-124-02          276 BRTN
YYY991         MOVE MPF-MAP-TS TO DBLINK-MAPFLD-124-03                  276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-MAPFLD-124 TO DBLINK-VIEW-NAME-CURRENT.          276 BRTN
YYY991     MOVE DBLINK-MAPFLD-124-KEY TO DBLINK-VIEW-200-CURRENT.       276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET MAP-MAPFLD                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE MPF-FK-MAP-MAP-NM TO DBLINK-MAP-MAPFLD-01.              276 BRTN
YYY991     MOVE MPF-FK-MAP-PKEY-TS TO DBLINK-MAP-MAPFLD-02.             276 BRTN
YYY991     MOVE MPF-MAP-TS TO DBLINK-MAP-MAPFLD-03.                     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET NAMESYN-MAPFLD                                              276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-NAMESYN-MAPFLD-F            276 BRTN
YYY991     IF MPF-NMS-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE MPF-NMS-SETF TO DBLINK-NAMESYN-MAPFLD-F             276 BRTN
YYY991         MOVE MPF-FK-NMS-RSYN-NM TO DBLINK-NAMESYN-MAPFLD-01      276 BRTN
YYY991         MOVE MPF-FK-NMS-PKEY-TS TO DBLINK-NAMESYN-MAPFLD-02      276 BRTN
YYY991         MOVE MPF-FK-NMS-RSYN-NSYN TO DBLINK-NAMESYN-MAPFLD-03    276 BRTN
YYY991         MOVE MPF-FK-MAP-MAP-NM TO DBLINK-NAMESYN-MAPFLD-04       276 BRTN
YYY991         MOVE MPF-FK-MAP-PKEY-TS TO DBLINK-NAMESYN-MAPFLD-05      276 BRTN
YYY991         MOVE MPF-MAP-TS TO DBLINK-NAMESYN-MAPFLD-06              276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET PANELFLD-MAPFLD                                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-PANELFLD-MAPFLD-F           276 BRTN
YYY991     IF MPF-PLF-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE MPF-PLF-SETF TO DBLINK-PANELFLD-MAPFLD-F            276 BRTN
YYY991         MOVE MPF-FK-PLF-PKEY-TS TO DBLINK-PANELFLD-MAPFLD-01     276 BRTN
YYY991         MOVE MPF-PLF-TS TO DBLINK-PANELFLD-MAPFLD-02             276 BRTN
YYY991     END-IF.                                                      276 BRTN
