      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4410-TRVL-INV' TO DBLINK-CURR-PARAGRAPH.     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4410-TRVL-INV' TO RECORD-NAME.                        276 BRTN
YYY991     MOVE 'F-TRVL' TO AREA-NAME.                                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4410-TRVL-INV TO DBLINK-RECORD-MADE-CURRENT.    276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4410-TRVL-INV-EXIT                       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE TRI-DOC-NBR TO DBLINK-R4410-TRVL-INV-01             276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4410-TRVL-INV TO DBLINK-VIEW-NAME-CURRENT.      276 BRTN
YYY991     MOVE DBLINK-R4410-TRVL-INV-KEY TO DBLINK-VIEW-200-CURRENT.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4410-4411                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4410-TRVL-INV-01 TO DBLINK-S-4410-4411-01.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4410-4411-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4410                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4073-4410-F               276 BRTN
YYY991     IF TRI-VDR-SETF1 = DBLINK-MEMBER                             276 BRTN
YYY991         MOVE TRI-VDR-SETF1 TO DBLINK-S-4073-4410-F               276 BRTN
YYY991         MOVE TRI-FK1-VDR-IREF-ID TO DBLINK-S-4073-4410-01        276 BRTN
YYY991         MOVE TRI-FK1-VDR-ENTPSN-IND TO DBLINK-S-4073-4410-02     276 BRTN
YYY991         MOVE TRI-DOC-NBR TO DBLINK-S-4073-4410-03                276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4410-I                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4073-4410-I-F             276 BRTN
YYY991     IF TRI-VDR-SETF2 = DBLINK-MEMBER                             276 BRTN
YYY991         MOVE TRI-VDR-SETF2 TO DBLINK-S-4073-4410-I-F             276 BRTN
YYY991         MOVE TRI-FK2-VDR-IREF-ID TO DBLINK-S-4073-4410-I-01      276 BRTN
YYY991         MOVE TRI-FK2-VDR-ENTPSN-IND TO DBLINK-S-4073-4410-I-02   276 BRTN
YYY991         MOVE TRI-VNDR-INV-NBR TO DBLINK-S-4073-4410-I-03         276 BRTN
YYY991         MOVE TRI-VDR-SET-TS TO DBLINK-S-4073-4410-I-04           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-TA-INV                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE TRI-DOC-NBR TO DBLINK-S-INDX-TA-INV-01.                 276 BRTN
YYY991     MOVE TRI-CRDT-MEMO TO DBLINK-S-INDX-TA-INV-02.               276 BRTN
