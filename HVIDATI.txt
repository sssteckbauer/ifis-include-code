       975-TO-R4045-ACTI SECTION.
           MOVE ATI-USER-CD TO
               USER-CODE-4045 OF R4045-ACTI
                                                                     .
           MOVE ATI-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4045 OF R4045-ACTI
                                                                     .
           MOVE ATI-UNVRS-CD TO
               UNVRS-CODE-4045 OF R4045-ACTI
                                                                     .
           MOVE ATI-COA-CD TO
               COA-CODE-4045 OF R4045-ACTI
                                                                     .
           MOVE ATI-ACCT-INDX-CD TO
               ACCT-INDX-CODE-4045 OF R4045-ACTI
                                                                     .
           MOVE ATI-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4045 OF R4045-ACTI
                                                                     .
       975-TO-R4045-ACTI-EXIT.
           EXIT.
