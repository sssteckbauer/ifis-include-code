       9400-CFS-ACCUM.
           MOVE    CFS-ACCT-NMBR TO WS-ACCT-NMBR-DECODE.
           IF      CFS-RECORD-TYPE = '10'
                   PERFORM 9410-ACCUM-BALANCE-SHEET
                      THRU 9410-EXIT
                   IF      FUND-BALANCE-ACCOUNT
                       PERFORM 9420-ACCUM-FUND-BALANCE
                          THRU 9420-EXIT.

           IF      CFS-RECORD-TYPE = '11'
               PERFORM 9430-ACCUM-TRANSFERS
                  THRU 9430-EXIT.

           IF      CFS-RECORD-TYPE = '20'
               PERFORM 9440-ACCUM-REVENUE
                  THRU 9440-EXIT.

           IF      CFS-RECORD-TYPE = '30'
               IF      PLANT-ACCOUNT
                   PERFORM 9460-ACCUM-PLANT-EXPENDITURE
                      THRU 9460-EXIT
               ELSE
                   PERFORM 9450-ACCUM-EXPENDITURE
                      THRU 9450-EXIT.

           IF      CFS-RECORD-TYPE = '31'
               PERFORM 9470-ACCUM-EXPENDITURE-OBJECT
                  THRU 9470-EXIT.

       9400-EXIT.
           EXIT.
      /
       9410-ACCUM-BALANCE-SHEET.
      **---------------------------------------------------------------
      **  BUDGETARY APPROPRIATION AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 1).
           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 7).

           IF CFS-10-BDGT-APPR-AMT-CURR-BAL > 0
              ADD CFS-10-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-DEBIT-AMOUNT (WS-PHASE, 1)
                  BACB-DEBIT-AMOUNT (WS-PHASE, 7)
           ELSE
              ADD CFS-10-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-CREDIT-AMOUNT (WS-PHASE, 1)
                  BACB-CREDIT-AMOUNT (WS-PHASE, 7).

           ADD CFS-10-BDGT-APPR-AMT-CURR-BAL TO
               BACB-NET-AMOUNT (WS-PHASE, 1)
               BACB-NET-AMOUNT (WS-PHASE, 7).

      **---------------------------------------------------------------
      **  BALANCE SHEET - FINANCIAL AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 1).
           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 8).

           IF CFS-10-FIN-AMT-CURR-BAL > 0
              ADD CFS-10-FIN-AMT-CURR-BAL TO
                  FACB-DEBIT-AMOUNT (WS-PHASE, 1)
                  FACB-DEBIT-AMOUNT (WS-PHASE, 8)
           ELSE
              ADD CFS-10-FIN-AMT-CURR-BAL TO
                  FACB-CREDIT-AMOUNT (WS-PHASE, 1)
                  FACB-CREDIT-AMOUNT (WS-PHASE, 8).

           ADD CFS-10-FIN-AMT-CURR-BAL TO
               FACB-NET-AMOUNT (WS-PHASE, 1)
               FACB-NET-AMOUNT (WS-PHASE, 8).

      **---------------------------------------------------------------
      **  BALANCE SHEET - FINANCIAL AMOUNT - CURRENT MONTH ACTIVITY
      **---------------------------------------------------------------

           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 1).
           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 8).

           IF CFS-10-FIN-AMT-CURR-MONTH-ACT > 0
              ADD CFS-10-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-DEBIT-AMOUNT (WS-PHASE, 1)
                  FACM-DEBIT-AMOUNT (WS-PHASE, 8)
           ELSE
              ADD CFS-10-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-CREDIT-AMOUNT (WS-PHASE, 1)
                  FACM-CREDIT-AMOUNT (WS-PHASE, 8).

           ADD CFS-10-FIN-AMT-CURR-MONTH-ACT    TO
                  FACM-NET-AMOUNT (WS-PHASE, 1)
                  FACM-NET-AMOUNT (WS-PHASE, 8).

       9410-EXIT.
           EXIT.
      /
       9420-ACCUM-FUND-BALANCE.

      **---------------------------------------------------------------
      **  BUDGETARY APPROPRIATION AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 2).

           IF CFS-10-BDGT-APPR-AMT-CURR-BAL > 0
              ADD CFS-10-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-DEBIT-AMOUNT (WS-PHASE, 2)
           ELSE
              ADD CFS-10-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-CREDIT-AMOUNT (WS-PHASE, 2).

           ADD CFS-10-BDGT-APPR-AMT-CURR-BAL TO
               BACB-NET-AMOUNT (WS-PHASE, 2).

      **---------------------------------------------------------------
      **  FUND BALANCE - FINANCIAL AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 2).

           IF CFS-10-FIN-AMT-CURR-BAL > 0
              ADD CFS-10-FIN-AMT-CURR-BAL TO
                  FACB-DEBIT-AMOUNT (WS-PHASE, 2)
           ELSE
              ADD CFS-10-FIN-AMT-CURR-BAL TO
                  FACB-CREDIT-AMOUNT (WS-PHASE, 2).

           ADD CFS-10-FIN-AMT-CURR-BAL TO
               FACB-NET-AMOUNT (WS-PHASE, 2).

      **---------------------------------------------------------------
      **  FUND BALANCE - FINANCIAL AMOUNT - CURRENT MONTH ACTIVITY
      **---------------------------------------------------------------

           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 2).

           IF CFS-10-FIN-AMT-CURR-MONTH-ACT > 0
              ADD CFS-10-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-DEBIT-AMOUNT (WS-PHASE, 2)
           ELSE
              ADD CFS-10-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-CREDIT-AMOUNT (WS-PHASE, 2).

           ADD CFS-10-FIN-AMT-CURR-MONTH-ACT    TO
                  FACM-NET-AMOUNT (WS-PHASE, 2).

       9420-EXIT.
           EXIT.
      /
       9430-ACCUM-TRANSFERS.


           IF CFS-11-TRANS-TYPE = '00'
              ADD 1 TO FAT-RECORD-COUNT (WS-PHASE, 1)
              ADD 1 TO FAT-RECORD-COUNT (WS-PHASE, 3)
              IF CFS-11-FIN-AMT-CURR-MONTH-ACT > 0
                 ADD CFS-11-FIN-AMT-CURR-MONTH-ACT TO
                     FAT-DEBIT-AMOUNT (WS-PHASE, 1)
                     FAT-DEBIT-AMOUNT (WS-PHASE, 3)
              ELSE
                 ADD CFS-11-FIN-AMT-CURR-MONTH-ACT TO
                     FAT-CREDIT-AMOUNT (WS-PHASE, 1)
                     FAT-CREDIT-AMOUNT (WS-PHASE, 3)
           ELSE
              ADD 1 TO FAT-RECORD-COUNT (WS-PHASE, 2)
              ADD 1 TO FAT-RECORD-COUNT (WS-PHASE, 3)
              IF CFS-11-FIN-AMT-CURR-MONTH-ACT > 0
                 ADD CFS-11-FIN-AMT-CURR-MONTH-ACT TO
                     FAT-DEBIT-AMOUNT (WS-PHASE, 2)
                     FAT-DEBIT-AMOUNT (WS-PHASE, 3)
              ELSE
                 ADD CFS-11-FIN-AMT-CURR-MONTH-ACT TO
                     FAT-CREDIT-AMOUNT (WS-PHASE, 2)
                     FAT-CREDIT-AMOUNT (WS-PHASE, 3).

           IF CFS-11-TRANS-TYPE = '00'
              ADD CFS-11-FIN-AMT-CURR-MONTH-ACT    TO
                  FAT-NET-AMOUNT (WS-PHASE, 1)
                  FAT-NET-AMOUNT (WS-PHASE, 3)
           ELSE
              ADD CFS-11-FIN-AMT-CURR-MONTH-ACT    TO
                  FAT-NET-AMOUNT (WS-PHASE, 2)
                  FAT-NET-AMOUNT (WS-PHASE, 3).

       9430-EXIT.
           EXIT.
      /
       9440-ACCUM-REVENUE.


      **---------------------------------------------------------------
      **  REVENUE - BUDGETARY APPROPRIATION AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 3).
           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 7).

           IF CFS-20-BDGT-APPR-AMT-CURR-BAL > 0
              ADD CFS-20-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-DEBIT-AMOUNT (WS-PHASE, 3)
                  BACB-DEBIT-AMOUNT (WS-PHASE, 7)
           ELSE
              ADD CFS-20-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-CREDIT-AMOUNT (WS-PHASE, 3)
                  BACB-CREDIT-AMOUNT (WS-PHASE, 7).

           ADD CFS-20-BDGT-APPR-AMT-CURR-BAL TO
               BACB-NET-AMOUNT (WS-PHASE, 3)
               BACB-NET-AMOUNT (WS-PHASE, 7).
      **---------------------------------------------------------------
      **  REVENUE - FINANCIAL AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 3).
           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 8).

           IF CFS-20-FIN-AMT-CURR-BAL > 0
              ADD CFS-20-FIN-AMT-CURR-BAL TO
                  FACB-DEBIT-AMOUNT (WS-PHASE, 3)
                  FACB-DEBIT-AMOUNT (WS-PHASE, 8)
           ELSE
              ADD CFS-20-FIN-AMT-CURR-BAL TO
                  FACB-CREDIT-AMOUNT (WS-PHASE, 3)
                  FACB-CREDIT-AMOUNT (WS-PHASE, 8).

           ADD CFS-20-FIN-AMT-CURR-BAL TO
               FACB-NET-AMOUNT (WS-PHASE, 3)
               FACB-NET-AMOUNT (WS-PHASE, 8).

      **---------------------------------------------------------------
      **  REVENUE - FINANCIAL AMOUNT - CURRENT MONTH ACTIVITY
      **---------------------------------------------------------------

           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 3).
           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 8).

           IF CFS-20-FIN-AMT-CURR-MONTH-ACT > 0
              ADD CFS-20-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-DEBIT-AMOUNT (WS-PHASE, 3)
                  FACM-DEBIT-AMOUNT (WS-PHASE, 8)
           ELSE
              ADD CFS-20-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-CREDIT-AMOUNT (WS-PHASE, 3)
                  FACM-CREDIT-AMOUNT (WS-PHASE, 8).

           ADD CFS-20-FIN-AMT-CURR-MONTH-ACT    TO
                  FACM-NET-AMOUNT (WS-PHASE, 3)
                  FACM-NET-AMOUNT (WS-PHASE, 8).

       9440-EXIT.
           EXIT.
      /
       9450-ACCUM-EXPENDITURE.

      **---------------------------------------------------------------
      **  EXPENDITURE - FINANCIAL AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 4).
           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 6).
           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 8).

           IF CFS-30-FIN-AMT-CURR-BAL > 0
              ADD CFS-30-FIN-AMT-CURR-BAL TO
                  FACB-DEBIT-AMOUNT (WS-PHASE, 4)
                  FACB-DEBIT-AMOUNT (WS-PHASE, 6)
                  FACB-DEBIT-AMOUNT (WS-PHASE, 8)
           ELSE
              ADD CFS-30-FIN-AMT-CURR-BAL TO
                  FACB-CREDIT-AMOUNT (WS-PHASE, 4)
                  FACB-CREDIT-AMOUNT (WS-PHASE, 6)
                  FACB-CREDIT-AMOUNT (WS-PHASE, 8).

           ADD CFS-30-FIN-AMT-CURR-BAL TO
               FACB-NET-AMOUNT (WS-PHASE, 4)
               FACB-NET-AMOUNT (WS-PHASE, 6)
               FACB-NET-AMOUNT (WS-PHASE, 8).

      **---------------------------------------------------------------
      **  EXPENDITURE - FINANCIAL AMOUNT - CURRENT MONTH ACTIVITY
      **---------------------------------------------------------------

           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 4).
           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 6).
           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 8).

           IF CFS-30-FIN-AMT-CURR-MONTH-ACT > 0
              ADD CFS-30-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-DEBIT-AMOUNT (WS-PHASE, 4)
                  FACM-DEBIT-AMOUNT (WS-PHASE, 6)
                  FACM-DEBIT-AMOUNT (WS-PHASE, 8)
           ELSE
              ADD CFS-30-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-CREDIT-AMOUNT (WS-PHASE, 4)
                  FACM-CREDIT-AMOUNT (WS-PHASE, 6)
                  FACM-CREDIT-AMOUNT (WS-PHASE, 8).

           ADD CFS-30-FIN-AMT-CURR-MONTH-ACT    TO
                  FACM-NET-AMOUNT (WS-PHASE, 4)
                  FACM-NET-AMOUNT (WS-PHASE, 6)
                  FACM-NET-AMOUNT (WS-PHASE, 8).

      **---------------------------------------------------------------
      **  EXPENDITURE -BUDGETARY APPROPRIATION AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 4).
           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 6).
           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 7).

           IF CFS-30-BDGT-APPR-AMT-CURR-BAL > 0
              ADD CFS-30-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-DEBIT-AMOUNT (WS-PHASE, 4)
                  BACB-DEBIT-AMOUNT (WS-PHASE, 6)
                  BACB-DEBIT-AMOUNT (WS-PHASE, 7)
           ELSE
              ADD CFS-30-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-CREDIT-AMOUNT (WS-PHASE, 4)
                  BACB-CREDIT-AMOUNT (WS-PHASE, 6)
                  BACB-CREDIT-AMOUNT (WS-PHASE, 7).

           ADD CFS-30-BDGT-APPR-AMT-CURR-BAL TO
               BACB-NET-AMOUNT (WS-PHASE, 4)
               BACB-NET-AMOUNT (WS-PHASE, 6)
               BACB-NET-AMOUNT (WS-PHASE, 7).

      **---------------------------------------------------------------
      **  EXPENDITURE - ENCUMBRANCE AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO EACB-RECORD-COUNT (WS-PHASE, 1).
           ADD 1 TO EACB-RECORD-COUNT (WS-PHASE, 3).

           IF CFS-30-ENCMBR-AMT-CURR-BAL > 0
              ADD CFS-30-ENCMBR-AMT-CURR-BAL TO
                  EACB-DEBIT-AMOUNT (WS-PHASE, 1)
                  EACB-DEBIT-AMOUNT (WS-PHASE, 3)
           ELSE
              ADD CFS-30-ENCMBR-AMT-CURR-BAL TO
                  EACB-CREDIT-AMOUNT (WS-PHASE, 1)
                  EACB-CREDIT-AMOUNT (WS-PHASE, 3).

           ADD CFS-30-ENCMBR-AMT-CURR-BAL TO
               EACB-NET-AMOUNT (WS-PHASE, 1)
               EACB-NET-AMOUNT (WS-PHASE, 3).

       9450-EXIT.
           EXIT.
      /
       9460-ACCUM-PLANT-EXPENDITURE.

      **---------------------------------------------------------------
      **  PLANT EXPEND-BUDGETARY APPROPRIATION AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 5).
           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 6).
           ADD 1 TO BACB-RECORD-COUNT (WS-PHASE, 7).

           IF CFS-30-BDGT-APPR-AMT-CURR-BAL > 0
              ADD CFS-30-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-DEBIT-AMOUNT (WS-PHASE, 5)
                  BACB-DEBIT-AMOUNT (WS-PHASE, 6)
                  BACB-DEBIT-AMOUNT (WS-PHASE, 7)
           ELSE
              ADD CFS-30-BDGT-APPR-AMT-CURR-BAL TO
                  BACB-CREDIT-AMOUNT (WS-PHASE, 5)
                  BACB-CREDIT-AMOUNT (WS-PHASE, 6)
                  BACB-CREDIT-AMOUNT (WS-PHASE, 7).

           ADD CFS-30-BDGT-APPR-AMT-CURR-BAL TO
               BACB-NET-AMOUNT (WS-PHASE, 5)
               BACB-NET-AMOUNT (WS-PHASE, 6)
               BACB-NET-AMOUNT (WS-PHASE, 7).

      **---------------------------------------------------------------
      **  PLANT EXPENDITURE - FINANCIAL AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 5).
           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 6).
           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 8).

           IF CFS-30-FIN-AMT-CURR-BAL > 0
              ADD CFS-30-FIN-AMT-CURR-BAL TO
                  FACB-DEBIT-AMOUNT (WS-PHASE, 5)
                  FACB-DEBIT-AMOUNT (WS-PHASE, 6)
                  FACB-DEBIT-AMOUNT (WS-PHASE, 8)
           ELSE
              ADD CFS-30-FIN-AMT-CURR-BAL TO
                  FACB-CREDIT-AMOUNT (WS-PHASE, 5)
                  FACB-CREDIT-AMOUNT (WS-PHASE, 6)
                  FACB-CREDIT-AMOUNT (WS-PHASE, 8).

           ADD CFS-30-FIN-AMT-CURR-BAL TO
               FACB-NET-AMOUNT (WS-PHASE, 5)
               FACB-NET-AMOUNT (WS-PHASE, 6)
               FACB-NET-AMOUNT (WS-PHASE, 8).

      **---------------------------------------------------------------
      **  PLANT EXPENDITURE - FINANCIAL AMOUNT - CURRENT MONTH ACTIVITY
      **---------------------------------------------------------------

           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 5).
           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 6).
           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 8).

           IF CFS-30-FIN-AMT-CURR-MONTH-ACT > 0
              ADD CFS-30-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-DEBIT-AMOUNT (WS-PHASE, 5)
                  FACM-DEBIT-AMOUNT (WS-PHASE, 6)
                  FACM-DEBIT-AMOUNT (WS-PHASE, 8)
           ELSE
              ADD CFS-30-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-CREDIT-AMOUNT (WS-PHASE, 5)
                  FACM-CREDIT-AMOUNT (WS-PHASE, 6)
                  FACM-CREDIT-AMOUNT (WS-PHASE, 8).

           ADD CFS-30-FIN-AMT-CURR-MONTH-ACT    TO
                  FACM-NET-AMOUNT (WS-PHASE, 5)
                  FACM-NET-AMOUNT (WS-PHASE, 6)
                  FACM-NET-AMOUNT (WS-PHASE, 8).

      **---------------------------------------------------------------
      **  PLANT EXPENDITURE - ENCUMBRANCE AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO EACB-RECORD-COUNT (WS-PHASE, 2).
           ADD 1 TO EACB-RECORD-COUNT (WS-PHASE, 3).

           IF CFS-30-ENCMBR-AMT-CURR-BAL > 0
              ADD CFS-30-ENCMBR-AMT-CURR-BAL TO
                  EACB-DEBIT-AMOUNT (WS-PHASE, 2)
                  EACB-DEBIT-AMOUNT (WS-PHASE, 3)
           ELSE
              ADD CFS-30-ENCMBR-AMT-CURR-BAL TO
                  EACB-CREDIT-AMOUNT (WS-PHASE, 2)
                  EACB-CREDIT-AMOUNT (WS-PHASE, 3).

           ADD CFS-30-ENCMBR-AMT-CURR-BAL TO
               EACB-NET-AMOUNT (WS-PHASE, 2)
               EACB-NET-AMOUNT (WS-PHASE, 3).

       9460-EXIT.
           EXIT.

     /
       9470-ACCUM-EXPENDITURE-OBJECT.

      **---------------------------------------------------------------
      **  OBJECT - FINANCIAL AMOUNT - CURRENT BALANCE
      **---------------------------------------------------------------

           ADD 1 TO FACB-RECORD-COUNT (WS-PHASE, 7).

           IF CFS-31-FIN-AMT-CURR-BAL > 0
              ADD CFS-31-FIN-AMT-CURR-BAL TO
                  FACB-DEBIT-AMOUNT (WS-PHASE, 7)
           ELSE
              ADD CFS-31-FIN-AMT-CURR-BAL TO
                  FACB-CREDIT-AMOUNT (WS-PHASE, 7).

           ADD CFS-31-FIN-AMT-CURR-BAL TO
               FACB-NET-AMOUNT (WS-PHASE, 7).

      **---------------------------------------------------------------
      **  OBJECT - FINANCIAL AMOUNT - CURRENT MONTH ACTIVITY
      **---------------------------------------------------------------

           ADD 1 TO FACM-RECORD-COUNT (WS-PHASE, 7).

           IF CFS-31-FIN-AMT-CURR-MONTH-ACT > 0
              ADD CFS-31-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-DEBIT-AMOUNT (WS-PHASE, 7)
           ELSE
              ADD CFS-31-FIN-AMT-CURR-MONTH-ACT TO
                  FACM-CREDIT-AMOUNT (WS-PHASE, 7).

           ADD CFS-31-FIN-AMT-CURR-MONTH-ACT TO
               FACM-NET-AMOUNT (WS-PHASE, 7).

       9470-EXIT.
           EXIT.
