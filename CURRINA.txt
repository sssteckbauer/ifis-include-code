      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4027-INDRT-APPL' TO DBLINK-CURR-PARAGRAPH.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4027-INDRT-APPL' TO RECORD-NAME.                      276 BRTN
YYY991     MOVE 'F-INDRT-COST' TO AREA-NAME.                            276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4027-INDRT-APPL TO DBLINK-RECORD-MADE-CURRENT.  276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4027-INDRT-APPL-EXIT                     276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE INA-FK-ICE-INDRT-CSTCD TO DBLINK-R4027-INDRT-APPL-01276 BRTN
YYY991         MOVE INA-FK-ICE-START-DT TO DBLINK-R4027-INDRT-APPL-02   276 BRTN
YYY991         MOVE INA-FK-ICE-TIME-STMP TO DBLINK-R4027-INDRT-APPL-03  276 BRTN
YYY991         MOVE INA-INDRT-COST-FR-ACCT TO DBLINK-R4027-INDRT-APPL-04276 BRTN
YYY991         MOVE INA-ICE-SET-TS TO DBLINK-R4027-INDRT-APPL-05        276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4027-INDRT-APPL TO DBLINK-VIEW-NAME-CURRENT.    276 BRTN
YYY991     MOVE DBLINK-R4027-INDRT-APPL-KEY TO DBLINK-VIEW-200-CURRENT. 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4065-4027                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE INA-FK-ICE-INDRT-CSTCD TO DBLINK-S-4065-4027-01.        276 BRTN
YYY991     MOVE INA-FK-ICE-START-DT TO DBLINK-S-4065-4027-02.           276 BRTN
YYY991     MOVE INA-FK-ICE-TIME-STMP TO DBLINK-S-4065-4027-03.          276 BRTN
YYY991     MOVE INA-INDRT-COST-FR-ACCT TO DBLINK-S-4065-4027-04.        276 BRTN
YYY991     MOVE INA-ICE-SET-TS TO DBLINK-S-4065-4027-05.                276 BRTN
