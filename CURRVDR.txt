      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4073-VENDOR' TO DBLINK-CURR-PARAGRAPH.       276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4073-VENDOR' TO RECORD-NAME.                          276 BRTN
YYY991     MOVE 'F-VENDOR' TO AREA-NAME.                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR TO DBLINK-RECORD-MADE-CURRENT.      276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4073-VENDOR-EXIT                         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE VDR-IREF-ID TO DBLINK-R4073-VENDOR-01               276 BRTN
YYY991         MOVE VDR-ENTPSN-IND TO DBLINK-R4073-VENDOR-02            276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR TO DBLINK-VIEW-NAME-CURRENT.        276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-KEY TO DBLINK-VIEW-200-CURRENT.     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4081                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4081-01.        276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4081-02.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4081-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4083                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4083-01.        276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4083-02.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4083-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4091                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4091-01.        276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4091-02.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4091-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4104                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4104-01.        276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4104-02.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4104-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4150-I                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4150-I-01.      276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4150-I-02.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4150-I-KEY-M.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4150-V                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4150-V-01.      276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4150-V-02.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4150-V-KEY-M.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4153                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4153-01.        276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4153-02.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4153-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4165                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4165-01.        276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4165-02.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4165-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4410                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4410-01.        276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4410-02.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4410-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4410-I                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-01 TO DBLINK-S-4073-4410-I-01.      276 BRTN
YYY991     MOVE DBLINK-R4073-VENDOR-02 TO DBLINK-S-4073-4410-I-02.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4073-4410-I-KEY-M.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-VNDR                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE VDR-VNDR-ID-DGT-ONE TO DBLINK-S-INDX-VNDR-01.           276 BRTN
YYY991     MOVE VDR-VNDR-ID-LST-NINE TO DBLINK-S-INDX-VNDR-02.          276 BRTN
