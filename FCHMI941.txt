    ***Created by Convert/DC version V8R03 on 11/09/00 at 14:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE RQST-FIMS-ENTY-CODE-4011-CH94 OF FCHWK94 TO
                WK01-FMS-ENTY-CD-4011-CH94.
      *%--------------------------------------------------------------%*
           MOVE RQST-ELMNT-NAME-4011-CH94 OF FCHWK94 TO
                WK01-ELMNT-NAME-4011-CH94.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH94 OF FCHWM94(0001) TO WK01-CODE-CH941.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH94 OF FCHWM94(0002) TO WK01-CODE-CH942.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH94 OF FCHWM94(0003) TO WK01-CODE-CH943.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH94 OF FCHWM94(0004) TO WK01-CODE-CH944.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH94 OF FCHWM94(0005) TO WK01-CODE-CH945.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH94 OF FCHWM94(0006) TO WK01-CODE-CH946.
      *%--------------------------------------------------------------%*
           MOVE FIMS-ENTY-CODE-4011-CH94 OF FCHWM94(0001) TO
                WK01-ENTY-CODE-4011-CH941.
      *%--------------------------------------------------------------%*
           MOVE FIMS-ENTY-CODE-4011-CH94 OF FCHWM94(0002) TO
                WK01-ENTY-CODE-4011-CH942.
      *%--------------------------------------------------------------%*
           MOVE FIMS-ENTY-CODE-4011-CH94 OF FCHWM94(0003) TO
                WK01-ENTY-CODE-4011-CH943.
      *%--------------------------------------------------------------%*
           MOVE FIMS-ENTY-CODE-4011-CH94 OF FCHWM94(0004) TO
                WK01-ENTY-CODE-4011-CH944.
      *%--------------------------------------------------------------%*
           MOVE FIMS-ENTY-CODE-4011-CH94 OF FCHWM94(0005) TO
                WK01-ENTY-CODE-4011-CH945.
      *%--------------------------------------------------------------%*
           MOVE FIMS-ENTY-CODE-4011-CH94 OF FCHWM94(0006) TO
                WK01-ENTY-CODE-4011-CH946.
      *%--------------------------------------------------------------%*
           MOVE ELMNT-NAME-4011-CH94 OF FCHWM94(0001) TO
                WK01-NAME-4011-CH941.
      *%--------------------------------------------------------------%*
           MOVE ELMNT-NAME-4011-CH94 OF FCHWM94(0002) TO
                WK01-NAME-4011-CH942.
      *%--------------------------------------------------------------%*
           MOVE ELMNT-NAME-4011-CH94 OF FCHWM94(0003) TO
                WK01-NAME-4011-CH943.
      *%--------------------------------------------------------------%*
           MOVE ELMNT-NAME-4011-CH94 OF FCHWM94(0004) TO
                WK01-NAME-4011-CH944.
      *%--------------------------------------------------------------%*
           MOVE ELMNT-NAME-4011-CH94 OF FCHWM94(0005) TO
                WK01-NAME-4011-CH945.
      *%--------------------------------------------------------------%*
           MOVE ELMNT-NAME-4011-CH94 OF FCHWM94(0006) TO
                WK01-NAME-4011-CH946.
      *%--------------------------------------------------------------%*
           MOVE OPTN-1-CODE-4012-CH94 OF FCHWM94(0001) TO
                WK01-1-CODE-4012-CH941.
      *%--------------------------------------------------------------%*
           MOVE OPTN-1-CODE-4012-CH94 OF FCHWM94(0002) TO
                WK01-1-CODE-4012-CH942.
      *%--------------------------------------------------------------%*
           MOVE OPTN-1-CODE-4012-CH94 OF FCHWM94(0003) TO
                WK01-1-CODE-4012-CH943.
      *%--------------------------------------------------------------%*
           MOVE OPTN-1-CODE-4012-CH94 OF FCHWM94(0004) TO
                WK01-1-CODE-4012-CH944.
      *%--------------------------------------------------------------%*
           MOVE OPTN-1-CODE-4012-CH94 OF FCHWM94(0005) TO
                WK01-1-CODE-4012-CH945.
      *%--------------------------------------------------------------%*
           MOVE OPTN-1-CODE-4012-CH94 OF FCHWM94(0006) TO
                WK01-1-CODE-4012-CH946.
      *%--------------------------------------------------------------%*
           MOVE OPTN-2-CODE-4012-CH94 OF FCHWM94(0001) TO
                WK01-2-CODE-4012-CH941.
      *%--------------------------------------------------------------%*
           MOVE OPTN-2-CODE-4012-CH94 OF FCHWM94(0002) TO
                WK01-2-CODE-4012-CH942.
      *%--------------------------------------------------------------%*
           MOVE OPTN-2-CODE-4012-CH94 OF FCHWM94(0003) TO
                WK01-2-CODE-4012-CH943.
      *%--------------------------------------------------------------%*
           MOVE OPTN-2-CODE-4012-CH94 OF FCHWM94(0004) TO
                WK01-2-CODE-4012-CH944.
      *%--------------------------------------------------------------%*
           MOVE OPTN-2-CODE-4012-CH94 OF FCHWM94(0005) TO
                WK01-2-CODE-4012-CH945.
      *%--------------------------------------------------------------%*
           MOVE OPTN-2-CODE-4012-CH94 OF FCHWM94(0006) TO
                WK01-2-CODE-4012-CH946.
      *%--------------------------------------------------------------%*
           IF LEVEL-NMBR-4012-CH94 OF FCHWM94(0001) NOT NUMERIC
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0001) TO
                WK01-NMBR-4012-CH941-X
           ELSE
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0001) TO
                WK01-NMBR-4012-CH941
           END-IF.
      *%--------------------------------------------------------------%*
           IF LEVEL-NMBR-4012-CH94 OF FCHWM94(0002) NOT NUMERIC
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0002) TO
                WK01-NMBR-4012-CH942-X
           ELSE
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0002) TO
                WK01-NMBR-4012-CH942
           END-IF.
      *%--------------------------------------------------------------%*
           IF LEVEL-NMBR-4012-CH94 OF FCHWM94(0003) NOT NUMERIC
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0003) TO
                WK01-NMBR-4012-CH943-X
           ELSE
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0003) TO
                WK01-NMBR-4012-CH943
           END-IF.
      *%--------------------------------------------------------------%*
           IF LEVEL-NMBR-4012-CH94 OF FCHWM94(0004) NOT NUMERIC
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0004) TO
                WK01-NMBR-4012-CH944-X
           ELSE
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0004) TO
                WK01-NMBR-4012-CH944
           END-IF.
      *%--------------------------------------------------------------%*
           IF LEVEL-NMBR-4012-CH94 OF FCHWM94(0005) NOT NUMERIC
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0005) TO
                WK01-NMBR-4012-CH945-X
           ELSE
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0005) TO
                WK01-NMBR-4012-CH945
           END-IF.
      *%--------------------------------------------------------------%*
           IF LEVEL-NMBR-4012-CH94 OF FCHWM94(0006) NOT NUMERIC
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0006) TO
                WK01-NMBR-4012-CH946-X
           ELSE
              MOVE LEVEL-NMBR-4012-CH94 OF FCHWM94(0006) TO
                WK01-NMBR-4012-CH946
           END-IF.
      *%--------------------------------------------------------------%*
           MOVE SYSDAT-SHORT-DESC-4012-CH94 OF FCHWM94(0001) TO
                WK01-SHORT-DESC-4012-CH941.
      *%--------------------------------------------------------------%*
           MOVE SYSDAT-SHORT-DESC-4012-CH94 OF FCHWM94(0002) TO
                WK01-SHORT-DESC-4012-CH942.
      *%--------------------------------------------------------------%*
           MOVE SYSDAT-SHORT-DESC-4012-CH94 OF FCHWM94(0003) TO
                WK01-SHORT-DESC-4012-CH943.
      *%--------------------------------------------------------------%*
           MOVE SYSDAT-SHORT-DESC-4012-CH94 OF FCHWM94(0004) TO
                WK01-SHORT-DESC-4012-CH944.
      *%--------------------------------------------------------------%*
           MOVE SYSDAT-SHORT-DESC-4012-CH94 OF FCHWM94(0005) TO
                WK01-SHORT-DESC-4012-CH945.
      *%--------------------------------------------------------------%*
           MOVE SYSDAT-SHORT-DESC-4012-CH94 OF FCHWM94(0006) TO
                WK01-SHORT-DESC-4012-CH946.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4011-CH94 OF FCHWM94(0001) TO
                WK01-CODE-4011-CH941.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4011-CH94 OF FCHWM94(0002) TO
                WK01-CODE-4011-CH942.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4011-CH94 OF FCHWM94(0003) TO
                WK01-CODE-4011-CH943.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4011-CH94 OF FCHWM94(0004) TO
                WK01-CODE-4011-CH944.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4011-CH94 OF FCHWM94(0005) TO
                WK01-CODE-4011-CH945.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4011-CH94 OF FCHWM94(0006) TO
                WK01-CODE-4011-CH946.
      *%--------------------------------------------------------------%*
           MOVE RQST-COA-CODE-4011-CH94 OF FCHWK94 TO
                WK01-COA-CODE-4011-CH94.
