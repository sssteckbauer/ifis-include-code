       975-TO-NAMESYN-083 SECTION.
           MOVE NMS-SYN-NAME TO
               SYN-NAME-083 OF NAMESYN-083
                                                                     .
           MOVE NMS-RDF-NAM TO
               RDF-NAM-083 OF NAMESYN-083
                                                                     .
           MOVE NMS-DEPEND-ON TO
               DEPEND-ON-083 OF NAMESYN-083
                                                                     .
           MOVE NMS-NAMESYN-FLAG TO
               NAMESYN-FLAG-083 OF NAMESYN-083
                                                                     .
           MOVE NMS-DR-LPOS TO
               DR-LPOS-083 OF NAMESYN-083
                                                                     .
           MOVE NMS-BUILDER TO
               BUILDER-083 OF NAMESYN-083
                                                                     .
       975-TO-NAMESYN-083-EXIT.
           EXIT.
