    ***Created by Convert/DC version V8R03 on 12/11/00 at 16:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH26 OF FCHWM26 TO WK01-CODE-CH26.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4008-CH26 OF FCHWK26 TO WK01-CODE-4008-CH26.
      *%--------------------------------------------------------------%*
           MOVE FUND-TYPE-CODE-4008-CH26 OF FCHWK26 TO
                WK01-TYPE-CODE-4008-CH26.
      *%--------------------------------------------------------------%*
           MOVE FUND-TYPE-DESC-4009-CH26 OF FCHWM26 TO
                WK01-TYPE-DESC-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4009-CH26 OF FCHWM26 TO WK01-DATE-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4008-CH26 OF FCHWM26 TO WK01-DATE-4008-CH26.
      *%--------------------------------------------------------------%*
           MOVE TIME-STAMP-4009-CH26 OF FCHWM26 TO WK01-STAMP-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-CH26 OF FCHWM26 TO WK01-PM-FLAG-CH26.
      *%--------------------------------------------------------------%*
           MOVE INTRL-FUND-TYPE-4009-CH26 OF FCHWM26 TO
                WK01-FUND-TYPE-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE INTRL-FNDT-DESC-4012-CH26 OF FCHWM26 TO
                WK01-FNDT-DESC-4012-CH26.
      *%--------------------------------------------------------------%*
           MOVE PREDCSR-FUND-TYPE-4009-CH26 OF FCHWM26 TO
                WK01-FUND-TYPE-4009-CH2A.
      *%--------------------------------------------------------------%*
           MOVE PREDCSR-FNDT-DESC-4009-CH26 OF FCHWM26 TO
                WK01-FNDT-DESC-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE CPTLZN-FUND-CODE-4009-CH26 OF FCHWM26 TO
                WK01-FUND-CODE-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE CPTLZN-FUND-TITLE-4005-CH26 OF FCHWM26 TO
                WK01-FUND-TITLE-4005-CH26.
      *%--------------------------------------------------------------%*
           MOVE CPTLZN-ACCT-CODE-4009-CH26 OF FCHWM26 TO
                WK01-ACCT-CODE-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE CPTLZN-ACCT-TITLE-4034-CH26 OF FCHWM26 TO
                WK01-ACCT-TITLE-4034-CH26.
      *%--------------------------------------------------------------%*
           MOVE DFLT-FROM-IND-4009-CH26 OF FCHWM26 TO
                WK01-FROM-IND-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-BDGT-CNTRL-4009-CH26 OF FCHWM26 TO
                WK01-INDX-BDGT-CNTRL-4009-C.
      *%--------------------------------------------------------------%*
           MOVE FUND-BDGT-CNTRL-4009-CH26 OF FCHWM26 TO
                WK01-BDGT-CNTRL-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-BDGT-CNTRL-4009-CH26 OF FCHWM26 TO
                WK01-BDGT-CNTRL-4009-CH2A.
      *%--------------------------------------------------------------%*
           MOVE ACCT-BDGT-CNTRL-4009-CH26 OF FCHWM26 TO
                WK01-BDGT-CNTRL-4009-CH2B.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-BDGT-CNTRL-4009-CH26 OF FCHWM26 TO
                WK01-BDGT-CNTRL-4009-CH2C.
      *%--------------------------------------------------------------%*
           MOVE CNTRL-PRD-CODE-4009-CH26 OF FCHWM26 TO
                WK01-PRD-CODE-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE NEXT-CHNG-DATE-CH26 OF FCHWM26 TO WK01-CHNG-DATE-CH26.
      *%--------------------------------------------------------------%*
           MOVE STATUS-DESC-CH26 OF FCHWM26 TO WK01-DESC-CH26.
      *%--------------------------------------------------------------%*
           MOVE CNTRL-SVRTY-CODE-4009-CH26 OF FCHWM26 TO
                WK01-SVRTY-CODE-4009-CH26.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4009-CH26 OF FCHWK26 TO WK01-DATE-4009-CH2A.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-BDGT-DESC-CH26 OF FCHWM26 TO
                WK01-INDX-BDGT-DESC-CH26.
      *%--------------------------------------------------------------%*
           MOVE FUND-BDGT-DESC-CH26 OF FCHWM26 TO WK01-BDGT-DESC-CH26.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-BDGT-DESC-CH26 OF FCHWM26 TO WK01-BDGT-DESC-CH2A.
      *%--------------------------------------------------------------%*
           MOVE ACCT-BDGT-DESC-CH26 OF FCHWM26 TO WK01-BDGT-DESC-CH2B.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-BDGT-DESC-CH26 OF FCHWM26 TO WK01-BDGT-DESC-CH2C.
      *%--------------------------------------------------------------%*
           MOVE CNTRL-PRD-DESC-4012-CH26 OF FCHWM26 TO
                WK01-PRD-DESC-4012-CH26.
      *%--------------------------------------------------------------%*
           MOVE CNTRL-SVRTY-DESC-4012-CH26 OF FCHWM26 TO
                WK01-SVRTY-DESC-4012-CH26.
