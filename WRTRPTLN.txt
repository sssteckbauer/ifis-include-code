      *                                                                 WRTRPTLN
      *      **  8930-WRITE-REPORT-LINE                                 WRTRPTLN
      *                                                                 WRTRPTLN
           IF (RPT-CC = '1')                                            WRTRPTLN
               WRITE REPORT-LINE AFTER ADVANCING NEXT-PAGE              WRTRPTLN
               MOVE  ZERO  TO  RPT-LINE-CNT                             WRTRPTLN
           ELSE IF (RPT-CC = SPACE)                                     WRTRPTLN
                    WRITE REPORT-LINE AFTER ADVANCING 1 LINES           WRTRPTLN
                    ADD  +1  TO  RPT-LINE-CNT                           WRTRPTLN
           ELSE IF (RPT-CC = '0')                                       WRTRPTLN
                    WRITE REPORT-LINE AFTER ADVANCING 2 LINES           WRTRPTLN
                    ADD  +2  TO  RPT-LINE-CNT                           WRTRPTLN
           ELSE IF (RPT-CC = '-')                                       WRTRPTLN
                    WRITE REPORT-LINE AFTER ADVANCING 3 LINES           WRTRPTLN
                    ADD  +3  TO  RPT-LINE-CNT.                          WRTRPTLN
