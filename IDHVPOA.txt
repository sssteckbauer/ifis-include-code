       976-FROM-R4061-PO-ACCT SECTION.

FCCCE**        MOVE USER-CODE-4061 OF R4061-PO-ACCT
FCCCE          MOVE DBLINK-USER-CD
               TO POA-USER-CD.

               MOVE LAST-ACTVY-DATE-4061 OF R4061-PO-ACCT
               TO POA-LAST-ACTVY-DT.

           IF SEQ-NMBR-4061 OF R4061-PO-ACCT  NOT NUMERIC
               MOVE ZEROS TO POA-SEQ-NBR
           ELSE
               MOVE SEQ-NMBR-4061 OF R4061-PO-ACCT
               TO POA-SEQ-NBR
           END-IF.

           IF PCT-DSTBN-4061 OF R4061-PO-ACCT  NOT NUMERIC
               MOVE ZEROS TO POA-PCT-DSTBN
           ELSE
               MOVE PCT-DSTBN-4061 OF R4061-PO-ACCT
               TO POA-PCT-DSTBN
           END-IF.

               MOVE COA-CODE-4061 OF R4061-PO-ACCT
               TO POA-COA-CD.

               MOVE ACCT-INDX-CODE-4061 OF R4061-PO-ACCT
               TO POA-ACCT-INDX-CD.

               MOVE FUND-CODE-4061 OF R4061-PO-ACCT
               TO POA-FUND-CD.

               MOVE ORGZN-CODE-4061 OF R4061-PO-ACCT
               TO POA-ORGN-CD.

               MOVE ACCT-CODE-4061 OF R4061-PO-ACCT
               TO POA-ACCT-CD.

               MOVE PRGRM-CODE-4061 OF R4061-PO-ACCT
               TO POA-PRGRM-CD.

               MOVE ACTVY-CODE-4061 OF R4061-PO-ACCT
               TO POA-ACTVY-CD.

               MOVE LCTN-CODE-4061 OF R4061-PO-ACCT
               TO POA-LCTN-CD.

           IF AMT-4061 OF R4061-PO-ACCT  NOT NUMERIC
               MOVE ZEROS TO POA-AMT
           ELSE
               MOVE AMT-4061 OF R4061-PO-ACCT
               TO POA-AMT
           END-IF.

               MOVE PRJCT-CODE-4061 OF R4061-PO-ACCT
               TO POA-PRJCT-CD.

               MOVE ACCT-ERROR-IND-4061 OF R4061-PO-ACCT
               TO POA-ACCT-ERROR-IND.

               MOVE RULE-CLASS-CODE-4061 OF R4061-PO-ACCT
               TO POA-RULE-CLS-CD.

               MOVE DSCNT-RULE-CLASS-4061 OF R4061-PO-ACCT
               TO POA-DSCNT-RULE-CLS.

               MOVE TAX-RULE-CLASS-4061 OF R4061-PO-ACCT
               TO POA-TAX-RULE-CLS.

               MOVE ADDL-CHRG-RULE-CLASS-4061 OF R4061-PO-ACCT
               TO POA-ADDL-CHRG-RULE-CLS.

               MOVE FSCL-YR-4061 OF R4061-PO-ACCT
               TO POA-FSCL-YR.

               MOVE PSTNG-PRD-4061 OF R4061-PO-ACCT
               TO POA-PSTNG-PRD.

               MOVE NSF-OVRDE-4061 OF R4061-PO-ACCT
               TO POA-NSF-OVRDE.

           IF ADDL-CHRG-4061 OF R4061-PO-ACCT  NOT NUMERIC
               MOVE ZEROS TO POA-ADDL-CHRG
           ELSE
               MOVE ADDL-CHRG-4061 OF R4061-PO-ACCT
               TO POA-ADDL-CHRG
           END-IF.

           IF TAX-AMT-4061 OF R4061-PO-ACCT  NOT NUMERIC
               MOVE ZEROS TO POA-TAX-AMT
           ELSE
               MOVE TAX-AMT-4061 OF R4061-PO-ACCT
               TO POA-TAX-AMT
           END-IF.

               MOVE LIEN-CLOSED-CODE-4061 OF R4061-PO-ACCT
               TO POA-LIEN-CLOSED-CD.

           IF PO-DSCNT-AMT-4061 OF R4061-PO-ACCT  NOT NUMERIC
               MOVE ZEROS TO POA-PO-DSCNT-AMT
           ELSE
               MOVE PO-DSCNT-AMT-4061 OF R4061-PO-ACCT
               TO POA-PO-DSCNT-AMT
           END-IF.
       976-FROM-R4061-PO-ACCT-EXIT.
           EXIT.
