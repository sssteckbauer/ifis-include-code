    ***Created by Convert/DC version V8R03 on 12/05/00 at 10:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE TARGET-FULL-NAME-UT26 OF FUTWM26 TO WK01-FULL-NAME-UT26.
      *%--------------------------------------------------------------%*
           MOVE REPLACE-ID-FULL-NAME-UT26 OF FUTWM26 TO
                WK01-ID-FULL-NAME-UT26.
      *%--------------------------------------------------------------%*
           MOVE REPLACE-ID-UT26 OF FUTWM26 TO WK01-ID-UT26.
      *%--------------------------------------------------------------%*
           MOVE TARGET-ID-UT26 OF FUTWM26 TO WK01-ID-UT2A.
      *%--------------------------------------------------------------%*
           MOVE CMPLT-IND-UT26 OF FUTWM26 TO WK01-IND-UT26.
      *%--------------------------------------------------------------%*
           MOVE DOC-TYPE-UT26 OF FUTWM26   TO  WK01-TYPE-UT26
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-UT26 OF FUTWM26 TO  WK01-DESC-UT26.
      *%--------------------------------------------------------------%*
           MOVE TARGET-TMPL-CODE-UT26      TO  WK01-TMPL-UT26
