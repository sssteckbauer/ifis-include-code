       975-TO-R4006-LCTN SECTION.
           MOVE LTN-USER-CD TO
               USER-CODE-4006 OF R4006-LCTN
                                                                     .
           MOVE LTN-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4006 OF R4006-LCTN
                                                                     .
           MOVE LTN-UNVRS-CD TO
               UNVRS-CODE-4006 OF R4006-LCTN
                                                                     .
           MOVE LTN-COA-CD TO
               COA-CODE-4006 OF R4006-LCTN
                                                                     .
           MOVE LTN-LCTN-CD TO
               LCTN-CODE-4006 OF R4006-LCTN
                                                                     .
           MOVE LTN-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4006 OF R4006-LCTN
                                                                     .
       975-TO-R4006-LCTN-EXIT.
           EXIT.
