    ***Created by Convert/DC version V8R03 on 12/11/00 at 11:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH12-Z = 'Y'
               MOVE WK01-CODE-CH12 TO ACTN-CODE-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4033-CH12-Z = 'Y'
               MOVE WK01-ACCT-CODE-4033-CH12 TO
                BANK-ACCT-CODE-4033-CH12 OF FCHWK12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHNG-DATE-CH12-Z = 'Y'
               MOVE WK01-CHNG-DATE-CH12 TO NEXT-CHNG-DATE-CH12 OF
                FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-CH12-Z = 'Y'
               MOVE WK01-DESC-CH12 TO STATUS-DESC-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4062-CH12-Z = 'Y'
               MOVE WK01-DATE-4062-CH12 TO END-DATE-4062-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4033-CH12-Z = 'Y'
               MOVE WK01-DATE-4033-CH12 TO ACTVY-DATE-4033-CH12 OF
                FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STAMP-4062-CH12-Z = 'Y'
               MOVE WK01-STAMP-4062-CH12 TO TIME-STAMP-4062-CH12 OF
                FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-4062-CH12-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-4062-CH12 TO
                BANK-ID-LAST-NINE-4062-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH12-Z = 'Y'
               MOVE WK01-KEY-6311-CH12 TO NAME-KEY-6311-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-NMBR-4213-CH12-Z = 'Y'
               MOVE WK01-ACCT-NMBR-4213-CH12 TO
                BANK-ACCT-NMBR-4213-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-DESC-4213-CH12-Z = 'Y'
               MOVE WK01-ACCT-DESC-4213-CH12 TO
                BANK-ACCT-DESC-4213-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUND-CODE-4062-CH12-Z = 'Y'
               MOVE WK01-FUND-CODE-4062-CH12 TO
                BANK-FUND-CODE-4062-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH12-Z = 'Y'
               MOVE WK01-TITLE-4005-CH12 TO FUND-TITLE-4005-CH12 OF
                FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4062-CH12-Z = 'Y'
               MOVE WK01-ACCT-CODE-4062-CH12 TO
                CASH-ACCT-CODE-4062-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TITLE-4034-CH12-Z = 'Y'
               MOVE WK01-CODE-TITLE-4034-CH12 TO
                ACCT-CODE-TITLE-4034-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4062-CH1A-Z = 'Y'
               MOVE WK01-ACCT-CODE-4062-CH1A TO
                INTRFND-ACCT-CODE-4062-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-TITLE-4034-CH12-Z = 'Y'
               MOVE WK01-ACCT-TITLE-4034-CH12 TO
                INTRFND-ACCT-TITLE-4034-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-4062-CH12-Z = 'Y'
               MOVE WK01-CODE-DESC-4062-CH12 TO
                BANK-CODE-DESC-4062-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4062-CH12-Z = 'Y'
               MOVE WK01-CODE-4062-CH12 TO COA-CODE-4062-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-CH12-Z = 'Y'
               MOVE WK01-PM-FLAG-CH12 TO AM-PM-FLAG-CH12 OF FCHWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4062-CH1A-Z = 'Y'
               MOVE WK01-DATE-4062-CH1A TO START-DATE-4062-CH12 OF
                FCHWK12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4076-CH12-Z = 'Y'
               MOVE WK01-TITLE-4076-CH12 TO COA-TITLE-4076-CH12 OF
                FCHWM12
           END-IF.
