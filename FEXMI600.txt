    ***Created by Convert/DC version V8R03 on 12/05/00 at 11:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX601-Z = 'Y'
               MOVE WK01-CODE-EX601 TO ACTN-CODE-EX60 OF FEXWM60(0001)
           END-IF.
           MOVE WK01-CODE-EX601-F TO WK01-CODE-EX60-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX602-Z = 'Y'
               MOVE WK01-CODE-EX602 TO ACTN-CODE-EX60 OF FEXWM60(0002)
           END-IF.
           MOVE WK01-CODE-EX602-F TO WK01-CODE-EX60-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX603-Z = 'Y'
               MOVE WK01-CODE-EX603 TO ACTN-CODE-EX60 OF FEXWM60(0003)
           END-IF.
           MOVE WK01-CODE-EX603-F TO WK01-CODE-EX60-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX604-Z = 'Y'
               MOVE WK01-CODE-EX604 TO ACTN-CODE-EX60 OF FEXWM60(0004)
           END-IF.
           MOVE WK01-CODE-EX604-F TO WK01-CODE-EX60-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX605-Z = 'Y'
               MOVE WK01-CODE-EX605 TO ACTN-CODE-EX60 OF FEXWM60(0005)
           END-IF.
           MOVE WK01-CODE-EX605-F TO WK01-CODE-EX60-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX606-Z = 'Y'
               MOVE WK01-CODE-EX606 TO ACTN-CODE-EX60 OF FEXWM60(0006)
           END-IF.
           MOVE WK01-CODE-EX606-F TO WK01-CODE-EX60-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX607-Z = 'Y'
               MOVE WK01-CODE-EX607 TO ACTN-CODE-EX60 OF FEXWM60(0007)
           END-IF.
           MOVE WK01-CODE-EX607-F TO WK01-CODE-EX60-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX608-Z = 'Y'
               MOVE WK01-CODE-EX608 TO ACTN-CODE-EX60 OF FEXWM60(0008)
           END-IF.
           MOVE WK01-CODE-EX608-F TO WK01-CODE-EX60-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX609-Z = 'Y'
               MOVE WK01-CODE-EX609 TO ACTN-CODE-EX60 OF FEXWM60(0009)
           END-IF.
           MOVE WK01-CODE-EX609-F TO WK01-CODE-EX60-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6010-Z = 'Y'
               MOVE WK01-CODE-EX6010 TO ACTN-CODE-EX60 OF FEXWM60(0010)
           END-IF.
           MOVE WK01-CODE-EX6010-F TO WK01-CODE-EX60-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX601-Z = 'Y'
               MOVE WK01-NMBR-EX601 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0001)
           END-IF.
           MOVE WK01-NMBR-EX601-F TO WK01-NMBR-EX60-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX602-Z = 'Y'
               MOVE WK01-NMBR-EX602 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0002)
           END-IF.
           MOVE WK01-NMBR-EX602-F TO WK01-NMBR-EX60-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX603-Z = 'Y'
               MOVE WK01-NMBR-EX603 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0003)
           END-IF.
           MOVE WK01-NMBR-EX603-F TO WK01-NMBR-EX60-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX604-Z = 'Y'
               MOVE WK01-NMBR-EX604 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0004)
           END-IF.
           MOVE WK01-NMBR-EX604-F TO WK01-NMBR-EX60-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX605-Z = 'Y'
               MOVE WK01-NMBR-EX605 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0005)
           END-IF.
           MOVE WK01-NMBR-EX605-F TO WK01-NMBR-EX60-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX606-Z = 'Y'
               MOVE WK01-NMBR-EX606 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0006)
           END-IF.
           MOVE WK01-NMBR-EX606-F TO WK01-NMBR-EX60-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX607-Z = 'Y'
               MOVE WK01-NMBR-EX607 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0007)
           END-IF.
           MOVE WK01-NMBR-EX607-F TO WK01-NMBR-EX60-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX608-Z = 'Y'
               MOVE WK01-NMBR-EX608 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0008)
           END-IF.
           MOVE WK01-NMBR-EX608-F TO WK01-NMBR-EX60-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX609-Z = 'Y'
               MOVE WK01-NMBR-EX609 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0009)
           END-IF.
           MOVE WK01-NMBR-EX609-F TO WK01-NMBR-EX60-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6010-Z = 'Y'
               MOVE WK01-NMBR-EX6010 TO RELEASE-NMBR-EX60 OF
                FEXWM60(0010)
           END-IF.
           MOVE WK01-NMBR-EX6010-F TO WK01-NMBR-EX60-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX601-Z = 'Y'
               MOVE WK01-INDX-CODE-EX601 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0001)
           END-IF.
           MOVE WK01-INDX-CODE-EX601-F TO WK01-INDX-CODE-EX60-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX602-Z = 'Y'
               MOVE WK01-INDX-CODE-EX602 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0002)
           END-IF.
           MOVE WK01-INDX-CODE-EX602-F TO WK01-INDX-CODE-EX60-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX603-Z = 'Y'
               MOVE WK01-INDX-CODE-EX603 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0003)
           END-IF.
           MOVE WK01-INDX-CODE-EX603-F TO WK01-INDX-CODE-EX60-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX604-Z = 'Y'
               MOVE WK01-INDX-CODE-EX604 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0004)
           END-IF.
           MOVE WK01-INDX-CODE-EX604-F TO WK01-INDX-CODE-EX60-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX605-Z = 'Y'
               MOVE WK01-INDX-CODE-EX605 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0005)
           END-IF.
           MOVE WK01-INDX-CODE-EX605-F TO WK01-INDX-CODE-EX60-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX606-Z = 'Y'
               MOVE WK01-INDX-CODE-EX606 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0006)
           END-IF.
           MOVE WK01-INDX-CODE-EX606-F TO WK01-INDX-CODE-EX60-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX607-Z = 'Y'
               MOVE WK01-INDX-CODE-EX607 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0007)
           END-IF.
           MOVE WK01-INDX-CODE-EX607-F TO WK01-INDX-CODE-EX60-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX608-Z = 'Y'
               MOVE WK01-INDX-CODE-EX608 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0008)
           END-IF.
           MOVE WK01-INDX-CODE-EX608-F TO WK01-INDX-CODE-EX60-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX609-Z = 'Y'
               MOVE WK01-INDX-CODE-EX609 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0009)
           END-IF.
           MOVE WK01-INDX-CODE-EX609-F TO WK01-INDX-CODE-EX60-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX6010-Z = 'Y'
               MOVE WK01-INDX-CODE-EX6010 TO ACCT-INDX-CODE-EX60 OF
                FEXWM60(0010)
           END-IF.
           MOVE WK01-INDX-CODE-EX6010-F TO WK01-INDX-CODE-EX60-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX601-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX601 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0001)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX601-F TO WK01-ID-LAST-NINE-EX60-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX602-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX602 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0002)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX602-F TO WK01-ID-LAST-NINE-EX60-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX603-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX603 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0003)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX603-F TO WK01-ID-LAST-NINE-EX60-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX604-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX604 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0004)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX604-F TO WK01-ID-LAST-NINE-EX60-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX605-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX605 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0005)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX605-F TO WK01-ID-LAST-NINE-EX60-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX606-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX606 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0006)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX606-F TO WK01-ID-LAST-NINE-EX60-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX607-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX607 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0007)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX607-F TO WK01-ID-LAST-NINE-EX60-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX608-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX608 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0008)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX608-F TO WK01-ID-LAST-NINE-EX60-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX609-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX609 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0009)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX609-F TO WK01-ID-LAST-NINE-EX60-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX6010-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX6010 TO VNDR-ID-LAST-NINE-EX60
                OF FEXWM60(0010)
           END-IF.
           MOVE WK01-ID-LAST-NINE-EX6010-F TO WK01-ID-LAST-NINE-EX60-F
                (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A1-Z = 'Y'
               MOVE WK01-CODE-EX6A1 TO CMDTY-CODE-EX60 OF FEXWM60(0001)
           END-IF.
           MOVE WK01-CODE-EX6A1-F TO WK01-CODE-EX6A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A2-Z = 'Y'
               MOVE WK01-CODE-EX6A2 TO CMDTY-CODE-EX60 OF FEXWM60(0002)
           END-IF.
           MOVE WK01-CODE-EX6A2-F TO WK01-CODE-EX6A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A3-Z = 'Y'
               MOVE WK01-CODE-EX6A3 TO CMDTY-CODE-EX60 OF FEXWM60(0003)
           END-IF.
           MOVE WK01-CODE-EX6A3-F TO WK01-CODE-EX6A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A4-Z = 'Y'
               MOVE WK01-CODE-EX6A4 TO CMDTY-CODE-EX60 OF FEXWM60(0004)
           END-IF.
           MOVE WK01-CODE-EX6A4-F TO WK01-CODE-EX6A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A5-Z = 'Y'
               MOVE WK01-CODE-EX6A5 TO CMDTY-CODE-EX60 OF FEXWM60(0005)
           END-IF.
           MOVE WK01-CODE-EX6A5-F TO WK01-CODE-EX6A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A6-Z = 'Y'
               MOVE WK01-CODE-EX6A6 TO CMDTY-CODE-EX60 OF FEXWM60(0006)
           END-IF.
           MOVE WK01-CODE-EX6A6-F TO WK01-CODE-EX6A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A7-Z = 'Y'
               MOVE WK01-CODE-EX6A7 TO CMDTY-CODE-EX60 OF FEXWM60(0007)
           END-IF.
           MOVE WK01-CODE-EX6A7-F TO WK01-CODE-EX6A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A8-Z = 'Y'
               MOVE WK01-CODE-EX6A8 TO CMDTY-CODE-EX60 OF FEXWM60(0008)
           END-IF.
           MOVE WK01-CODE-EX6A8-F TO WK01-CODE-EX6A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A9-Z = 'Y'
               MOVE WK01-CODE-EX6A9 TO CMDTY-CODE-EX60 OF FEXWM60(0009)
           END-IF.
           MOVE WK01-CODE-EX6A9-F TO WK01-CODE-EX6A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX6A10-Z = 'Y'
               MOVE WK01-CODE-EX6A10 TO CMDTY-CODE-EX60 OF FEXWM60(0010)
           END-IF.
           MOVE WK01-CODE-EX6A10-F TO WK01-CODE-EX6A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A1-Z = 'Y'
               MOVE WK01-NMBR-EX6A1 TO FILE-NMBR-EX60 OF FEXWM60(0001)
           END-IF.
           MOVE WK01-NMBR-EX6A1-F TO WK01-NMBR-EX6A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A2-Z = 'Y'
               MOVE WK01-NMBR-EX6A2 TO FILE-NMBR-EX60 OF FEXWM60(0002)
           END-IF.
           MOVE WK01-NMBR-EX6A2-F TO WK01-NMBR-EX6A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A3-Z = 'Y'
               MOVE WK01-NMBR-EX6A3 TO FILE-NMBR-EX60 OF FEXWM60(0003)
           END-IF.
           MOVE WK01-NMBR-EX6A3-F TO WK01-NMBR-EX6A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A4-Z = 'Y'
               MOVE WK01-NMBR-EX6A4 TO FILE-NMBR-EX60 OF FEXWM60(0004)
           END-IF.
           MOVE WK01-NMBR-EX6A4-F TO WK01-NMBR-EX6A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A5-Z = 'Y'
               MOVE WK01-NMBR-EX6A5 TO FILE-NMBR-EX60 OF FEXWM60(0005)
           END-IF.
           MOVE WK01-NMBR-EX6A5-F TO WK01-NMBR-EX6A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A6-Z = 'Y'
               MOVE WK01-NMBR-EX6A6 TO FILE-NMBR-EX60 OF FEXWM60(0006)
           END-IF.
           MOVE WK01-NMBR-EX6A6-F TO WK01-NMBR-EX6A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A7-Z = 'Y'
               MOVE WK01-NMBR-EX6A7 TO FILE-NMBR-EX60 OF FEXWM60(0007)
           END-IF.
           MOVE WK01-NMBR-EX6A7-F TO WK01-NMBR-EX6A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A8-Z = 'Y'
               MOVE WK01-NMBR-EX6A8 TO FILE-NMBR-EX60 OF FEXWM60(0008)
           END-IF.
           MOVE WK01-NMBR-EX6A8-F TO WK01-NMBR-EX6A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A9-Z = 'Y'
               MOVE WK01-NMBR-EX6A9 TO FILE-NMBR-EX60 OF FEXWM60(0009)
           END-IF.
           MOVE WK01-NMBR-EX6A9-F TO WK01-NMBR-EX6A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX6A10-Z = 'Y'
               MOVE WK01-NMBR-EX6A10 TO FILE-NMBR-EX60 OF FEXWM60(0010)
           END-IF.
           MOVE WK01-NMBR-EX6A10-F TO WK01-NMBR-EX6A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-RELEASE-NMBR-EX60-Z = 'Y'
               MOVE WK01-RELEASE-NMBR-EX60 TO REQ-RELEASE-NMBR-EX60 OF
                FEXWK60
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-INDX-CODE-EX60-Z = 'Y'
               MOVE WK01-ACCT-INDX-CODE-EX60 TO REQ-ACCT-INDX-CODE-EX60
                OF FEXWK60
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX601-Z = 'Y'
               MOVE WK01-ERROR-IND-EX601 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0001)
           END-IF.
           MOVE WK01-ERROR-IND-EX601-F TO WK01-ERROR-IND-EX60-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX602-Z = 'Y'
               MOVE WK01-ERROR-IND-EX602 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0002)
           END-IF.
           MOVE WK01-ERROR-IND-EX602-F TO WK01-ERROR-IND-EX60-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX603-Z = 'Y'
               MOVE WK01-ERROR-IND-EX603 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0003)
           END-IF.
           MOVE WK01-ERROR-IND-EX603-F TO WK01-ERROR-IND-EX60-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX604-Z = 'Y'
               MOVE WK01-ERROR-IND-EX604 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0004)
           END-IF.
           MOVE WK01-ERROR-IND-EX604-F TO WK01-ERROR-IND-EX60-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX605-Z = 'Y'
               MOVE WK01-ERROR-IND-EX605 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0005)
           END-IF.
           MOVE WK01-ERROR-IND-EX605-F TO WK01-ERROR-IND-EX60-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX606-Z = 'Y'
               MOVE WK01-ERROR-IND-EX606 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0006)
           END-IF.
           MOVE WK01-ERROR-IND-EX606-F TO WK01-ERROR-IND-EX60-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX607-Z = 'Y'
               MOVE WK01-ERROR-IND-EX607 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0007)
           END-IF.
           MOVE WK01-ERROR-IND-EX607-F TO WK01-ERROR-IND-EX60-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX608-Z = 'Y'
               MOVE WK01-ERROR-IND-EX608 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0008)
           END-IF.
           MOVE WK01-ERROR-IND-EX608-F TO WK01-ERROR-IND-EX60-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX609-Z = 'Y'
               MOVE WK01-ERROR-IND-EX609 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0009)
           END-IF.
           MOVE WK01-ERROR-IND-EX609-F TO WK01-ERROR-IND-EX60-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6010-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6010 TO HDR-ERROR-IND-EX60 OF
                FEXWM60(0010)
           END-IF.
           MOVE WK01-ERROR-IND-EX6010-F TO WK01-ERROR-IND-EX60-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A1-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A1 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0001)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A1-F TO WK01-ERROR-IND-EX6A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A2-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A2 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0002)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A2-F TO WK01-ERROR-IND-EX6A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A3-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A3 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0003)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A3-F TO WK01-ERROR-IND-EX6A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A4-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A4 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0004)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A4-F TO WK01-ERROR-IND-EX6A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A5-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A5 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0005)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A5-F TO WK01-ERROR-IND-EX6A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A6-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A6 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0006)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A6-F TO WK01-ERROR-IND-EX6A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A7-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A7 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0007)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A7-F TO WK01-ERROR-IND-EX6A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A8-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A8 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0008)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A8-F TO WK01-ERROR-IND-EX6A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A9-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A9 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0009)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A9-F TO WK01-ERROR-IND-EX6A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-EX6A10-Z = 'Y'
               MOVE WK01-ERROR-IND-EX6A10 TO DTL-ERROR-IND-EX60 OF
                FEXWM60(0010)
           END-IF.
           MOVE WK01-ERROR-IND-EX6A10-F TO WK01-ERROR-IND-EX6A-F (10).
