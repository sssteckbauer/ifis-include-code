    ***Created by Convert/DC version V8R03 on 11/09/00 at 16:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE FUND-TITLE-4005-GA05 OF FGAWM05 TO WK01-TITLE-4005-GA05.
      *%--------------------------------------------------------------%*
           MOVE TOT-BEG-BAL-AMT-GA05 OF FGAWM05 TO
                WK01-BEG-BAL-AMT-GA05.
      *%--------------------------------------------------------------%*
           MOVE TOT-CRD-BAL-AMT-GA05 OF FGAWM05 TO
                WK01-CRD-BAL-AMT-GA05.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0001) TO
                WK01-ACCT-CODE-4003-GA051.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0002) TO
                WK01-ACCT-CODE-4003-GA052.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0003) TO
                WK01-ACCT-CODE-4003-GA053.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0004) TO
                WK01-ACCT-CODE-4003-GA054.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0005) TO
                WK01-ACCT-CODE-4003-GA055.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0006) TO
                WK01-ACCT-CODE-4003-GA056.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0007) TO
                WK01-ACCT-CODE-4003-GA057.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0008) TO
                WK01-ACCT-CODE-4003-GA058.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0009) TO
                WK01-ACCT-CODE-4003-GA059.
      *%--------------------------------------------------------------%*
           MOVE BAL-ACCT-CODE-4003-GA05 OF FGAWM05(0010) TO
                WK01-ACCT-CODE-4003-GA0510.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0001) TO
                WK01-CODE-TITLE-4034-GA051.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0002) TO
                WK01-CODE-TITLE-4034-GA052.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0003) TO
                WK01-CODE-TITLE-4034-GA053.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0004) TO
                WK01-CODE-TITLE-4034-GA054.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0005) TO
                WK01-CODE-TITLE-4034-GA055.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0006) TO
                WK01-CODE-TITLE-4034-GA056.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0007) TO
                WK01-CODE-TITLE-4034-GA057.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0008) TO
                WK01-CODE-TITLE-4034-GA058.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0009) TO
                WK01-CODE-TITLE-4034-GA059.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-GA05 OF FGAWM05(0010) TO
                WK01-CODE-TITLE-4034-GA0510.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0001) TO WK01-BAL-AMT-GA051.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0002) TO WK01-BAL-AMT-GA052.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0003) TO WK01-BAL-AMT-GA053.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0004) TO WK01-BAL-AMT-GA054.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0005) TO WK01-BAL-AMT-GA055.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0006) TO WK01-BAL-AMT-GA056.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0007) TO WK01-BAL-AMT-GA057.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0008) TO WK01-BAL-AMT-GA058.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0009) TO WK01-BAL-AMT-GA059.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-AMT-GA05 OF FGAWM05(0010) TO
                WK01-BAL-AMT-GA0510.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0001) TO
                WK01-BAL-AMT-GA0A1.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0002) TO
                WK01-BAL-AMT-GA0A2.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0003) TO
                WK01-BAL-AMT-GA0A3.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0004) TO
                WK01-BAL-AMT-GA0A4.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0005) TO
                WK01-BAL-AMT-GA0A5.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0006) TO
                WK01-BAL-AMT-GA0A6.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0007) TO
                WK01-BAL-AMT-GA0A7.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0008) TO
                WK01-BAL-AMT-GA0A8.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0009) TO
                WK01-BAL-AMT-GA0A9.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-AMT-GA05 OF FGAWM05(0010) TO
                WK01-BAL-AMT-GA0A10.
      *%--------------------------------------------------------------%*
           MOVE TOT-DESC-GA05 OF FGAWM05 TO WK01-DESC-GA05.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0001) TO
                WK01-BAL-DC-IND-GA051.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0002) TO
                WK01-BAL-DC-IND-GA052.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0003) TO
                WK01-BAL-DC-IND-GA053.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0004) TO
                WK01-BAL-DC-IND-GA054.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0005) TO
                WK01-BAL-DC-IND-GA055.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0006) TO
                WK01-BAL-DC-IND-GA056.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0007) TO
                WK01-BAL-DC-IND-GA057.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0008) TO
                WK01-BAL-DC-IND-GA058.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0009) TO
                WK01-BAL-DC-IND-GA059.
      *%--------------------------------------------------------------%*
           MOVE BEG-BAL-DC-IND-GA05 OF FGAWM05(0010) TO
                WK01-BAL-DC-IND-GA0510.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0001) TO
                WK01-BAL-DC-IND-GA0A1.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0002) TO
                WK01-BAL-DC-IND-GA0A2.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0003) TO
                WK01-BAL-DC-IND-GA0A3.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0004) TO
                WK01-BAL-DC-IND-GA0A4.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0005) TO
                WK01-BAL-DC-IND-GA0A5.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0006) TO
                WK01-BAL-DC-IND-GA0A6.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0007) TO
                WK01-BAL-DC-IND-GA0A7.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0008) TO
                WK01-BAL-DC-IND-GA0A8.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0009) TO
                WK01-BAL-DC-IND-GA0A9.
      *%--------------------------------------------------------------%*
           MOVE CRNT-BAL-DC-IND-GA05 OF FGAWM05(0010) TO
                WK01-BAL-DC-IND-GA0A10.
      *%--------------------------------------------------------------%*
           MOVE BEG-TOT-DC-IND-GA05 OF FGAWM05 TO WK01-TOT-DC-IND-GA05.
      *%--------------------------------------------------------------%*
           MOVE CRNT-TOT-DC-IND-GA05 OF FGAWM05 TO WK01-TOT-DC-IND-GA0A.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-GA05 OF FGAWK05 TO WK01-CODE-GA05.
      *%--------------------------------------------------------------%*
           MOVE FSCL-YR-GA05 OF FGAWK05 TO WK01-YR-GA05.
      *%--------------------------------------------------------------%*
           MOVE ACTG-PRD-GA05 OF FGAWK05 TO WK01-PRD-GA05.
      *%--------------------------------------------------------------%*
           MOVE FUND-CODE-GA05 OF FGAWK05 TO WK01-CODE-GA0A.
      *%--------------------------------------------------------------%*
           MOVE PRD-START-DATE-4024-GA05 OF FGAWK05 TO
                WK01-START-DATE-4024-GA05.
      *%--------------------------------------------------------------%*
           MOVE PRD-END-DATE-4024-GA05 OF FGAWK05 TO
                WK01-END-DATE-4024-GA05.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-GA05 OF FGAWK05 TO WK01-CODE-4003-GA05.
