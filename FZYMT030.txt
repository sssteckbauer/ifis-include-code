    ***Created by Convert/DC version V8R03 on 07/18/00 at 16:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DTL-CD-CLMN-LBL-SY03-Z = 'Y'
               MOVE WK01-DTL-CD-CLMN-LBL-SY03 TO
                RQST-DTL-CODE-CLMN-LABEL-SY03 OF FZYWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY031-Z = 'Y'
               MOVE WK01-CODE-SY031 TO ACTN-CODE-SY03 OF FZYWM03(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY031-Z = 'Y'
               MOVE WK01-CODE-6601-SY031 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY031-Z = 'Y'
               MOVE WK01-DESC-6601-SY031 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A1-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A1 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY031-Z = 'Y'
               MOVE WK01-1-6601-SY031 TO FLAG-1-6601-SY03 OF
                FZYWM03(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY031-Z = 'Y'
               MOVE WK01-2-6601-SY031 TO FLAG-2-6601-SY03 OF
                FZYWM03(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CLMN-LBL-1-SY03-Z = 'Y'
               MOVE WK01-CODE-CLMN-LBL-1-SY03 TO
                DTL-CODE-CLMN-LABEL-1-SY03 OF FZYWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-LABEL-1-SY03-Z = 'Y'
               MOVE WK01-1-LABEL-1-SY03 TO FLAG-1-LABEL-1-SY03 OF
                FZYWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-LABEL-1-SY03-Z = 'Y'
               MOVE WK01-2-LABEL-1-SY03 TO FLAG-2-LABEL-1-SY03 OF
                FZYWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CLMN-LBL-2-SY03-Z = 'Y'
               MOVE WK01-CODE-CLMN-LBL-2-SY03 TO
                DTL-CODE-CLMN-LABEL-2-SY03 OF FZYWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-LABEL-2-SY03-Z = 'Y'
               MOVE WK01-1-LABEL-2-SY03 TO FLAG-1-LABEL-2-SY03 OF
                FZYWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-LABEL-2-SY03-Z = 'Y'
               MOVE WK01-2-LABEL-2-SY03 TO FLAG-2-LABEL-2-SY03 OF
                FZYWM03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY031-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY031 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0001)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY032-Z = 'Y'
               MOVE WK01-CODE-SY032 TO ACTN-CODE-SY03 OF FZYWM03(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY033-Z = 'Y'
               MOVE WK01-CODE-SY033 TO ACTN-CODE-SY03 OF FZYWM03(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY034-Z = 'Y'
               MOVE WK01-CODE-SY034 TO ACTN-CODE-SY03 OF FZYWM03(0004)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY035-Z = 'Y'
               MOVE WK01-CODE-SY035 TO ACTN-CODE-SY03 OF FZYWM03(0005)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY036-Z = 'Y'
               MOVE WK01-CODE-SY036 TO ACTN-CODE-SY03 OF FZYWM03(0006)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY037-Z = 'Y'
               MOVE WK01-CODE-SY037 TO ACTN-CODE-SY03 OF FZYWM03(0007)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY038-Z = 'Y'
               MOVE WK01-CODE-SY038 TO ACTN-CODE-SY03 OF FZYWM03(0008)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY039-Z = 'Y'
               MOVE WK01-CODE-SY039 TO ACTN-CODE-SY03 OF FZYWM03(0009)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY0310-Z = 'Y'
               MOVE WK01-CODE-SY0310 TO ACTN-CODE-SY03 OF FZYWM03(0010)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY0311-Z = 'Y'
               MOVE WK01-CODE-SY0311 TO ACTN-CODE-SY03 OF FZYWM03(0011)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY0312-Z = 'Y'
               MOVE WK01-CODE-SY0312 TO ACTN-CODE-SY03 OF FZYWM03(0012)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-SY0313-Z = 'Y'
               MOVE WK01-CODE-SY0313 TO ACTN-CODE-SY03 OF FZYWM03(0013)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY032-Z = 'Y'
               MOVE WK01-CODE-6601-SY032 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY033-Z = 'Y'
               MOVE WK01-CODE-6601-SY033 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY034-Z = 'Y'
               MOVE WK01-CODE-6601-SY034 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0004)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY035-Z = 'Y'
               MOVE WK01-CODE-6601-SY035 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0005)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY036-Z = 'Y'
               MOVE WK01-CODE-6601-SY036 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0006)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY037-Z = 'Y'
               MOVE WK01-CODE-6601-SY037 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0007)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY038-Z = 'Y'
               MOVE WK01-CODE-6601-SY038 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0008)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY039-Z = 'Y'
               MOVE WK01-CODE-6601-SY039 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0009)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY0310-Z = 'Y'
               MOVE WK01-CODE-6601-SY0310 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0010)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY0311-Z = 'Y'
               MOVE WK01-CODE-6601-SY0311 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0011)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY0312-Z = 'Y'
               MOVE WK01-CODE-6601-SY0312 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0012)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6601-SY0313-Z = 'Y'
               MOVE WK01-CODE-6601-SY0313 TO DTL-CODE-6601-SY03 OF
                FZYWM03(0013)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY032-Z = 'Y'
               MOVE WK01-DESC-6601-SY032 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY033-Z = 'Y'
               MOVE WK01-DESC-6601-SY033 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY034-Z = 'Y'
               MOVE WK01-DESC-6601-SY034 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0004)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY035-Z = 'Y'
               MOVE WK01-DESC-6601-SY035 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0005)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY036-Z = 'Y'
               MOVE WK01-DESC-6601-SY036 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0006)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY037-Z = 'Y'
               MOVE WK01-DESC-6601-SY037 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0007)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY038-Z = 'Y'
               MOVE WK01-DESC-6601-SY038 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0008)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY039-Z = 'Y'
               MOVE WK01-DESC-6601-SY039 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0009)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0310-Z = 'Y'
               MOVE WK01-DESC-6601-SY0310 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0010)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0311-Z = 'Y'
               MOVE WK01-DESC-6601-SY0311 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0011)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0312-Z = 'Y'
               MOVE WK01-DESC-6601-SY0312 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0012)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0313-Z = 'Y'
               MOVE WK01-DESC-6601-SY0313 TO SHORT-DESC-6601-SY03 OF
                FZYWM03(0013)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A2-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A2 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A3-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A3 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A4-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A4 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0004)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A5-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A5 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0005)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A6-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A6 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0006)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A7-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A7 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0007)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A8-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A8 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0008)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A9-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A9 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0009)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A10-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A10 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0010)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A11-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A11 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0011)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A12-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A12 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0012)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6601-SY0A13-Z = 'Y'
               MOVE WK01-DESC-6601-SY0A13 TO LONG-DESC-6601-SY03 OF
                FZYWM03(0013)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY032-Z = 'Y'
               MOVE WK01-1-6601-SY032 TO FLAG-1-6601-SY03 OF
                FZYWM03(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY033-Z = 'Y'
               MOVE WK01-1-6601-SY033 TO FLAG-1-6601-SY03 OF
                FZYWM03(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY034-Z = 'Y'
               MOVE WK01-1-6601-SY034 TO FLAG-1-6601-SY03 OF
                FZYWM03(0004)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY035-Z = 'Y'
               MOVE WK01-1-6601-SY035 TO FLAG-1-6601-SY03 OF
                FZYWM03(0005)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY036-Z = 'Y'
               MOVE WK01-1-6601-SY036 TO FLAG-1-6601-SY03 OF
                FZYWM03(0006)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY037-Z = 'Y'
               MOVE WK01-1-6601-SY037 TO FLAG-1-6601-SY03 OF
                FZYWM03(0007)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY038-Z = 'Y'
               MOVE WK01-1-6601-SY038 TO FLAG-1-6601-SY03 OF
                FZYWM03(0008)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY039-Z = 'Y'
               MOVE WK01-1-6601-SY039 TO FLAG-1-6601-SY03 OF
                FZYWM03(0009)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY0310-Z = 'Y'
               MOVE WK01-1-6601-SY0310 TO FLAG-1-6601-SY03 OF
                FZYWM03(0010)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY0311-Z = 'Y'
               MOVE WK01-1-6601-SY0311 TO FLAG-1-6601-SY03 OF
                FZYWM03(0011)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY0312-Z = 'Y'
               MOVE WK01-1-6601-SY0312 TO FLAG-1-6601-SY03 OF
                FZYWM03(0012)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-6601-SY0313-Z = 'Y'
               MOVE WK01-1-6601-SY0313 TO FLAG-1-6601-SY03 OF
                FZYWM03(0013)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY032-Z = 'Y'
               MOVE WK01-2-6601-SY032 TO FLAG-2-6601-SY03 OF
                FZYWM03(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY033-Z = 'Y'
               MOVE WK01-2-6601-SY033 TO FLAG-2-6601-SY03 OF
                FZYWM03(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY034-Z = 'Y'
               MOVE WK01-2-6601-SY034 TO FLAG-2-6601-SY03 OF
                FZYWM03(0004)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY035-Z = 'Y'
               MOVE WK01-2-6601-SY035 TO FLAG-2-6601-SY03 OF
                FZYWM03(0005)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY036-Z = 'Y'
               MOVE WK01-2-6601-SY036 TO FLAG-2-6601-SY03 OF
                FZYWM03(0006)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY037-Z = 'Y'
               MOVE WK01-2-6601-SY037 TO FLAG-2-6601-SY03 OF
                FZYWM03(0007)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY038-Z = 'Y'
               MOVE WK01-2-6601-SY038 TO FLAG-2-6601-SY03 OF
                FZYWM03(0008)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY039-Z = 'Y'
               MOVE WK01-2-6601-SY039 TO FLAG-2-6601-SY03 OF
                FZYWM03(0009)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY0310-Z = 'Y'
               MOVE WK01-2-6601-SY0310 TO FLAG-2-6601-SY03 OF
                FZYWM03(0010)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY0311-Z = 'Y'
               MOVE WK01-2-6601-SY0311 TO FLAG-2-6601-SY03 OF
                FZYWM03(0011)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY0312-Z = 'Y'
               MOVE WK01-2-6601-SY0312 TO FLAG-2-6601-SY03 OF
                FZYWM03(0012)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-6601-SY0313-Z = 'Y'
               MOVE WK01-2-6601-SY0313 TO FLAG-2-6601-SY03 OF
                FZYWM03(0013)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DTL-CODE-SY03-Z = 'Y'
               MOVE WK01-DTL-CODE-SY03 TO RQST-DTL-CODE-SY03 OF FZYWK03
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY032-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY032 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0002)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY033-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY033 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0003)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY034-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY034 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0004)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY035-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY035 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0005)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY036-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY036 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0006)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY037-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY037 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0007)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY038-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY038 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0008)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY039-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY039 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0009)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY0310-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY0310 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0010)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY0311-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY0311 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0011)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY0312-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY0312 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0012)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INTV-CD-FLG-6601-SY0313-Z = 'Y'
               MOVE WK01-INTV-CD-FLG-6601-SY0313 TO
                ACTV-INATV-CODE-FLAG-6601-SY03 OF FZYWM03(0013)
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACTV-INTV-CD-FLG-SY03-Z = 'Y'
               MOVE WK01-ACTV-INTV-CD-FLG-SY03 TO
                RQST-ACTV-INATV-CODE-FLAG-SY03 OF FZYWK03
           END-IF.
