    ***Created by Convert/DC version V8R03 on 12/11/00 at 11:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RQST-CODE-PU01-Z = 'Y'
               MOVE WK01-RQST-CODE-PU01      TO RQST-CODE-PU01 OF
                FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF  WK01-ORGN-CODE-Z     = 'Y'
               MOVE WK01-ORGN-CODE      TO ORGZN-CODE-PU01 OF
                FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF  WK01-TITLE-4010-PU01-Z = 'Y'
               MOVE  WK01-TITLE-4010-PU01  TO ORGZN-TITLE-4010-PU01 OF
                FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-PU01-Z = 'Y'
               MOVE WK01-NAME-PU01 TO RQST-NAME-PU01 OF
                FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-PU01-Z = 'Y'
               MOVE WK01-AREA-CODE-PU01 TO
                                   TLPHN-AREA-CODE-PU01 OF FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-PU01-Z = 'Y'
               MOVE WK01-XCHNG-ID-PU01 TO TLPHN-XCHNG-ID-PU01
                OF FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF  WK01-SEQ-ID-PU01-Z  = 'Y'
               MOVE  WK01-SEQ-ID-PU01  TO TLPHN-SEQ-ID-PU01 OF
                   FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XTNSN-ID-PU01-Z = 'Y'
               MOVE WK01-XTNSN-ID-PU01 TO TLPHN-XTNSN-ID-PU01
                OF FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TOTAL-PU01-Z = 'Y'
               MOVE WK01-TOTAL-PU01 TO RQST-TOTAL-PU01 OF
                FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-PU01-Z = 'Y'
               MOVE WK01-NAME-KEY-PU01 TO VNDR-NAME-PU01 OF FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-PU01-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-PU01 TO VNDR-ID-LAST-NINE-PU01 OF
                FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RQ-EMAIL-PU01-Z     = 'Y'
               MOVE WK01-RQ-EMAIL-PU01     TO      RQ-EMAIL-PU01     OF
                FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ENTERED-BY-PU01-Z   = 'Y'
               MOVE WK01-ENTERED-BY-PU01   TO  ENTERED-BY-PU01 OF
                FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ENTER-EMAIL-PU01-Z  = 'Y'
               MOVE WK01-ENTER-EMAIL-PU01   TO ENTERED-BY-EMAIL-PU01
                OF FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE2-PU01-Z = 'Y'
               MOVE WK01-AREA-CODE2-PU01 TO
                                   TLPHN-AREA-CODE2-PU01 OF FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID2-PU01-Z = 'Y'
               MOVE WK01-XCHNG-ID2-PU01 TO TLPHN-XCHNG-ID2-PU01
                OF FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF  WK01-SEQ-ID2-PU01-Z  = 'Y'
               MOVE  WK01-SEQ-ID2-PU01  TO TLPHN-SEQ-ID2-PU01 OF
                   FPUWM01
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XTNSN-ID2-PU01-Z = 'Y'
               MOVE WK01-XTNSN-ID2-PU01 TO TLPHN-XTNSN-ID2-PU01
                OF FPUWM01
           END-IF.
