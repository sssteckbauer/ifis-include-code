000100******************************************************************
000200* THIS IS SQL INCLUDE MEMBER 'DB2ERRBP'.                         *
000300* THIS IS THE PROCEDURAL INCLUDE MEMBER USED BY DB2 BATCH        *
000400* PROGRAMS TO EXECUTE DB2-ERROR-ROUTINE.  THE ASSOCIATED         *
000500* WORKING STORAGE RECORD LAYOUT IS STORED IN MEMBER 'DB2ERRBW'.  *
000600* DB2ERRBP DISPLAYS DB2 ERROR MSG TO SYSOUT, ROLLS BACK UPDATES, *
000700* SETS RETURN-CODE TO '9', AND CALLS 'ABORT' TO PRODUCE A DUMP.  *
000800******************************************************************
000900
001000******************************************************************
001100** THE FOLLOWING MOVE STATEMENTS MUST BE EXECUTED ONE TIME      **
001200** PRIOR TO INVOKING DB2-ERROR-ROUTINE.                         **
001300** DML NUMBER CAN BE THE PARAGRAPH NUMBER BUT SHOULD BE UNIQUE. **
001400**                                                              **
001500**   MOVE (PROGRAM NUMBER)       TO DB2-PROGRAM-NAME            **
001600**   MOVE (UNIQUE DML NUMBER)    TO DB2-DML-NUMBER              **
001700**                                                              **
001800** THE FOLLOWING LINES CAN BE PLACED AFTER EACH SQL STATEMENT:  **
001900**                                                              **
002000**   EVALUATE SQLCODE                                           **
002100**     WHEN +0                                                  **
002200**       CONTINUE                                               **
002300**     WHEN +100                                                **
002400**       PERFORM NOT-FOUND-LOGIC                                **
002500**     WHEN OTHER                                               **
002600**       MOVE (UNIQUE DML NUMBER) TO DB2-DML-NUMBER             **
002700**       PERFORM DB2-ERROR-ROUTINE                              **
002800**   END-EVALUATE.                                              **
002900******************************************************************
003000
003100*************************
003200 DB2-ERROR-ROUTINE.
003300*************************
003400
003500     IF SQLCODE = +0
003600         NEXT SENTENCE
003700     ELSE
003800         PERFORM DB2-DSNTIAR-MESSAGE
003900            THRU DB2-DSNTIAR-MESSAGE-EXIT
004000         PERFORM DB2-DML-ERROR-MESSAGE
004100            THRU DB2-DML-ERROR-MESSAGE-EXIT
004200         MOVE 9                  TO RETURN-CODE
004300         CALL 'ABORT'
004400     END-IF.
004500
004600 DB2-ERROR-ROUTINE-EXIT.         EXIT.
004700
004800*************************
004900 DB2-DSNTIAR-MESSAGE.
005000*************************
005100
005200     CALL 'DSNTIAR' USING SQLCA
005300                          DB2-SQL-ERROR-MSG
005400                          DB2-SQL-ERR-TEXT-LEN.
005500
005600     IF RETURN-CODE NOT = ZERO
005700         MOVE '>>> DSNTIAR CALL ERROR, RC='
005800                                 TO DB2-SQL-MR-EXPL
005900         MOVE RETURN-CODE        TO DB2-SQL-MR-RETC
006000         DISPLAY DB2-SQL-MSG-REC
006100     END-IF.
006200
006300     PERFORM DB2-DSNTIAR-DISPLAY-RTN
006400        THRU DB2-DSNTIAR-DISPLAY-RTN-EXIT
006500           VARYING ERR-IDX FROM +1 BY +1
006600             UNTIL ERR-IDX    > +31.
006700
006800 DB2-DSNTIAR-MESSAGE-EXIT.       EXIT.
006900
007000*************************
007100 DB2-DML-ERROR-MESSAGE.
007200*************************
007300
007400     MOVE SQLCODE                TO DB2-SQLCODE.
007500
007600     EXEC SQL
007700         ROLLBACK
007800     END-EXEC.
007900
008000     IF SQLCODE = +0
008100         MOVE DB2-ROLLBACK       TO DB2-ROLLBACK-MSG
008200     ELSE
008300         MOVE DB2-NO-ROLLBACK    TO DB2-ROLLBACK-MSG
008400     END-IF.
008500
008600     DISPLAY '  '.
008700     DISPLAY DB2-ERROR-MSG.
008800
008900 DB2-DML-ERROR-MESSAGE-EXIT.     EXIT.
009000
009100***************************
009200 DB2-DSNTIAR-DISPLAY-RTN.
009300***************************
009400
009500     DISPLAY DB2-SQL-ERR-TEXT(ERR-IDX).
009600
009700 DB2-DSNTIAR-DISPLAY-RTN-EXIT.   EXIT.
009800
009900************* END OF INCLUDE MEMBER 'DB2ERRBP' *******************
