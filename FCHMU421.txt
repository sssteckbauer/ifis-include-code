    ***Created by Convert/DC version V8R03 on 11/20/00 at 09:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE MGR-LAST-NINE-4019-CH42 OF FCHWK42 TO
                WK01-LAST-NINE-4019-CH42.
      *%--------------------------------------------------------------%*
           MOVE ADR-TYPE-CODE-6137-CH42 OF FCHWK42 TO
                WK01-TYPE-CODE-6137-CH42.
      *%--------------------------------------------------------------%*
           MOVE MAP-RQST-DATE-CH42 OF FCHWK42 TO WK01-RQST-DATE-CH42.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH42 OF FCHWM42 TO WK01-CODE-CH42.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-CH42 OF FCHWM42(0001) TO
                WK01-ADR-6139-CH421.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-CH42 OF FCHWM42(0002) TO
                WK01-ADR-6139-CH422.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-CH42 OF FCHWM42(0003) TO
                WK01-ADR-6139-CH423.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-CH42 OF FCHWM42(0004) TO
                WK01-ADR-6139-CH424.
      *%--------------------------------------------------------------%*
           MOVE CITY-NAME-6139-CH42 OF FCHWM42 TO WK01-NAME-6139-CH42.
      *%--------------------------------------------------------------%*
           MOVE STATE-CODE-6151-CH42 OF FCHWM42 TO WK01-CODE-6151-CH42.
      *%--------------------------------------------------------------%*
           MOVE ZIP-CODE-6152-CH42 OF FCHWM42 TO WK01-CODE-6152-CH42.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-CH42 OF FCHWM42 TO WK01-CODE-6153-CH42.
      *%--------------------------------------------------------------%*
           MOVE MAP-START-DATE-CH42 OF FCHWM42 TO WK01-START-DATE-CH42.
      *%--------------------------------------------------------------%*
           MOVE MAP-END-DATE-CH42 OF FCHWM42 TO WK01-END-DATE-CH42.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-CH42 OF FCHWM42 TO WK01-AREA-CODE-CH42.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-CH42 OF FCHWM42 TO WK01-XCHNG-ID-CH42.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-CH42 OF FCHWM42 TO WK01-SEQ-ID-CH42.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-CH42 OF FCHWM42 TO WK01-XTNSN-ID-CH42.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-6137-CH42 OF FCHWM42 TO WK01-DESC-6137-CH42.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6117-CH42 OF FCHWM42 TO WK01-KEY-6117-CH42.
