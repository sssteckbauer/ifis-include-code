       975-TO-R4181-RULE-EFCTV SECTION.
           MOVE RLE-USER-CD TO
               USER-CODE-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               TIME-STAMP-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-STATUS TO
               STATUS-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-RULE-CLS-DESC TO
               RULE-CLASS-DESC-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-RULE-CLS-TYP TO
               RULE-CLASS-TYPE-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-RSRV-BDGT-IND TO
               RSRV-BDGT-IND-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-BAL-MTHD-IND TO
               BAL-MTHD-IND-4181 OF R4181-RULE-EFCTV
                                                                     .
           MOVE RLE-CMPLT-IND TO
               CMPLT-IND-4181 OF R4181-RULE-EFCTV
                                                                     .
       975-TO-R4181-RULE-EFCTV-EXIT.
           EXIT.
