       976-FROM-R6258 SECTION.

FCCCE**        MOVE USER-CODE-6258 OF R6258
FCCCE          MOVE DBLINK-USER-CD
               TO CMT-USER-CD.

               MOVE LAST-ACTVY-DATE-6258 OF R6258
               TO CMT-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-6258 OF R6258
               TO CMT-UNVRS-CD.

               MOVE COMM-TYPE-6258 OF R6258
               TO CMT-COMM-TYP.

               MOVE SHORT-DESC-6258 OF R6258
               TO CMT-SHORT-DESC.

               MOVE LONG-DESC-6258 OF R6258
               TO CMT-LONG-DESC.

               MOVE OFC-CODE-6258 OF R6258
               TO CMT-OFC-CD.
       976-FROM-R6258-EXIT.
           EXIT.
