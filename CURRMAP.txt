      *      CREATED BY CONVERT/DB V8R01 ON 05/03/00 AT 10:54         *
YYY991     MOVE '985-CURR-MODLST-055' TO DBLINK-CURR-PARAGRAPH.         276 BRTN
YYY991                                                                  276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'MODLST-055' TO RECORD-NAME.                            276 BRTN
YYY991                                                                  276 BRTN
YYY991     MOVE DBLINK-MODLST-055 TO DBLINK-RECORD-MADE-CURRENT.        276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-MODLST-055-EXIT                           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE MDL-FK-PRO-PROG-NM TO DBLINK-MODLST-055-01          276 BRTN
YYY991         MOVE MDL-FK-PRO-PKEY-TS TO DBLINK-MODLST-055-02          276 BRTN
YYY991         MOVE MDL-PRO-TS TO DBLINK-MODLST-055-03                  276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991     MOVE DBLINK-MODLST-055 TO DBLINK-VIEW-NAME-CURRENT.          276 BRTN
YYY991     MOVE DBLINK-MODLST-055-KEY TO DBLINK-VIEW-200-CURRENT.       276 BRTN
YYY991                                                                  276 BRTN
YYY991     MOVE MDL-FK-MOD-MOD-NM TO DBLINK-MODULE-MODLST-01.           276 BRTN
YYY991     MOVE MDL-FK-MOD-PKEY-TS TO DBLINK-MODULE-MODLST-02.          276 BRTN
YYY991     MOVE MDL-MOD-TS TO DBLINK-MODULE-MODLST-03.                  276 BRTN
YYY991                                                                  276 BRTN
YYY991     MOVE MDL-FK-PRO-PROG-NM TO DBLINK-PROG-MODLST-01.            276 BRTN
YYY991     MOVE MDL-FK-PRO-PKEY-TS TO DBLINK-PROG-MODLST-02.            276 BRTN
YYY991     MOVE MDL-PRO-TS TO DBLINK-PROG-MODLST-03.                    276 BRTN
