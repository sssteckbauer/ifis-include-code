      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYSC01 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYSC01.

      ******************************************************************
      ******
      ***  INITIALIZE SCRATCH WORK RECORD
      ******************************************************************
      ******
           IF AC99W-FLAG-FIRST-TIME = AC99W-YES
               MOVE CURR-RSPNS-ID-SY00 TO RSP-NAME-SY99
      *%       DELETE SCRATCH AREA ID RSP-NAME-SY99 ALL
               MOVE RSP-NAME-SY99 TO DCGSCRCH-ID-5-8
               PERFORM 870-DELETE-SCRATCH
                  THRU 870-DELETE-SCRATCH-EXIT
      *%       ON (ERROR-STATUS =  '4303'
      *%                OR
      *%           ERROR-STATUS = '4305')
               IF
               NOT (ERROR-STATUS = '4303' OR ERROR-STATUS = '4305')


                   PERFORM 999-SQL-STATUS
                      THRU 999-SQL-STATUS-EXIT
               ELSE
                   CONTINUE
               END-IF
               MOVE SPACES TO PAGE-FLAG-SY99
               MOVE 1 TO PAGE-NUM-SY99
               MOVE 1 TO SUB-4-SY00
           END-IF.

       200-FSYSC01-EXIT.
           EXIT.
