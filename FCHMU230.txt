    ***Created by Convert/DC version V8R03 on 09/27/00 at 15:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH23-Z = 'Y'
               MOVE WK01-CODE-CH23 TO ACTN-CODE-CH23 OF FCHWM23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4024-CH23-Z = 'Y'
               MOVE WK01-CODE-4024-CH23 TO COA-CODE-4024-CH23 OF FCHWK23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-YR-4024-CH23-Z = 'Y'
               MOVE WK01-YR-4024-CH23 TO FSCL-YR-4024-CH23 OF FCHWK23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-4024-CH23-Z = 'Y'
               MOVE WK01-OF-PRDS-4024-CH23 TO NMBR-OF-PRDS-4024-CH23 OF
                FCHWM23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4076-CH23-Z = 'Y'
               MOVE WK01-TITLE-4076-CH23 TO COA-TITLE-4076-CH23 OF
                FCHWK23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-YR-STRT-DT-4024-CH23-Z = 'Y'
               MOVE WK01-YR-STRT-DT-4024-CH23 TO
                FSCL-YR-START-DATE-4024-CH23 OF FCHWM23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRD-STATUS-4024-CH23-Z = 'Y'
               MOVE WK01-PRD-STATUS-4024-CH23 TO
                ACRL-PRD-STATUS-4024-CH23 OF FCHWM23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRD-STATUS-DESC-CH23-Z = 'Y'
               MOVE WK01-PRD-STATUS-DESC-CH23 TO
                ACRL-PRD-STATUS-DESC-CH23 OF FCHWM23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4024-CH23-Z = 'Y'
               MOVE WK01-DATE-4024-CH23 TO ACTVY-DATE-4024-CH23 OF
                FCHWM23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-YR-END-DAT-4024-CH23-Z = 'Y'
               MOVE WK01-YR-END-DAT-4024-CH23 TO
                FSCL-YR-END-DATE-4024-CH23 OF FCHWM23
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH231-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH231 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0001)
           END-IF.
           MOVE WK01-START-DATE-4024-CH231-F TO
                WK01-START-DATE-4024-CH23-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH231-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH231 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0001)
           END-IF.
           MOVE WK01-END-DATE-4024-CH231-F TO WK01-END-DATE-4024-CH23-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH231-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH231 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0001)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH231-F TO
                WK01-OF-QTR-IND-4024-CH23-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH231-Z = 'Y'
               MOVE WK01-OF-PRDS-CH231 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0001)
           END-IF.
           MOVE WK01-OF-PRDS-CH231-F TO WK01-OF-PRDS-CH23-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH232-Z = 'Y'
               MOVE WK01-OF-PRDS-CH232 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0002)
           END-IF.
           MOVE WK01-OF-PRDS-CH232-F TO WK01-OF-PRDS-CH23-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH233-Z = 'Y'
               MOVE WK01-OF-PRDS-CH233 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0003)
           END-IF.
           MOVE WK01-OF-PRDS-CH233-F TO WK01-OF-PRDS-CH23-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH234-Z = 'Y'
               MOVE WK01-OF-PRDS-CH234 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0004)
           END-IF.
           MOVE WK01-OF-PRDS-CH234-F TO WK01-OF-PRDS-CH23-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH235-Z = 'Y'
               MOVE WK01-OF-PRDS-CH235 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0005)
           END-IF.
           MOVE WK01-OF-PRDS-CH235-F TO WK01-OF-PRDS-CH23-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH236-Z = 'Y'
               MOVE WK01-OF-PRDS-CH236 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0006)
           END-IF.
           MOVE WK01-OF-PRDS-CH236-F TO WK01-OF-PRDS-CH23-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH237-Z = 'Y'
               MOVE WK01-OF-PRDS-CH237 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0007)
           END-IF.
           MOVE WK01-OF-PRDS-CH237-F TO WK01-OF-PRDS-CH23-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH238-Z = 'Y'
               MOVE WK01-OF-PRDS-CH238 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0008)
           END-IF.
           MOVE WK01-OF-PRDS-CH238-F TO WK01-OF-PRDS-CH23-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH239-Z = 'Y'
               MOVE WK01-OF-PRDS-CH239 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0009)
           END-IF.
           MOVE WK01-OF-PRDS-CH239-F TO WK01-OF-PRDS-CH23-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH2310-Z = 'Y'
               MOVE WK01-OF-PRDS-CH2310 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0010)
           END-IF.
           MOVE WK01-OF-PRDS-CH2310-F TO WK01-OF-PRDS-CH23-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH2311-Z = 'Y'
               MOVE WK01-OF-PRDS-CH2311 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0011)
           END-IF.
           MOVE WK01-OF-PRDS-CH2311-F TO WK01-OF-PRDS-CH23-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH2312-Z = 'Y'
               MOVE WK01-OF-PRDS-CH2312 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0012)
           END-IF.
           MOVE WK01-OF-PRDS-CH2312-F TO WK01-OF-PRDS-CH23-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-OF-PRDS-CH2313-Z = 'Y'
               MOVE WK01-OF-PRDS-CH2313 TO NMBR-OF-PRDS-CH23 OF
                FCHWM23(0013)
           END-IF.
           MOVE WK01-OF-PRDS-CH2313-F TO WK01-OF-PRDS-CH23-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH231-Z = 'Y'
               MOVE WK01-STATUS-4024-CH231 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0001)
           END-IF.
           MOVE WK01-STATUS-4024-CH231-F TO WK01-STATUS-4024-CH23-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH231-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH231 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0001)
           END-IF.
           MOVE WK01-STATUS-DESC-CH231-F TO WK01-STATUS-DESC-CH23-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH232-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH232 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0002)
           END-IF.
           MOVE WK01-START-DATE-4024-CH232-F TO
                WK01-START-DATE-4024-CH23-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH233-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH233 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0003)
           END-IF.
           MOVE WK01-START-DATE-4024-CH233-F TO
                WK01-START-DATE-4024-CH23-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH234-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH234 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0004)
           END-IF.
           MOVE WK01-START-DATE-4024-CH234-F TO
                WK01-START-DATE-4024-CH23-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH235-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH235 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0005)
           END-IF.
           MOVE WK01-START-DATE-4024-CH235-F TO
                WK01-START-DATE-4024-CH23-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH236-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH236 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0006)
           END-IF.
           MOVE WK01-START-DATE-4024-CH236-F TO
                WK01-START-DATE-4024-CH23-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH237-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH237 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0007)
           END-IF.
           MOVE WK01-START-DATE-4024-CH237-F TO
                WK01-START-DATE-4024-CH23-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH238-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH238 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0008)
           END-IF.
           MOVE WK01-START-DATE-4024-CH238-F TO
                WK01-START-DATE-4024-CH23-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH239-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH239 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0009)
           END-IF.
           MOVE WK01-START-DATE-4024-CH239-F TO
                WK01-START-DATE-4024-CH23-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH2310-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH2310 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0010)
           END-IF.
           MOVE WK01-START-DATE-4024-CH2310-F TO
                WK01-START-DATE-4024-CH23-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH2311-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH2311 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0011)
           END-IF.
           MOVE WK01-START-DATE-4024-CH2311-F TO
                WK01-START-DATE-4024-CH23-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH2312-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH2312 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0012)
           END-IF.
           MOVE WK01-START-DATE-4024-CH2312-F TO
                WK01-START-DATE-4024-CH23-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-CH2313-Z = 'Y'
               MOVE WK01-START-DATE-4024-CH2313 TO
                PRD-START-DATE-4024-CH23 OF FCHWM23(0013)
           END-IF.
           MOVE WK01-START-DATE-4024-CH2313-F TO
                WK01-START-DATE-4024-CH23-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH232-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH232 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0002)
           END-IF.
           MOVE WK01-END-DATE-4024-CH232-F TO WK01-END-DATE-4024-CH23-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH233-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH233 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0003)
           END-IF.
           MOVE WK01-END-DATE-4024-CH233-F TO WK01-END-DATE-4024-CH23-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH234-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH234 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0004)
           END-IF.
           MOVE WK01-END-DATE-4024-CH234-F TO WK01-END-DATE-4024-CH23-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH235-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH235 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0005)
           END-IF.
           MOVE WK01-END-DATE-4024-CH235-F TO WK01-END-DATE-4024-CH23-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH236-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH236 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0006)
           END-IF.
           MOVE WK01-END-DATE-4024-CH236-F TO WK01-END-DATE-4024-CH23-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH237-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH237 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0007)
           END-IF.
           MOVE WK01-END-DATE-4024-CH237-F TO WK01-END-DATE-4024-CH23-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH238-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH238 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0008)
           END-IF.
           MOVE WK01-END-DATE-4024-CH238-F TO WK01-END-DATE-4024-CH23-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH239-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH239 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0009)
           END-IF.
           MOVE WK01-END-DATE-4024-CH239-F TO WK01-END-DATE-4024-CH23-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH2310-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH2310 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0010)
           END-IF.
           MOVE WK01-END-DATE-4024-CH2310-F TO
                WK01-END-DATE-4024-CH23-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH2311-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH2311 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0011)
           END-IF.
           MOVE WK01-END-DATE-4024-CH2311-F TO
                WK01-END-DATE-4024-CH23-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH2312-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH2312 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0012)
           END-IF.
           MOVE WK01-END-DATE-4024-CH2312-F TO
                WK01-END-DATE-4024-CH23-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-CH2313-Z = 'Y'
               MOVE WK01-END-DATE-4024-CH2313 TO PRD-END-DATE-4024-CH23
                OF FCHWM23(0013)
           END-IF.
           MOVE WK01-END-DATE-4024-CH2313-F TO
                WK01-END-DATE-4024-CH23-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH232-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH232 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0002)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH232-F TO
                WK01-OF-QTR-IND-4024-CH23-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH233-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH233 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0003)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH233-F TO
                WK01-OF-QTR-IND-4024-CH23-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH234-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH234 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0004)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH234-F TO
                WK01-OF-QTR-IND-4024-CH23-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH235-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH235 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0005)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH235-F TO
                WK01-OF-QTR-IND-4024-CH23-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH236-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH236 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0006)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH236-F TO
                WK01-OF-QTR-IND-4024-CH23-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH237-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH237 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0007)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH237-F TO
                WK01-OF-QTR-IND-4024-CH23-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH238-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH238 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0008)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH238-F TO
                WK01-OF-QTR-IND-4024-CH23-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH239-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH239 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0009)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH239-F TO
                WK01-OF-QTR-IND-4024-CH23-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH2310-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH2310 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0010)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH2310-F TO
                WK01-OF-QTR-IND-4024-CH23-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH2311-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH2311 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0011)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH2311-F TO
                WK01-OF-QTR-IND-4024-CH23-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH2312-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH2312 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0012)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH2312-F TO
                WK01-OF-QTR-IND-4024-CH23-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-OF-QTR-IND-4024-CH2313-Z = 'Y'
               MOVE WK01-OF-QTR-IND-4024-CH2313 TO
                END-OF-QTR-IND-4024-CH23 OF FCHWM23(0013)
           END-IF.
           MOVE WK01-OF-QTR-IND-4024-CH2313-F TO
                WK01-OF-QTR-IND-4024-CH23-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH232-Z = 'Y'
               MOVE WK01-STATUS-4024-CH232 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0002)
           END-IF.
           MOVE WK01-STATUS-4024-CH232-F TO WK01-STATUS-4024-CH23-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH233-Z = 'Y'
               MOVE WK01-STATUS-4024-CH233 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0003)
           END-IF.
           MOVE WK01-STATUS-4024-CH233-F TO WK01-STATUS-4024-CH23-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH234-Z = 'Y'
               MOVE WK01-STATUS-4024-CH234 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0004)
           END-IF.
           MOVE WK01-STATUS-4024-CH234-F TO WK01-STATUS-4024-CH23-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH235-Z = 'Y'
               MOVE WK01-STATUS-4024-CH235 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0005)
           END-IF.
           MOVE WK01-STATUS-4024-CH235-F TO WK01-STATUS-4024-CH23-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH236-Z = 'Y'
               MOVE WK01-STATUS-4024-CH236 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0006)
           END-IF.
           MOVE WK01-STATUS-4024-CH236-F TO WK01-STATUS-4024-CH23-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH237-Z = 'Y'
               MOVE WK01-STATUS-4024-CH237 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0007)
           END-IF.
           MOVE WK01-STATUS-4024-CH237-F TO WK01-STATUS-4024-CH23-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH238-Z = 'Y'
               MOVE WK01-STATUS-4024-CH238 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0008)
           END-IF.
           MOVE WK01-STATUS-4024-CH238-F TO WK01-STATUS-4024-CH23-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH239-Z = 'Y'
               MOVE WK01-STATUS-4024-CH239 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0009)
           END-IF.
           MOVE WK01-STATUS-4024-CH239-F TO WK01-STATUS-4024-CH23-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH2310-Z = 'Y'
               MOVE WK01-STATUS-4024-CH2310 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0010)
           END-IF.
           MOVE WK01-STATUS-4024-CH2310-F TO WK01-STATUS-4024-CH23-F
                (10).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH2311-Z = 'Y'
               MOVE WK01-STATUS-4024-CH2311 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0011)
           END-IF.
           MOVE WK01-STATUS-4024-CH2311-F TO WK01-STATUS-4024-CH23-F
                (11).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH2312-Z = 'Y'
               MOVE WK01-STATUS-4024-CH2312 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0012)
           END-IF.
           MOVE WK01-STATUS-4024-CH2312-F TO WK01-STATUS-4024-CH23-F
                (12).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-4024-CH2313-Z = 'Y'
               MOVE WK01-STATUS-4024-CH2313 TO PRD-STATUS-4024-CH23 OF
                FCHWM23(0013)
           END-IF.
           MOVE WK01-STATUS-4024-CH2313-F TO WK01-STATUS-4024-CH23-F
                (13).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH232-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH232 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0002)
           END-IF.
           MOVE WK01-STATUS-DESC-CH232-F TO WK01-STATUS-DESC-CH23-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH233-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH233 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0003)
           END-IF.
           MOVE WK01-STATUS-DESC-CH233-F TO WK01-STATUS-DESC-CH23-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH234-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH234 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0004)
           END-IF.
           MOVE WK01-STATUS-DESC-CH234-F TO WK01-STATUS-DESC-CH23-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH235-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH235 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0005)
           END-IF.
           MOVE WK01-STATUS-DESC-CH235-F TO WK01-STATUS-DESC-CH23-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH236-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH236 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0006)
           END-IF.
           MOVE WK01-STATUS-DESC-CH236-F TO WK01-STATUS-DESC-CH23-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH237-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH237 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0007)
           END-IF.
           MOVE WK01-STATUS-DESC-CH237-F TO WK01-STATUS-DESC-CH23-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH238-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH238 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0008)
           END-IF.
           MOVE WK01-STATUS-DESC-CH238-F TO WK01-STATUS-DESC-CH23-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH239-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH239 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0009)
           END-IF.
           MOVE WK01-STATUS-DESC-CH239-F TO WK01-STATUS-DESC-CH23-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH2310-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH2310 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0010)
           END-IF.
           MOVE WK01-STATUS-DESC-CH2310-F TO WK01-STATUS-DESC-CH23-F
                (10).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH2311-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH2311 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0011)
           END-IF.
           MOVE WK01-STATUS-DESC-CH2311-F TO WK01-STATUS-DESC-CH23-F
                (11).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH2312-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH2312 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0012)
           END-IF.
           MOVE WK01-STATUS-DESC-CH2312-F TO WK01-STATUS-DESC-CH23-F
                (12).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-DESC-CH2313-Z = 'Y'
               MOVE WK01-STATUS-DESC-CH2313 TO PRD-STATUS-DESC-CH23 OF
                FCHWM23(0013)
           END-IF.
           MOVE WK01-STATUS-DESC-CH2313-F TO WK01-STATUS-DESC-CH23-F
                (13).
