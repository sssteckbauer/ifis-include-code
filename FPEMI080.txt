    ***Created by Convert/DC version V8R03 on 12/08/00 at 13:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-PE08-Z = 'Y'
               MOVE WK01-KEY-6311-PE08 TO NAME-KEY-6311-PE08 OF FPEWM08
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE081-Z = 'Y'
               MOVE WK01-CODE-PE081 TO ACTN-CODE-PE08 OF FPEWM08(0001)
           END-IF.
           MOVE WK01-CODE-PE081-F TO WK01-CODE-PE08-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE082-Z = 'Y'
               MOVE WK01-CODE-PE082 TO ACTN-CODE-PE08 OF FPEWM08(0002)
           END-IF.
           MOVE WK01-CODE-PE082-F TO WK01-CODE-PE08-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE083-Z = 'Y'
               MOVE WK01-CODE-PE083 TO ACTN-CODE-PE08 OF FPEWM08(0003)
           END-IF.
           MOVE WK01-CODE-PE083-F TO WK01-CODE-PE08-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE084-Z = 'Y'
               MOVE WK01-CODE-PE084 TO ACTN-CODE-PE08 OF FPEWM08(0004)
           END-IF.
           MOVE WK01-CODE-PE084-F TO WK01-CODE-PE08-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE085-Z = 'Y'
               MOVE WK01-CODE-PE085 TO ACTN-CODE-PE08 OF FPEWM08(0005)
           END-IF.
           MOVE WK01-CODE-PE085-F TO WK01-CODE-PE08-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE086-Z = 'Y'
               MOVE WK01-CODE-PE086 TO ACTN-CODE-PE08 OF FPEWM08(0006)
           END-IF.
           MOVE WK01-CODE-PE086-F TO WK01-CODE-PE08-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE087-Z = 'Y'
               MOVE WK01-CODE-PE087 TO ACTN-CODE-PE08 OF FPEWM08(0007)
           END-IF.
           MOVE WK01-CODE-PE087-F TO WK01-CODE-PE08-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE088-Z = 'Y'
               MOVE WK01-CODE-PE088 TO ACTN-CODE-PE08 OF FPEWM08(0008)
           END-IF.
           MOVE WK01-CODE-PE088-F TO WK01-CODE-PE08-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE089-Z = 'Y'
               MOVE WK01-CODE-PE089 TO ACTN-CODE-PE08 OF FPEWM08(0009)
           END-IF.
           MOVE WK01-CODE-PE089-F TO WK01-CODE-PE08-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE0810-Z = 'Y'
               MOVE WK01-CODE-PE0810 TO ACTN-CODE-PE08 OF FPEWM08(0010)
           END-IF.
           MOVE WK01-CODE-PE0810-F TO WK01-CODE-PE08-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE081-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE081 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0001)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE081-F TO
                WK01-TYPE-CODE-6185-PE08-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE082-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE082 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0002)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE082-F TO
                WK01-TYPE-CODE-6185-PE08-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE083-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE083 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0003)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE083-F TO
                WK01-TYPE-CODE-6185-PE08-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE084-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE084 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0004)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE084-F TO
                WK01-TYPE-CODE-6185-PE08-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE085-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE085 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0005)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE085-F TO
                WK01-TYPE-CODE-6185-PE08-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE086-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE086 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0006)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE086-F TO
                WK01-TYPE-CODE-6185-PE08-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE087-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE087 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0007)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE087-F TO
                WK01-TYPE-CODE-6185-PE08-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE088-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE088 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0008)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE088-F TO
                WK01-TYPE-CODE-6185-PE08-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE089-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE089 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0009)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE089-F TO
                WK01-TYPE-CODE-6185-PE08-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE0810-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE0810 TO
                ADR-TYPE-CODE-6185-PE08 OF FPEWM08(0010)
           END-IF.
           MOVE WK01-TYPE-CODE-6185-PE0810-F TO
                WK01-TYPE-CODE-6185-PE08-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE081-Z = 'Y'
               MOVE WK01-DESC-6137-PE081 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0001)
           END-IF.
           MOVE WK01-DESC-6137-PE081-F TO WK01-DESC-6137-PE08-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE082-Z = 'Y'
               MOVE WK01-DESC-6137-PE082 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0002)
           END-IF.
           MOVE WK01-DESC-6137-PE082-F TO WK01-DESC-6137-PE08-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE083-Z = 'Y'
               MOVE WK01-DESC-6137-PE083 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0003)
           END-IF.
           MOVE WK01-DESC-6137-PE083-F TO WK01-DESC-6137-PE08-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE084-Z = 'Y'
               MOVE WK01-DESC-6137-PE084 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0004)
           END-IF.
           MOVE WK01-DESC-6137-PE084-F TO WK01-DESC-6137-PE08-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE085-Z = 'Y'
               MOVE WK01-DESC-6137-PE085 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0005)
           END-IF.
           MOVE WK01-DESC-6137-PE085-F TO WK01-DESC-6137-PE08-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE086-Z = 'Y'
               MOVE WK01-DESC-6137-PE086 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0006)
           END-IF.
           MOVE WK01-DESC-6137-PE086-F TO WK01-DESC-6137-PE08-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE087-Z = 'Y'
               MOVE WK01-DESC-6137-PE087 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0007)
           END-IF.
           MOVE WK01-DESC-6137-PE087-F TO WK01-DESC-6137-PE08-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE088-Z = 'Y'
               MOVE WK01-DESC-6137-PE088 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0008)
           END-IF.
           MOVE WK01-DESC-6137-PE088-F TO WK01-DESC-6137-PE08-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE089-Z = 'Y'
               MOVE WK01-DESC-6137-PE089 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0009)
           END-IF.
           MOVE WK01-DESC-6137-PE089-F TO WK01-DESC-6137-PE08-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE0810-Z = 'Y'
               MOVE WK01-DESC-6137-PE0810 TO LONG-DESC-6137-PE08 OF
                FPEWM08(0010)
           END-IF.
           MOVE WK01-DESC-6137-PE0810-F TO WK01-DESC-6137-PE08-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE081-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE081 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0001)
           END-IF.
           MOVE WK01-START-DATE-6185-PE081-F TO
                WK01-START-DATE-6185-PE08-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE082-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE082 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0002)
           END-IF.
           MOVE WK01-START-DATE-6185-PE082-F TO
                WK01-START-DATE-6185-PE08-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE083-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE083 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0003)
           END-IF.
           MOVE WK01-START-DATE-6185-PE083-F TO
                WK01-START-DATE-6185-PE08-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE084-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE084 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0004)
           END-IF.
           MOVE WK01-START-DATE-6185-PE084-F TO
                WK01-START-DATE-6185-PE08-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE085-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE085 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0005)
           END-IF.
           MOVE WK01-START-DATE-6185-PE085-F TO
                WK01-START-DATE-6185-PE08-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE086-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE086 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0006)
           END-IF.
           MOVE WK01-START-DATE-6185-PE086-F TO
                WK01-START-DATE-6185-PE08-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE087-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE087 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0007)
           END-IF.
           MOVE WK01-START-DATE-6185-PE087-F TO
                WK01-START-DATE-6185-PE08-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE088-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE088 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0008)
           END-IF.
           MOVE WK01-START-DATE-6185-PE088-F TO
                WK01-START-DATE-6185-PE08-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE089-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE089 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0009)
           END-IF.
           MOVE WK01-START-DATE-6185-PE089-F TO
                WK01-START-DATE-6185-PE08-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-6185-PE0810-Z = 'Y'
               MOVE WK01-START-DATE-6185-PE0810 TO
                MAP-START-DATE-6185-PE08 OF FPEWM08(0010)
           END-IF.
           MOVE WK01-START-DATE-6185-PE0810-F TO
                WK01-START-DATE-6185-PE08-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE081-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE081 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0001)
           END-IF.
           MOVE WK01-END-DATE-6185-PE081-F TO WK01-END-DATE-6185-PE08-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE082-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE082 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0002)
           END-IF.
           MOVE WK01-END-DATE-6185-PE082-F TO WK01-END-DATE-6185-PE08-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE083-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE083 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0003)
           END-IF.
           MOVE WK01-END-DATE-6185-PE083-F TO WK01-END-DATE-6185-PE08-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE084-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE084 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0004)
           END-IF.
           MOVE WK01-END-DATE-6185-PE084-F TO WK01-END-DATE-6185-PE08-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE085-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE085 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0005)
           END-IF.
           MOVE WK01-END-DATE-6185-PE085-F TO WK01-END-DATE-6185-PE08-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE086-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE086 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0006)
           END-IF.
           MOVE WK01-END-DATE-6185-PE086-F TO WK01-END-DATE-6185-PE08-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE087-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE087 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0007)
           END-IF.
           MOVE WK01-END-DATE-6185-PE087-F TO WK01-END-DATE-6185-PE08-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE088-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE088 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0008)
           END-IF.
           MOVE WK01-END-DATE-6185-PE088-F TO WK01-END-DATE-6185-PE08-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE089-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE089 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0009)
           END-IF.
           MOVE WK01-END-DATE-6185-PE089-F TO WK01-END-DATE-6185-PE08-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-6185-PE0810-Z = 'Y'
               MOVE WK01-END-DATE-6185-PE0810 TO MAP-END-DATE-6185-PE08
                OF FPEWM08(0010)
           END-IF.
           MOVE WK01-END-DATE-6185-PE0810-F TO
                WK01-END-DATE-6185-PE08-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-CD-6185-P08-Z = 'Y'
               MOVE WK01-ADR-TYPE-CD-6185-P08 TO
                RQST-ADR-TYPE-CODE-6185-PE08 OF FPEWK08
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NIN-6186-P08-Z = 'Y'
               MOVE WK01-ID-LAST-NIN-6186-P08 TO
                ENTY-ID-LAST-NINE-6186-PE08 OF FPEWK08
           END-IF.
