      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R6353-ENTY-CHG' TO DBLINK-CURR-PARAGRAPH.     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R6353-ENTY-CHG' TO RECORD-NAME.                        276 BRTN
YYY991     MOVE 'F-ENTYX' TO AREA-NAME.                                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG TO DBLINK-RECORD-MADE-CURRENT.    276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R6353-ENTY-CHG-EXIT                       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE ECG-FK-ENT-ENTY-ONE TO DBLINK-R6353-ENTY-CHG-01     276 BRTN
YYY991         MOVE ECG-FK-ENT-ENTY-NINE TO DBLINK-R6353-ENTY-CHG-02    276 BRTN
YYY991         MOVE ECG-CHANGE-DT TO DBLINK-R6353-ENTY-CHG-03           276 BRTN
YYY991         MOVE ECG-PCH-SET-TS TO DBLINK-R6353-ENTY-CHG-04          276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG TO DBLINK-VIEW-NAME-CURRENT.      276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG-KEY TO DBLINK-VIEW-200-CURRENT.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6353-6186                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG-01 TO DBLINK-S-6353-6186-01.      276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG-02 TO DBLINK-S-6353-6186-02.      276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG-03 TO DBLINK-S-6353-6186-03.      276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG-04 TO DBLINK-S-6353-6186-04.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-6353-6186-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6353-6352                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG-01 TO DBLINK-S-6353-6352-01.      276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG-02 TO DBLINK-S-6353-6352-02.      276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG-03 TO DBLINK-S-6353-6352-03.      276 BRTN
YYY991     MOVE DBLINK-R6353-ENTY-CHG-04 TO DBLINK-S-6353-6352-04.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-6353-6352-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6311-6353                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ECG-FK-ENT-ENTY-ONE TO DBLINK-S-6311-6353-01.           276 BRTN
YYY991     MOVE ECG-FK-ENT-ENTY-NINE TO DBLINK-S-6311-6353-02.          276 BRTN
YYY991     MOVE ECG-CHANGE-DT TO DBLINK-S-6311-6353-03.                 276 BRTN
YYY991     MOVE ECG-PCH-SET-TS TO DBLINK-S-6311-6353-04.                276 BRTN
