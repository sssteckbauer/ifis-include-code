       976-FROM-R4084-PO-CLASS SECTION.

FCCCE**        MOVE USER-CODE-4084 OF R4084-PO-CLASS
FCCCE          MOVE DBLINK-USER-CD
               TO POC-USER-CD.

               MOVE LAST-ACTVY-DATE-4084 OF R4084-PO-CLASS
               TO POC-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4084 OF R4084-PO-CLASS
               TO POC-UNVRS-CD.

               MOVE PO-CLASS-CODE-4084 OF R4084-PO-CLASS
               TO POC-PO-CLS-CD.

               MOVE PO-CLASS-DESC-4084 OF R4084-PO-CLASS
               TO POC-PO-CLS-DESC.

           IF START-DATE-4084 OF R4084-PO-CLASS  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POC-START-DT
           ELSE
               MOVE START-DATE-4084 OF R4084-PO-CLASS
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POC-START-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POC-START-DT
               END-IF
           END-IF.

           IF END-DATE-4084 OF R4084-PO-CLASS  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POC-END-DT
           ELSE
               MOVE END-DATE-4084 OF R4084-PO-CLASS
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POC-END-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POC-END-DT
               END-IF
           END-IF.

           IF ACTVY-DATE-4084 OF R4084-PO-CLASS  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POC-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4084 OF R4084-PO-CLASS
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POC-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POC-ACTVY-DT
               END-IF
           END-IF.

           IF MAX-AMT-4084 OF R4084-PO-CLASS  NOT NUMERIC
               MOVE ZEROS TO POC-MAX-AMT
           ELSE
               MOVE MAX-AMT-4084 OF R4084-PO-CLASS
               TO POC-MAX-AMT
           END-IF.
       976-FROM-R4084-PO-CLASS-EXIT.
           EXIT.
