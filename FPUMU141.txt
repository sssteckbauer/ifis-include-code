    ***Created by Convert/DC version V8R03 on 12/04/00 at 11:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE PO-NMBR-4083-PU14 OF FPUWK14 TO WK01-NMBR-4083-PU14.
      *%--------------------------------------------------------------%*
           MOVE CHNG-SEQ-NMBR-4096-PU14 OF FPUWK14 TO
                WK01-SEQ-NMBR-4096-PU14.
      *%--------------------------------------------------------------%*
           MOVE ITEM-NMBR-4085-PU14 OF FPUWK14 TO WK01-NMBR-4085-PU14.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-DESC-4074-PU14 OF FPUWM14 TO WK01-DESC-4074-PU14.
      *%--------------------------------------------------------------%*
           MOVE VNDR-ID-LAST-NINE-4073-PU14 OF FPUWM14 TO
                WK01-ID-LAST-NINE-4073-PU14.
      *%--------------------------------------------------------------%*
           MOVE VNDR-NAME-4073-PU14 OF FPUWM14 TO WK01-NAME-4073-PU14.
      *%--------------------------------------------------------------%*
           MOVE RQST-SEQ-NMBR-PU14 OF FPUWK14 TO WK01-SEQ-NMBR-PU14.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU14 OF FPUWM14 TO WK01-CODE-PU14.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-4061-PU14 OF FPUWM14 TO WK01-NMBR-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4061-PU14 OF FPUWM14 TO WK01-CODE-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-CODE-4061-PU14 OF FPUWM14 TO
                WK01-INDX-CODE-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE FUND-CODE-4061-PU14 OF FPUWM14 TO WK01-CODE-4061-PU1A.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-4061-PU14 OF FPUWM14 TO WK01-CODE-4061-PU1B.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4061-PU14 OF FPUWM14 TO WK01-CODE-4061-PU1C.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-CODE-4061-PU14 OF FPUWM14 TO WK01-CODE-4061-PU1D.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-CODE-4061-PU14 OF FPUWM14 TO WK01-CODE-4061-PU1E.
      *%--------------------------------------------------------------%*
           MOVE LCTN-CODE-4061-PU14 OF FPUWM14 TO WK01-CODE-4061-PU1F.
      *%--------------------------------------------------------------%*
           MOVE ACCT-ERROR-IND-4061-PU14 OF FPUWM14 TO
                WK01-ERROR-IND-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE AMT-4061-PU14 OF FPUWM14 TO WK01-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE PCT-DSTBN-4061-PU14 OF FPUWM14 TO WK01-DSTBN-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE TAX-AMT-4061-PU14 OF FPUWM14 TO WK01-AMT-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-4061-PU14 OF FPUWM14 TO WK01-CHRG-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE RULE-CLASS-CODE-4061-PU14 OF FPUWM14 TO
                WK01-CLASS-CODE-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE TAX-PCT-DSTBN-4061-PU14 OF FPUWM14 TO
                WK01-PCT-DSTBN-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE TAX-RULE-CLASS-4061-PU14 OF FPUWM14 TO
                WK01-RULE-CLASS-4061-PU14.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-PCT-DSTBN-4061-PU14 OF FPUWM14 TO
                WK01-CHRG-PCT-DSTBN-4061-P1.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-RULE-CLASS-4061-PU14 OF FPUWM14 TO
                WK01-CHRG-RUL-CLSS-4061-P14.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-4096-PU14 OF FPUWM14 TO WK01-CHRG-4096-PU14.
      *%--------------------------------------------------------------%*
           MOVE DIST-TOTAL-PU14 OF FPUWM14 TO WK01-TOTAL-PU14.
      *%--------------------------------------------------------------%*
           MOVE TOT-PCT-DSTBN-4061-PU14 OF FPUWM14 TO
                WK01-PCT-DSTBN-4061-PU1A.
      *%--------------------------------------------------------------%*
           MOVE EXTENDED-PRICE-4085-PU14 OF FPUWM14 TO
                WK01-PRICE-4085-PU14.
      *%--------------------------------------------------------------%*
           MOVE TAX-AMT-4085-PU14 OF FPUWM14 TO WK01-AMT-4085-PU14.
      *%--------------------------------------------------------------%*
           MOVE ITEM-DSCNT-AMT-PU14 OF FPUWM14 TO WK01-DSCNT-AMT-PU14.
      *%--------------------------------------------------------------%*
           MOVE PO-DSCNT-AMT-PU14 OF FPUWM14 TO WK01-DSCNT-AMT-PU1A.
      *%--------------------------------------------------------------%*
           MOVE TOT-PCT-DSTBN-TAX-4061-PU14 OF FPUWM14 TO
                WK01-PCT-DSTBN-TAX-4061-P14.
      *%--------------------------------------------------------------%*
           MOVE DIST-TOTAL-TAX-PU14 OF FPUWM14 TO WK01-TOTAL-TAX-PU14.
      *%--------------------------------------------------------------%*
           MOVE TOT-PCT-DSTBN-ADDL-4061-PU14 OF FPUWM14 TO
                WK01-PCT-DSTBN-ADDL-4061-P1.
      *%--------------------------------------------------------------%*
           MOVE DIST-TOTAL-ADDL-CHRG-PU14 OF FPUWM14 TO
                WK01-TOTAL-ADDL-CHRG-PU14.
      *%--------------------------------------------------------------%*
           MOVE SEQ-COUNT-PU14 OF FPUWM14 TO WK01-COUNT-PU14.
      *%--------------------------------------------------------------%*
           MOVE TOTAL-C-O-PU14 OF FPUWM14 TO WK01-C-O-PU14.
      *%--------------------------------------------------------------%*
           MOVE OPEN-FLD-FLAG-PU14 OF FPUWK14 TO WK01-FLD-FLAG-PU14.
      *%--------------------------------------------------------------%*
           MOVE FED-FUND-MSG-PU14 OF FPUWM14 TO WK01-FUND-MSG-PU14.
