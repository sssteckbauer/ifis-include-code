    ***Created by Convert/DC version V8R03 on 12/11/00 at 08:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FZYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FZYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FZYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FZYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FZYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FZYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FZYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0001) TO WK01-CODE-PE071.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0002) TO WK01-CODE-PE072.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0003) TO WK01-CODE-PE073.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0004) TO WK01-CODE-PE074.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0005) TO WK01-CODE-PE075.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0006) TO WK01-CODE-PE076.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0007) TO WK01-CODE-PE077.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0008) TO WK01-CODE-PE078.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0009) TO WK01-CODE-PE079.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0010) TO WK01-CODE-PE0710.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0011) TO WK01-CODE-PE0711.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PE07 OF FPEWM07(0012) TO WK01-CODE-PE0712.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0001) TO
                WK01-CODE-6153-PE071.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0002) TO
                WK01-CODE-6153-PE072.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0003) TO
                WK01-CODE-6153-PE073.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0004) TO
                WK01-CODE-6153-PE074.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0005) TO
                WK01-CODE-6153-PE075.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0006) TO
                WK01-CODE-6153-PE076.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0007) TO
                WK01-CODE-6153-PE077.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0008) TO
                WK01-CODE-6153-PE078.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0009) TO
                WK01-CODE-6153-PE079.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0010) TO
                WK01-CODE-6153-PE0710.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0011) TO
                WK01-CODE-6153-PE0711.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-PE07 OF FPEWM07(0012) TO
                WK01-CODE-6153-PE0712.
      *%--------------------------------------------------------------%*
           MOVE RQST-CNTRY-CODE-PE07 OF FPEWK07 TO WK01-CNTRY-CODE-PE07.
      *%--------------------------------------------------------------%*
           MOVE RQST-FULL-NAME-PE07                  TO
                                          WK01-RQST-FULL-NAME-PE07
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0001) TO
                WK01-NAME-6153-PE071.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0002) TO
                WK01-NAME-6153-PE072.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0003) TO
                WK01-NAME-6153-PE073.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0004) TO
                WK01-NAME-6153-PE074.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0005) TO
                WK01-NAME-6153-PE075.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0006) TO
                WK01-NAME-6153-PE076.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0007) TO
                WK01-NAME-6153-PE077.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0008) TO
                WK01-NAME-6153-PE078.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0009) TO
                WK01-NAME-6153-PE079.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0010) TO
                WK01-NAME-6153-PE0710.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0011) TO
                WK01-NAME-6153-PE0711.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6153-PE07 OF FPEWM07(0012) TO
                WK01-NAME-6153-PE0712.
