    ***Created by Convert/DC version V8R03 on 12/04/00 at 13:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4162-AP29-Z = 'Y'
               MOVE WK01-NMBR-4162-AP29 TO SEQ-NMBR-4162-AP29 OF FAPWK29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP29-Z = 'Y'
               MOVE WK01-CODE-AP29 TO ACTN-CODE-AP29 OF FAPWM29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4162-AP29-Z = 'Y'
               MOVE WK01-CODE-4162-AP29 TO ORGN-CODE-4162-AP29 OF
                FAPWK29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-L-4012-AP29-Z = 'Y'
               MOVE WK01-CODE-DESC-L-4012-AP29 TO
                ORGN-CODE-DESC-L-4012-AP29 OF FAPWM29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4162-AP29-Z = 'Y'
               MOVE WK01-ACCT-CODE-4162-AP29 TO
                BANK-ACCT-CODE-4162-AP29 OF FAPWK29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-4062-AP29-Z = 'Y'
               MOVE WK01-CODE-DESC-4062-AP29 TO
                BANK-CODE-DESC-4062-AP29 OF FAPWM29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RUN-DATE-4162-AP29-Z = 'Y'
               MOVE WK01-RUN-DATE-4162-AP29 TO CHECK-RUN-DATE-4162-AP29
                OF FAPWM29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-NMBR-4162-AP29-Z = 'Y'
               MOVE WK01-CHECK-NMBR-4162-AP29 TO
                START-CHECK-NMBR-4162-AP29 OF FAPWM29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-NMBR-AP29-Z = 'Y'
               MOVE WK01-CHECK-NMBR-AP29 TO END-CHECK-NMBR-AP29 OF
                FAPWM29
           END-IF.
