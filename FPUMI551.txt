    ***Created by Convert/DC version V8R03 on 12/11/00 at 10:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE PO-NMBR-PU50 OF FPUWK50 TO WK01-NMBR-PU50.
      *%--------------------------------------------------------------%*
           MOVE CHNG-SEQ-NMBR-PU50 OF FPUWK50 TO WK01-SEQ-NMBR-PU50.
      *%--------------------------------------------------------------%*
           MOVE VNDR-ID-LAST-NINE-PU50 OF FPUWK50 TO
                WK01-ID-LAST-NINE-PU50.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-PU50 OF FPUWK50 TO WK01-KEY-PU50.
      *%--------------------------------------------------------------%*
           MOVE ORDER-DATE-PU50 OF FPUWK50 TO WK01-DATE-PU50.
      *%--------------------------------------------------------------%*
           MOVE PO-AMT-PU50 OF FPUWK50 TO WK01-AMT-PU50.
      *%--------------------------------------------------------------%*
           MOVE INV-AMT-PU50 OF FPUWK50 TO WK01-AMT-PU5A.
      *%--------------------------------------------------------------%*
           MOVE PO-TAX-AMT-PU50 OF FPUWK50 TO WK01-TAX-AMT-PU50.
      *%--------------------------------------------------------------%*
           MOVE INV-TAX-AMT-PU50 OF FPUWK50 TO WK01-TAX-AMT-PU5A.
      *%--------------------------------------------------------------%*
           MOVE PO-ADDL-AMT-PU50 OF FPUWK50 TO WK01-ADDL-AMT-PU50.
      *%--------------------------------------------------------------%*
           MOVE INV-ADDL-AMT-PU50 OF FPUWK50 TO WK01-ADDL-AMT-PU5A.
      *%--------------------------------------------------------------%*
           MOVE TOTAL-PO-AMT-PU50 OF FPUWK50 TO WK01-PO-AMT-PU50.
      *%--------------------------------------------------------------%*
           MOVE TOTAL-INV-AMT-PU50 OF FPUWK50 TO WK01-INV-AMT-PU50.
      *%--------------------------------------------------------------%*
           MOVE DATA-DESC-PU50 OF FPUWK50 TO WK01-DESC-PU50.
      *%--------------------------------------------------------------%*
           MOVE CNCL-DATE-PU50 OF FPUWK50 TO WK01-DATE-PU5A.
      *%--------------------------------------------------------------%*
           MOVE TOTAL-C-O-PU50 OF FPUWK50 TO WK01-C-O-PU50.
      *%--------------------------------------------------------------%*
           MOVE ITEM-NMBR-PU55 OF FPUWK55 TO WK01-NMBR-PU55.
      *%--------------------------------------------------------------%*
           MOVE SEQ-COUNT-PU55 OF FPUWK55 TO WK01-COUNT-PU55.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-CODE-PU55 OF FPUWK55 TO WK01-CODE-PU55.
      *%--------------------------------------------------------------%*
           MOVE QTY-PU55 OF FPUWK55 TO WK01-PU55.
      *%--------------------------------------------------------------%*
           MOVE UNIT-PRICE-PU55 OF FPUWK55 TO WK01-PRICE-PU55.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-PU55 OF FPUWM55 TO WK01-CODE-PU5A.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-CODE-PU55 OF FPUWM55 TO WK01-INDX-CODE-PU55.
      *%--------------------------------------------------------------%*
           MOVE FUND-CODE-PU55 OF FPUWM55 TO WK01-CODE-PU5B.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-PU55 OF FPUWM55 TO WK01-CODE-PU5C.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-PU55 OF FPUWM55 TO WK01-CODE-PU5D.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-CODE-PU55 OF FPUWM55 TO WK01-CODE-PU5E.
      *%--------------------------------------------------------------%*
           MOVE LCTN-CODE-PU55 OF FPUWM55 TO WK01-CODE-PU5F.
      *%--------------------------------------------------------------%*
           MOVE TAX-AMT-PU55 OF FPUWM55 TO WK01-AMT-PU55.
      *%--------------------------------------------------------------%*
           MOVE ACCT-AMT-PU55 OF FPUWM55 TO WK01-AMT-PU5B.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-PU55 OF FPUWM55 TO WK01-CHRG-PU55.
      *%--------------------------------------------------------------%*
           MOVE INV-ACCT-AMT-PU55 OF FPUWM55 TO WK01-ACCT-AMT-PU55.
      *%--------------------------------------------------------------%*
           MOVE INV-TAX-AMT-PU55 OF FPUWM55 TO WK01-TAX-AMT-PU55.
      *%--------------------------------------------------------------%*
           MOVE INV-ADDL-CHRG-PU55 OF FPUWM55 TO WK01-ADDL-CHRG-PU55.
      *%--------------------------------------------------------------%*
           MOVE LEFT-ACCT-AMT-PU55 OF FPUWM55 TO WK01-ACCT-AMT-PU5A.
      *%--------------------------------------------------------------%*
           MOVE LEFT-TAX-AMT-PU55 OF FPUWM55 TO WK01-TAX-AMT-PU5B.
      *%--------------------------------------------------------------%*
           MOVE LEFT-ADDL-CHRG-PU55 OF FPUWM55 TO WK01-ADDL-CHRG-PU5A.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-CODE-PU55 OF FPUWM55 TO WK01-CODE-PU5G.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-PU55 OF FPUWK55 TO WK01-NMBR-PU5A.
